<?php

use Illuminate\Database\Seeder;

class AttributesSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!\App\Models\VolunteerAttribute::where('slug', 'show_if_actor')->first()) {
            \App\Models\VolunteerAttribute::create([
                'slug' => 'show_if_actor',
                'type' => 'checkbox',
                'entities' => ['App\Models\Volunteer'],
                'name' => 'Chcę grać w widowisku',
                'group' => 'show'
            ]);
        }

        if(!\App\Models\VolunteerAttribute::where('slug', 'show_if_helper')->first()) {
            \App\Models\VolunteerAttribute::create([
                'slug' => 'show_if_helper',
                'type' => 'checkbox',
                'entities' => ['App\Models\Volunteer'],
                'name' => 'Chcę pomagać w widowisku',
                'group' => 'show'
            ]);
        }
    }
}
