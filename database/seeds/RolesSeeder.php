<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    private function insertRole($id, $name, $permissions) {
        $role = \App\Models\UserRole::find($id);
        if (!$role) {
            $role = new \App\Models\UserRole();
            $role->id = $id;
        }
        $role->name = $name;
        $role->save();

        $role->permissions()->sync($permissions);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertRole(1, 'Administrator', [
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22
        ]);
    }
}
