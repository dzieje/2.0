<?php

use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{

    private function insertPermission($id, $name, $ability) {
        $role = \App\Models\Permission::find($id);
        if (!$role) {
            $role = new \App\Models\Permission();
            $role->id = $id;
        }
        $role->name = $name;
        $role->ability = $ability;
        $role->save();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertPermission(1, 'Zarządzanie aktorami', 'actors');
        $this->insertPermission(2, 'Zarządzanie scenami', 'scenes');
        $this->insertPermission(3, 'Zarządzanie rolami', 'roles');
        $this->insertPermission(4, 'Zarządzanie grupami', 'groups');
        $this->insertPermission(5, 'Zarządzanie kulisami', 'wings');
        $this->insertPermission(6, 'Zarządzanie harmonogramem', 'timetables');
        $this->insertPermission(7, 'Animator', 'animators');
        $this->insertPermission(8, 'Zarządzanie wolontariuszami', 'volunteers');
        $this->insertPermission(9, 'Zarządzanie użytkownikami', 'users');
        $this->insertPermission(10, 'Wysyłka mailingu', 'mailings');
        $this->insertPermission(11, 'Wysyłka sms', 'sms');
        $this->insertPermission(12, 'Organizator scen', 'scenes_panel');
        $this->insertPermission(13, 'Organizator prób', 'rehearsals_panel');
        $this->insertPermission(14, 'Nieobecności', 'absences');
        $this->insertPermission(15, 'Zmiany przydziału rół', 'changes');
        $this->insertPermission(16, 'Organizator widowisk', 'shows_panel');
        $this->insertPermission(17, 'Porządki', 'cleanups');
        $this->insertPermission(18, 'Organizator aktorów', 'actors_panel');
        $this->insertPermission(19, 'Panel kostiumów', 'costumes_panel');
        $this->insertPermission(20, 'Panel rekwizytów', 'props_panel');
        $this->insertPermission(21, 'Formularze', 'surveys');
        $this->insertPermission(22, 'Inspicjent', 'stage_manager');
    }
}
