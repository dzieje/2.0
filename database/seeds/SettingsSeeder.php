<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    private function insertSetting($id, $key, $type, $name, $value = null) {
        $setting = \App\Models\Setting::find($id);
        if (!$setting) {
            $setting = new \App\Models\Setting();
            $setting->id = $id;
        }
        $setting->key = $key;
        $setting->type = $type;
        $setting->name = $name;
        if($value) {
            $setting->value = $value;
        }
        $setting->save();

    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertSetting(1, 'registration_email', 'mail','Email rejestracyjny');
        $this->insertSetting(2, 'volunteer_attributes_list', 'list','Lista kolumn w zarządzaniu wolontariuszami');
    }
}
