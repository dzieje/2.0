<?php

use Illuminate\Database\Seeder;

class MessageTypesSeeder extends Seeder
{
    private function insertType($id, $name) {
        $type = \App\Models\MessageType::find($id);
        if (!$type) {
            $type = new \App\Models\MessageType();
            $type->id = $id;
        }
        $type->name = $name;
        $type->save();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertType(1, 'mail');
        $this->insertType(2, 'sms');
    }
}
