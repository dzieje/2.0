<?php

use Illuminate\Database\Seeder;

class ProjectTypesSeeder extends Seeder
{

    private function insertType($id, $name, $slug) {
        $type = \App\Models\ProjectType::find($id);
        if (!$type) {
            $type = new \App\Models\ProjectType();
            $type->id = $id;
        }
        $type->name = $name;
        $type->slug = $slug;
        $type->save();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertType(1, 'Widowisko', 'spectacle');
        $this->insertType(2, 'Normalny', 'project');
    }
}
