<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('project_id')->nullable();
            $table->string('name')->nullable();
            $table->softDeletes();
            $table->nullableTimestamps();

            $table->foreign('project_id')->references('id')->on('projects');
        });

        Schema::create('volunteer_group', function(Blueprint $table)
        {
            $table->bigInteger('volunteer_id')->unsigned()->index();
            $table->bigInteger('group_id')->unsigned()->nullable()->index();

            $table->foreign('volunteer_id')->references('id')->on('volunteers');
            $table->foreign('group_id')->references('id')->on('groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
