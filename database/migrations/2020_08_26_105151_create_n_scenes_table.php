<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNScenesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('scenes', function(Blueprint $table)
		{
			$table->id('id');
			$table->string('name');
			$table->time('time_start')->nullable();
			$table->time('time_end')->nullable();
			$table->softDeletes();
			$table->nullableTimestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('scenes');
	}

}
