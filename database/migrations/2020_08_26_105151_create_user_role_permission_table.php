<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserRolePermissionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_role_permission', function(Blueprint $table)
		{
			$table->bigInteger('user_role_id')->unsigned()->index('user_role_permission_user_role_id_foreign');
			$table->bigInteger('permission_id')->unsigned()->nullable()->index('user_role_permission_permission_id_foreign');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_role_permission');
	}

}
