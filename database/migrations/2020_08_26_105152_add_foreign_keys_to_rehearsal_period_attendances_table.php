<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRehearsalPeriodAttendancesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rehearsal_period_attendances', function(Blueprint $table)
		{
			$table->foreign('rehearsal_period_id')->references('id')->on('rehearsal_periods')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('volunteer_id')->references('id')->on('volunteers')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rehearsal_period_attendances', function(Blueprint $table)
		{
			$table->dropForeign('rehearsal_period_attendances_rehearsal_period_id_foreign');
			$table->dropForeign('rehearsal_period_attendances_user_id_foreign');
			$table->dropForeign('rehearsal_period_attendances_volunteer_id_foreign');
		});
	}

}
