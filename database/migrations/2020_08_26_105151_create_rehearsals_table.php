<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRehearsalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rehearsals', function(Blueprint $table)
		{
			$table->id('id');
			$table->boolean('if_general')->default(0);
			$table->string('name');
			$table->dateTime('rehearsal')->nullable();
			$table->softDeletes();
			$table->nullableTimestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rehearsals');
	}

}
