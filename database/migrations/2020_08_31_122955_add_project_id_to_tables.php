<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProjectIdToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rehearsals', function (Blueprint $table) {
            $table->unsignedBigInteger('project_id')->nullable()->after('id');

            $table->foreign('project_id')->references('id')->on('projects');
        });

        Schema::table('roles', function (Blueprint $table) {
            $table->unsignedBigInteger('project_id')->nullable()->after('id');

            $table->foreign('project_id')->references('id')->on('projects');
        });

        Schema::table('scenes', function (Blueprint $table) {
            $table->unsignedBigInteger('project_id')->nullable()->after('id');

            $table->foreign('project_id')->references('id')->on('projects');
        });

        Schema::table('shows', function (Blueprint $table) {
            $table->unsignedBigInteger('project_id')->nullable()->after('id');

            $table->foreign('project_id')->references('id')->on('projects');
        });

        Schema::table('stage_managers', function (Blueprint $table) {
            $table->unsignedBigInteger('project_id')->nullable()->after('id');

            $table->foreign('project_id')->references('id')->on('projects');
        });

        Schema::table('wings', function (Blueprint $table) {
            $table->unsignedBigInteger('project_id')->nullable()->after('id');

            $table->foreign('project_id')->references('id')->on('projects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
