<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVolunteerShowTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('volunteer_absences', function(Blueprint $table)
		{
			$table->bigInteger('volunteer_id')->unsigned()->index('volunteer_absences_volunteer_id_foreign');
			$table->bigInteger('show_id')->unsigned()->index('volunteer_absences_show_id_foreign');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('volunteer_absences');
	}

}
