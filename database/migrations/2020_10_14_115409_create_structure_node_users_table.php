<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStructureNodeUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('structure_node_user', function (Blueprint $table) {
            $table->unsignedBigInteger('structure_node_id');
            $table->unsignedBigInteger('user_id');

            $table->foreign('structure_node_id')->references('id')->on('structure_nodes')->onDelete('cascade');;
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');;
        });

        Schema::table('structures', function(Blueprint  $table){
            $table->dropForeign('structures_leader_user_id_foreign');
            $table->dropColumn('leader_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('structure_node_users');
    }
}
