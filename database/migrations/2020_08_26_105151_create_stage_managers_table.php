<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStageManagersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('stage_managers', function(Blueprint $table)
		{
			$table->id('id');
			$table->bigInteger('user_id')->unsigned()->nullable()->index('stage_managers_user_id_foreign');
			$table->string('filename')->nullable();
			$table->string('original_filename')->nullable();
			$table->nullableTimestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('stage_managers');
	}

}
