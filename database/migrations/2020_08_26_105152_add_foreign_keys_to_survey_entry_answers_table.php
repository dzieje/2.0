<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSurveyEntryAnswersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('survey_entry_answers', function(Blueprint $table)
		{
			$table->foreign('survey_entry_id')->references('id')->on('survey_entries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('survey_question_id')->references('id')->on('survey_questions')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('survey_entry_answers', function(Blueprint $table)
		{
			$table->dropForeign('survey_entry_answers_survey_entry_id_foreign');
			$table->dropForeign('survey_entry_answers_survey_question_id_foreign');
		});
	}

}
