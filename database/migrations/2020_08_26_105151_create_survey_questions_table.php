<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSurveyQuestionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('survey_questions', function(Blueprint $table)
		{
			$table->id('id');
			$table->bigInteger('survey_id')->unsigned()->nullable()->index('survey_questions_survey_id_foreign');
			$table->text('question', 65535);
			$table->softDeletes();
			$table->nullableTimestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('survey_questions');
	}

}
