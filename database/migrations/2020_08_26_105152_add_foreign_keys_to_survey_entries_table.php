<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSurveyEntriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('survey_entries', function(Blueprint $table)
		{
			$table->foreign('survey_id')->references('id')->on('surveys')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('volunteer_id')->references('id')->on('volunteers')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('survey_entries', function(Blueprint $table)
		{
			$table->dropForeign('survey_entries_survey_id_foreign');
			$table->dropForeign('survey_entries_volunteer_id_foreign');
		});
	}

}
