<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('messages', function(Blueprint $table)
		{
			$table->id('id');
			$table->bigInteger('user_id')->unsigned()->nullable()->index('messages_user_id_foreign');
			$table->bigInteger('message_type_id')->unsigned()->nullable()->index('messages_message_type_id_foreign');
			$table->string('subject')->nullable();
			$table->text('content')->nullable();
			$table->string('signed_by')->nullable();
			$table->string('reply_to')->nullable();
			$table->dateTime('send_time')->nullable();
			$table->softDeletes();
			$table->nullableTimestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('messages');
	}

}
