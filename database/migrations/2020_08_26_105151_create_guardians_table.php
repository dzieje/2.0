<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGuardiansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('guardians', function(Blueprint $table)
		{
			$table->id('id');
			$table->string('name')->nullable();
			$table->string('surname')->nullable();
			$table->string('gender')->nullable();
			$table->date('date_of_birth')->nullable();
			$table->string('street')->nullable();
			$table->string('house_number', 30)->nullable();
			$table->string('flat_number', 30)->nullable();
			$table->string('post_code', 10)->nullable();
			$table->string('city')->nullable();
			$table->string('phone', 20)->nullable();
			$table->string('email')->nullable();
			$table->boolean('guardian_permission')->nullable();
			$table->nullableTimestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('guardians');
	}

}
