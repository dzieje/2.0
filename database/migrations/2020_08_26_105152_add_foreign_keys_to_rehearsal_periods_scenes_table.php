<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRehearsalPeriodsScenesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rehearsal_periods_scenes', function(Blueprint $table)
		{
			$table->foreign('scene_id')->references('id')->on('scenes')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('rehearsal_period_id')->references('id')->on('rehearsal_periods')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rehearsal_periods_scenes', function(Blueprint $table)
		{
			$table->dropForeign('rehearsal_periods_scenes_scene_id_foreign');
			$table->dropForeign('rehearsal_periods_scenes_rehearsal_period_id_foreign');
		});
	}

}
