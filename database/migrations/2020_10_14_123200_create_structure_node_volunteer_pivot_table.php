<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStructureNodeVolunteerPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('structure_node_volunteer', function (Blueprint $table) {
            $table->unsignedBigInteger('structure_node_id');
            $table->unsignedBigInteger('volunteer_id');

            $table->foreign('structure_node_id')->references('id')->on('structure_nodes')->onDelete('cascade');
            $table->foreign('volunteer_id')->references('id')->on('volunteers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('structure_node_volunteer_pivot');
    }
}
