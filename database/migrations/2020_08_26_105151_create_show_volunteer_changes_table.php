<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShowVolunteerChangesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('show_volunteer_changes', function(Blueprint $table)
		{
			$table->id('id');
			$table->bigInteger('user_id')->unsigned()->nullable()->index('show_volunteer_changes_user_id_foreign');
			$table->bigInteger('show_id')->unsigned()->nullable()->index('show_volunteer_changes_show_id_foreign');
			$table->bigInteger('volunteer_id')->unsigned()->nullable()->index('show_volunteer_changes_volunteer_id_foreign');
			$table->bigInteger('role_id')->unsigned()->nullable()->index('show_volunteer_changes_role_id_foreign');
			$table->boolean('type')->nullable();
			$table->nullableTimestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('show_volunteer_changes');
	}

}
