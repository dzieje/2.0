<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSurveyEntriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('survey_entries', function(Blueprint $table)
		{
			$table->id('id');
			$table->bigInteger('survey_id')->unsigned()->nullable()->index('survey_entries_survey_id_foreign');
			$table->bigInteger('volunteer_id')->unsigned()->nullable()->index('survey_entries_volunteer_id_foreign');
			$table->softDeletes();
			$table->nullableTimestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('survey_entries');
	}

}
