<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRehearsalPeriodsRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rehearsal_periods_roles', function(Blueprint $table)
		{
			$table->bigInteger('rehearsal_period_id')->unsigned()->index('rehearsal_periods_roles_rehearsal_period_id_foreign');
			$table->bigInteger('role_id')->unsigned()->nullable()->index('rehearsal_periods_roles_role_id_foreign');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rehearsal_periods_roles');
	}

}
