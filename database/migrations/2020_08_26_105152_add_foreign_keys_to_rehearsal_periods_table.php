<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRehearsalPeriodsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rehearsal_periods', function(Blueprint $table)
		{
			$table->foreign('rehearsal_id')->references('id')->on('rehearsals')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rehearsal_periods', function(Blueprint $table)
		{
			$table->dropForeign('rehearsal_periods_rehearsal_id_foreign');
		});
	}

}
