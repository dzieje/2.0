<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSurveyEntryAnswersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('survey_entry_answers', function(Blueprint $table)
		{
			$table->id('id');
			$table->bigInteger('survey_entry_id')->unsigned()->nullable()->index('survey_entry_answers_survey_entry_id_foreign');
			$table->boolean('answer')->nullable();
			$table->bigInteger('survey_question_id')->unsigned()->nullable()->index('survey_entry_answers_survey_question_id_foreign');
			$table->nullableTimestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('survey_entry_answers');
	}

}
