<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVolunteerShowTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('volunteer_absences', function(Blueprint $table)
		{
			$table->foreign('show_id')->references('id')->on('shows')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('volunteer_id')->references('id')->on('volunteers')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('volunteer_absences', function(Blueprint $table)
		{
			$table->dropForeign('volunteer_absences_show_id_foreign');
			$table->dropForeign('volunteer_absences_volunteer_id_foreign');
		});
	}

}
