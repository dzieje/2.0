<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNPropsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('props', function(Blueprint $table)
		{
			$table->id('id');
			$table->bigInteger('role_id')->unsigned()->nullable()->index();
			$table->string('name');
			$table->boolean('type')->default(0);
			$table->softDeletes();
			$table->nullableTimestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('props');
	}

}
