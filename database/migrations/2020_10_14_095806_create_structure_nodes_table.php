<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStructureNodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('structures', function(Blueprint $table){
           $table->dropNestedSet();
        });
        Schema::create('structure_nodes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('structure_id')->nullable();
            $table->nestedSet();
            $table->string('name')->nullable();
            $table->timestamps();

            $table->foreign('structure_id')->references('id')->on('structures');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('structure_nodes');
    }
}
