<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRehearsalPeriodAttendancesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rehearsal_period_attendances', function(Blueprint $table)
		{
			$table->id('id');
			$table->bigInteger('rehearsal_period_id')->unsigned()->nullable()->index('rehearsal_period_attendances_rehearsal_period_id_foreign');
			$table->bigInteger('user_id')->unsigned()->nullable()->index('rehearsal_period_attendances_user_id_foreign');
			$table->bigInteger('volunteer_id')->unsigned()->nullable()->index('rehearsal_period_attendances_volunteer_id_foreign');
			$table->boolean('status')->nullable()->default(1);
			$table->nullableTimestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rehearsal_period_attendances');
	}

}
