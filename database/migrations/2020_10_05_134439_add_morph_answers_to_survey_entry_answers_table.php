<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMorphAnswersToSurveyEntryAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('survey_entry_answers', function (Blueprint $table) {
            $table->morphs('question');

            $table->dropForeign('survey_entry_answers_survey_question_id_foreign');
            $table->dropColumn('survey_question_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('survey_entry_answers', function (Blueprint $table) {
            $table->dropMorphs('question');
        });
    }
}
