<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRehearsalPeriodsRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rehearsal_periods_roles', function(Blueprint $table)
		{
			$table->foreign('role_id')->references('id')->on('roles')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('rehearsal_period_id')->references('id')->on('rehearsal_periods')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rehearsal_periods_roles', function(Blueprint $table)
		{
			$table->dropForeign('rehearsal_periods_roles_role_id_foreign');
			$table->dropForeign('rehearsal_periods_roles_rehearsal_period_id_foreign');
		});
	}

}
