<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMessageEntitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('message_entities', function(Blueprint $table)
		{
			$table->id('id');
			$table->bigInteger('message_id')->unsigned()->nullable()->index('message_entities_message_id_foreign');
			$table->bigInteger('volunteer_id')->unsigned()->nullable()->index('message_entities_volunteer_id_foreign');
			$table->string('uuid')->nullable();
			$table->dateTime('send_time')->nullable();
			$table->json('response')->nullable();
			$table->softDeletes();
			$table->nullableTimestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('message_entities');
	}

}
