<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRehearsalPeriodsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rehearsal_periods', function(Blueprint $table)
		{
			$table->id('id');
			$table->bigInteger('rehearsal_id')->unsigned()->nullable()->index('rehearsal_periods_rehearsal_id_foreign');
			$table->dateTime('date_from')->nullable();
			$table->dateTime('date_to')->nullable();
			$table->softDeletes();
			$table->nullableTimestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rehearsal_periods');
	}

}
