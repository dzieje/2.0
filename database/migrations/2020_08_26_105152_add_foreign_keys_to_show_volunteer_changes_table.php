<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToShowVolunteerChangesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('show_volunteer_changes', function(Blueprint $table)
		{
			$table->foreign('role_id')->references('id')->on('roles')->onUpdate('RESTRICT')->onDelete('SET NULL');
			$table->foreign('show_id')->references('id')->on('shows')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('SET NULL');
			$table->foreign('volunteer_id')->references('id')->on('volunteers')->onUpdate('RESTRICT')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('show_volunteer_changes', function(Blueprint $table)
		{
			$table->dropForeign('show_volunteer_changes_role_id_foreign');
			$table->dropForeign('show_volunteer_changes_show_id_foreign');
			$table->dropForeign('show_volunteer_changes_user_id_foreign');
			$table->dropForeign('show_volunteer_changes_volunteer_id_foreign');
		});
	}

}
