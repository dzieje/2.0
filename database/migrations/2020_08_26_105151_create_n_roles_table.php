<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('roles', function(Blueprint $table)
		{
			$table->id('id');
			$table->bigInteger('scene_id')->unsigned()->nullable()->index();
			$table->bigInteger('wing_id')->unsigned()->nullable()->index();
			$table->string('name');
			$table->time('time_start')->nullable();
			$table->time('time_end')->nullable();
			$table->boolean('min_actors')->nullable();
			$table->boolean('max_actors')->nullable();
			$table->boolean('if_single_actor')->default(0);
			$table->boolean('if_understudy')->default(0);
			$table->softDeletes();
			$table->nullableTimestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('roles');
	}

}
