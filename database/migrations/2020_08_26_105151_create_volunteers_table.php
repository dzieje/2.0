<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVolunteersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('volunteers', function(Blueprint $table)
		{
			$table->id('id');
			$table->string('gender', 10)->nullable();
			$table->string('name')->nullable();
			$table->string('surname')->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('email')->nullable();
			$table->date('date_of_birth')->nullable();
			$table->bigInteger('guardian_id')->unsigned()->nullable();
			$table->dateTime('received_date')->nullable();
			$table->string('registration_token')->nullable();
			$table->string('short_hash')->nullable();
			$table->dateTime('timesheet_date')->nullable();
			$table->boolean('timesheet_all')->nullable();
			$table->string('contract_type')->nullable();
			$table->nullableTimestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('volunteers');
	}

}
