![logo Park Dzieje](https://www.parkdzieje.pl/assets/images/elem/parkdzieje-logotyp-3.3ce2187d.png){width=400}

[Odwiedź stronę Parku Dzieje](https://parkdzieje.pl/stowarzyszenie)

## O projekcie Dzieje 2.0
Projekt zakłada stworzenie aplikacji dedykowanej organizacjom przygotowującym wydarzenia kulturalne. Stowarzyszenie opierając się na doświadczeniach organizacji widowisk plenerowych zidentyfikował opotrzebę narzędzia służącego wzmocnieniu kompetencji cyfrowych kadr kultury (szczególnie z obszaru NGO). Szczególnie istotnymi elementami platformy są moduły służące zarządzaniu strukturą organizacji (w tym usprawnieniu komunikacji), zapewnieniu obsługi wymaganych procesów, utrzymaniu wszystkich zbiorów danych (również osobowych) w jednym miejscu – tym samym odpowiadając na potrzeby funkcjonalne i wymagania prawne (np.: RODO).

Dzięki stworzeniu platformy z narzędziami webowym możliwe też będzie dzielenie się wewnątrz struktury organizacji treściami kulturalnymi. Profesjonalizacja kadr kultury poprzez wyposażanie ich w narzędzia wzmacniające kompetencje cyfrowe, może ułatwić znalezienie odpowiedniego sposobu na nową rzeczywistość wkraczającą także do świata kultury.


![Projekt dofinansowano ze środków Narodowego Centrum Kultury](https://parkdzieje.pl/assets/images/prjektdofinansoany-mkidn.jpg)
### Wymagania
- PHP >= 7.2.5
- BCMath PHP Extension
- Ctype PHP Extension
- Fileinfo PHP extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

### Instalacja
Po więcej informacji, zajrzyj na https://laravel.com/docs/7.x

- git pull
- composer install
- modyfikacja pliku .env z danymi dostepowymi
- php artisan migrate
- php artisan db:seed
