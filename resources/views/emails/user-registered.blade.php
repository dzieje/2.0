<p>Witaj {{ $user->name }}</p>

<p>W aplikacji {{ config('app.name') }} zostało utworzone konto.</p>

<p>
    Obecnie Twoje dane do logowania to:
    <br>
    <strong>Login:</strong> <span>{{$user->login}}</span>
    <br>
    <strong>Hasło:</strong> <span>{{$password}}</span>
</p>
<p>
    <a href="{{ url('admin') }}">{{ url('admin') }}</a>
</p>
<p>Po uruchomieniu aplikacji przejdź do ustawień konta aby zmienić hasło.</p>
