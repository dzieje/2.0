<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
    </style>
    <!--[if !mso]><!--><style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto:400,700,400italic,700italic);
    </style><link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto:400,700,400italic,700italic" rel="stylesheet" type="text/css" /><!--<![endif]-->
    <meta name="robots" content="noindex,nofollow" />
</head>
<!--[if mso]>
<body class="mso">
<![endif]-->
<!--[if !mso]><!-->
<body class="full-padding" style="margin: 0;padding: 0;min-width: 100%;background-color: {{ $setting->mail_background_color }};">
<!--<![endif]-->
<center class="wrapper" style="display: table;table-layout: fixed;width: 100%;min-width: 700px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;background-color: {{ $setting->mail_background_color }};">
    <div class="spacer" style="font-size: 1px;line-height: 20px;width: 100%;">&nbsp;</div>
    {!!  $setting->header !!}


    <table class="one-col-bg" style="border-collapse: collapse;border-spacing: 0;width: 100%;">
        <tbody><tr>
            <td style="padding: 0;vertical-align: middle;" align="center">
                <table class="one-col centered column-bg" style="border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;background-color: {{ $setting->content_background_color }};width: 700px;table-layout: fixed;" emb-background-style>
                    <tbody><tr>
                        <td class="column" style="padding: 0;vertical-align: middle;text-align: left;">
                            <div><div class="column-top" style="font-size: 40px;line-height: 40px;transition-timing-function: cubic-bezier(0, 0, 0.2, 1);transition-duration: 150ms;transition-property: all;">&nbsp;</div></div>
                            <table class="contents" style="border-collapse: collapse;border-spacing: 0;table-layout: fixed;width: 100%;">
                                <tbody><tr>
                                    <td class="padded" style="padding: 0;vertical-align: middle;padding-left: 56px;padding-right: 56px;word-break: break-word;word-wrap: break-word;">

                                        <div style="line-height:10px;font-size:1px">&nbsp;</div>

                                    </td>
                                </tr>
                                </tbody></table>

                            <table class="contents" style="border-collapse: collapse;border-spacing: 0;table-layout: fixed;width: 100%;">
                                <tbody><tr>
                                    <td class="padded" style="padding: 0;vertical-align: middle;padding-left: 56px;padding-right: 56px;word-wrap: break-word; color: {{ $setting->content_font_color }}">
                                        {!! isset($content) ? $content : '' !!}
                                        @yield('content')
                                    </td>
                                </tr>
                                </tbody></table>
                            <table class="contents" style="border-collapse: collapse;border-spacing: 0;table-layout: fixed;width: 100%;">
                                <tbody><tr>
                                    <td class="padded" style="padding: 0;vertical-align: middle;padding-left: 56px;padding-right: 56px;word-break: break-word;word-wrap: break-word;">
                                        <div style="line-height:5px;font-size:1px">&nbsp;</div>
                                    </td>
                                </tr>
                                </tbody></table>
                            <div class="column-bottom" style="font-size: 40px;line-height: 20px;transition-timing-function: cubic-bezier(0, 0, 0.2, 1);transition-duration: 150ms;transition-property: all;">&nbsp;</div>
                        </td>
                    </tr>
                    </tbody></table>
            </td>
        </tr>
        </tbody></table>
    {!! $setting->footer !!}

</center>

</body></html>
