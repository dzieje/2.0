@extends('panel.index')

@section('card.content')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.3.2/main.min.css">


    <div id='calendar'></div>


@endsection

@section('scripts')
    @parent
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.3.2/main.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.3.2/locales-all.min.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');
            var calendar = new FullCalendar.Calendar(calendarEl, {
                initialView: 'dayGridMonth',
                eventSources: [
                    {
                        url: '{{ url('panel/calendar/feed') }}'
                    }

                ]
            });
            calendar.setOption('locale', 'pl');
            calendar.render();
        });
    </script>
@endsection
