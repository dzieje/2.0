@extends('panel.index')

@section('card.content')

    @foreach($tasks as $task)
        <div class="card mb-3" >
            <div class="card-body">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-gray-100 small m-0 p-2 mb-2">
                        @foreach($task->node->ancestors as $ancestor)
                        <li class="breadcrumb-item">{{ $ancestor->name }}</li>
                        @endforeach
                        <li class="breadcrumb-item active" aria-current="page">{{ $task->node->name }}</li>
                    </ol>
                </nav>
                <h5 class="card-title">{{ $task->title }}</h5>
                <hr>
                <div class="card-text bg-gray-200 rounded p-2">
                    {!! $task->content !!}
                </div>
                <p class="card-text text-right">
                    <small class="text-muted">
                        <i class="fas fa-calendar fa-fw"></i>
                        {{ \Jenssegers\Date\Date::createFromTimestamp($task->valid_from->timestamp)->diffForHumans() }}
                        -
                        {{ \Jenssegers\Date\Date::createFromTimestamp($task->valid_to->timestamp)->diffForHumans() }}
                    </small>
                </p>
            </div>
        </div>
    @endforeach

@endsection
