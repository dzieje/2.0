@extends('layouts.panel')

@section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-4">
                @include('panel.partials.profile')
            </div>
            <div class="col-lg-8">
                <div class="card border-info shadow">
                    <div class="card-body">
                        @include('panel.partials.nav')

                        <hr>

                        @yield('card.content')
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
