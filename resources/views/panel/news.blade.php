@extends('panel.index')

@section('card.content')

    @foreach($news as $news_item)
        <div class="card mb-3" >
            <div class="row no-gutters">
                @if($news_item->project)
                    <div class="col-sm-4 col-md-2 rotate-sm-l-90 text-center d-flex flex-column justify-content-center align-content-center">
                        <p class="mb-0">
                            {{ $news_item->project->name }}
                        </p>
                        <hr class="bg-dark w-75">
                    </div>
                @else
                    <div class="col-sm-4 col-md-2 d-flex justify-content-center align-content-center">
                    </div>
                @endif
                <div class="col-sm-8 col-md-10">
                    <div class="card-body">
                        <h5 class="card-title">{{ $news_item->title }}</h5>
                        <hr>
                        <div class="card-text bg-gray-200 rounded p-2">
                            {!! $news_item->content !!}
                        </div>
                        <p class="card-text text-right">
                            <small class="text-muted">
                                <i class="fas fa-user-alt fa-fw"></i>
                                {{ $news_item->user->name }}
                            </small>
                            <small class="text-muted">
                                <i class="fas fa-clock fa-fw"></i>
                                {{ \Jenssegers\Date\Date::createFromTimestamp($news_item->created_at->timestamp)->diffForHumans() }}
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

@endsection
