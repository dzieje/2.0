<ul class="nav nav-pills justify-content-center">
    <li class="nav-item m-1">
        <a href="{{ url('panel/news') }}" class="nav-link btn btn-sm btn-{{ Request::segment(2) != 'news' ? 'outline-' : '' }}info">
            <i class="fas fa-envelope-open-text fa-fw"></i>
            <span class="hidden-xs">Aktualności</span>
            <span class="badge badge-info">
                {{ \App\Models\News::count() }}
            </span>
        </a>
    </li>
    <li class="nav-item m-1">
        <a href="{{ url('panel/tasks') }}" class="nav-link btn btn-sm btn-{{ Request::segment(2) != 'tasks' ? 'outline-' : '' }}info">
            <i class="fas fa-tasks fa-fw"></i>
            <span class="hidden-xs">
                Zadania
            </span>
            <span class="badge badge-info">
                {{ $tasks_count }}
            </span>
        </a>
    </li>
    <li class="nav-item m-1">
        <a href="{{ url('panel/projects') }}" class="nav-link btn btn-sm btn-{{ Request::segment(2) != 'projects' ? 'outline-' : '' }}info">
            <i class="fas fa-clipboard-list fa-fw"></i> <span class="hidden-xs">Projekty</span>
        </a>
    </li>
    <li class="nav-item m-1">
        <a href="{{ url('panel/calendar') }}" class="nav-link btn btn-sm btn-{{ Request::segment(2) != 'calendar' ? 'outline-' : '' }}info">
            <i class="fas fa-calendar-alt fa-fw"></i> <span class="hidden-xs">Kalendarz</span>
        </a>
    </li>
    <li class="nav-item m-1">
        <a href="{{ url('panel/edit-profile') }}" class="nav-link btn btn-sm btn-{{ Request::segment(2) != 'edit-profile' ? 'outline-' : '' }}info">
            <i class="fas fa-pencil-alt fa-fw"></i> <span class="hidden-xs">Edycja profilu</span>
        </a>
    </li>
</ul>
