<div class="card border-info shadow">
    <div class="card-body text-center bg-gradient-info rounded-top">
        <div class="d-flex justify-content-between">
            <a href="{{ url('panel/edit-profile') }}" class="btn btn-xs btn-outline-warning">
                <i class="fas fa-pencil-alt fa-fw"></i> edytuj profil
            </a>
            <a class="btn btn-xs btn-outline-light" href="{{ route('panel.logout') }}" onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                {{ __('wyloguj') }}
            </a>
            <form id="logout-form" action="{{ route('panel.logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </div>
        <hr>
        <img src="{{ !auth()->user()->avatar_filename ? 'https://bootdey.com/img/Content/avatar/avatar7.png' : url('avatars',[ auth()->user()->avatar_filename ]) }}"
             class="w-50 border-2 rounded-circle border border-1">
        <h6 class="mt-2 text-white">{{ auth()->user()->full_name }}</h6>
    </div>
    <div class="card-body ">
        <ul class="list-group shadow-none">
            <li class="list-group-item d-flex justify-content-between align-content-center">
                <div>
                    <i class="fa fa-phone-square fa-fw"></i>
                    <span>
                         {{ auth()->user()->phone }}
                    </span>
                </div>
                <small>Telefon</small>
            </li>
            <li class="list-group-item d-flex justify-content-between align-content-center">
                <div>
                    <i class="fa fa-envelope fa-fw"></i>
                    <span>
                         {{ auth()->user()->email }}
                    </span>
                </div>
                <small>Email</small>
            </li>
        </ul>
        <div class="card border-left-primary shadow h-100 py-2 mt-4">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Dołączone projekty</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                            <span class="badge badge-primary">
                                {{ auth()->user()->projects->count() }}
                            </span>
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
