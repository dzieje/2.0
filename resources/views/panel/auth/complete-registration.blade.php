@extends('layouts.panel')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
                      <div class="col-lg-10">
                <div class="row mt-md-5 pt-md-5 pb-md-5 pl-md-5 bg-gradient-info">
                    <div class="col-md-3 text-white text-center d-flex flex-column justify-content-center align-items-center pr-md-5 mb-5">
                        <img src="{{ asset('images/logo.png') }}" class="mb-5 rounded mx-auto d-lg-block w-100 d-none" style="filter: brightness(100);">
                        <h3 class="mb-2">Witamy,</h3>
                        <p>jeśli posiadasz już konto w systemie przejdź do logowania:</p>
                        <a href="{{ url('panel/login') }}" class="btn btn-outline-light btn-sm">
                            <i class="fas fa-sign-in-alt fa-fw"></i>
                            Zaloguj się
                        </a>
                    </div>
                    <div class="col-md-9 rounded-top-left-5 rounded-bottom-left-5 bg-white p-md-5">
                        {!! Form::open(['url' => url('panel/submit', [$volunteer->uuid]), 'id' => 'page-form']) !!}
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4 class="lead" >Witaj <span class="text-info font-weight-bold">{{ $volunteer->full_name }}</span>, dokończ rejestrację konta w systemie {{ env('APP_NAME') }}:</h4>
                                </div>
                                <div class="col-sm-12">
                                    <hr>
                                </div>
                                @if(count($errors) > 0 )
                                    <div class="col-sm-12">
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <ul class="p-0 m-0" style="list-style: none;">
                                                @foreach($errors->all() as $error)
                                                    <li>{{$error}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label> Imię *</label>
                                        <input name="name" type="text" class="form-control" placeholder="Imię" value="{{ $volunteer->name }}" required/>
                                    </div>
                                    <div class="form-group">
                                        <label >Nazwisko *</label>
                                        <input name="surname" type="text" class="form-control" placeholder="Nazwisko" value="{{ $volunteer->surname }}" required/>
                                    </div>
                                    <div class="form-group">
                                        <label >Login *</label>
                                        <input name="login" type="text" class="form-control" placeholder="Login do systemu" value="{{ $volunteer->login }}" required/>
                                    </div>
                                    <div class="form-group">
                                        <label >Hasło *</label>
                                        <input type="password" name="password" class="form-control" placeholder="Hasło" required/>
                                    </div>
                                    <div class="form-group">
                                        <label >Powtórz hasło *</label>
                                        <input type="password" name="password_confirmation" class="form-control"  placeholder="Powtórz hasło" required/>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label >Email *</label>
                                        <input name="email" type="email" class="form-control" placeholder="Email" value="{{ $volunteer->email }}" required/>
                                    </div>
                                    <div class="form-group">
                                        <label >Telefon</label>
                                        <input name="phone" type="text" class="form-control" placeholder="Telefon" value="{{ $volunteer->phone }}" />
                                    </div>
                                    <div class="form-group">
                                        <label >Płeć</label>
                                        <br>
                                        <label class="radio inline">
                                            <input type="radio" name="gender" value="male" @if($volunteer->gender == 'male') checked @endif>
                                            <span> mężczyzna </span>
                                        </label>
                                        <label class="radio inline">
                                            <input type="radio" name="gender" value="female" @if($volunteer->gender == 'female') checked @endif>
                                            <span> kobieta </span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6 offset-md-3">
                                    <div class="form-group pl-md-5 pr-md-5">
                                        <button type="submit" class="btn btn-info btn-block btn-sm">
                                            Zarejestruj!
                                        </button>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
