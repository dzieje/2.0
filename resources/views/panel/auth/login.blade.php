@extends('layouts.panel')

@section('content')
    <div class="container">
        <div class="row mt-5 pt-5 pb-5 pl-5 bg-gradient-info">
            <div class="col-md-3 text-white text-center d-flex flex-column justify-content-center align-items-center pr-5">
                <img src="{{ asset('images/logo.png') }}" class="mb-5 rounded mx-auto d-block w-100 image-filter-white">
                <h3 class="mb-2">Witamy,</h3>
                <p>zaloguj się by przejść do swojego panelu.</p>
            </div>
            <div class="col-md-9 rounded-top-left-5 rounded-bottom-left-5 bg-white p-5">
                @if($errors->any())
                    {!! implode('', $errors->all('<div class="alert alert-danger">:message</div>')) !!}
                @endif
                {!! Form::open(['url' => url('panel/login'), 'id' => 'page-form', 'method' => 'post']) !!}
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <div class="form-group">
                                <label >Login *</label>
                                <input name="login" type="text" class="form-control" placeholder="Login" required/>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <label >Hasło *</label>
                                    <input type="password" name="password" class="form-control" placeholder="Hasło" required/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 offset-md-3">
                            <div class="form-group pl-md-5 pr-md-5">
                                <button type="submit" class="btn btn-info btn-block btn-sm">
                                    <i class="fas fa-sign-in-alt fa-fw"></i> Zaloguj się!
                                </button>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
