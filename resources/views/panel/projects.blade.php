@extends('panel.index')

@section('card.content')

    @foreach(auth()->user()->projects as $project)
        <div class="card mb-3" >
            <div class="card-body">
                <h5 class="card-title d-flex justify-content-between">
                    {{ $project->name }}

                    @if($project->pivot->contract)
                        <span class="badge badge-info p-2">
                            umowa:
                            {{ $project->pivot->contract }}
                        </span>
                    @endif
                </h5>
            </div>
        </div>
    @endforeach

@endsection
