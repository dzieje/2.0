@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        @foreach($charts as $dataset)
            <div class="col-sm-12 col-md-4">
                <div class="card">
                    <div class="card-header bg-info text-white">
                        {{ $dataset['project']->name }}
                    </div>
                    <div class="card-body">
                        {!! $dataset['chart']->container() !!}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection

@section('scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>

        @foreach($charts as $dataset)
            {!! $dataset['chart']->script() !!}
        @endforeach
@endsection
