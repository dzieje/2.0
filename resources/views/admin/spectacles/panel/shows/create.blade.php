<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Tworzenie widowiska </h5>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="panel-body">
        <form action="{{ url('admin/spectacle/'.$project->slug.'/panel/shows/store') }}" method="post" id="dialog-form">
            {!! csrf_field() !!}
            <div class="form-group">
                <label>Data widowiska:</label>
                <div class="row">
                    <div class="col-6">
                        {!!  Form::date('show_date', '', array('class' => 'form-control required')) !!}
                    </div>
                    <div class="col-6">
                        {!!  Form::time('show_time', '',array('class' => 'form-control required')) !!}
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa dodawanie">Dodaj</button>
</div>
