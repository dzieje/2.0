@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-6 ">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                        Zarządzanie widowiskami
                        </h6>
                        <span target="{{ url('admin/spectacle/'.$project->slug.'/panel/shows/create') }}" class="btn btn-xs btn-info modal-open" data-toggle="modal" data-target="#modal">
                            <i class="fas fa-plus fa-fw"></i> dodaj
                        </span>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-hover">
                                <thead>
                                <th>#</th>
                                <th></th>
                                <th>Data widowiska</th>
                                </thead>
                                @foreach($shows as $lp => $show)
                                    <tr>
                                        <td>{{ ++$lp }}.</td>
                                        <td>
                                            <a class="btn btn-xs btn-primary" href="{{ url('admin/spectacle/'.$project->slug.'/panel/shows/generate-outline', [$show->id, 'pdf']) }}" >
                                                <i class="fa fa-file-pdf fa-fw" aria-hidden="true"></i> generuj konspekt
                                            </a>
                                        </td>
                                        <td>
                                            {{ \Jenssegers\Date\Date::createFromFormat('Y-m-d',  $show->show->format('Y-m-d'))->format('j F Y')  }}
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

