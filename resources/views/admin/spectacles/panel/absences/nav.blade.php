<ul class="nav nav-pills"  role="tablist">
    <li class="nav-item" role="presentation">
        <a  @if( Request::segment(6) == 'list') class="nav-link active" @else class="nav-link" @endif href="{{ url('admin/spectacle/'.$project->slug.'/panel/absences/list') }}">Lista nieobecności</a>
    </li>
    <li  class="nav-item" role="presentation" >
        <a @if(Request::segment(6) == 'roles' || Request::segment(6) == 'roles-show') class="nav-link active" @else class="nav-link" @endif  href="{{ url('admin/spectacle/'.$project->slug.'/panel/absences/roles') }}">Zestawienie ról</a>
    </li>
</ul>
