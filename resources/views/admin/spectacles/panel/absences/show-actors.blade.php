<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Aktorzy z rolą <i>{{ $role->name }}</i> </h4>
</div>
<div class="modal-body">
    <div class="panel">
        <table class="table table-hover table-condensed table-bordered">
            <thead>
                <th>#</th>
                <th>Grupy</th>
                <th>Nazwisko</th>
                <th>Imię</th>
                <Th>Telefon</Th>
                <th>Email</th>
                <th></th>
            </thead>
            @foreach($role->actors as $lp => $actor)
                <tr
                @if($actor->shows->contains($show->id))
                    class="bg-danger"
                @endif
                >
                    <td width="10px">{{ ++$lp }}.</td>
                    <td>
                        @if($actor->groups->count() > 0)
                            {{ implode(', ', $actor->groups->pluck('name')->toArray()) }}
                        @endif
                    </td>
                    <td><strong>{{ $actor->surname }}</strong></td>
                    <td><strong>{{ $actor->name }}</strong></td>
                    <td>{{ $actor->phone }}</td>
                    <td>{{ $actor->email }}</td>
                    <td>
                        @if($role->if_single_actor == 1)
                            <span target="{{ url('admin/n-manage/roles/detach-actor', [$role->id, $actor->id]) }}" class="btn btn-xs btn-danger detach-actor">
                                <i class="fa fa-trash-o fa-fw"></i> odepnij actora
                            </span>
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
</div>

<script>
    $('.detach-actor').on('click', function(){
        var $this = $(this);
        if(confirm('potwierdź odpięcie aktora od roli')){
            $.ajax({
                type: "POST",
                url: $this.attr('target'),
                assync: false,
                cache: false,
                success: function (data) {
                    if (data.code == '0') location.reload();
                    else if (data.code == '1') self.location = data.url;
                },
                dataType: 'json'
            });
        }
    });
</script>