@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            Lista nieobecności
                        </h6>
                        <div class="d-flex  align-items-center">
                            @include('admin.spectacles.panel.absences.nav')
                            <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSearchForm" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="btn btn-sm btn-outline-info">
                                <i class="fas fa-search"></i>
                                @if(count( array_filter(Request::all() ) ) > 0)
                                    <span class="badge badge-info">
                                        {{ count( array_filter(Request::all() ) ) }}
                                    </span>
                                @endif
                            </span>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <nav class="navbar navbar-light bg-light">
                            <div class="collapse navbar-collapse" id="navbarSearchForm">
                                <form class="form-inline" role="search" id="search-form" action="{{ url('admin/spectacle/'.$project->slug.'/panel/absences/list') }}" method="get">
                                    <div class="form-group form-group-sm flex-column text-center mr-1 ml-1 mb-2">

                                        <label>Filtrowanie aktorów <span class="badge badge-dark ml-2">{{ $actors->total() }}</span></label>
                                        <div>
                                            <a class="btn btn-xs btn-danger" href="{{ url('admin/spectacle/'.$project->slug.'/panel/absences/list') }}">
                                                <i class="fa fa-trash-alt fa-fw"></i> usuń filtry
                                            </a>
                                            <span class="btn btn-xs btn-primary export" target="{{  url('admin/spectacle/'.$project->slug.'/panel/absences/export-actors') }}">
                                                <i class="fa fa-file-excel fa-fw"></i> exportuj xls
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <div class="divider">|</div>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">imię:
                                        <input name="name" value="{{ Request::get('name') ? Request::get('name') : '' }}" type="text" class="form-control form-control-sm" placeholder="imię">
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">nazwisko:
                                        <input name="surname" value="{{ Request::get('surname') ? Request::get('surname') : '' }}" type="text" class="form-control form-control-sm" placeholder="nazwisko">
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <div class="divider">|</div>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">grupa:
                                        <select name="group" class="form-control form-control-sm">
                                            <option value="0">--- wybierz ---</option>
                                            <option value="-1" @if((Request::get('group') == '-1')) selected @endif>--- bez przypisanej grupy ---</option>
                                            @foreach($groups as $group_id => $group)
                                                <option value="{{ $group_id }}" {{ (Request::get('group') == $group_id) ? 'selected' : '' }}>{{ $group }}</option>
                                            @endforeach
                                        </select>
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">rola:
                                        <select name="role" class="form-control form-control-sm">
                                            <option value="0">--- wybierz ---</option>
                                            @foreach($roles as $role_id => $role)
                                                <option value="{{ $role_id }}" {{ (Request::get('role') == $role_id) ? 'selected' : '' }}>{{ $role }}</option>
                                            @endforeach
                                        </select>
                                        </label>
                                    </div>
                                </form>
                            </div>
                        </nav>
                        <div class="table-responsive">
                            <p class="mt-2">Oczekiwanie na formularze od: <span class="badge badge-info">{{ $allactorsWaiting }}</span></p>
                            <p class="alert alert-info">
                                10 losowych:
                            @foreach($actorsWaiting as $key => $wActor)
                                {{$wActor->name }} {{$wActor->surname }} (tel. <a href="tel:{{$wActor->phone}}">{{$wActor->phone}}</a> – <a target="_blank" href="/time-sheet/{{$wActor->registration_token}}">link</a>)
                            @endforeach
                            </p>
                            <table class="table table-sm table-hover">
                                <thead>
                                <th>#</th>
                                <th></th>
                                <th>Data formularza</th>
                                <th>Nazwisko</th>
                                <th>Imię</th>
                                <th>Telefon</th>
                                <Th>Email</Th>
                                <th>Wiek</th>
                                <th>Grupa</th>
                                <th>Role</th>
                                <th></th>
                                <th></th>
                                <Th></Th>
                                </thead>
                                @foreach($actors as $actor)
                                    <tr>
                                        <td>{{ ($actors->currentPage() - 1) * $actors->perPage() + $loop->iteration  }}.</td>
                                        <td>
                                            @if($actor->if_actor === 0)
                                                tylko pomoc
                                            @elseif($actor->if_actor === 3)
                                                za dużo nieobecności: {{$actor->shows->count()}}
                                            @elseif(is_null($actor->if_actor))
                                                zrezygnował
                                            @elseif($actor->shows->count() > 0)
                                            <a href="{{ url('admin/spectacle/'.$project->slug.'/panel/absences/show', [$actor->id]) }}" class="btn btn-xs btn-danger">
                                                <i class="fa fa-calendar fa-fw"></i> nieobecności  <span class="badge">{{ $actor->shows->count() }}</span>
                                            </a>
                                            @endif
                                        </td>
                                        <td>{{ $actor->timesheet_date }}</td>
                                        <td>{{ $actor->surname }}</td>
                                        <td>{{ $actor->name }}</td>
                                        <td>{{ $actor->phone }}</td>
                                        <td>{{ $actor->email }}</td>
                                        <td>
                                            {{ $actor->date_of_birth->format('Y-m-d') }}
                                            <span class="badge">{{ $actor->date_of_birth->diffInYears(Carbon\Carbon::now()) }}</span>
                                        </td>
                                        <td>
                                            @if($actor->group)
                                                {{ $actor->group->name }}
                                            @else
                                                ---
                                            @endif
                                        </td>
                                        <td>
                                            @if( $actor->shows->count() > 0)
                                            <a href="{{ url('admin/spectacle/'.$project->slug.'/panel/absences/edit', [$actor->id]) }}" class="btn btn-xs btn-warning">
                                                <i class="fa fa-pencil fa-fw"></i>  edytuj nieobecności
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>

                    <div class="card-footer text-right">
                        {!! $actors->appends(Request::query())->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $('#search-form input').on('keyup keypress', function (e) {
            if(e.which === 13){
                $('#search-form').submit();
            }
        });
        $('#search-form select').on('change', function () {
            $('#search-form').submit();
        });

        $('.export').on('click', function(){
            var url = $(this).attr('target') + '?' + $('#search-form').serialize();

            self.location = url;
        });
    </script>
@endsection
