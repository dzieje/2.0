@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1 text-right">
            <a href="{{ url('admin/panel/absences/edit', [$actor->id]) }}" class="btn btn-sm btn-warning marg-btm">
                <i class="fa fa-pencil fa-fw"></i>  edytuj nieobecności
            </a>

            <a href="{{ url('admin/panel/absences/list') }}" class="btn btn-sm btn-default marg-btm">
                <i class="fa fa-reply fa-fw" aria-hidden="true"></i> wróć
            </a>
        </div>
        <div class="col-lg-10 col-lg-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Lista nieobecności <i>{{ $actor->name }} {{ $actor->surname }}</i>
                </div>
                <div class="panel-body">
                    <div class="row">
                    @foreach($actor->shows as $show)
                        <div class="col-sm-6 col-md-4 col-lg-3 marg-btm">
                            <div class="text-danger">
                                <i class="fa fa-minus-square fa-fw" aria-hidden="true"></i>
                                {{ \Jenssegers\Date\Date::createFromFormat('Y-m-d',  $show->show->format('Y-m-d'))->format('j F Y') }}
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
