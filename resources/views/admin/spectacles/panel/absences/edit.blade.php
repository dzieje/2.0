@extends('layouts.admin')

@section('content')
    <style>
        .checkbox input[type='checkbox']{
            opacity: 0;
            width: 18px;
        }
        .checkbox input[type='checkbox'] + label{
            margin: 0;
            clear: none;

            padding: 5px 0 4px 35px;

            cursor: pointer;

            background: url('/../../images/off.png') left center no-repeat;
        }

        .checkbox input[type='checkbox']:checked + label{
            background-image: url('/../../images/check.png');
        }
    </style>

    <div class="row ">
        <div class="col-lg-8 col-lg-offset-2 text-right marg-btm">
            <a href="{{ url('admin/panel/absences/list') }}" class="btn btn-sm btn-default">
                <i class="fa fa-reply fa-fw" aria-hidden="true"></i> anuluj
            </a>
        </div>
        <div class="col-lg-8 col-lg-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Lista nieobecności <i>{{ $actor->name }} {{ $actor->surname }}</i>
                    </h3>
                </div>
                <div class="panel-body">
                    {!! Form::open(['url' => url('admin/panel/absences/update', [$actor->id]), 'id' => 'page-form']) !!}
                    <div class="row">
                        @foreach($shows as $show)
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="checkbox">
                                    <input name="shows[]" id="show{{ $show->id }}" value="{{ $show->id }}" type="checkbox"
                                           @if($actor->shows->contains($show->id)) checked @endif >
                                    <label for="show{{ $show->id }}">
                                        {{ \Jenssegers\Date\Date::createFromFormat('Y-m-d',  $show->show->format('Y-m-d'))->format('j F Y') }}
                                    </label>
                                </div>
                            </div>
                        @endforeach
                        <div class="col-sm-12">
                            <hr>
                        </div>
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Zapisz listę nieobecności</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
