@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1 text-right">
            <a href="{{ url('admin/panel/cleanups/edit', [$volunteer->id]) }}" class="btn btn-sm btn-warning marg-btm">
                <i class="fa fa-pencil fa-fw"></i>  edytuj porządki
            </a>

            <a href="{{ url('admin/panel/cleanups') }}" class="btn btn-sm btn-default marg-btm">
                <i class="fa fa-reply fa-fw" aria-hidden="true"></i> wróć
            </a>
        </div>
        <div class="col-lg-10 col-lg-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Lista porządków <i>{{ $volunteer->name }} {{ $volunteer->surname }}</i>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-6 col-lg-offset-3">
                            <table class="table table-condensed table-hover">
                                <tbody>
                                @foreach($volunteer->cleanupPeriods as $cleanupPeriod)
                                    <tr>
                                        <td>
                                            <span class="lead">
                                                {{ \Jenssegers\Date\Date::createFromFormat('Y-m-d',  $cleanupPeriod->cleanup->cleaning_day->format('Y-m-d'))->format('l')  }}
                                                <br>
                                                <small>{{ $cleanupPeriod->cleanup->cleaning_day->format('Y-m-d') }}</small>
                                            </span>
                                        </td>
                                        <td style="text-align: left;vertical-align: middle;">
                                            <span class="lead">
                                                {{ substr($cleanupPeriod->cleaning_time, 0, -3) }}
                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
