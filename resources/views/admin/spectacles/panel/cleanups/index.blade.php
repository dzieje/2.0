@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Lista porządków
                </div>
                <div class="panel-body">
                    <nav class="navbar navbar-default navbar-sm marg-top-min" style="margin-bottom: 0px;">
                        <div class="container-fluid">
                            <form class="navbar-form navbar-left" role="search" id="search-form" action="{{ url('admin/panel/cleanups') }}" method="get">
                                <div class="form-group form-group-sm text-center ">
                                    <label>Filtrowanie wolontariuszy <span class="badge">{{ $volunteers->total() }}</span></label><br>
                                    <a class="btn btn-xs btn-danger" href="{{ url('admin/panel/cleanups') }}">
                                        <i class="fa fa-remove fa-fw"></i> usuń filtry
                                    </a>
                                    <span class="btn btn-xs btn-primary export" target="{{  url('admin/panel/cleanups/export') }}">
                                        <i class="fa fa-file-excel-o fa-fw"></i> exportuj xls
                                    </span>
                                </div>
                                <div class="form-group form-group-sm">
                                    <div class="divider">|</div>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label style="width: 100%" class="text-center">imię:</label><br>
                                    <input name="name" value="{{ Request::get('name') ? Request::get('name') : '' }}" type="text" class="form-control" placeholder="imię">
                                </div>
                                <div class="form-group form-group-sm">
                                    <label style="width: 100%" class="text-center">nazwisko:</label><br>
                                    <input name="surname" value="{{ Request::get('surname') ? Request::get('surname') : '' }}" type="text" class="form-control" placeholder="nazwisko">
                                </div>
                            </form>
                        </div>
                    </nav>
                    <div class="table-responsive marg-top">
                        <table class="table table-condensed table-hover">
                            <thead>
                            <th>#</th>
                            <th></th>
                            <th>Nazwisko</th>
                            <th>Imię</th>
                            <th>Telefon</th>
                            <Th>Email</Th>
                            <th>Wiek</th>
                            <th></th>
                            <th></th>
                            <Th></Th>
                            </thead>
                            <?php $lp = (($volunteers->currentPage()-1)*$volunteers->perPage()) + 1;?>
                            @foreach($volunteers as $volunteer)
                                <tr>
                                    <td>{{ $lp++ }}.</td>
                                    <td>
                                        <a href="{{ url('admin/panel/cleanups/show', [$volunteer->id]) }}" class="btn btn-xs btn-danger">
                                            <i class="fa fa-calendar fa-fw"></i> porządki  <span class="badge">{{ $volunteer->cleanupPeriods->count() }}</span>
                                        </a>
                                    </td>
                                    <td>{{ $volunteer->surname }}</td>
                                    <td>{{ $volunteer->name }}</td>
                                    <td>{{ $volunteer->phone }}</td>
                                    <td>{{ $volunteer->email }}</td>
                                    <td>
                                        @if($volunteer->date_of_birth)
                                            {{ $volunteer->date_of_birth->format('Y-m-d') }}
                                            <span class="badge">{{ $volunteer->date_of_birth->diffInYears(Carbon\Carbon::now()) }}</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ url('admin/panel/cleanups/edit', [$volunteer->id]) }}" class="btn btn-xs btn-warning">
                                            <i class="fa fa-pencil fa-fw"></i>  edytuj porządki
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>

                <div class="panel-footer text-right">
                    {!! $volunteers->appends(Request::query())->links() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $('#search-form input').on('keyup keypress', function (e) {
            if(e.which === 13){
                $('#search-form').submit();
            }
        });
        $('#search-form select').on('change', function () {
            $('#search-form').submit();
        });

        $('.export').on('click', function(){
            var url = $(this).attr('target') + '?' + $('#search-form').serialize();

            self.location = url;
        });
    </script>
@endsection