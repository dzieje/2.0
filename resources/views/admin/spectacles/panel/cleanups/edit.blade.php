@extends('layouts.admin')

@section('content')
    <style>
        .checkbox input[type='checkbox']{
            opacity: 0;
            width: 18px;
        }
        .checkbox input[type='checkbox'] + label{
            margin: 0;
            clear: none;

            padding: 5px 0 4px 35px;

            cursor: pointer;

            background: url('/../../images/off.png') left center no-repeat;
        }

        .checkbox input[type='checkbox']:checked + label{
            background-image: url('/../../images/check.png');
        }
    </style>

    <div class="row ">
        <div class="col-lg-8 col-lg-offset-2 text-right marg-btm">
            <a href="{{ url('admin/panel/cleanups') }}" class="btn btn-sm btn-default">
                <i class="fa fa-reply fa-fw" aria-hidden="true"></i> anuluj
            </a>
        </div>
        <div class="col-lg-8 col-lg-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Lista porządków <i>{{ $volunteer->name }} {{ $volunteer->surname }}</i>
                    </h3>
                </div>
                <div class="panel-body">
                    {!! Form::open(['url' => url('admin/panel/cleanups/update', [$volunteer->id]), 'id' => 'page-form']) !!}
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-condensed">
                                <thead>
                                <th style="vertical-align: middle;">Pomogę od godziny:</th>
                                @foreach($cleanups as $cleanup)
                                    <th class="text-center">
                                        {{ \Jenssegers\Date\Date::createFromFormat('Y-m-d',  $cleanup->cleaning_day->format('Y-m-d'))->format('l')  }}
                                        <br>
                                        <small>( {{ $cleanup->cleaning_day->format('Y-m-d') }} )</small>
                                    </th>
                                @endforeach
                                </thead>
                                @foreach( $times as $time)
                                    <tr>
                                        <td style="vertical-align: middle;">
                                            <strong class="lead">{{ substr($time, 0, -3) }}</strong>
                                        </td>
                                        @foreach($cleanups as $cleanup)
                                            <td class="text-center">
                                                <div class="radio">
                                                    <label>
                                                        {!! Form::radio('cleanups['.$cleanup->id.']', $cleanup->periods->where('cleaning_time', $time)->first()->id, ($volunteer->cleanupPeriods->contains($cleanup->periods->where('cleaning_time', $time)->first()->id) ? 'checked' : null)) !!}
                                                    </label>
                                                </div>
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                <tr>
                                    <td></td>
                                    @foreach($cleanups as $cleanup)
                                        <td class="text-center">
                                            <span class="btn btn-danger btn-xs remove off-disable" data-cleanup="{{$cleanup->id}}">
                                                <i class="fa fa-remove"></i>
                                            </span>
                                        </td>
                                    @endforeach
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-12">
                            <hr>
                        </div>
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Zapisz zmiany</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    @parent
    <script>
        $('.remove').on('click', function(){
            var cleanup = $(this).data('cleanup');

            $('input[name="cleanups[' + cleanup + ']"]').attr('checked',false);
        });
    </script>
@endsection