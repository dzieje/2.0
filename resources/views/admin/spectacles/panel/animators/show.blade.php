<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Informacje o <i>{{ $actor->surname }} {{ $actor->name }}</i></h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <nav class="navbar navbar-default">
                        <div class="row">

                            <div class="col-sm-12">

                                <p class="navbar-text navbar-text-small">
                                    nazwisko: <label>{{ $actor->surname }}</label>
                                </p>
                                <p class="navbar-text navbar-text-small">
                                    imię: <label>{{ $actor->name }}</label>
                                </p>
                                <p class="navbar-text navbar-text-small">
                                    telefon: <label>{{ $actor->phone }}</label>
                                </p>
                                <p class="navbar-text navbar-text-small">
                                    email: <label>{{ $actor->email }}</label>
                                </p>
                                <p class="navbar-text navbar-text-small">
                                    angażuje się: <label>@if($actor->respondent_type == 'first') pierwszy raz @else kolejny @endif</label></p>
                                <p class="navbar-text navbar-text-small">
                                    płeć: <label>@if($actor->gender == 'male') mężczyzna @else kobieta @endif</label>
                                </p>
                                <p class="navbar-text navbar-text-small">
                                    adres:
                                    <label>
                                        {{ $actor->street }} {{ $actor->house_number }}@if($actor->flat_number != '')/{{ $actor->flat_number }}@endif {{ $actor->post_code }} {{ $actor->city }}
                                    </label>
                                </p>
                                <p class="navbar-text navbar-text-small">
                                    PESEL: <label>{{ $actor->personal_id }}</label>
                                </p>
                                <p class="navbar-text navbar-text-small">
                                    data urodzin:
                                    <label>
                                        {{ $actor->date_of_birth->format('Y-m-d') }}
                                        <span class="badge">{{ $actor->date_of_birth->diffInYears(Carbon\Carbon::now()) }}</span>
                                    </label>
                                </p>
                                <p class="navbar-text navbar-text-small">
                                    wzrost: <label>{{ $actor->height }}</label>
                                </p>
                                <p class="navbar-text navbar-text-small">
                                    rozmiar butów: <label>{{ round($actor->shoes_size,0) }}</label>
                                </p>
                                <p class="navbar-text navbar-text-small">
                                    rozmiar koszulki: <label>{{ $actor->shirt_size }}</label>
                                </p>
                            </div>
                            <div class="col-sm-12">
                                <p class="navbar-text navbar-text-small" >
                                    grupa: <label>
                                        @if($actor->group) {{ $actor->group->name }} @else --- @endif
                                    </label>
                                </p>
                            </div>
                        </div>
                    </nav>

                    <div>
                        <span class="label label-success pull-right marg-left">dzieje</span>
                        <span class="label label-danger pull-right">statysta</span>
                    </div>
                    <table class="table table-hover ">
                        <thead>
                        <th>#</th>
                        <th>scena</th>
                        <th>rola</th>
                        <th>rekwizyty</th>
                        <th>kostiumy</th>
                        </thead>
                        @foreach($actor->roles as $k => $role)
                            <tr>
                                <td>{{ ++$k }}.</td>
                                <td>{{ $role->scene->name }}</td>
                                <td>{{ $role->name }}</td>
                                <td>
                                    @if($role->props->count() > 0)
                                        @foreach($role->props as $prop)
                                            @if($prop->type == 'Dzieje')
                                                <span class="text-success">
                                                    {{ $prop->name }}
                                                </span>
                                            @else
                                                <span class="text-danger">
                                                    {{ $prop->name }}
                                                </span>
                                            @endif

                                            @if($role->props->count() > 1) ; @endif
                                        @endforeach
                                    @else
                                        ---
                                    @endif
                                </td>
                                <td>
                                    @if($role->costumes->count() > 0)
                                        @foreach($role->costumes as $costume)
                                            @if($costume->type == 'Dzieje')
                                                <span class="text-success">
                                                    {{ $costume->name }}
                                                </span>
                                            @else
                                                <span class="text-danger">
                                                    {{ $costume->name }}
                                                </span>
                                            @endif


                                            @if($role->costumes->count() > 1) ; @endif
                                        @endforeach
                                    @else
                                        ---
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
</div>