<ul class="nav nav-pills nav-condensed pull-right">
    <li role="presentation" @if(! Request::segment(4)) class="active" @endif>
        <a href="{{ url('admin/panel/animators') }}">Lista dzieci</a>
    </li>
    <li role="presentation" @if(Request::segment(4) == 'timetables') class="active" @endif>
        <a href="{{ url('admin/panel/animators/timetables') }}">Próby</a>
    </li>
</ul>