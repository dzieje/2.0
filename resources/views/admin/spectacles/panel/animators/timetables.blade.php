@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Panel animatora - próby

                    @include('panel.animators.nav')
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed table-hover">
                            <thead>
                            <th>#</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>Nazwa próby</th>
                            <th>Data próby</th>
                            <th></th>
                            </thead>
                            <?php $lp = (($rehearsals->currentPage()-1)*$rehearsals->perPage()) + 1;?>
                            @foreach($rehearsals as $rehearsal)
                                <tr>
                                    <td>{{ $lp++ }}.</td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary btn-xs dropdown-toggle off-disable" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-file-pdf-o fa-fw" aria-hidden="true"></i> generuj pdf <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a  href="{{ url('admin/panel/animators/generate-outline', [$rehearsal->id]) }}" >
                                                        <i class="fa fa-file-pdf-o fa-fw" aria-hidden="true"></i> generuj konspekt
                                                    </a>
                                                </li>
                                                <li>
                                                    <a  href="{{ url('admin/panel/animators/generate-outline-guardian', [$rehearsal->id, 1]) }}" >
                                                        <i class="fa fa-file-pdf-o fa-fw" aria-hidden="true"></i> generuj konspekt - odbiór
                                                    </a>
                                                </li>
                                                <li>
                                                    <a  href="{{ url('admin/panel/animators/generate-outline-guardian', [$rehearsal->id, 2]) }}" >
                                                        <i class="fa fa-file-pdf-o fa-fw" aria-hidden="true"></i> generuj konspekt - sam
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td>
                                        <a class="btn btn-success btn-xs" href="{{ url('admin/panel/animators/generate-list', [$rehearsal->id, 1]) }}" >
                                            <i class="fa fa-file-excel-o fa-fw" aria-hidden="true"></i> generuj listę - odbiór
                                        </a>
                                    </td>
                                    <td>
                                        <a class="btn btn-success btn-xs" href="{{ url('admin/panel/animators/generate-list', [$rehearsal->id, 2]) }}" >
                                            <i class="fa fa-file-excel-o fa-fw" aria-hidden="true"></i> generuj listę - sam
                                        </a>
                                    </td>
                                    <td>{{ $rehearsal->name }}</td>
                                    <td>{{ $rehearsal->rehearsal->format('Y-m-d') }}</td>
                                    <td>
                                        <a href="{{ url('admin/panel/animators/attendance', [$rehearsal->id]) }}" class="btn btn-success btn-xs">
                                            <i class="fa fa-users fa-fw"></i> lista obecności
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>

                <div class="panel-footer text-right">
                    {!! $rehearsals->links() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

