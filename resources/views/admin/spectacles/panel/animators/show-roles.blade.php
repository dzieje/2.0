<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Role przypisane do <i>{{ $actor->name }} {{ $actor->surname }}</i> </h4>
</div>
<div class="modal-body">
    <div class="panel">
        <table class="table table-hover table-condensed table-bordered">
            <thead>
                <th>#</th>
                <th>scena</th>
                <th>rola</th>
            </thead>
            @foreach($actor->roles as $lp => $role)
                <tr>
                    <td width="10px">{{ ++$lp }}.</td>
                    <td><strong>{{ $role->scene->name }}</strong></td>
                    <td>
                        <strong>{{ $role->name }}</strong>
                        @if($role->if_single_actor == 1)
                            <label class="label label-warning">indywid.</label>
                        @endif
                        <br/>
                        @if($role->if_understudy == 1)
                            <label class="label label-info">dubler</label>
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
</div>