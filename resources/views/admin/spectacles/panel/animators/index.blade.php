@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Panel animatora - dzieci
                    @include('panel.animators.nav')
                </div>
                <div class="panel-body">
                    <nav class="navbar navbar-default navbar-sm marg-top-min" style="margin-bottom: 0px;">
                        <div class="container-fluid">
                                <form class="navbar-form navbar-left" role="search" id="search-form" action="{{ url('admin/panel/animators') }}" method="get">
                                    <div class="form-group form-group-sm text-center ">
                                        <label>Filtrowanie dzieci <span class="badge">{{ $actors->total() }}</span></label><br>
                                        <a class="btn btn-xs btn-danger" href="{{ url('admin/panel/animators') }}">
                                            <i class="fa fa-remove fa-fw"></i> usuń filtry
                                        </a>
                                        <span class="btn btn-primary btn-xs" id="export-results">
                                            <i class="fa fa-file-excel-o fa-fw" aria-hidden="true"></i> eksportuj wyniki
                                        </span>
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <label style="width: 100%" class="text-center">imię:</label><br>
                                        <input name="name" value="{{ Request::get('name') ? Request::get('name') : '' }}" type="text" class="form-control" placeholder="imię">
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <label style="width: 100%" class="text-center">nazwisko:</label><br>
                                        <input name="surname" value="{{ Request::get('surname') ? Request::get('surname') : '' }}" type="text" class="form-control" placeholder="nazwisko">
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <label style="width: 100%" class="text-center">grupa:</label><br>
                                        <select name="group" class="form-control">
                                            <option value="0">--- wybierz ---</option>
                                            <option value="-1" @if((Request::get('group') == '-1')) selected @endif>--- bez przypisanej grupy ---</option>
                                            @foreach($groups as $group_id => $group)
                                                <option value="{{ $group_id }}" {{ (Request::get('group') == $group_id) ? 'selected' : '' }}>{{ $group }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <label style="width: 100%" class="text-center">typ zgody:</label><br>
                                        <select name="guardian_permission" class="form-control">
                                            <option value="0">--- wybierz ---</option>
                                            <option value="1" @if((Request::get('guardian_permission') == '1')) selected @endif>odbiór</option>
                                            <option value="2" @if((Request::get('guardian_permission') == '2')) selected @endif>sam</option>
                                        </select>
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <label style="width: 100%" class="text-center">grupa dzieci:</label><br>
                                        <select name="children" class="form-control">
                                            <option value="-1">--- wybierz ---</option>
                                            <option value="1" @if((Request::get('children') == '1')) selected @endif>tak</option>
                                            <option value="0" @if((Request::get('children') == '0')) selected @endif>nie</option>
                                        </select>
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <label style="width: 100%" class="text-center">próba:</label><br>
                                        <select name="rehearsal" class="form-control">
                                            <option value="0">--- wybierz ---</option>
                                            @foreach($rehearsals as $rehearsal_id => $rehearsal)
                                                <option value="{{ $rehearsal_id }}" {{ (Request::get('rehearsal') == $rehearsal_id) ? 'selected' : '' }}>{{ $rehearsal }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </form>
                        </div>
                    </nav>
                    <div class="table-responsive marg-top">
                        <table class="table table-condensed table-hover">
                            <thead>
                            <th>#</th>
                            <th></th>
                            <th>Nazwisko</th>
                            <th>Imię</th>
                            <th>Telefon</th>
                            <Th>Email</Th>
                            <th>Wiek</th>
                            <th>Grupa</th>
                            <th>Role</th>
                            <th></th>
                            <th></th>
                            <Th></Th>
                            </thead>
                            <?php $lp = (($actors->currentPage()-1)*$actors->perPage()) + 1;?>
                            @foreach($actors as $actor)
                                <tr>
                                    <td>{{ $lp++ }}.</td>
                                    <td>
                                        <a href="{{ url('admin/panel/animators/generate-rehearsals-timetable', [$actor->id]) }}" class="btn btn-primary btn-xs">
                                            <i class="fa fa-file-pdf-o fa-fw" aria-hidden="true"></i>
                                            gen. harm. prób
                                        </a>
                                        <a href="{{ url('admin/panel/animators/generate-roles', [$actor->id]) }}" class="btn btn-primary btn-xs">
                                            <i class="fa fa-file-pdf-o fa-fw" aria-hidden="true"></i>
                                            gen. spis ról
                                        </a>
                                    </td>
                                    <td>{{ $actor->surname }}</td>
                                    <td>{{ $actor->name }}</td>
                                    <td>{{ $actor->phone }}</td>
                                    <td>{{ $actor->email }}</td>
                                    <td>
                                        {{ $actor->date_of_birth->format('Y-m-d') }}
                                        <span class="badge">{{ $actor->date_of_birth->diffInYears(Carbon\Carbon::now()) }}</span>
                                    </td>
                                    <td>
                                        @if($actor->groups->count() > 0)
                                            {{ implode(', ', $actor->groups->pluck('name')->toArray()) }}
                                        @else
                                            ---
                                        @endif
                                    </td>
                                    <td>
                                        <span target="{{ url('admin/panel/animators/show-roles', [$actor->id]) }}" class="btn btn-xs btn-info modal-open-lg" data-toggle="modal" data-target="#modal-lg">
                                            <i class="fa fa-search fa-fw"></i>  <span class="badge">{{ $actor->roles->count() }} </span>
                                        </span>
                                    </td>
                                    <td>
                                        <span target="{{ url('admin/panel/animators/show', [$actor->id]) }}" class="btn btn-info btn-xs modal-open-lg off-disable" data-toggle="modal" data-target="#modal-lg" data-backdrop="true">
                                            <i class="fa fa-search fa-fw"></i> szczegóły
                                        </span>
                                    </td>
                                    <td>
                                        <span class="btn btn-xs btn-info modal-open" data-toggle="modal" data-target="#modal" target="{{ url('admin/panel/animators/guardian', [$actor->guardian_id]) }}">
                                            <i class="fa fa-user fa-fw"></i> opiekun
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>

                <div class="panel-footer text-right">
                    {!! $actors->appends(Request::query())->links() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $('#search-form input').on('keyup keypress', function (e) {
            if(e.which === 13){
                $('#search-form').submit();
            }
        });
        $('#search-form select').on('change', function () {
            $('#search-form').submit();
        });
        $('#export-results').on('click', function(){
            $.ajax({
                type: "POST",
                url: '/admin/panel/animators/export',
                data: $('#search-form').serialize(),
                assync:false,
                cache:false,
                success: function( data ) {
                    location.reload();
                }
            });
        });
    </script>
@endsection
