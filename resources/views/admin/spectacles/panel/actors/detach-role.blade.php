<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Odłączanie roli dla <i>{{ $actor->name }} {{ $actor->surname }}</i> </h5>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <form action="{{ url('admin/spectacle/'.$project->slug.'/panel/actors/detach-role', [$actor->id, $role->id]) }}" method="post" id="dialog-form">
        {!! csrf_field() !!}
        Potwierdź odłączenie roli <strong><i>{{ $role->name }}</i></strong>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa dodawanie">Potwierdź</button>
</div>
