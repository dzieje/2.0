<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Dodanie roli dla <i>{{ $actor->name }} {{ $actor->surname }}</i> </h5>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <h4>Scena {{ $scene->name }}</h4>
    <form action="{{ url('admin/spectacle/'.$project->slug.'/panel/actors/update-roles', [$actor->id]) }}" method="post" id="dialog-form">
        {!! csrf_field() !!}
        <div class="row">
        @foreach($scene->roles as $role)
            <div class="col-sm-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="{{ $role->id }}" name="roles[]"> {{ $role->name }}
                    </label>
                </div>
            </div>
        @endforeach
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa dodawanie">Zapisz</button>
</div>
