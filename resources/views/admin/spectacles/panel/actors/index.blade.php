@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                        Panel aktorów
                        </h6>
                        <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSearchForm" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="btn btn-sm btn-outline-info">
                                <i class="fas fa-search"></i>
                                @if(count( array_filter(Request::all() ) ) > 0)
                                    <span class="badge badge-info">
                                        {{ count( array_filter(Request::all() ) ) }}
                                    </span>
                                @endif
                            </span>
                        </button>
                    </div>
                    <div class="card-body">
                        <nav class="navbar navbar-light bg-light">
                            <div class="collapse navbar-collapse" id="navbarSearchForm">
                                <form class="form-inline" role="search" role="search" id="search-form" action="{{ url('admin/spectacle/'.$project->slug.'/panel/actors') }}" method="get">
                                    <div class="form-group form-group-sm flex-column text-center mr-1 ml-1 mb-2">
                                        <label>Filtrowanie aktorów <span class="badge badge-dark ml-2">{{ $actors->total() }}</span></label>
                                        <div>
                                            <a class="btn btn-xs btn-danger" href="{{ url('admin/spectacle/'.$project->slug.'/panel/actors') }}">
                                                <i class="fa fa-trash-alt fa-fw"></i> usuń filtry
                                            </a>
                                            <span class="btn btn-xs btn-primary export" target="{{  url('admin/spectacle/'.$project->slug.'/panel/actors/export') }}">
                                                <i class="fa fa-file-excel fa-fw"></i> exportuj xls
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <div class="divider">|</div>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">imię:
                                        <input name="name" value="{{ Request::get('name') ? Request::get('name') : '' }}" type="text" class="form-control form-control-sm" placeholder="imię">
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">nazwisko:
                                        <input name="surname" value="{{ Request::get('surname') ? Request::get('surname') : '' }}" type="text" class="form-control form-control-sm" placeholder="nazwisko">
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <div class="divider">|</div>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">płeć:
                                        <select name="gender" class="form-control form-control-sm">
                                            <option value="0">--- wybierz ---</option>
                                            <option value="female" {{ (Request::get('gender') == 'female') ? 'selected' : '' }}>kobieta</option>
                                            <option value="male" {{ (Request::get('gender') == 'male') ? 'selected' : '' }}>mężczyzna</option>
                                        </select>
                                        </label>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <div class="divider">|</div>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">wiek od:
                                        <input type="text" class="form-control form-control-sm number" name="age_from" value="{{ Request::get('age_from') }}"/>
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">wiek do:
                                        <input type="text" class="form-control form-control-sm number" name="age_to" value="{{ Request::get('age_to') }}"/>
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm ">
                                        <div class="divider">|</div>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">grupa:
                                        <select name="group" class="form-control form-control-sm">
                                            <option value="0">--- wybierz ---</option>
                                            <option value="-1" @if((Request::get('group') == '-1')) selected @endif>--- bez przypisanej grupy ---</option>
                                            @foreach($groups as $group_id => $group)
                                                <option value="{{ $group_id }}" {{ (Request::get('group') == $group_id) ? 'selected' : '' }}>{{ $group }}</option>
                                            @endforeach
                                        </select>
                                        </label>
                                    </div>
                                </form>
                            </div>
                        </nav>
                        <div class="table-responsive">
                            <table class="table table-sm table-hover table-bordered">
                                <thead>
                                <th>#</th>
                                <th>aktor</th>
                                <th>grupy</th>
                                <th>wiek</th>
                                @foreach($scenes as $scene)
                                    <th>{{ $scene->name }}</th>
                                @endforeach
                                </thead>
                                @foreach($actors as $actor)
                                    <tr>
                                        <td>{{ ($actors->currentPage() - 1) * $actors->perPage() + $loop->iteration  }}.</td>
                                        <td>{{ $actor->name }} {{ $actor->surname }}</td>
                                        <td>{{ ($actor->groups->count() > 0) ? implode(', ', $actor->groups->pluck('name')->toArray() ): '' }}</td>
                                        <td>{{ $actor->date_of_birth->diffInYears(Carbon\Carbon::now()) }}</td>
                                        @foreach($scenes as $scene)
                                            <td class="text-center">
                                                <?php $has_role = false; ?>
                                                @foreach($scene->roles as $role)
                                                    @if($actor->roles->contains($role->id))
                                                        <span class="btn btn-xs btn-info modal-open" data-toggle="modal" data-target="#modal" target="{{ url('admin/spectacle/'.$project->slug.'/panel/actors/detach-role', [$actor->id, $role->id]) }}" off-disable data-toggle="tooltip" data-placement="top" title="{{ $role->name }}" style="overflow:hidden; white-space:nowrap; text-overflow:ellipsis;  width:100px; padding: 0px; ">
                                                            <small>
                                                            {{ $role->wing->name }} - {{ $role->name }}
                                                            </small>
                                                        </span>
                                                        <?php $has_role = true;?>
                                                    @endif
                                                @endforeach

                                                @if(! $has_role)
                                                    <span class="btn btn-xs btn-warning modal-open-lg" data-toggle="modal" data-target="#modal-lg" target="{{ url('admin/spectacle/'.$project->slug.'/panel/actors/assign-role', [$actor->id, $scene->id]) }}" >
                                                        <i class="fa fa-question"></i>
                                                    </span>
                                                @endif
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>

                    <div class="card-footer text-right">
                        {!! $actors->appends(Request::query())->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })

        $('#search-form input').on('keyup keypress', function (e) {
            if(e.which === 13){
                $('#search-form').submit();
            }
        });
        $('#search-form select').on('change', function () {
            $('#search-form').submit();
        });
        $('.export-results').on('click', function(){
            var url = $(this).attr('target');

            $.ajax({
                type: "POST",
                url: url,
                data: $('#search-form').serialize(),
                assync:false,
                cache:false,
                success: function( data ) {
                    location.reload();
                }
            });
        });

        $('.export').on('click', function(){
            var url = $(this).attr('target') + '?' + $('#search-form').serialize();

            self.location = url;
        });
    </script>
@endsection
