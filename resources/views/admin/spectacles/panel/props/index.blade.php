@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            Panel rekwizytów
                        </h6>
                        <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSearchForm" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="btn btn-sm btn-outline-info">
                                <i class="fas fa-search"></i>
                                @if(count( array_filter(Request::all() ) ) > 0)
                                    <span class="badge badge-info">
                                        {{ count( array_filter(Request::all() ) ) }}
                                    </span>
                                @endif
                            </span>
                        </button>
                    </div>
                    <div class="card-body">
                        <nav class="navbar navbar-light bg-light">
                            <div class="collapse navbar-collapse" id="navbarSearchForm">
                                <form class="form-inline" role="search" id="search-form"
                                      action="{{ url('admin/spectacle/'.$project->slug.'/panel/props') }}" method="get">
                                    <div class="form-group form-group-sm flex-column text-center mr-1 ml-1 mb-2">
                                        <label>Filtrowanie <span class="badge badge-dark ml-1">{{ $props->total() }}</span></label>

                                        <div>
                                            <a class="btn btn-xs btn-danger" href="{{ url('admin/spectacle/'.$project->slug.'/panel/props') }}">
                                                <i class="fa fa-trash-alt fa-fw"></i> usuń filtry
                                            </a>
                                            <span class="btn btn-primary btn-xs" target="{{ url('admin/spectacle/'.$project->slug.'/panel/props/export') }}"
                                                  id="export-results">
                                                <i class="fa fa-file-excel fa-fw" aria-hidden="true"></i> eksportuj wyniki
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <div class="divider">|</div>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">rola:
                                        <select name="role" class="form-control">
                                            <option value="0">--- wybierz ---</option>
                                            @foreach($roles  as $role)
                                                <option value="{{ $role->id }}" {{ (Request::get('role') == $role->id) ? 'selected' : '' }}>
                                                    {{ $role->name }}
                                                    @if($role->scene)
                                                        ({{ $role->scene->name }})
                                                    @endif
                                                </option>
                                            @endforeach
                                        </select>
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">scena:
                                        <select name="scene" class="form-control">
                                            <option value="0">--- wybierz ---</option>
                                            @foreach($scenes  as $scene)
                                                <option value="{{ $scene->id }}" {{ (Request::get('scene') == $scene->id) ? 'selected' : '' }}>
                                                    {{ $scene->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">Kulisa:
                                        <select name="wing" class="form-control">
                                            <option value="0">--- wybierz ---</option>
                                            @foreach($wings  as $wing)
                                                <option value="{{ $wing->id }}" {{ (Request::get('wing') == $wing->id) ? 'selected' : '' }}>
                                                    {{ $wing->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">przygotowuje:
                                        <select name="type" class="form-control">
                                            <option value="2">--- wybierz ---</option>
                                            <option value="0" {{ (Request::get('type', 2) == 0) ? 'selected' : '' }}>
                                                Dzieje
                                            </option>
                                            <option value="1" {{ (Request::get('type', 2) == 1) ? 'selected' : '' }}>
                                                Statysta
                                            </option>
                                        </select>
                                        </label>
                                    </div>
                                </form>
                            </div>
                        </nav>
                        <div class="table-responsive">
                            <table class="table table-sm table-hover table-bordered">
                                <thead>
                                <th>#</th>
                                <th>ilość</th>
                                <th>rekwizyt</th>
                                <th>rola</th>
                                <th>scena</th>
                                <th>kulisa</th>
                                <th>przygotowuje</th>
                                </thead>
                                <?php $lp = (($props->currentPage() - 1) * $props->perPage()) + 1;?>
                                @foreach($props as $prop)
                                    <tr>
                                        <td>{{ $lp++ }}.</td>
                                        <td class="text-center">
                                            <strong>
                                                {{ ($prop->role) ? $prop->role->actors->count() : 0 }}
                                            </strong>
                                        </td>
                                        <td style="white-space: nowrap;">{{ $prop->name }}</td>
                                        <td style="white-space: nowrap;">{{ ($prop->role) ? $prop->role->name : ''}}</td>
                                        <td style="white-space: nowrap;">{{ ($prop->role) ? $prop->role->scene->name : ''}}</td>
                                        <td  style="white-space: nowrap;">
                                            {{ $prop->role ? ($prop->role->wing ? $prop->role->wing->name : '') : '' }}
                                        </td>
                                        <td>{{ ($prop->type == 0) ? 'dzieje' : 'statysta' }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>

                    <div class="panel-footer text-right">
                        {!! $props->appends(Request::query())->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })

        $('#search-form input').on('keyup keypress', function (e) {
            if (e.which === 13) {
                $('#search-form').submit();
            }
        });
        $('#search-form select').on('change', function () {
            $('#search-form').submit();
        });
        $('#export-results').on('click', function () {
            var url = $(this).attr('target');

            $.ajax({
                type: "POST",
                url: url,
                data: $('#search-form').serialize(),
                assync: false,
                cache: false,
                success: function (data) {
                    location.reload();
                }
            });
        });

        $('.export').on('click', function () {
            var url = $(this).attr('target') + '?' + $('#search-form').serialize();

            self.location = url;
        });
    </script>
@endsection
