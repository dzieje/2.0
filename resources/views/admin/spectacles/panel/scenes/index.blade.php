@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                        Panel scen
                        </h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-hover">
                                <thead>
                                <th>#</th>
                                <th></th>
                                <th>Nazwa sceny</th>
                                <th></th>
                                <Th></Th>
                                <Th></Th>
                                </thead>
                                @foreach($scenes as $scene)
                                    <tr>
                                        <td>{{ ($scenes->currentPage() - 1) * $scenes->perPage() + $loop->iteration  }}.</td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary btn-xs dropdown-toggle off-disable" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-file fa-fw" aria-hidden="true"></i> generuj <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-left">
                                                    <li>
                                                        <a href="{{ url('admin/spectacle/'.$project->slug.'/panel/scenes/generate-outline', [$scene->id, 'pdf']) }}" >
                                                            <i class="fa fa-file-pdf fa-fw" aria-hidden="true"></i> konspekt pdf
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ url('admin/spectacle/'.$project->slug.'/panel/scenes/generate-outline', [$scene->id, 'xls']) }}" >
                                                            <i class="fa fa-file-excel fa-fw" aria-hidden="true"></i> konspekt xls
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>{{ $scene->name }}</td>
                                        <td>
                                            <span target="{{ url('admin/spectacle/'.$project->slug.'/panel/scenes/show-roles', [$scene->id]) }}" class="btn btn-xs btn-info modal-open-lg off-disable" data-toggle="modal" data-target="#modal-lg">
                                                <i class="fa fa-search fa-fw"></i> pokaż role <span class="badge badge-light">{{ $scene->roles->count() }} </span>
                                            </span>
                                        </td>
                                        <td>
                                            <span target="{{ url('admin/spectacle/'.$project->slug.'/panel/scenes/show-wings', [$scene->id]) }}" class="btn btn-xs btn-info modal-open-lg off-disable" data-toggle="modal" data-target="#modal-lg">
                                                <i class="fa fa-search fa-fw"></i> pokaż kulisy <span class="badge badge-light">{{ $scene->roles->pluck('wing.id')->unique()->count() }} </span>
                                            </span>
                                        </td>
                                        <td>

                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>

                    <div class="card-footer text-right">
                        {!! $scenes->appends(Request::query())->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>

    </script>
@endsection
