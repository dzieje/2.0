<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Kulisy w <i>{{ $scene->name }}</i> </h5>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="panel">
        <table class="table table-hover table-condensed table-bordered">
                @foreach($wings as $lp => $wing)
                    <tr>
                        <td width="10px">{{ ++$lp }}.</td>
                        <td><strong>{{ $wing->name }}</strong></td>
                    </tr>
                @endforeach
        </table>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
</div>
