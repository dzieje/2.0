<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Role przypisane do <i>{{ $scene->name }}</i> </h5>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="panel">
        <table class="table table-hover table-condensed table-bordered">
            @foreach($scene->roles as $lp => $role)
                <tr>
                    <td width="10px">{{ ++$lp }}.</td>
                    <td><strong>{{ $role->name }}</strong></td>
                    <td>{{ $role->wing ? $role->wing->name : '---' }}</td>
                    <Td>
                        <span target="{{ url('admin/spectacle/'.$project->slug.'/panel/scenes/show-actors', [$role->id]) }}" class="btn btn-xs btn-info show-actors" >
                            <i class="fa fa-search fa-fw"></i>  aktorzy <span class="badge badge-light">{{ $role->actors->count() }}</span>
                        </span>
                    </Td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
</div>

<script>
    $('.show-actors').on('click', function(){
        $.ajax({
            type: "GET",
            url: $(this).attr('target'),
            assync:false,
            cache:false,
            dataType: 'html',
            beforeSend: function(){
                $('#modal .modal-content').html('<div class="text-center col-sm-12 col-md-6 col-lg-4" id="scene-loader" style="display: none;"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>');
            },
            success: function( data ) {
                $('#modal .modal-content').html(data);
            }
        });
    })

</script>
