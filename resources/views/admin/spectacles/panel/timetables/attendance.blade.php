@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <<div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            Zarządzanie listą obecności próby <strong>{{ $rehearsal->name }}</strong>
                        </h6>
                        <div>
                            <a href="{{ url('admin/spectacle/'.$project->slug.'/panel/timetables') }}" class="btn btn-sm btn-default "><< powrót</a>
                            <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSearchForm" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="btn btn-sm btn-outline-info">
                                    <i class="fas fa-search"></i>
                                    @if(count( array_filter(Request::all() ) ) > 0)
                                        <span class="badge badge-info">
                                            {{ count( array_filter(Request::all() ) ) }}
                                        </span>
                                    @endif
                                </span>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <nav class="navbar navbar-light bg-light">
                            <div class="collapse navbar-collapse" id="navbarSearchForm">
                                <form class="form-inline" role="search" id="search-form" action="{{ url('admin/spectacle/'.$project->slug.'/panel/timetables/attendance', [$rehearsal->id]) }}" method="get">
                                    {{ Form::hidden('rehearsal_id', $rehearsal->id) }}
                                    <div class="form-group form-group-sm flex-column text-center mr-1 ml-1 mb-2">
                                        <label>Filtrowanie aktorów <span class="badge badge-light ml-1">{{ $actors->total() }}</span></label><br>
                                        <div>
                                            <a class="btn btn-xs btn-danger" href="{{ url('admin/spectacle/'.$project->slug.'/panel/timetables/attendance', [$rehearsal->id]) }}">
                                                <i class="fa fa-trash-alt fa-fw"></i> usuń filtry
                                            </a>
                                            <span class="btn btn-primary btn-xs" id="export-results">
                                                <i class="fa fa-file-excel fa-fw" aria-hidden="true"></i> eksportuj wyniki
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">imię:
                                        <input name="name" value="{{ Request::get('name') ? Request::get('name') : '' }}" type="text" class="form-control" placeholder="imię">
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">nazwisko:
                                        <input name="surname" value="{{ Request::get('surname') ? Request::get('surname') : '' }}" type="text" class="form-control" placeholder="nazwisko">
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">telefon:
                                        <input name="phone" value="{{ Request::get('phone') ? Request::get('phone') : '' }}" type="text" class="form-control" placeholder="telefon">
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">email:
                                        <input name="email" value="{{ Request::get('email') ? Request::get('email') : '' }}" type="text" class="form-control" placeholder="email">
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">kulisa:
                                        <select name="wing" class="form-control">
                                            <option value="0">--- wybierz ---</option>
                                            @foreach($wings as $wing_id => $wing)
                                                <option value="{{ $wing_id }}" {{ (Request::get('wing') == $wing_id) ? 'selected' : '' }}>{{ $wing }}</option>
                                            @endforeach
                                        </select>
                                        </label>
                                    </div>
                                </form>
                            </div>
                        </nav>
                        <div class="table-responsive">
                            <table class="table table-sm table-hover table-bordered table-attendance">
                                <thead>
                                <th>#</th>
                                <th>Nazwisko</th>
                                <th>Imię</th>
                                <th>Telefon</th>
                                <Th>Email</Th>
                                <th>Grupa</th>
                                @foreach ($rehearsal->periods as $period)
                                    <th >{{ $period->date_from->format('H:i').'-'.$period->date_to->format('H:i') }}</th>
                                @endforeach
                                </thead>
                                @foreach($actors as $actor)
                                    <tr>
                                        <td>{{ ($actors->currentPage() - 1) * $actors->perPage() + $loop->iteration  }}.</td>
                                        <td>{{ $actor->surname }}</td>
                                        <td>{{ $actor->name }}</td>
                                        <td>{{ $actor->phone }}</td>
                                        <td>{{ $actor->email }}</td>
                                        <td>
                                            @if($actor->groups->count() > 0)
                                                {{ implode(', ', $actor->groups->pluck('name')->toArray()) }}
                                            @else
                                                ---
                                            @endif
                                        </td>
                                        @foreach ($rehearsal->periods as $rehearsal_period)
                                            <?php $if_period = false; ?>
                                            @foreach ($actor->roles as $role)
                                                @foreach ($role->periods as $period)
                                                    @if($period->id == $rehearsal_period->id)
                                                        <?php $if_period = true;?>
                                                    @endif
                                                @endforeach
                                            @endforeach

                                                @if($if_period )
                                                    <td class="active">
                                                        <div class="btn-group" data-toggle="buttons">
                                                            <label class="btn btn-sm btn-success off-disable @if(isset($attendances[$actor->id]) && isset($attendances[$actor->id][$rehearsal_period->id])  && $attendances[$actor->id][$rehearsal_period->id] == 1) active @endif">
                                                                <input class="attendance-radio" value="1" type="radio" name="options{{ $actor->id }}_{{ $rehearsal_period->id }}" autocomplete="off"
                                                                       @if(isset($attendances[$actor->id]) && isset($attendances[$actor->id][$rehearsal_period->id])  && $attendances[$actor->id][$rehearsal_period->id] == 1) checked @endif
                                                                       data-rehearsal="{{ $rehearsal->id }}" data-actor="{{ $actor->id }}" data-period="{{ $rehearsal_period->id }}"
                                                                > tak
                                                            </label>
                                                            <label class="btn btn-sm btn-danger off-disable @if(isset($attendances[$actor->id]) && isset($attendances[$actor->id][$rehearsal_period->id]) && $attendances[$actor->id][$rehearsal_period->id] == 0) active @endif" >
                                                                <input class="attendance-radio" value="0" type="radio" name="options{{ $actor->id }}_{{ $rehearsal_period->id }}"  autocomplete="off"
                                                                       @if(isset($attendances[$actor->id]) && isset($attendances[$actor->id][$rehearsal_period->id]) && $attendances[$actor->id][$rehearsal_period->id] == 0) checked @endif
                                                                       data-rehearsal="{{ $rehearsal->id }}" data-actor="{{ $actor->id }}" data-period="{{ $rehearsal_period->id }}"
                                                                > nie
                                                            </label>

                                                        </div>
                                                    </td>
                                                @else
                                                    <td>
                                                    </td>
                                                @endif
                                        @endforeach

                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>

                    <div class="card-footer text-right">
                        {!! $actors->appends(Request::query())->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $('.attendance-radio').on('change', function(){
            var actor_id = $(this).data('actor');
            var period_id = $(this).data('period');
            var rehearsal_id = $(this).data('rehearsal');
            var status = $(this).val();

            $.ajax({
                data: { actor_id: actor_id, period_id: period_id, rehearsal_id: rehearsal_id, status: status },
                type: 'POST',
                assync: false,
                cache: false,
                dataType: 'json',
                url: '/admin/spectacle/{{$project->slug}}/panel/timetables/set-attendance',
                success: function (data) {
                    $.notify({
                        message: 'Zmiany zapisano pomyślnie.'
                    },{
                        type: 'info',
                        delay: 1000,
                        placement: {
                            from: 'bottom',
                            align: 'right'
                        }
                    });
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    $.notify({
                        message: 'Wystąpił błąd w trakcie aktualizacji.'
                    },{
                        type: 'danger',
                        delay: 1000,
                        placement: {
                            from: 'bottom',
                            align: 'right'
                        }
                    });
                }
            });
        });


        $('#search-form input').on('keyup keypress', function (e) {
            if(e.which === 13){
                $('#search-form').submit();
            }
        });
        $('#search-form select').on('change', function () {
            $('#search-form').submit();
        });
        $('#export-results').on('click', function(){
            $.ajax({
                type: "POST",
                url: '/admin/panel/timetables/export-attendance',
                data: $('#search-form').serialize(),
                assync:false,
                cache:false,
                success: function( data ) {
                    location.reload();
                }
            });
        });
    </script>
@endsection
