@foreach($scene->roles as $role)
    <div class="col-sm-6 col-md-4 col-lg-3">
        <div class="checkbox">
            <label>
                <input type="checkbox" class="scene-role" name="roles[{{ $nb }}][]" data-scene="{{ $nb }}" checked value="{{ $role->id }}">
                {{ $role->name }} @if($role->wing)(<strong>{{ $role->wing->name }}</strong>)@endif
            </label>
        </div>
    </div>
@endforeach