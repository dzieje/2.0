@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <a href="{{ url('admin/spectacle/'.$project->slug.'/panel/timetables') }}" class="btn btn-sm btn-default float-right">
                    <i class="fa fa-reply fa-fw" aria-hidden="true"></i> wróć
                </a>
                <div class="page-header">
                    <h2>Próba dnia {{ $rehearsal->rehearsal->format('Y-m-d') }} <small>{{ $rehearsal->name }}</small></h2>
                </div>
            </div>
            @foreach($rehearsal->periods as $period)
                <div class="col-sm-12">
                    <div class="card shadow">
                        <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                            <h6 class="m-0 font-weight-bold text-info">
                            {{ $period->date_from->format('H:i') }} - {{ $period->date_to->format('H:i') }}
                            </h6>
                        </div>
                        <div class="card-body">
                            @foreach($period->scenes as $scene)
                                <h3 class="page-header marg-top-min"><small>scena:</small> {{ $scene->name }}</h3>
                                <div class="row">
                                    @foreach($period->roles->where('n_scene_id', $scene->id)->all() as $role)
                                        <div class="col-sm-6 col-md-4 col-lg-3">
                                            <i class="fa fa-fw fa-minus"></i> {{ $role->name }} @if($role->wing)(<strong>{{ $role->wing->name }}</strong>)@endif
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
    </script>
@endsection
