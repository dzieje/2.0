@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            Zarządzanie wolontariuszami
                        </h6>
                        <div>
                            <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSearchForm" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="btn btn-sm btn-outline-info">
                                    <i class="fas fa-search"></i>
                                    @if(count( array_filter(Request::all() ) ) > 0)
                                        <span class="badge badge-info">
                                            {{ count( array_filter(Request::all() ) ) }}
                                        </span>
                                    @endif
                                </span>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <nav class="navbar navbar-light bg-light">
                            <div class="collapse navbar-collapse" id="navbarSearchForm">
                                <form class="form-inline" role="search" id="search-form"  action="{{ url('admin/project/'.$project->slug.'/participants') }}" method="get">
                                    <div class="form-group form-group-sm flex-column text-center mr-1 ml-1 mb-2">
                                        <label>Filtrowanie uczestników <span class="badge badge-dark ml-1">{{ $volunteers->total() }}</span></label>
                                        <div>
                                            <a class="btn btn-xs btn-danger" href="{{ url('admin/project/'.$project->slug.'/participants') }}">
                                                <i class="fa fa-trash-alt fa-fw"></i> usuń filtry
                                            </a>
                                            <span class="btn btn-primary btn-xs export-results" target="{{ url('admin/project/'.$project->slug.'/participants/export') }}">
                                                <i class="fa fa-file-excel fa-fw" aria-hidden="true"></i> eksportuj wyniki
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <div class="divider">|</div>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">imię:
                                            <input name="name" value="{{ Request::get('name') ? Request::get('name') : '' }}" type="text" class="form-control form-control-sm" placeholder="imię">
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">nazwisko:
                                            <input name="surname" value="{{ Request::get('surname') ? Request::get('surname') : '' }}" type="text" class="form-control form-control-sm" placeholder="nazwisko">
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">płeć:
                                            <select name="gender" class="form-control form-control-sm">
                                                <option value="0">--- wybierz ---</option>
                                                <option value="female" {{ (Request::get('gender') == 'female') ? 'selected' : '' }}>kobieta</option>
                                                <option value="male" {{ (Request::get('gender') == 'male') ? 'selected' : '' }}>mężczyzna</option>
                                            </select>
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <div class="divider">|</div>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">wiek od:
                                            <input type="text" class="form-control form-control-sm number" name="age_from" value="{{ Request::get('age_from') }}"/>
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">wiek do:
                                            <input type="text" class="form-control form-control-sm number" name="age_to" value="{{ Request::get('age_to') }}"/>
                                        </label>
                                    </div>
                                </form>
                            </div>
                        </nav>
                        <div class="table-responsive mt-2">
                            <table class="table table-sm table-hover">
                                <thead>
                                <th>#</th>
                                <th></th>
                                <th>Nazwisko</th>
                                <th>Imię</th>
                                <th>Telefon</th>
                                <Th>Email</Th>
                                @foreach($attributes as $attribute)
                                    <th>{{ $attribute->name }}</th>
                                @endforeach
                                <th></th>
                                <th></th>
                                <th></th>
                                </thead>
                                @foreach($volunteers as $volunteer)
                                    <tr>
                                        <td>{{ ($volunteers->currentPage() - 1) * $volunteers->perPage() + $loop->iteration  }}.</td>
                                        <td>
                                            @if($volunteer->date_of_birth && $volunteer->projectVolunteers->where('project_id', $project->id)->first() && $project->settingContractTemplate())
                                                <a target="_blank" href="{{ url('admin/project/'.$project->slug.'/participants/generate-contract', [$volunteer->id]) }}" class="btn btn-primary btn-xs off-disable">
                                                    <i class="fas fa-file-pdf fa-fw" aria-hidden="true"></i>
                                                    gen. umowę.
                                                </a>
                                            @endif
                                        </td>
                                        <td>{{ $volunteer->surname }}</td>
                                        <td>{{ $volunteer->name }}</td>
                                        <td>{{ $volunteer->phone }}</td>
                                        <td>{{ $volunteer->email }}</td>
                                        @foreach($attributes as $attribute)
                                            <td>
                                                @if($attribute->is_collection)
                                                    @foreach($volunteer->{$attribute->slug} as $value)
                                                        <span class="badge badge-info">{{ $value }}</span>
                                                    @endforeach
                                                @else
                                                    @if($attribute->type == 'checkbox')
                                                        @if($volunteer->{$attribute->slug} == 1)
                                                            tak
                                                        @else
                                                            nie
                                                        @endif
                                                    @else
                                                        {{ $volunteer->{$attribute->slug} }}
                                                    @endif
                                                @endif
                                            </td>
                                        @endforeach
                                        <td>
                                            <span target="{{ url('admin/project/'.$project->slug.'/participants/show', [$volunteer->id]) }}" class="btn btn-info btn-xs modal-open-lg off-disable" data-toggle="modal" data-target="#modal-lg" data-backdrop="true">
                                                <i class="fa fa-search fa-fw"></i> szczegóły
                                            </span>
                                        </td>
                                        <td>
                                            <a href="{{ url('admin/project/'.$project->slug.'/participants/edit', [$volunteer->id]) }}" class="btn btn-xs btn-warning">
                                                <i class="fa fa-pencil-alt fa-fw"></i> edytuj
                                            </a>
                                        </td>
                                        <td>
                                            <span target="{{ url('admin/project/'.$project->slug.'/participants/delete', [$volunteer->id]) }}" class="btn btn-xs btn-danger modal-open" data-toggle="modal" data-target="#modal">
                                                <i class="fa fa-trash-alt fa-fw"></i> odepnij
                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>

                    <div class="card-footer ">
                        {!! $volunteers->appends(Request::query())->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $('#search-form input').on('keyup keypress', function (e) {
            if(e.which === 13){
                $('#search-form').submit();
            }
        });
        $('#search-form select').on('change', function () {
            $('#search-form').submit();
        });
        $('#search-form input[type="checkbox"]').on('change', function () {
            $('#search-form').submit();
        });
        $('.export-results').on('click', function(){
            var url = $(this).attr('target');

            $.ajax({
                type: "POST",
                url: url,
                data: $('#search-form').serialize(),
                assync:false,
                cache:false,
                success: function( data ) {
                    location.reload();
                }
            });
        });

        $('.export').on('click', function(){
            var url = $(this).attr('target') + '?' + $('#search-form').serialize();

            self.location = url;
        });
    </script>
@endsection
