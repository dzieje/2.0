@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-lg-8">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            Inspicjenci
                        </h6>
                        <label class="btn btn-primary btn-sm off-disable">
                            Wgraj skrypt <input id="script_file" name="script_file" type="file" hidden style=" display: none !important;">
                        </label>
                    </div>
                    <table class="table table-sm table-hover">
                        <thead>
                        <th>#</th>
                        <th>plik</th>
                        <th>data wgrania</th>
                        <th></th>
                        </thead>
                        @foreach($scripts as $script)
                            <tr>
                                <td>{{ ($scripts->currentPage() - 1) * $scripts->perPage() + $loop->iteration  }}.</td>
                                <td>{{ $script->original_filename }}</td>
                                <td>{{ $script->created_at->format('Y-m-d H:i') }}</td>
                                <td>
                                    <a target="_blank" href="{{ url('admin/spectacle/'.$project->slug.'/stage-manager/run', [$script->id]) }}" class="btn btn-primary btn-xs off-disable">
                                        <i class="fa fa-fw fa-play"></i> uruchom
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </table>

                    <div class="card-footer text-right">
                        {!! $scripts->appends(Request::query())->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $(document).ready(function () {
            $("input[name='script_file']").on('change', function(){

                var fd = new FormData();
                var files = $('#script_file')[0].files[0];
                fd.append('file',files);

                $.ajax({
                    url: '{{ url('admin/spectacle/'.$project->slug.'/stage-manager/upload') }}',
                    type: 'post',
                    data: fd,
                    contentType: false,
                    processData: false,
                    success: function(response){
                        self.location.reload();
                    },
                });
            });
        })


        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endsection
