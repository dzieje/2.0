<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Inspicjent</title>
    <meta name="description" content="The HTML5 Herald">
    <meta name="author" content="SitePoint">

    <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
    <script
            src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>
</head>

<body>
<div class="container bg-black w-screen h-screen max-w-full grid grid-cols-2 text-white ">
    <div class="flex flex-col items-center justify-center border-b-2 border-r-2 border-purple-500 divide-y divide-gray-400 divide-opacity-25" style="height: 50vh;" data-wing="1"></div>
    <div class="flex flex-col items-center justify-center border-b-2 border-l-2 border-purple-500 divide-y divide-gray-400 divide-opacity-25" style="height: 50vh;" data-wing="2"></div>
    <div class="flex flex-col items-center justify-center border-t-2 border-r-2 border-purple-500 divide-y divide-gray-400 divide-opacity-25" style="height: 50vh;" data-wing="3"></div>
    <div class="flex flex-col items-center justify-center border-t-2 border-l-2 border-purple-500 divide-y divide-gray-400 divide-opacity-25" style="height: 50vh;" data-wing="4"></div>
</div>
</body>

<script>
    let start_time;

    function Generator() {};

    Generator.prototype.rand =  Math.floor(Math.random() * 26) + Date.now();

    Generator.prototype.getId = function() {
        return this.rand++;
    };

    let idGen = new Generator();


    function CountDownTimer(duration, role, object, wing_id, granularity) {
        this.duration = duration;
        this.granularity = granularity || 100;
        this.tickFtns = [];
        this.running = false;
        this.object = object;
        this.role = role;
        this.wing_id = wing_id;
    }

    CountDownTimer.prototype.start = function() {
        if (this.running) {
            return;
        }
        this.running = true;
        var start = Date.now(),
            that = this,
            diff, obj;

        this.object.find('.role').text(this.role);

        (function timer() {
            diff = that.duration - (((Date.now() - start) / 1000) | 0);

            if (diff > 0) {
                setTimeout(timer, that.granularity);
            } else {
                diff = 0;
                that.running = false;
            }

            obj = CountDownTimer.parse(diff);
            that.tickFtns.forEach(function(ftn) {
                ftn.call(this, obj.minutes, obj.seconds, diff);
            }, that);
        }());
    };

    CountDownTimer.prototype.onTick = function(ftn) {
        if (typeof ftn === 'function') {
            this.tickFtns.push(ftn);
        }
        return this;
    };

    CountDownTimer.prototype.expired = function() {
        return !this.running;
    };

    CountDownTimer.prototype.reClass = function() {
        let elements = $('[data-wing=' + this.wing_id + '] .countdown-container:visible').length;

        $('[data-wing=' + this.wing_id + '] .countdown-container:visible').each(function(index){
            let text_class = "";
            let bar_bg = "";
            let bar_height = "";
            let width = "";

            if(elements - index === 1){
                text_class = "text-4xl";
                bar_bg = "pink";
                bar_height = "h-4";
                width = "w-9/12";
            }else if(elements - index  === 2){
                text_class = "text-2xl";
                bar_bg = "green";
                bar_height = "h-3";
                width = "w-7/12";
            }else if(elements - index  === 3){
                text_class = "text-xl";
                bar_bg = "blue";
                bar_height = "h-2";
                width = "w-5/12";
            }else if(elements - index  === 4){
                text_class = "text-base";
                bar_bg = "gray";
                bar_height = "h-1";
                width = "w-3/12";
            }

            $(this).attr('class', width+" text-center pt-2 pb-2 countdown-container " );
            $(this).find('.progress-bar').attr('class', 'progress-bar overflow-hidden '+bar_height+' text-xs flex rounded bg-'+bar_bg+'-100' );
            $(this).find('.bar').attr('class', 'bar shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg-'+bar_bg+'-500' );
            $(this).find('.role').attr('class', 'role '+text_class );
            $(this).find('.countdown').attr('class', 'countdown '+text_class );
        });
    };

    CountDownTimer.parse = function(seconds) {
        return {
            'minutes': (seconds / 60) | 0,
            'seconds': (seconds % 60) | 0
        };
    };


    function format(minutes, seconds, diff) {
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        let display = this.object.find('.countdown');
        display.text( minutes + ':' + seconds );
    }

    function progressbar(minutes, seconds, diff) {
        let percentage = (diff / this.duration) * 100;
        let display = this.object.find('.progress-bar .bar');

        display.css('width', percentage+'%');
    }

    function sleep(delay) {
        var start = new Date().getTime();
        while (new Date().getTime() < start + delay);
    }

    function generateObject(wing_id){
        return '<div class="text-center mt-5 countdown-container " style="display: none;">\n' +
            '                <p class="role"> </p>\n' +
            '                <div class="relative pt-1" >\n' +
            '                    <div class="progress-bar overflow-hiddentext-xs flex rounded ">\n' +
            '                        <div style="width:100%" class="bar shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center "></div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '                <p class="countdown"> </p>\n' +
            '            </div>'
    }

    function addCountDownTimer(wing_id, role){
        let duration = moment.duration(role.time).asSeconds();

        let htmlObject = generateObject(wing_id);
        let object = $(htmlObject);
        object.prop('id', idGen.getId());
        $('[data-wing=' + wing_id + ']').first().prepend(object);

        let timer = new CountDownTimer(duration, role.name, object, wing_id);

        timer.onTick(format).onTick(progressbar).onTick(countDownFinished);

        return timer;
    }

    let wings = [];
    wings[0] = [];
    wings[1] = [];
    wings[2] = [];
    wings[3] = [];
    wings[4] = [];

    function countDownFinished() {
        if (this.expired()) {
            this.object.remove();

            let timer = wings[this.wing_id].shift();
            timer.object.show();
            timer.reClass();
        }
    }

    let running = false;

    $(document).ready(function(){
        $('.container').on('click', function(){
            if(! running) {
                running = true;
                $.ajax({
                    type: "GET",
                    url: '{{ url('admin/spectacle/'.$project->slug.'/stage-manager/load', [$script->id]) }}',
                    dataType: 'json',
                    assync: false,
                    cache: false,
                    success: function (data) {
                        $.each(data, function (i, wing) {
                            $.each(wing.roles, function (role_id, role) {
                                wings[role.n_wing_id].push(addCountDownTimer(role.n_wing_id, {
                                    name: role.name,
                                    time: role.time_start
                                }));
                            });
                        });

                        start_time = new Date().getTime();

                        $.each(wings, function (i, wing) {
                            $.each(wing, function (timer_id, timer) {
                                timer.start();
                            });

                            if (i > 0) {
                                for (k = 0; k < 3; k++) {
                                    let timer = wing.shift();

                                    timer.object.show();

                                    if (k === 2) {
                                        timer.reClass();
                                    }
                                }
                            }
                        });


                    }
                });
            }
        });
    });
</script>
</html>
