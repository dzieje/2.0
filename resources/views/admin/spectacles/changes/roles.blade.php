@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-12 col-lg-10 col-lg-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Lista ról na widowisku {{ \Jenssegers\Date\Date::createFromFormat('Y-m-d',  $show->show->format('Y-m-d'))->format('j F Y')  }}

                    <a href="{{ url('admin/n-manage/changes') }}" class="btn btn-default btn-xs pull-right marg-right">
                        <i class="fa fa-rotate-left fa-fw"></i> powrót
                    </a>
                </div>
                <div class="panel-body">
                    <nav class="navbar navbar-default navbar-sm marg-top-min" style="margin-bottom: 0px;">
                        <div class="container-fluid">
                            <form class="navbar-form navbar-left" role="search" id="search-form" action="{{ url('admin/n-manage/changes/roles', [$show->id]) }}" method="get">
                                <div class="form-group form-group-sm text-center ">
                                    <label>Filtrowanie ról <span class="badge">{{ $roles->total() }}</span></label><br>
                                    <a class="btn btn-xs btn-danger" href="{{ url('admin/n-manage/changes/roles', [$show->id]) }}">
                                        <i class="fa fa-remove fa-fw"></i> usuń filtry
                                    </a>
                                </div>
                                <div class="form-group form-group-sm">
                                    <div class="divider">|</div>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label style="width: 100%" class="text-center">nazwa:</label><br>
                                    <input name="name" value="{{ Request::get('name') ? Request::get('name') : '' }}" type="text" class="form-control" placeholder="nazwa">
                                </div>
                                <div class="form-group form-group-sm">
                                    <label style="width: 100%" class="text-center">scena:</label><br>
                                    <select name="scene" class="form-control">
                                        <option value="0">--- wybierz ---</option>
                                        @foreach($scenes as $scene_id => $scene)
                                            <option value="{{ $scene_id }}" {{ (Request::get('scene') == $scene_id) ? 'selected' : '' }}>{{ $scene }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label style="width: 100%" class="text-center">kulisa:</label><br>
                                    <select name="wing" class="form-control">
                                        <option value="0">--- wybierz ---</option>
                                        @foreach($wings as $wing_id => $wing)
                                            <option value="{{ $wing_id }}" {{ (Request::get('wing') == $wing_id) ? 'selected' : '' }}>{{ $wing }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group form-group-sm">
                                    <div class="divider">|</div>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label class="marg-top">
                                        <input type="checkbox" name="if_single_actor" value="1" @if(Request::get('if_single_actor', 0)) checked @endif> rola indywidualna
                                    </label>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label class="marg-top">
                                        <input type="checkbox" name="if_multi_actor" value="1" @if(Request::get('if_multi_actor', 0)) checked @endif> rola grupowa
                                    </label>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label class="marg-top">
                                        <input type="checkbox" name="if_understudy" value="1" @if(Request::get('if_understudy', 0)) checked @endif> dubler
                                    </label>
                                </div>
                            </form>
                        </div>
                    </nav>
                    <div class="table-responsive marg-top">
                        <table class="table table-condensed table-hover">
                            <thead>
                            <th>#</th>
                            <th></th>
                            <th>Nazwa roli</th>
                            <th>Scena</th>
                            <th>Kulisa</th>
                            <th>Aktorzy</th>
                            </thead>
                            <?php $lp = (($roles->currentPage()-1)*$roles->perPage()) + 1;?>
                            @foreach($roles as $role)
                                <tr>
                                    <td>{{ $lp++ }}.</td>
                                    <td>
                                        @if($role->if_single_actor == 1)
                                            <label class="label label-warning">indywid.</label>
                                        @endif
                                        <br/>
                                        @if($role->if_understudy == 1)
                                            <label class="label label-info">dubler</label>
                                        @endif
                                    </td>
                                    <td>{{ $role->name }}</td>
                                    <td>{{ ($role->scene) ? $role->scene->name : '---' }}</td>
                                    <td>{{ ($role->wing) ? $role->wing->name : '---' }}</td>
                                    <td>
                                        @php
                                            if(isset($absences[$role->id])){
                                                $present = $role->actors->count()-$absences[$role->id];
                                            }else{
                                                $present = $role->actors->count();
                                            }
                                            if($role->actors->count() > 0){
                                                $percentage = ($present/$role->actors->count()) * 100;
                                            }else{
                                                $percentage = 0;
                                            }
                                        @endphp

                                        <a href="{{ url('admin/n-manage/changes/actors', [$show->id, $role->id]) }}" class="btn btn-xs
@if($percentage <= 70)
                                                btn-danger
@else
                                                btn-info
@endif

                                            ">
                                            <i class="fa fa-search fa-fw"></i>  aktorzy
                                            <span class="badge">
                                                {{ $present }}
                                                /
                                                {{ $role->actors->count() }}
                                            </span>

                                            - zastępstw
                                            <span class="badge">
                                                {{ $role->changes->count() }}
                                            </span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>

                <div class="panel-footer text-right">
                    {!! $roles->appends(Request::query())->links() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $('#search-form input').on('keyup keypress', function (e) {
            if(e.which === 13){
                $('#search-form').submit();
            }
        });
        $(':checkbox').change(function() {
            $('#search-form').submit();
        });
        $('#search-form select').on('change', function () {
            $('#search-form').submit();
        });
    </script>
@endsection