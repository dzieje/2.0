<tr data-actor="{{ $actor->id }}">
    <td>
        @foreach($roles as $role)
            <label class="label label-info">{{ $role->name }}</label>
        @endforeach
    </td>
    <td>{{ $actor->name }}</td>
    <td>{{ $actor->surname }}</td>
    <td>{{ $actor->phone }}</td>
    <td>{{ $actor->email }}</td>
    <td>
        @if($actor->groups->count() > 0)
            {{ implode(', ', $actor->groups->pluck('name')->toArray()) }}
        @endif
    </td>
    <td>
        {!! Form::hidden('actors[]',  $actor->id) !!}
        <span class="btn btn-xs btn-danger detach-actor off-disable" data-actor="{{ $actor->id }}">
            <i class="fa fa-trash-o fa-fw"></i>
            odepnij
        </span>
    </td>
</tr>