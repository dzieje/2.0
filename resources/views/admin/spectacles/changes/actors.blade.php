@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-12 col-lg-10 col-lg-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Lista aktorów do roli {{ $role->name }} na widowisku {{ \Jenssegers\Date\Date::createFromFormat('Y-m-d',  $show->show->format('Y-m-d'))->format('j F Y')  }}
                    <span target="{{ url('admin/n-manage/changes/add-replacement', [$show->id, $role->id]) }}" class="btn btn-xs btn-info modal-open-lg  pull-right" data-toggle="modal" data-target="#modal-lg">
                        <i class="fa fa-exchange fa-fw"></i> dodaj zastępstwo
                    </span>
                    <a href="{{ url('admin/n-manage/changes/roles', [$show->id]) }}" class="btn btn-default btn-xs pull-right marg-right">
                        <i class="fa fa-rotate-left fa-fw"></i> powrót
                    </a>
                </div>
                <div class="panel-body">
                    <div class="table-responsive marg-top">
                        <table class="table table-hover table-condensed table-bordered">
                            <thead>
                            <th>#</th>
                            <th>Grupy</th>
                            <th>Nazwisko</th>
                            <th>Imię</th>
                            <Th>Telefon</Th>
                            <th>Email</th>
                            <th></th>
                            </thead>
                            <?php $lp = 0;?>
                            @foreach($role->actors as $actor)
                                <tr
                                @if($actor->shows->contains($show->id))
                                class="bg-danger"
                                @endif
                                >
                                    <td width="10px">{{ ++$lp }}.</td>
                                    <td>
                                        @if($actor->groups->count() > 0)
                                            {{ implode(', ', $actor->groups->pluck('name')->toArray()) }}
                                        @endif
                                    </td>
                                    <td><strong>{{ $actor->surname }}</strong></td>
                                    <td><strong>{{ $actor->name }}</strong></td>
                                    <td>{{ $actor->phone }}</td>
                                    <td>{{ $actor->email }}</td>
                                    <td>

                                    </td>
                                </tr>
                            @endforeach
                            @foreach($show->changes as $change)
                                <tr class="bg-success">
                                    <td width="10px">{{ ++$lp }}.</td>
                                    <td>
                                        @if($change->actor->groups->count() > 0)
                                            {{ implode(', ', $change->actor->groups->pluck('name')->toArray()) }}
                                        @endif
                                    </td>
                                    <td><strong>{{ $change->actor->surname }}</strong></td>
                                    <td><strong>{{ $change->actor->name }}</strong></td>
                                    <td>{{ $change->actor->phone }}</td>
                                    <td>{{ $change->actor->email }}</td>
                                    <td>
                                        <span target="{{ url('admin/n-manage/changes/delete-replacement', [$change->id]) }}" class="btn btn-xs btn-info modal-open  pull-right" data-toggle="modal" data-target="#modal">
                                            <i class="fa fa-trash-o fa-fw"></i> odepnij zastępstwo
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>

    </script>
@endsection