<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Dodawanie zastępstwa do roli <i>{{ $role->name }}</i> </h4>
</div>
<div class="modal-body">
    <form action="{{ url('admin/n-manage/changes/store-replacement', [$show->id, $role->id]) }}" method="post" id="dialog-form">
        {!! Form::token() !!}
        <div class="form-group">
            <label>Wyszukaj aktora:</label>
            {!! Form::text('term', null, ['class' => 'add-actor form-control']) !!}
        </div>
        <div class="text-center" id="actor-loader" style="display: none;"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>
        <div id="actors-container">
            <table class="table">
                <thead>
                    <th>role grane w scenie</th>
                    <th>imię</th>
                    <th>nazwisko</th>
                    <th>telefon</th>
                    <th>email</th>
                    <th>grupa</th>
                    <th></th>
                </thead>
            </table>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" id="set" data-dismiss="modal">Zapisz</button>
</div>

<script>
    var actors = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/admin/n-manage/changes/search-actor',
            replace: function(url, query) { return url + "?limit=15&q=" + query; },
            filter: function (data) {
                return $.map(data.actors, function (actor) {
                    return actor
                });
            }
        }
    });

    $('.add-actor').typeahead({
            minLength: 2,
            highlight: true,
            hint: true
        },
        { name: 'name', displayKey: 'name', minLength: 2, source: actors, limit: 15 });

    $('.add-actor').on('typeahead:selected', function(ev, datum) {
        var volunteer_id = datum.id;

        $.ajax({
            type: "GET",
            url: '/admin/n-manage/changes/load-actor',
            data: { volunteer_id: volunteer_id, role_id: "{{ $role->id }}" },
            assync:false,
            cache:false,
            dataType: 'html',
            beforeSend: function(){
                $('#actor-loader').show();
            },
            success: function( data ) {
                $('#actor-loader').hide();
                $('#actors-container table').append( data );
            }
        });
    });

    $('.add-actor').on('typeahead:close', function(){
        $(this).val('');
    });

    $('#actors-container').on('click', '.detach-actor', function(){
        var actor_id = $(this).data('actor');
        $('tr[data-actor="'+actor_id+'"]').remove();
    });
</script>