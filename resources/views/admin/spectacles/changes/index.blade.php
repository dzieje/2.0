@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            Zarządzanie zmianami ról - wybierz Widowisko
                        </h6>
                    </div>
                    <div class="card-body">
                        <div class="list-group col-sm-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                            @foreach($shows as $show)
                                <a href="{{ url('admin/spectacle/'.$project->slug.'/changes/roles', [$show->id]) }}" class="list-group-item text-center">
                                    {{ \Jenssegers\Date\Date::createFromFormat('Y-m-d',  $show->show->format('Y-m-d'))->format('j F Y')  }}
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

