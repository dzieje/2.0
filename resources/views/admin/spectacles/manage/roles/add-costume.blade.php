<div class="col-md-6">
    <div class="card mb-2">
        <div class="card-header bg-transparent text-right">
            <span class="btn btn-xs btn-danger remove-costume">
                <i class="fa fa-trash-alt fa-fw"></i> usuń
            </span>
        </div>
        <div class="card-body">
            <div class="form-group form-group-sm">
                <label class="control-label">Nazwa kostiumu:</label>
                {!! Form::text('costumes[]', null, ['class' =>'form-control required']) !!}
            </div>
            <div class="form-group form-group-sm">
                <label class="control-label">Typ:</label><br/>
                {!! Form::select('costume_types[]', ['0' => 'Dzieje', '1' => 'Statysta'], 0 , ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
</div>
