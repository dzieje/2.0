@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                {!! Form::open(['url' => url('admin/spectacle/'.$project->slug.'/manage/roles/update', [$role->id]), 'id' => 'page-form']) !!}
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            Edycja roli
                        </h6>
                        <a href="{{ URL::previous() }}" class="btn btn-default btn-xs pull-right">
                            <i class="fa fa-remove fa-fw"></i> anuluj
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6 col-md-4">
                                <div class="form-group">
                                    <label>Nazwa roli:</label>
                                    {!!  Form::text('name', $role->name, array('class' => 'form-control required', 'placeholder' => 'nazwa')) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <div class="form-group">
                                    <label>Scena:</label>
                                    {!!  Form::select('n_scene_id', $scenes, $role->scene_id, array('class' => 'form-control required')) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <div class="form-group">
                                    <label>Kulisa:</label>
                                    {!!  Form::select('n_wing_id', $wings, $role->wing_id, array('class' => 'form-control required')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label>Min. aktorów:</label>
                                    {!!  Form::number('min_actors', $role->min_actors, array('class' => 'form-control  number', 'min' => 0,'placeholder' => 'min. aktorów')) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label>Max. aktorów:</label>
                                    {!!  Form::number('max_actors', $role->max_actors, array('class' => 'form-control  number', 'min' => 0,'placeholder' => 'max. aktorów')) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label>Godzina rozpoczęcia:</label>
                                    {!!  Form::time('time_start', $role->time_start, array('class' => 'form-control ', 'step' => 1)) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label>Godzina zakończenia:</label>
                                    {!!  Form::time('time_end', $role->time_end, array('class' => 'form-control ', 'step' => 1)) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="if_single_actor" value="1" @if($role->if_single_actor == 1) checked @endif > rola indywidualna
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="if_understudy" value="1" @if($role->if_understudy == 1) checked @endif> dubler
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h4>Kostiumy
                                            <span id="add-costume" class="btn btn-primary btn-xs pull-right off-disable">
                                                <i class="fa fa-plus fa-fw"></i> dodaj
                                            </span>
                                        </h4>
                                        <hr/>
                                        <div class="text-center" id="costume-loader" style="display: none;"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>
                                        <div id="costumes-container">
                                            @foreach($role->costumes as $costume)
                                                <div class="marg-top-min">
                                                    <div class="col-sm-12">
                                                        <span class="btn btn-xs btn-danger pull-right remove-costume">
                                                            <i class="fa fa-remove"></i> usuń
                                                        </span>
                                                    </div>
                                                    <div class="form-group form-group-sm col-md-6">
                                                        <label class="control-label">Nazwa kostiumu:</label>
                                                        {!! Form::text('costumes[]', $costume->name, ['class' =>'form-control required']) !!}
                                                    </div>
                                                    <div class="form-group form-group-sm col-md-6">
                                                        <label class="control-label">Typ:</label><br/>
                                                        {!! Form::select('costume_types[]', ['0' => 'Dzieje', '1' => 'Statysta'], ($costume->type == 'Dzieje') ? 0 : 1 , ['class' => 'form-control']) !!}
                                                    </div>
                                                    <hr/>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card ">
                                    <div class="card-body">
                                        <h4>Rekwizyty
                                            <span id="add-prop" class="btn btn-primary btn-xs pull-right off-disable">
                                                <i class="fa fa-plus fa-fw"></i> dodaj
                                            </span>
                                        </h4>
                                        <hr/>
                                        <div class="text-center" id="prop-loader" style="display: none;"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>
                                        <div id="props-container">
                                            @foreach($role->props as $prop)
                                                <div class="marg-top-min">
                                                    <div class="col-sm-12">
                                                        <span class="btn btn-xs btn-danger pull-right remove-prop">
                                                            <i class="fa fa-remove"></i> usuń
                                                        </span>
                                                    </div>
                                                    <div class="form-group form-group-sm col-md-6">
                                                        <label class="control-label">Nazwa rekwizytu:</label>
                                                        {!! Form::text('props[]', $prop->name, ['class' =>'form-control required']) !!}
                                                    </div>
                                                    <div class="form-group form-group-sm col-md-6">
                                                        <label class="control-label">Typ:</label><br/>
                                                        {!! Form::select('prop_types[]', ['0' => 'Dzieje', '1' => 'Statysta'], ($prop->type == 'Dzieje') ? 0 : 1 , ['class' => 'form-control']) !!}
                                                    </div>
                                                    <hr/>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        {!! Form::submit('Zapisz dane roli', ['class' => 'btn btn-primary off-disable']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $('#costumes-container').on('click', '.remove-costume', function(){
            $(this).parent().parent().remove();
        });

        $('#add-costume').on('click', function(){
            $.ajax({
                type: "GET",
                url: '/admin/spectacle/{{$project->slug}}/manage/roles/costume-add',
                assync:false,
                cache:false,
                dataType: 'html',
                beforeSend: function(){
                    $('#costume-loader').show();
                },
                success: function( data ) {
                    $('#costume-loader').hide();
                    $('#costumes-container').prepend( data );
                }
            });
        });

        $('#props-container').on('click', '.remove-prop', function(){
            $(this).parent().parent().remove();
        });

        $('#add-prop').on('click', function(){
            $.ajax({
                type: "GET",
                url: '/admin/spectacle/{{$project->slug}}/manage/roles/prop-add',
                assync:false,
                cache:false,
                dataType: 'html',
                beforeSend: function(){
                    $('#prop-loader').show();
                },
                success: function( data ) {
                    $('#prop-loader').hide();
                    $('#props-container').prepend( data );
                }
            });
        });
    </script>
@endsection
