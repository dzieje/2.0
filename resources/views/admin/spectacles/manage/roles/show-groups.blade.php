<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Grupy przypisane do <i>{{ $role->name }}</i> </h5>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="panel">
        <table class="table table-hover table-sm table-bordered">
            @foreach($role->groups as $lp => $group)
                <tr>
                    <td width="10px">{{ ++$lp }}.</td>
                    <td><strong>{{ $group->name }}</strong></td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
</div>
