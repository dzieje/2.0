@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12 ">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                        Zarządzanie rolami
                        </h6>

                        <a href="{{ url('admin/spectacle/'.$project->slug.'/manage/roles/create') }}" class="btn btn-xs btn-info">
                            <i class="fa fa-plus fa-fw"></i> dodaj rolę
                        </a>
                    </div>
                    <div class="card-body">
                        <nav class="navbar navbar-default navbar-sm marg-top-min" style="margin-bottom: 0px;">
                            <div class="container-fluid">
                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                                    <form class="navbar-form navbar-left" role="search" id="search-form" action="{{ url('admin/spectacle/'.$project->slug.'/manage/roles') }}" method="get">
                                        <div class="form-group form-group-sm text-center ">
                                            <label>Filtrowanie ról <span class="badge">{{ $roles->total() }}</span></label><br>
                                            <a class="btn btn-xs btn-danger" href="{{ url('admin/spectacle/'.$project->slug.'/manage/roles') }}">
                                                <i class="fa fa-remove fa-fw"></i> usuń filtry
                                            </a>
                                        </div>
                                        <div class="form-group form-group-sm">
                                            <div class="divider">|</div>
                                        </div>
                                        <div class="form-group form-group-sm">
                                            <label style="width: 100%" class="text-center">nazwa:</label><br>
                                            <input name="name" value="{{ Request::get('name') ? Request::get('name') : '' }}" type="text" class="form-control" placeholder="nazwa">
                                        </div>
                                        <div class="form-group form-group-sm">
                                            <label style="width: 100%" class="text-center">scena:</label><br>
                                            <select name="scene" class="form-control">
                                                <option value="0">--- wybierz ---</option>
                                                @foreach($scenes as $scene_id => $scene)
                                                    <option value="{{ $scene_id }}" {{ (Request::get('scene') == $scene_id) ? 'selected' : '' }}>{{ $scene }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group form-group-sm">
                                            <label style="width: 100%" class="text-center">kulisa:</label><br>
                                            <select name="wing" class="form-control">
                                                <option value="0">--- wybierz ---</option>
                                                @foreach($wings as $wing_id => $wing)
                                                    <option value="{{ $wing_id }}" {{ (Request::get('wing') == $wing_id) ? 'selected' : '' }}>{{ $wing }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group form-group-sm">
                                            <div class="divider">|</div>
                                        </div>
                                        <div class="form-group form-group-sm">
                                            <label class="marg-top">
                                                <input type="checkbox" name="if_single_actor" value="1" @if(Request::get('if_single_actor', 0)) checked @endif> rola indywidualna
                                            </label>
                                        </div>
                                        <div class="form-group form-group-sm">
                                            <label class="marg-top">
                                                <input type="checkbox" name="if_multi_actor" value="1" @if(Request::get('if_multi_actor', 0)) checked @endif> rola grupowa
                                            </label>
                                        </div>
                                        <div class="form-group form-group-sm">
                                            <label class="marg-top">
                                                <input type="checkbox" name="if_understudy" value="1" @if(Request::get('if_understudy', 0)) checked @endif> dubler
                                            </label>
                                        </div>
                                        <div class="form-group form-group-sm">
                                            <div class="divider">|</div>
                                        </div>
                                        <div class="form-group form-group-sm">
                                            <label class="marg-top">
                                                <input type="checkbox" name="if_noactor" value="1" @if(Request::get('if_noactor', 0)) checked @endif> bez aktora
                                            </label>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </nav>
                        <div class="table-responsive marg-top">
                            <table class="table table-sm table-hover">
                                <thead>
                                <th>#</th>
                                <th></th>
                                <th>Nazwa roli</th>
                                <th>Scena</th>
                                <th>Kulisa</th>
                                <th>Czas</th>
                                <th>Aktorzy</th>
                                <th>Rekwizyty</th>
                                <th>Kostiumy</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <Th></Th>
                                <Th></Th>
                                </thead>
                                @foreach($roles as $role)
                                    <tr>
                                        <td>{{ ($roles->currentPage() - 1) * $roles->perPage() + $loop->iteration  }}.</td>
                                        <td>
                                            @if($role->if_single_actor == 1)
                                                <label class="badge badge-warning">indywid.</label>
                                            @endif
                                            <br/>
                                            @if($role->if_understudy == 1)
                                                <label class="badge badge-info">dubler</label>
                                            @endif
                                        </td>
                                        <td>{{ $role->name }}</td>
                                        <td>{{ ($role->scene) ? $role->scene->name : '---' }}</td>
                                        <td>{{ ($role->wing) ? $role->wing->name : '---' }}</td>
                                        <td>
                                            {{ ($role->time_start) }} - {{ ($role->time_end) }}
                                        </td>
                                        <td>
                                            <span target="{{ url('admin/spectacle/'.$project->slug.'/manage/roles/show-actors', [$role->id]) }}" class="btn btn-xs
                                             @if(!$role->min_actors || !$role->max_actors )
                                                btn-info
                                            @elseif($role->min_actors <= $role->actors->count() && $role->max_actors >= $role->actors->count())
                                                btn-success
                                            @else
                                                btn-danger
                                            @endif
                                                modal-open-lg" data-toggle="modal" data-target="#modal-lg">
                                                <i class="fa fa-search fa-fw"></i>  aktorzy
                                                <span class="badge badge-light">
                                                    {{ $role->min_actors }}
                                                    /
                                                    {{ $role->actors->count() }}
                                                    /
                                                    {{ $role->max_actors }}
                                                </span>
                                            </span>
                                        </td>
                                        <td>
                                            <span target="{{ url('admin/spectacle/'.$project->slug.'/manage/roles/show-props', [$role->id]) }}" class="btn btn-xs btn-info modal-open-lg" data-toggle="modal" data-target="#modal-lg">
                                                <i class="fa fa-search fa-fw"></i>  rekwizyty <span class="badge badge-light">{{ $role->props->count() }} </span>
                                            </span>
                                        </td>
                                        <td>
                                            <span target="{{ url('admin/spectacle/'.$project->slug.'/manage/roles/show-costumes', [$role->id]) }}" class="btn btn-xs btn-info modal-open-lg" data-toggle="modal" data-target="#modal-lg">
                                                <i class="fa fa-search fa-fw"></i>  kostiumy <span class="badge badge-light">{{ $role->costumes->count() }}</span>
                                            </span>
                                        </td>
                                        <td>
                                            <span target="{{ url('admin/spectacle/'.$project->slug.'/manage/roles/show', [$role->id]) }}" class="btn btn-xs btn-info modal-open" data-toggle="modal" data-target="#modal">
                                                <i class="fa fa-search fa-fw"></i> pokaż
                                            </span>
                                        </td>
                                        <td>
                                            <a href="{{ url('admin/spectacle/'.$project->slug.'/manage/roles/manage-actors', [$role->id]) }}" class="btn btn-xs btn-primary">
                                                <i class="fa fa-pencil-alt fa-fw"></i> zarządzaj aktorami z rolą
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ url('admin/spectacle/'.$project->slug.'/manage/roles/edit', [$role->id]) }}" class="btn btn-xs btn-warning">
                                                <i class="fa fa-pencil-alt fa-fw"></i> edytuj
                                            </a>
                                        </td>
                                        <td>
                                            <span target="{{ url('admin/spectacle/'.$project->slug.'/manage/roles/delete', [$role->id]) }}" class="btn btn-xs btn-danger modal-open" data-toggle="modal" data-target="#modal">
                                                <i class="fa fa-trash-alt fa-fw"></i> usuń
                                            </span>
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary btn-xs dropdown-toggle off-disable" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-file-excel fa-fw" aria-hidden="true"></i> eksportuj <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li>
                                                        <a href="{{ url('admin/spectacle/'.$project->slug.'/manage/roles/export-emails', [$role->id]) }}" >
                                                            <i class="fa fa-file-excel-o fa-fw" aria-hidden="true"></i> exportuj maile xls
                                                        </a>
                                                    </li>
                                                    <li role="separator" class="divider"></li>
                                                    <li>
                                                        <a href="{{ url('admin/spectacle/'.$project->slug.'/manage/roles/export-phones', [$role->id]) }}" >
                                                            <i class="fa fa-file-excel-o fa-fw" aria-hidden="true"></i> exportuj tel. csv
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>

                    <div class="card-footer text-right">
                        {!! $roles->appends(Request::query())->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $('#search-form input').on('keyup keypress', function (e) {
            if(e.which === 13){
                $('#search-form').submit();
            }
        });
        $(':checkbox').change(function() {
            $('#search-form').submit();
        });
        $('#search-form select').on('change', function () {
            $('#search-form').submit();
        });
    </script>
@endsection
