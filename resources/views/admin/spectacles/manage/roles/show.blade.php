<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Szczegóły roli {{ $role->name }}</h5>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <form action="" method="post">
        <div class="form-group">
            <strong class="control-label">Nazwa roli</strong>
            <p class="form-control-static">{{ $role->name }}</p>
        </div>
        <div class="form-group">
            <strong class="control-label">Scena</strong>
            <p class="form-control-static">{{ ($role->scene) ? $role->scene->name : '---'}}</p>
        </div>
        <div class="form-group">
            <strong class="control-label">Kulisa</strong>
            <p class="form-control-static">{{ ($role->wing) ? $role->wing->name :'---' }}</p>
        </div>
        <div class="form-group">
            <strong class="control-label">Kostiumy</strong>
            <p class="form-control-static">
                @foreach($role->costumes as $costume)
                    @if($costume->type == 'Dzieje')
                        <span class="text-success">
                            {{ $costume->name }}
                        </span>
                    @else
                        <span class="text-danger">
                            {{ $costume->name }}
                        </span>
                    @endif

                    @if($role->costumes->count() > 1) ; @endif
                @endforeach
            </p>
        </div>
        <div class="form-group">
            <strong class="control-label">Rekwizyty</strong>
            <p class="form-control-static">
                @foreach($role->props as $prop)
                    @if($prop->type == 'Dzieje')
                        <span class="text-success">
                            {{ $prop->name }}
                        </span>
                    @else
                        <span class="text-danger">
                            {{ $prop->name }}
                        </span>
                    @endif

                    @if($role->props->count() > 1) ; @endif
                @endforeach
            </p>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
</div>
