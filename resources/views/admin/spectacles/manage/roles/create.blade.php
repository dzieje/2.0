@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                {!! Form::open(['url' => url('admin/spectacle/'.$project->slug.'/manage/roles/store'), 'id' => 'page-form']) !!}
                {!! Form::hidden('project_id', $project->id) !!}
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            Tworzenie nowej roli
                        </h6>
                        <a href="{{ URL::previous() }}" class="btn btn-dark btn-sm">
                            <i class="fas fa-times fa-fw"></i> anuluj
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6 col-md-4">
                                <div class="form-group">
                                    <label>Nazwa roli:</label>
                                    {!!  Form::text('name', '', array('class' => 'form-control required', 'placeholder' => 'nazwa')) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <div class="form-group">
                                    <label>Scena:</label>
                                    {!!  Form::select('scene_id', $scenes, 0, array('class' => 'form-control required')) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <div class="form-group">
                                    <label>Kulisa:</label>
                                    {!!  Form::select('wing_id', $wings, 0, array('class' => 'form-control required')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label>Min. aktorów:</label>
                                    {!!  Form::number('min_actors', null, array('class' => 'form-control  number', 'min' => 0,'placeholder' => 'min. aktorów')) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label>Max. aktorów:</label>
                                    {!!  Form::number('max_actors', null, array('class' => 'form-control  number', 'min' => 0,'placeholder' => 'max. aktorów')) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label>Godzina rozpoczęcia:</label>
                                    {!!  Form::time('time_start', null, array('class' => 'form-control ', 'step' => 1)) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label>Godzina zakończenia:</label>
                                    {!!  Form::time('time_end', null, array('class' => 'form-control ', 'step' => 1)) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="if_single_actor" value="1"  > rola indywidualna
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="if_understudy" value="1" > dubler
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <h4>Kostiumy
                                            <span id="add-costume" class="btn btn-primary btn-xs pull-right off-disable">
                                                <i class="fa fa-plus fa-fw"></i> dodaj
                                            </span>
                                        </h4>
                                        <hr/>
                                        <div class="text-center" id="costume-loader" style="display: none;"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>
                                        <div id="costumes-container" class="row">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <h4>Rekwizyty
                                            <span id="add-prop" class="btn btn-primary btn-xs pull-right off-disable">
                                                <i class="fa fa-plus fa-fw"></i> dodaj
                                            </span>
                                        </h4>
                                        <hr/>
                                        <div class="text-center" id="prop-loader" style="display: none;"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>
                                        <div id="props-container" class="row">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        {!! Form::submit('Zapisz rolę', ['class' => 'btn btn-primary off-disable']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $('#costumes-container').on('click', '.remove-costume', function(){
            $(this).parent().parent().parent().remove();
        });

        $('#add-costume').on('click', function(){
            $.ajax({
                type: "GET",
                url: '{{ url('admin/spectacle/'.$project->slug.'/manage/roles/costume-add') }}',
                assync:false,
                cache:false,
                dataType: 'html',
                beforeSend: function(){
                    $('#costume-loader').show();
                },
                success: function( data ) {
                    $('#costume-loader').hide();
                    $('#costumes-container').prepend( data );
                }
            });
        });

        $('#props-container').on('click', '.remove-prop', function(){
            $(this).parent().parent().parent().remove();
        });

        $('#add-prop').on('click', function(){
            $.ajax({
                type: "GET",
                url: '{{ url('admin/spectacle/'.$project->slug.'/manage/roles/prop-add') }}',
                assync:false,
                cache:false,
                dataType: 'html',
                beforeSend: function(){
                    $('#prop-loader').show();
                },
                success: function( data ) {
                    $('#prop-loader').hide();
                    $('#props-container').prepend( data );
                }
            });
        });
    </script>
@endsection
