@foreach($group->actors as $actor)
<tr data-actor="{{ $actor->id }}">
    <td>{{ $actor->name }}</td>
    <td>{{ $actor->surname }}</td>
    <td>{{ $actor->phone }}</td>
    <td>{{ $actor->email }}</td>
    <td>
        @foreach($actor->groups as $group)
            {{ $group->name }},
        @endforeach
    </td>
    <td>
        {!! Form::hidden('actors[]',  $actor->id) !!}
        <span class="btn btn-xs btn-danger detach-actor off-disable" data-actor="{{ $actor->id }}">
            <i class="fa fa-trash-o fa-fw"></i>
            odepnij
        </span>
    </td>
</tr>
@endforeach