<div class="col-sm-12">
    <h5>Wybierz grupy do roli <i>{{ $role->name }}</i>
        <span class="btn btn-xs btn-danger pull-right remove-role off-disabled" data-role="{{ $role->id }}" data-scene="{{ $role->n_scene_id }}">
            <i class="fa fa-trash fa-fw"></i> usuń
        </span>
    </h5>
    <div class="row">
        @forelse($groups as $group)
            <div class="col-sm-12 col-md-4 col-lg-3">
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input class="group-checkbox" data-group="{{ $group->id }}" data-role="{{ $role->id }}" type="checkbox" name="groups[{{ $role->id }}][]" value="{{ $group->id }}"> {{ $group->name }} ({{$group->actors->count()}})
                        </label>
                    </div>
                </div>
            </div>
        @empty
            <p class="text-danger"><i>brak grup do wskazania</i></p>
        @endforelse
        <div class="col-sm-12">
            <hr/>
        </div>
    </div>
</div>
