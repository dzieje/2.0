@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-10">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            Zarządzanie kulisami
                        </h6>
                        <form action="{{ url('admin/spectacle/'.$project->slug.'/manage/wings/stage-manager') }}" method="post">
                            <span target="{{ url('admin/spectacle/'.$project->slug.'/manage/wings/create') }}" class="btn btn-xs btn-primary modal-open" data-toggle="modal" data-target="#modal">
                                <i class="fa fa-plus fa-fw"></i> dodaj kulisę
                            </span>
                            {!! csrf_field() !!}
                            <button type="submit" class="btn btn-xs btn-info ">
                                <i class="fa fa-file-excel fa-fw"></i> inspicjent
                            </button>
                        </form>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-hover">
                                <thead>
                                    <th>#</th>
                                    <th>Nazwa kulisy</th>
                                    <Th></Th>
                                </thead>
                                @foreach($wings as $wing)
                                    <tr>
                                        <td>{{ ($wings->currentPage() - 1) * $wings->perPage() + $loop->iteration  }}.</td>
                                        <td>{{ $wing->name }}</td>
                                        <td>
                                            <a href="{{ url('admin/spectacle/'.$project->slug.'/manage/wings/export', [$wing->id]) }}" class="btn btn-xs btn-primary">
                                                <i class="fa fa-file-excel fa-fw" aria-hidden="true"></i> exportuj dane aktorów
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>

                    <div class="card-footer text-right">
                        {!! $wings->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>

    </script>
@endsection
