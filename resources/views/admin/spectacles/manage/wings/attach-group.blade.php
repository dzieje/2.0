@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-12 col-lg-10 col-lg-offset-1">
            {!! Form::open(['url' => url('admin/n-manage/wings/attach-group'), 'id' => 'page-form']) !!}
            <div class="panel panel-default">
                <div class="panel-heading">
                    Dołączanie grupy z kulisy {{ $wing->name }} do roli
                    <a href="{{ url('admin/n-manage/wings') }}" class="btn btn-default btn-xs pull-right">
                        <i class="fa fa-remove fa-fw"></i> anuluj
                    </a>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <h4>Scena</h4>
                                    <div class="input-group">
                                        {!!  Form::select('n_scene_id', $scenes, null, array('class' => 'form-control')) !!}
                                        <span class="input-group-btn">
                                            <button class="btn btn-default add-scene off-disable" type="button"><i class="fa fa-plus fa-fw"></i> wybierz scenę</button>
                                        </span>
                                    </div>
                                    <hr/>
                                    <div class="text-center" id="scene-loader" style="display: none;"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>
                                    <div id="scenes-container">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::hidden('wing_id', $wing->id) !!}
                <div class="panel-footer text-right">
                    <span class="btn btn-primary" id="save-and-add">
                        Zapisz i dodaj kolejną
                    </span>
                    {!! Form::submit('Zapisz i wyjdź', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $('#scenes-container').on('click', '.remove-role', function(){
            $(this).find('.group-checkbox').each( function(){
                var group = $(this).data('group');
                if($(this).is(":checked")) {
                    $('.group-checkbox[data-group="'+group+'"]').removeAttr('disabled', 'disabled');
                }
            });

            var scene_id = $(this).data('scene');
            var role_id = $(this).data('role');

            $('select[data-scene="' + scene_id + '"] option[value="'+role_id+'"]').show();
            $(this).parent().parent().remove();
        });
        $('#scenes-container').on('click', '.remove-scene', function(){
            $(this).parent().parent().parent().remove();
        });
        $('#scenes-container').on('change', '.group-checkbox', function () {
            var group = $(this).data('group');
            var role = $(this).data('role');
            if($(this).is(":checked")) {
                $('.group-checkbox[data-group="'+group+'"]').attr('disabled', 'disabled');
                $(this).removeAttr('disabled');
            }else{
                $('.group-checkbox[data-group="'+group+'"]').removeAttr('disabled', 'disabled');
            }
        });

        $('.add-scene').on('click', function(){
            var scene_id = $('select[name="n_scene_id"] option:selected').val();
            var wing_id = $('input[name="wing_id"]').val();

            $.ajax({
                type: "GET",
                url: '/admin/n-manage/wings/load-scene',
                data: {scene_id: scene_id, wing_id: wing_id},
                assync:false,
                cache:false,
                dataType: 'html',
                beforeSend: function(){
                    $('#scene-loader').show();
                },
                success: function( data ) {
                    $('#scene-loader').hide();
                    $('#scenes-container').html( data );
                }
            });
        });

        $('#scenes-container').on('click', '.add-role', function(){
            var scene_id = $(this).data('scene');
            var wing_id = $('input[name="wing_id"]').val();
            var role_id = $('select[data-scene="'+scene_id+'"] option:selected').val();
            if(role_id != '0') {

                $('select[data-scene="' + scene_id + '"] option:selected').hide();
                $('select[data-scene="' + scene_id + '"] option[value=0]').prop('selected', true)

                $.ajax({
                    type: "GET",
                    url: '/admin/n-manage/wings/load-role',
                    data: {scene_id: scene_id, wing_id: wing_id, role_id: role_id},
                    assync: false,
                    cache: false,
                    dataType: 'html',
                    beforeSend: function () {
                        $('.role-loader[data-scene="' + scene_id + '"]').show();
                    },
                    success: function (data) {
                        $('.role-loader[data-scene="' + scene_id + '"]').hide();
                        $('.roles-container[data-scene="' + scene_id + '"]').prepend(data);
                    }
                });
            }
        });

        $('#save-and-add').on('click', function(){
            $.ajax({
                type: "POST",
                url: '/admin/n-manage/wings/attach-group',
                data: $('#page-form').serialize(),
                assync:false,
                cache:false,
                success: function( data ) {
                    location.reload();
                }
            });
        });

    </script>
@endsection