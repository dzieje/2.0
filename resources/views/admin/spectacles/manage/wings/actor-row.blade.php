<tr data-actor="{{ $actor->id }}">
    <td>{{ $actor->name }}</td>
    <td>{{ $actor->surname }}</td>
    <td>{{ $actor->phone }}</td>
    <td>{{ $actor->email }}</td>
    <td>
        {!! Form::hidden('actor_id',  $actor->id) !!}
    </td>
</tr>