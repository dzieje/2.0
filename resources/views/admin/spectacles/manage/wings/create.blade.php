<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Tworzenie kulisy </h5>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="panel-body">
        <form action="{{ url('admin/spectacle/'.$project->slug.'/manage/wings/store') }}" method="post" id="dialog-form">
            {!! csrf_field() !!}
            <div class="form-group">
                <label>Nazwa:</label>
                {!!  Form::text('name', '', array('class' => 'form-control required', 'placeholder' => 'nazwa')) !!}
            </div>
        </form>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa dodawanie">Dodaj</button>
</div>
