<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            scena: <i>{{ $scene->name }}</i>
        </div>
        <div class="panel-body">
            <div class="input-group">
                {!!  Form::select('n_role_id', $roles, 0, array('class' => 'form-control', 'data-scene' => $scene->id)) !!}
                <span class="input-group-btn">
                    <button class="btn btn-default add-role off-disable" type="button" data-scene="{{ $scene->id }}"><i class="fa fa-plus fa-fw"></i> wybierz rolę</button>
                </span>
            </div>
            <hr/>
            <div class="text-center role-loader" data-scene="{{ $scene->id }}" style="display: none;"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>
            <div class="roles-container" data-scene="{{ $scene->id }}">

            </div>
        </div>
    </div>
</div>