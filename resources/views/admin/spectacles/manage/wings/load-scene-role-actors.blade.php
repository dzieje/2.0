<div class="col-sm-12 col-lg-6 col-lg-offset-3">
    <div class="row">
        <div class="col-sm-12">
            <h5>Wyszukaj aktora do roli <i>{{ $role->name }}</i></h5>
        </div>
        <div class="col-sm-12">
            {!! Form::text('add_actor', null, ['class' => 'form-control add-actor', 'autocomplete' => 'off', 'placeholder' => 'wyszukaj aktora'])!!}
        </div>
        <hr/>
        <div class="col-sm-12">
            <div class="text-center" id="actor-loader" style="display: none;"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>
            <div id="actors-container">
                <table class="table table-condensed marg-top">

                </table>
            </div>
        </div>
        <div class="col-sm-12">
            <hr/>
        </div>
    </div>
</div>


<script>
    var actors = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/admin/n-manage/wings/search-actor?role_id={{ $role->id }}',
            replace: function(url, query) { return url + "?limit=15&q=" + query; },
            filter: function (data) {
                return $.map(data.actors, function (actor) {
                    return actor
                });
            }
        }
    });

    $('.add-actor').typeahead({
            minLength: 2,
            highlight: true,
            hint: true
        },
        { name: 'name', displayKey: 'name', minLength: 2, source: actors, limit: 15 });

    $('.add-actor').on('typeahead:selected', function(ev, datum) {
        var volunteer_id = datum.id;
        $.ajax({
            type: "GET",
            url: '/admin/n-manage/wings/load-actor',
            data: { volunteer_id: volunteer_id },
            assync:false,
            cache:false,
            dataType: 'html',
            beforeSend: function(){
                $('#actor-loader').show();
            },
            success: function( data ) {
                $('#actor-loader').hide();
                $('#actors-container table').html( data );
            }
        });
    });

    $('.add-actor').on('typeahead:close', function(){
        $(this).val('');
    });

    $('#actors-container').on('click', '.detach-actor', function(){
        var actor_id = $(this).data('actor');
        $('tr[data-actor="'+actor_id+'"]').remove();
    });

</script>