<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Grupy przypisane do <i>{{ $wing->name }}</i> </h4>
</div>
<div class="modal-body">
    <div class="panel">
        <table class="table table-hover table-condensed table-bordered">
            <thead>
            <th>#</th>
            <th>grupa</th>
            </thead>
            @foreach($wing->groups as $lp => $group)
                <tr>
                    <td width="10px">{{ ++$lp }}.</td>
                    <td><strong>{{ $group->name }}</strong></td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
</div>