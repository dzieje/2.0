<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Aktorzy z grupą <i>{{ $group->name }}</i> </h5>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="panel">
        <table class="table table-hover table-condensed table-bordered">
            <thead>
                <th>#</th>
                <th>Nazwisko</th>
                <th>Imię</th>
                <Th>Telefon</Th>
                <th>Email</th>
            </thead>
            @foreach($actors as $lp => $actor)
                <tr>
                    <td width="10px">{{ ++$lp }}.</td>
                    <td><strong>{{ $actor->surname }}</strong></td>
                    <td><strong>{{ $actor->name }}</strong></td>
                    <td>{{ $actor->phone }}</td>
                    <td>{{ $actor->email }}</td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
</div>
