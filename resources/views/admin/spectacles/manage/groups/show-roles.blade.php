<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Role w grupie <i>{{ $group->name }}</i> </h4>
</div>
<div class="modal-body">
    <div class="panel">
        <table class="table table-hover table-condensed table-bordered">
            <thead>
                <th>#</th>
                <th>Scena</th>
                <th>Rola</th>
                <th></th>
            </thead>
            @foreach($group->roles()->orderBy('n_scene_id')->get() as $lp => $role)
                <tr>
                    <td width="10px">{{ ++$lp }}.</td>
                    <td>{{ $role->scene->name }}</td>
                    <td>{{ $role->name }}</td>
                    <td>
                        <span target="{{ url('admin/n-manage/groups/delete-role', [$group->id, $role->id]) }}" class="btn btn-xs btn-danger detach-role">
                            <i class="fa fa-trash-o fa-fw"></i> odepnij rolę
                        </span>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
</div>
<script>
    $('.detach-role').on('click', function(){
        var $this = $(this);
        if(confirm('potwierdź odpięcie roli od grupy')){
            $.ajax({
                type: "POST",
                url: $this.attr('target'),
                assync: false,
                cache: false,
                success: function (data) {
                    if (data.code == '0') location.reload();
                    else if (data.code == '1') self.location = data.url;
                },
                dataType: 'json'
            });
        }
    });
</script>