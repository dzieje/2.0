@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-8">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">

                        Zarządzanie grupami
                        </h6>
                        <div>
                            <span target="{{ url('admin/spectacle/'.$project->slug.'/manage/groups/create') }}" class="btn btn-xs btn-primary modal-open" data-toggle="modal" data-target="#modal">
                                <i class="fa fa-plus fa-fw"></i> dodaj grupę
                            </span>
                            <span target="{{ url('admin/spectacle/'.$project->slug.'/manage/groups/export') }}" class="btn btn-xs btn-info export mr-2">
                                <i class="fa fa-file-excel fa-fw" aria-hidden="true"></i> exportuj
                            </span>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="table-responsive ">
                            <table class="table table-sm table-hover">
                                <thead>
                                <th>#</th>
                                <th></th>
                                <th>Nazwa grupy</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <Th></Th>
                                </thead>
                                @foreach($groups as $group)
                                    <tr>
                                        <td>{{ ($groups->currentPage() - 1) * $groups->perPage() + $loop->iteration  }}.</td>
                                        <td>
                                            <span target="{{ url('admin/spectacle/'.$project->slug.'/manage/groups/edit', [$group->id]) }}" class="btn btn-xs btn-warning modal-open" data-toggle="modal" data-target="#modal">
                                                <i class="fa fa-pencil-alt fa-fw"></i> edytuj
                                            </span>
                                        </td>
                                        <td>{{ $group->name }}</td>
                                        <td>
                                            <span target="{{ url('admin/spectacle/'.$project->slug.'/manage/groups/show-actors', [$group->id]) }}" class="btn btn-xs btn-info modal-open-lg off-disable" data-toggle="modal" data-target="#modal-lg">
                                                <i class="fa fa-search fa-fw"></i> pokaż aktorów <span class="badge badge-light">{{ $group->actors->count() }} </span>
                                            </span>
                                        </td>
                                        <td>
                                            <a href="{{ url('admin/spectacle/'.$project->slug.'/manage/groups/manage-actors', [$group->id]) }}" class="btn btn-xs btn-primary">
                                                <i class="fa fa-cogs fa-fw"></i> zarządzaj aktorami w grupie
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>

                    <div class="card-footer text-right">
                        {!! $groups->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $('#search-form input').on('keyup keypress', function (e) {
            if(e.which === 13){
                $('#search-form').submit();
            }
        });
        $(':checkbox').change(function() {
            $('#search-form').submit();
        });
        $('#search-form select').on('change', function () {
            $('#search-form').submit();
        });
        $('.export').on('click', function(){
            var url = $(this).attr('target') + '?' + $('#search-form').serialize();

            self.location = url;
        });
    </script>
@endsection
