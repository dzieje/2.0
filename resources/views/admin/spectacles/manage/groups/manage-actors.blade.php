@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                {!! Form::open(['url' => url('admin/spectacle/'.$project->slug.'/manage/groups/update-actors', [$group->id]), 'id' => 'page-form']) !!}
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            Zarządzaj aktorami w grupie {{ $group->name }}
                        </h6>
                        <a href="{{ URL::previous() }}" class="btn btn-default btn-xs pull-right">
                            <i class="fa fa-remove fa-fw"></i> anuluj
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-sm-12 col-lg-10 col-lg-offset-1">
                                <div class="card">
                                    <div class="card-body">
                                        <h4>Aktorzy</h4>
                                        <hr/>
                                        <div class="row justify-content-center">
                                            <div class="col-sm-12 col-md-6 ">
                                                {!! Form::text('add_actor', null, ['class' => 'form-control add-actor', 'autocomplete' => 'off', 'placeholder' => 'wyszukaj aktora'])!!}
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="text-center" id="actor-loader" style="display: none;"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>
                                        <div id="actors-container">
                                            <table class="table table-condensed table-hover">
                                                <thead>
                                                <th>imię</th>
                                                <th>nazwisko</th>
                                                <th>telefon</th>
                                                <th>email</th>
                                                <th></th>
                                                </thead>
                                                @foreach($group->actors as $actor)
                                                    <tr data-actor="{{ $actor->id }}">
                                                        <td>{{ $actor->name }}</td>
                                                        <td>{{ $actor->surname }}</td>
                                                        <td>{{ $actor->phone }}</td>
                                                        <td>{{ $actor->email }}</td>
                                                        <td>
                                                            {!! Form::hidden('actors[]',  $actor->id) !!}
                                                            <span class="btn btn-xs btn-danger detach-actor off-disable" data-actor="{{ $actor->id }}">
                                                                <i class="fa fa-trash-o fa-fw"></i>
                                                                odepnij
                                                            </span>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        {!! Form::submit('Zapisz', ['class' => 'btn btn-primary']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script src="/js/bloodhound.js"></script>
    <script src="/js/typeahead.jquery.js"></script>
    <script>

        var actors = new Bloodhound({
            datumTokenizer: function (datum) {
                return Bloodhound.tokenizers.whitespace(datum.value);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: '/admin/spectacle/{{ $project->slug }}/manage/groups/search-actor',
                replace: function(url, query) { return url + "?limit=15&q=" + query; },
                filter: function (data) {
                    return $.map(data.actors, function (actor) {
                        return actor
                    });
                }
            }
        });

        var actorsTypeahead = $('.add-actor').typeahead({
                minLength: 2,
                highlight: true,
                hint: true
            },
            { name: 'name', displayKey: 'name', minLength: 2, source: actors, limit: 15 });

        $('.add-actor').on('typeahead:selected', function(ev, datum) {
            var volunteer_id = datum.id;

            $.ajax({
                type: "GET",
                url: '/admin/spectacle/{{ $project->slug }}/manage/groups/load-actor',
                data: { volunteer_id: volunteer_id },
                assync:false,
                cache:false,
                dataType: 'html',
                beforeSend: function(){
                    $('#actor-loader').show();
                },
                success: function( data ) {
                    $('#actor-loader').hide();
                    $('#actors-container table').append( data );
                    actorsTypeahead.typeahead('val','');
                }
            });
        });

        $('.add-actor').on('typeahead:close', function(){
            $(this).val('');
        });

        $('#actors-container').on('click', '.detach-actor', function(){
            var actor_id = $(this).data('actor');
            $('tr[data-actor="'+actor_id+'"]').remove();
        });

    </script>
@endsection
