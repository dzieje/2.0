<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Dodawanie roli do aktorów w grupie {{ $group->name }} </h4>
</div>
<div class="modal-body">
    <div class="panel-body">
        <form action="{{ url('admin/n-manage/groups/attach-role', [$group->id]) }}" method="post" id="dialog-form">
            {!! Form::token() !!}
            {!! Form::hidden('group_id', $group->id, ['id' => 'group_id']) !!}
            <div class="form-group">
                <label>Wybierz scenę:</label>
                {!! Form::select('scene_id',$scenes, null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group" id="roles">
                <label>Wybierz rolę:</label>
                {!! Form::select('role_id',[], null, ['class' => 'form-control']) !!}
            </div>
        </form>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa zapisywanie">Dodaj</button>
</div>
<script>
    $('select[name="scene_id"]').on('change',  function(){
        var group_id = $('#group_id').val();
        $.ajax({
            type: "GET",
            url: '/admin/n-manage/groups/roles',
            data: {scene_id: $(this).val(), group_id: group_id},
            assync:false,
            cache:false,
            dataType: 'html',
            success: function( data ) {
                $('#roles').html(data);
            }
        });
    }).change();
</script>