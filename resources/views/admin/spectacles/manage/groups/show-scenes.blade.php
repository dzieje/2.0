<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Sceny w grupie <i>{{ $group->name }}</i> </h4>
</div>
<div class="modal-body">
    <div class="panel">
        <table class="table table-hover table-condensed table-bordered">
            <thead>
                <th>#</th>
                <th>Nazwa</th>
            </thead>
            @foreach($scenes as $lp => $scene)
                <tr>
                    <td width="10px">{{ ++$lp }}.</td>
                    <td>{{ $scene->name }}</td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
</div>