<tr data-actor="{{ $actor->id }}">
    <td>{{ $actor->name }}</td>
    <td>{{ $actor->surname }}</td>
    <td>{{ $actor->phone }}</td>
    <td>{{ $actor->email }}</td>
    <td>
        {!! Form::hidden('actors[]',  $actor->id) !!}
        <span class="btn btn-xs btn-danger detach-actor off-disable" data-actor="{{ $actor->id }}">
            <i class="fa fa-trash-alt fa-fw"></i>
            odepnij
        </span>
    </td>
</tr>
