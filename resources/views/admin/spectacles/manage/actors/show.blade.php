<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Informacje o <i>{{ $actor->surname }} {{ $actor->name }}</i></h5>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <nav class="navbar navbar-default">
        <div class="row">
            <div class="col-sm-12">
                <p class="navbar-text navbar-text-small p-0">
                    nazwisko: <strong>{{ $actor->surname }}</strong>
                </p>
                <p class="navbar-text navbar-text-small p-0">
                    imię: <strong>{{ $actor->name }}</strong>
                </p>
                <p class="navbar-text navbar-text-small p-0">
                    telefon: <strong>{{ $actor->phone }}</strong>
                </p>
                <p class="navbar-text navbar-text-small p-0">
                    email: <strong>{{ $actor->email }}</strong>
                </p>
                <p class="navbar-text navbar-text-small p-0">
                    płeć: <strong>@if($actor->gender == 'male') mężczyzna @else kobieta @endif</strong>
                </p>
                <p class="navbar-text navbar-text-small p-0">
                    data urodzin:
                    <strong>
                        {{ $actor->date_of_birth->format('Y-m-d') }}
                        <span class="badge badge-info">{{ $actor->date_of_birth->diffInYears(Carbon\Carbon::now()) }}</span>
                    </strong>
                </p>
            </div>
            <div class="col-sm-12">
                <p class="navbar-text navbar-text-small p-0" >
                    grupa: <strong>
                        @if($actor->group) {{ $actor->group->name }} @else --- @endif
                    </strong>
                </p>
            </div>
        </div>
    </nav>

    <div class="d-sm-flex align-items-center justify-content-between py-3">
        <a href="{{ url('admin/spectacle/'.$project->slug.'/manage/actors/edit', [$actor->id]) }}" class="btn btn-xs btn-warning pull-right marg-btm">
            <i class="fa fa-pencil-alt fa-fw"></i> edytuj
        </a>
        <div>
            <span class="badge badge-success ml-2">dzieje</span>
            <span class="badge badge-danger ">statysta</span>
        </div>
    </div>
    <table class="table table-hover ">
        <thead>
        <th>#</th>
        <th>scena</th>
        <th>rola</th>
        <th>rekwizyty</th>
        <th>kostiumy</th>
        </thead>
        @foreach($actor->roles as $k => $role)
            <tr>
                <td>{{ ++$k }}.</td>
                <td>{{ $role->scene->name }}</td>
                <td>{{ $role->name }}</td>
                <td>
                    @if($role->props->count() > 0)
                        @foreach($role->props as $prop)
                            @if($prop->type == 'Dzieje')
                                <span class="text-success">
                                    {{ $prop->name }}
                                </span>
                            @else
                                <span class="text-danger">
                                    {{ $prop->name }}
                                </span>
                            @endif

                            @if($role->props->count() > 1) ; @endif
                        @endforeach
                    @else
                        ---
                    @endif
                </td>
                <td>
                    @if($role->costumes->count() > 0)
                        @foreach($role->costumes as $costume)
                            @if($costume->type == 'Dzieje')
                                <span class="text-success">
                                    {{ $costume->name }}
                                </span>
                            @else
                                <span class="text-danger">
                                    {{ $costume->name }}
                                </span>
                            @endif


                            @if($role->costumes->count() > 1) ; @endif
                        @endforeach
                    @else
                        ---
                    @endif
                </td>
            </tr>
        @endforeach
    </table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
</div>
