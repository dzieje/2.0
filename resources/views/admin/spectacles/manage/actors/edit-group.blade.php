<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Przypisywanie grupy <i>{{ $actor->name }} {{ $actor->surname }}</i> </h4>
</div>
<div class="modal-body">
    <form action="{{ url('admin/n-manage/actors/update-group', [$actor->id]) }}" method="post" id="dialog-form">
        {!! Form::token() !!}
        <div class="panel">
            <div class="form-group">
                <label for="exampleInputEmail1">Wybierz grupę</label>
                {!! Form::select('group_id', $groups, $actor->n_group_id, ['class' => 'form-control']) !!}
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa dodawanie">Zapisz</button>
</div>
