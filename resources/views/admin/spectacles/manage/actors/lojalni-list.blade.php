<div class="col-sm-12 col-md-8 col-md-offset-2">
    <table class="table table-hover table-condensed">
        <thead>
            <th width="30px">#</th>
            <th width="30px"></th>
            <th width="150px"></th>
            <th>imię</th>
            <th>nazwisko</th>
            <th>ID</th>
        </thead>
        @foreach($accounts as $k => $account)
            <tr>
                <td>{{ ++ $k }}.</td>
                <td>
                    <input type="radio" name="identifier" value="{{ $account['identifier'] }}">
                </td>
                <td>
                    @if($account['avatar_filename'])
                        <img src="{{ url('admin/n-manage/actors/lojalni-avatar', [$account['identifier']]) }}" width="100">
                    @endif
                </td>
                <td>
                    {{ $account['name'] }}
                </td>
                <td>
                    {{ $account['surname'] }}
                </td>
                <td>
                    {{ $account['identifier'] }}
                </td>
            </tr>
        @endforeach
    </table>
</div>
