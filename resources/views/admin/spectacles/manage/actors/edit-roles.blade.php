<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Zarządzanie rolami <i>{{ $actor->name }} {{ $actor->surname }}</i> </h4>
</div>
<div class="modal-body">
    <form action="{{ url('admin/n-manage/actors/update-roles', [$actor->id]) }}" method="post" id="dialog-form">
        {!! Form::token() !!}
        <div class="panel">
            <div class="form-group">
                <label for="exampleInputEmail1">Wybierz rolę</label>
                <div class="input-group">
                    {!! Form::select('role_select', $roles, null, ['class' => 'form-control']) !!}
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-primary add-role off-disable">
                            <i class="fa fa-fw fa-plus"></i> dołącz
                        </button>
                    </div>
                </div>
            </div>
            <table class="table table-hover table-condensed table-bordered roles-table">
                <thead>
                    <th>rola</th>
                    <th></th>
                </thead>
                @foreach($actor->roles as $lp => $role)
                    <tr data-role="{{ $role->id }}">
                        <td><strong>{{ $role->name }}</strong></td>
                        <td>
                            {!! Form::hidden('role_id[]', $role->id) !!}
                            <span class="btn btn-danger btn-xs detach-role" data-role="{{ $role->id }}">
                                <i class="fa fa-trash-o fa-fw"></i> odepnij
                            </span>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa dodawanie">Zapisz</button>
</div>
<script>
    $('.roles-table').on('click', '.detach-role', function(){
        var role_id = $(this).data('role');
        $('tr[data-role="'+role_id+'"').remove();
    });
    $('.add-role').on('click', function(){
        var role_id = $('select[name="role_select"] option:selected').val();
        var role_name = $('select[name="role_select"] option:selected').html();

        if($('tr[data-role="'+role_id+'"').length == 0){
            $('.roles-table').append('<tr data-role="'+role_id+'"><td><strong>'+role_name+'</strong></td><td><input type="hidden" name="role_id[]" value="'+role_id+'"/><span class="btn btn-danger btn-xs detach-role" data-role="'+role_id+'"><i class="fa fa-trash-o fa-fw"></i> odepnij</span></td></tr>');
        }
    });
</script>