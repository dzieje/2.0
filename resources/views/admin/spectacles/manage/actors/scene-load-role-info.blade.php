<div class="form-group form-group-sm">
    <label class="control-label">Rekwizyty:</label>
    <p>
        @if($role->props->count() > 0)
            @foreach($role->props as $prop)
                @if($prop->type == 'Dzieje')
                    <span class="text-success">
                        {{ $prop->name }}
                    </span>
                @else
                    <span class="text-danger">
                        {{ $prop->name }}
                    </span>
                @endif

                @if($role->props->count() > 1) ; @endif
            @endforeach
        @else
            ---
        @endif
    </p>
</div>
<div class="form-group form-group-sm">
    <label class="control-label">Kostiumy:</label>
    <p>
        @if($role->costumes->count() > 0)
            @foreach($role->costumes as $costume)
                @if($costume->type == 'Dzieje')
                    <span class="text-success">
                        {{ $costume->name }}
                    </span>
                @else
                    <span class="text-danger">
                        {{ $costume->name }}
                    </span>
                @endif

                @if($role->costumes->count() > 1) ; @endif
            @endforeach
        @else
            ---
        @endif
    </p>
</div>
