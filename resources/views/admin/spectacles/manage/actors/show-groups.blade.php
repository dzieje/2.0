<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Grupy przypisane do <i>{{ $actor->name }} {{ $actor->surname }}</i> </h5>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <table class="table table-sm table-hover table-bordered">
        <thead>
            <th>#</th>
            <th>grupa</th>
        </thead>
        @foreach($actor->groups as $lp => $group)
            <tr>
                <td width="10px">{{ ++$lp }}.</td>
                <td><strong>{{ $group->name }}</strong></td>
            </tr>
        @endforeach
    </table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
</div>
