@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            {!! Form::open(['url' => url('admin/n-manage/actors/store'), 'id' => 'page-form']) !!}
            <div class="panel panel-default">
                <div class="panel-heading">
                    Tworzenie konta aktora i wolontariusza
                    <a href="{{ URL::previous() }}" class="btn btn-default btn-xs pull-right">
                        <i class="fa fa-remove fa-fw"></i> anuluj
                    </a>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6 col-lg-4">
                            <div class="form-group">
                                <label>Nazwisko:</label>
                                {!! Form::text('surname', null, ['class' => 'form-control required', 'placeholder' => 'podaj nazwisko']) !!}
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="form-group">
                                <label>Imię:</label>
                                {!! Form::text('name', null, ['class' => 'form-control required', 'placeholder' => 'podaj imię']) !!}
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="form-group">
                                <label>Telefon:</label>
                                {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'podaj telefon']) !!}
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="form-group">
                                <label>Email:</label>
                                {!! Form::text('email', null, ['class' => 'form-control email', 'placeholder' => 'podaj email']) !!}
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="form-group">
                                <label>Angażuje się:</label>
                                {!! Form::select('respondent_type', [null => '---wybierz---', 'first' => 'pierwszy raz', 'multiple' => 'kolejny'], '', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="form-group">
                                <label>Płeć:</label>
                                {!! Form::select('gender', [null => '---wybierz---', 'male' => 'mężczyzna', 'female' => 'kobieta'], '', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="form-group">
                                <label>Adres ulica:</label>
                                {!! Form::text('street', '', ['class' => 'form-control', 'placeholder' => 'podaj ulicę']) !!}
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="form-group">
                                <label>Adres nr domu:</label>
                                {!! Form::text('house_number', '', ['class' => 'form-control', 'placeholder' => 'podaj nr domu']) !!}
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="form-group">
                                <label>Adres nr mieszkania:</label>
                                {!! Form::text('flat_number', '', ['class' => 'form-control', 'placeholder' => 'podaj nr mieszkania']) !!}
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="form-group">
                                <label>Adres kod pocztowy:</label>
                                {!! Form::text('post_code', '', ['class' => 'form-control', 'placeholder' => 'podaj kod pocztowy']) !!}
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="form-group">
                                <label>Adres miasto:</label>
                                {!! Form::text('city', '', ['class' => 'form-control', 'placeholder' => 'podaj miasto']) !!}
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="form-group">
                                <label>PESEL:</label>
                                {!! Form::text('personal_id', '', ['class' => 'form-control', 'placeholder' => 'podaj PESEL']) !!}
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="form-group">
                                <label>Data urodzenia:</label>
                                {!! Form::text('date_of_birth', '', ['class' => 'form-control dynamic-datepicker', 'placeholder' => 'podaj datę urodzenia']) !!}
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="form-group">
                                <label>Wzrost:</label>
                                {!! Form::text('height', '', ['class' => 'form-control', 'placeholder' => 'podaj wzrost']) !!}
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="form-group">
                                <label>Rozmiar butów:</label>
                                <select name="shoes_size" class="form-control">
                                    <option value="">-- wybierz ---</option>
                                    @for($i = 20; $i < 50; $i++)
                                        <option value="{{ $i }}" >
                                            {{ $i }}
                                        </option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="form-group">
                                <label>Rozmiar koszulki:</label>
                                <select name="shirt_size" class="form-control" >
                                    <option value="">--- wybierz ---</option>
                                    <option value="s" >
                                        S
                                    </option>
                                    <option value="m">
                                        M
                                    </option>
                                    <option value="l">
                                        L
                                    </option>
                                    <option value="xl">
                                        XL
                                    </option>
                                    <option value="xxl">
                                        XXL
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <hr/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
                            <div class="form-group">
                                <label>Grupa:</label>
                                {!! Form::select('n_group_id', $groups, null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>

                </div>
                <div class="panel-footer text-right">
                    {!! Form::submit('Zapisz dane aktora', ['class' => 'btn btn-primary off-disable']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $('.scenes-container').on('click', '.remove-scene', function(){
            $(this).parents('.scene-container').remove();
        });

        $('.add-scene').on('click', function(){
            $.ajax({
                type: "GET",
                url: '/admin/n-manage/actors/scene-add',
                assync:false,
                cache:false,
                dataType: 'html',
                beforeSend: function(){
                    $('#scene-loader').show();
                },
                success: function( data ) {
                    $('#scene-loader').hide();
                    $('#scenes-container').prepend( data );
                }
            });
        });

        $('body').on('change', '.scenes', function(){
            var obj = $(this);

            $.ajax({
                type: "GET",
                url: '/admin/n-manage/actors/scene-load-roles',
                data: {scene_id: $(this).val() },
                assync:false,
                cache:false,
                dataType: 'html',
                beforeSend: function(){
                    obj.parent().next().html( '<div class="text-center"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>' );
                },
                success: function( data ) {
                    obj.parent().next().html( data );
                }
            });
        });

        $('body').on('change', '.roles', function(){
            var obj = $(this);

            $.ajax({
                type: "GET",
                url: '/admin/n-manage/actors/scene-load-role-info',
                data: {role_id: $(this).val() },
                assync:false,
                cache:false,
                dataType: 'html',
                beforeSend: function(){
                    obj.parent().next().html( '<div class="text-center"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>' );
                },
                success: function( data ) {
                    obj.parent().next().html( data );
                }
            });
        });

        $('.groups-container').on('click', '.remove-group', function(){
            $(this).parents('.group-container').remove();
        });
        $('.add-group').on('click', function(){
            $.ajax({
                type: "GET",
                url: '/admin/n-manage/actors/group-add',
                assync:false,
                cache:false,
                dataType: 'html',
                beforeSend: function(){
                    $('#group-loader').show();
                },
                success: function( data ) {
                    $('#group-loader').hide();
                    $('#groups-container').prepend( data );
                }
            });
        });
    </script>
@endsection