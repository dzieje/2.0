<div class="col-sm-12 col-md-6 col-lg-4 scene-container">
    <div class="panel panel-primary">
        <div class="panel-body">
            <span class="btn btn-xs btn-danger pull-right remove-scene">
                <i class="fa fa-remove"></i> usuń
            </span>
            <div class="form-group form-group-sm">
                <label class="control-label">Scena:</label>
                {!! Form::select('scenes[]', $scenes, 0, ['class' =>'form-control scenes']) !!}
            </div>
            <div>

            </div>
        </div>
    </div>
</div>
