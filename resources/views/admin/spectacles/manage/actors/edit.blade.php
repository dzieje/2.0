@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                {!! Form::open(['url' => url('admin/spectacle/'.$project->slug.'/manage/actors/update', [$actor->id]), 'id' => 'page-form']) !!}
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                        Edycja danych aktora {{ $actor->surname }} {{ $actor->name }}
                        </h6>
                        <a href="{{ URL::previous() }}" class="btn btn-outline-dark btn-sm pull-right">
                            <i class="fa fa-trash-alt fa-fw"></i> anuluj
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label>Nazwisko:</label>
                                    {!! Form::text('surname', $actor->surname, ['class' => 'form-control required', 'placeholder' => 'podaj nazwisko']) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label>Imię:</label>
                                    {!! Form::text('name', $actor->name, ['class' => 'form-control required', 'placeholder' => 'podaj imię']) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label>Telefon:</label>
                                    {!! Form::text('phone', $actor->phone, ['class' => 'form-control', 'placeholder' => 'podaj telefon']) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label>Email:</label>
                                    {!! Form::text('email', $actor->email, ['class' => 'form-control email', 'placeholder' => 'podaj email']) !!}
                                </div>
                            </div>



                            <div class="col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label>Płeć:</label>
                                    {!! Form::select('gender', [null => '---wybierz---', 'male' => 'mężczyzna', 'female' => 'kobieta'], $actor->gender, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label>Data urodzenia:</label>
                                    {!! Form::date('date_of_birth', $actor->date_of_birth->format('Y-m-d'), ['class' => 'form-control dynamic-datepicker', 'placeholder' => 'podaj datę urodzenia']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        {!! Form::submit('Zapisz zmiany', ['class' => 'btn btn-primary off-disable']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $('.scenes-container').on('click', '.remove-scene', function(){
            $(this).parents('.scene-container').remove();
        });
        $('.add-scene').on('click', function(){
            $.ajax({
                type: "GET",
                url: '/admin/spectacle/{{ $project->slug }}/manage/actors/scene-add',
                assync:false,
                cache:false,
                dataType: 'html',
                beforeSend: function(){
                    $('#scene-loader').show();
                },
                success: function( data ) {
                    $('#scene-loader').hide();
                    $('#scenes-container').prepend( data );
                }
            });
        });
        $('body').on('change', '.scenes', function(){
            var obj = $(this);

            $.ajax({
                type: "GET",
                url: '/admin/spectacle/{{ $project->slug }}/manage/actors/scene-load-roles',
                data: {scene_id: $(this).val() },
                assync:false,
                cache:false,
                dataType: 'html',
                beforeSend: function(){
                    obj.parent().next().html( '<div class="text-center"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>' );
                },
                success: function( data ) {
                    obj.parent().next().html( data );
                }
            });
        });

        $('body').on('change', '.roles', function(){
            var obj = $(this);

            $.ajax({
                type: "GET",
                url: '/admin/spectacle/{{ $project->slug }}/manage/actors/scene-load-role-info',
                data: {role_id: $(this).val() },
                assync:false,
                cache:false,
                dataType: 'html',
                beforeSend: function(){
                    obj.parent().next().html( '<div class="text-center"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>' );
                },
                success: function( data ) {
                    obj.parent().next().html( data );
                }
            });
        });


        $('.groups-container').on('click', '.remove-group', function(){
            $(this).parents('.group-container').remove();
        });
        $('.add-group').on('click', function(){
            $.ajax({
                type: "GET",
                url: '/admin/spectacle/{{ $project->slug }}/manage/actors/group-add',
                assync:false,
                cache:false,
                dataType: 'html',
                beforeSend: function(){
                    $('#group-loader').show();
                },
                success: function( data ) {
                    $('#group-loader').hide();
                    $('#groups-container').prepend( data );
                }
            });
        });

    </script>
@endsection
