<div class="col-sm-12 col-md-6 col-lg-4 group-container">
    <div class="panel panel-primary">
        <div class="panel-body">
            <span class="btn btn-xs btn-danger pull-right remove-group">
                <i class="fa fa-remove"></i> usuń
            </span>
            <div class="form-group form-group-sm">
                <label class="control-label">Grupa:</label>
                {!! Form::select('groups[]', $groups, 0, ['class' =>'form-control groups']) !!}
            </div>
            <div>

            </div>
        </div>
    </div>
</div>
