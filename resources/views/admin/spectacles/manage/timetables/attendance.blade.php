@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Zarządzanie listą obecności próby <strong>{{ $rehearsal->name }}</strong>
                    <a href="{{ url('admin/n-manage/timetables') }}" class="btn btn-sm btn-default pull-right"><< powrót</a>
                </div>
                <div class="panel-body">
                    <nav class="navbar navbar-default navbar-sm marg-top-min" style="margin-bottom: 0px;">
                        <div class="container-fluid">
                            <div class="" id="bs-example-navbar-collapse-2">
                                <form class="navbar-form navbar-left" role="search" id="search-form" action="{{ url('admin/n-manage/timetables/attendance', [$rehearsal->id]) }}" method="get">
                                    {{ Form::hidden('rehearsal_id', $rehearsal->id) }}
                                    <div class="form-group form-group-sm text-center ">
                                        <label>Filtrowanie aktorów <span class="badge">{{ $actors->total() }}</span></label><br>
                                        <a class="btn btn-xs btn-danger" href="{{ url('admin/n-manage/timetables/attendance', [$rehearsal->id]) }}">
                                            <i class="fa fa-remove fa-fw"></i> usuń filtry
                                        </a>
                                        <span class="btn btn-primary btn-xs" id="export-results">
                                            <i class="fa fa-file-excel-o fa-fw" aria-hidden="true"></i> eksportuj wyniki
                                        </span>
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label style="width: 100%" class="text-center">imię:</label><br>
                                        <input name="name" value="{{ Request::get('name') ? Request::get('name') : '' }}" type="text" class="form-control" placeholder="imię">
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <label style="width: 100%" class="text-center">nazwisko:</label><br>
                                        <input name="surname" value="{{ Request::get('surname') ? Request::get('surname') : '' }}" type="text" class="form-control" placeholder="nazwisko">
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <label style="width: 100%" class="text-center">telefon:</label><br>
                                        <input name="phone" value="{{ Request::get('phone') ? Request::get('phone') : '' }}" type="text" class="form-control" placeholder="telefon">
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <label style="width: 100%" class="text-center">email:</label><br>
                                        <input name="email" value="{{ Request::get('email') ? Request::get('email') : '' }}" type="text" class="form-control" placeholder="email">
                                    </div>

                                    <div class="form-group form-group-sm">
                                        <label style="width: 100%" class="text-center">kulisa:</label><br>
                                        <select name="wing" class="form-control">
                                            <option value="0">--- wybierz ---</option>
                                            @foreach($wings as $wing_id => $wing)
                                                <option value="{{ $wing_id }}" {{ (Request::get('wing') == $wing_id) ? 'selected' : '' }}>{{ $wing }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </nav>
                    <div class="table-responsive marg-top">
                        <table class="table table-condensed table-hover table-bordered table-attendance">
                            <thead>
                            <th>#</th>
                            <th>Nazwisko</th>
                            <th>Imię</th>
                            <th>Telefon</th>
                            <Th>Email</Th>
                            <th>Grupa</th>
                            @foreach ($rehearsal->periods as $period)
                                <th >{{ $period->date_from->format('H:i').'-'.$period->date_to->format('H:i') }}</th>
                            @endforeach
                            </thead>
                            <?php $lp = (($actors->currentPage()-1)*$actors->perPage()) + 1;?>
                            @foreach($actors as $actor)
                                <tr>
                                    <td>{{ $lp++ }}.</td>
                                    <td>{{ $actor->surname }}</td>
                                    <td>{{ $actor->name }}</td>
                                    <td>{{ $actor->phone }}</td>
                                    <td>{{ $actor->email }}</td>
                                    <td>
                                        @if($actor->groups->count() > 0)
                                            {{ implode(', ', $actor->groups->pluck('name')->toArray()) }}
                                        @else
                                            ---
                                        @endif
                                    </td>
                                    @foreach ($rehearsal->periods as $rehearsal_period)
                                        <?php $if_period = false; ?>
                                        @foreach ($actor->roles as $role)
                                            @foreach ($role->periods as $period)
                                                @if($period->id == $rehearsal_period->id)
                                                    <?php $if_period = true;?>
                                                @endif
                                            @endforeach
                                        @endforeach

                                            @if($if_period )
                                                <td class="active">
                                                    <div class="btn-group" data-toggle="buttons">
                                                        <label class="btn btn-sm btn-success off-disable @if(isset($attendances[$actor->id]) && isset($attendances[$actor->id][$rehearsal_period->id])  && $attendances[$actor->id][$rehearsal_period->id] == 1) active @endif">
                                                            <input class="attendance-radio" value="1" type="radio" name="options{{ $actor->id }}_{{ $rehearsal_period->id }}" autocomplete="off"
                                                                   @if(isset($attendances[$actor->id]) && isset($attendances[$actor->id][$rehearsal_period->id])  && $attendances[$actor->id][$rehearsal_period->id] == 1) checked @endif
                                                                   data-rehearsal="{{ $rehearsal->id }}" data-actor="{{ $actor->id }}" data-period="{{ $rehearsal_period->id }}"
                                                            > tak
                                                        </label>
                                                        <label class="btn btn-sm btn-danger off-disable @if(isset($attendances[$actor->id]) && isset($attendances[$actor->id][$rehearsal_period->id]) && $attendances[$actor->id][$rehearsal_period->id] == 0) active @endif" >
                                                            <input class="attendance-radio" value="0" type="radio" name="options{{ $actor->id }}_{{ $rehearsal_period->id }}"  autocomplete="off"
                                                                @if(isset($attendances[$actor->id]) && isset($attendances[$actor->id][$rehearsal_period->id]) && $attendances[$actor->id][$rehearsal_period->id] == 0) checked @endif
                                                                data-rehearsal="{{ $rehearsal->id }}" data-actor="{{ $actor->id }}" data-period="{{ $rehearsal_period->id }}"
                                                            > nie
                                                        </label>

                                                    </div>
                                                </td>
                                            @else
                                                <td>
                                                </td>
                                            @endif
                                    @endforeach

                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>

                <div class="panel-footer text-right">
                    {!! $actors->appends(Request::query())->links() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $('.attendance-radio').on('change', function(){
            var actor_id = $(this).data('actor');
            var period_id = $(this).data('period');
            var rehearsal_id = $(this).data('rehearsal');
            var status = $(this).val();

            $.ajax({
                data: { actor_id: actor_id, period_id: period_id, rehearsal_id: rehearsal_id, status: status },
                type: 'POST',
                assync: false,
                cache: false,
                dataType: 'json',
                url: '/admin/n-manage/timetables/set-attendance',
                success: function (data) {
                    $.notify({
                        message: 'Zmiany zapisano pomyślnie.'
                    },{
                        type: 'info',
                        delay: 1000,
                        placement: {
                            from: 'bottom',
                            align: 'right'
                        }
                    });
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    $.notify({
                        message: 'Wystąpił błąd w trakcie aktualizacji.'
                    },{
                        type: 'danger',
                        delay: 1000,
                        placement: {
                            from: 'bottom',
                            align: 'right'
                        }
                    });
                }
            });
        });


        $('#search-form input').on('keyup keypress', function (e) {
            if(e.which === 13){
                $('#search-form').submit();
            }
        });
        $('#search-form select').on('change', function () {
            $('#search-form').submit();
        });
        $('#export-results').on('click', function(){
            $.ajax({
                type: "POST",
                url: '/admin/n-manage/timetables/export-attendance',
                data: $('#search-form').serialize(),
                assync:false,
                cache:false,
                success: function( data ) {
                    location.reload();
                }
            });
        });
    </script>
@endsection
