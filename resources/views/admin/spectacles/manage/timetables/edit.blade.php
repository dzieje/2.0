@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            {!! Form::open(['url' => url('admin/n-manage/timetables/update', [$rehearsal->id]), 'id' => 'page-form']) !!}
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edycja próby
                    <a href="{{ URL::to('admin/n-manage/timetables') }}" class="btn btn-default btn-xs pull-right">
                        <i class="fa fa-remove fa-fw"></i> anuluj
                    </a>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6 col-lg-4 col-lg-offset-2">
                            <div class="form-group">
                                <label>Nazwa próby:</label>
                                {!!  Form::text('name', $rehearsal->name, array('class' => 'form-control', 'placeholder' => 'nazwa')) !!}
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="form-group">
                                <label>Data próby:</label>
                                {!!  Form::text('rehearsal', $rehearsal->rehearsal->format('Y-m-d'), array('class' => 'form-control required date dynamic-datepicker', 'placeholder' => 'dzień próby')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <hr/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Przedziały czasowe
                                <span id="add-period" class="btn btn-sm btn-primary pull-right off-disable">
                                    <i class="fa fa-plus fa-fw"></i> dodaj przedział
                                </span>
                            </h4>
                            <hr/>
                            <div class="text-center" id="period-loader" style="display: none;"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>
                            <div id="periods-container">
                                @foreach($rehearsal->periods as $period)
                                    <div class="panel panel-default marg-top-min period-container">
                                        <div class="panel-heading text-right">
                                            <span class="btn btn-xs btn-danger delete-period off-disable">
                                                <i class="fa fa-remove"></i> usuń
                                            </span>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row ">
                                                <div class="form-group form-group-sm col-md-4">
                                                    <label class="control-label">Przedział od:</label>

                                                    <div class="input-group bootstrap-timepicker timepicker">
                                                        <input value="{{  $period->date_from->format('H:i') }}" id="timepicker_from{{ $period->id }}" name="date_from[{{ $period->id }}]" type="text" data-provide="timepicker" data-show-meridian="false" class="form-control required">
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                                    </div>

                                                </div>
                                                <div class="form-group form-group-sm col-md-4">
                                                    <label class="control-label">Przedział do:</label><br/>
                                                    <div class="input-group bootstrap-timepicker timepicker">
                                                        <input value="{{  $period->date_to->format('H:i') }}" id="timepicker_to{{ $period->id }}" name="date_to[{{ $period->id }}]" type="text" data-provide="timepicker" data-show-meridian="false"  class="form-control required">
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-sm col-md-4 text-right">
                                                    <span class="btn btn-sm btn-primary add-scene marg-top off-disable" data-period="{{ $period->id }}">
                                                        <i class="fa fa-plus fa-fw"></i>
                                                        dodaj scenę
                                                    </span>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="text-center scene-loader" style="display: none;"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="scenes-container panel-group" role="tablist" aria-multiselectable="true">
                                                        @foreach($period->scenes as $scene)
                                                            <?php $nb = rand(1000, 9999);?>
                                                            <div class="panel panel-default" data-scene="{{ $scene->id }}{{ $nb }}">
                                                                <div class="panel-heading" role="tab" id="heading{{ $scene->id }}{{ $nb }}">
                                                                    <h4 class="panel-title">
                                                                        <a class="accordion-header" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $scene->id }}{{ $nb }}" aria-expanded="true" aria-controls="collapse{{ $scene->id }}">
                                                                            #Scena {{ $scene->name }}
                                                                        </a>
                                                                        <span class="btn btn-xs btn-danger delete-scene pull-right off-disable" data-scene="{{ $scene->id }}{{ $nb }}">
                <i class="fa fa-fw fa-trash-o"></i> usuń scenę
            </span>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapse{{ $scene->id }}{{ $nb }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="panel-body">
                                                                        <div class="row">
                                                                            <div class="form-group form-group-sm col-sm-12 col-md-8">
                                                                                <label class="control-label">Wskaż scenę:</label><br/>
                                                                                {{ Form::select('scenes['.$period->id.']['.$nb.']', $scenes, $scene->id, ['class' => 'form-control scene-select', 'data-scene' => ($scene->id).($nb)]) }}
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <span class="btn btn-xs btn-info marg-top check-all off-disable" data-scene="{{ $scene->id }}{{ $nb }}">
                                                                                    <i class="fa fa-check-square-o fa-fw"></i> wybierz wszystkie role
                                                                                </span>
                                                                                <span class="btn btn-xs btn-warning marg-top uncheck-all off-disable" data-scene="{{ $scene->id }}{{ $nb }}">
                                                                                    <i class="fa fa-square-o fa-fw"></i> odznacz wszystkie role
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <h5 class="page-header marg-top-min">Role</h5>
                                                                            </div>
                                                                            <div class="scene-roles-container" data-scene="{{ $scene->id }}{{ $nb }}">
                                                                                @foreach($scene->roles as $role)
                                                                                    <div class="col-sm-6 col-md-4 col-lg-3">
                                                                                        <div class="checkbox">
                                                                                            <label>
                                                                                                <input type="checkbox" class="scene-role" name="roles[{{ $nb }}][]" data-scene="{{ $scene->id }}{{ $nb }}"  value="{{ $role->id }}" @if($period->roles->contains($role->id)) checked @endif>
                                                                                                {{ $role->name }} @if($role->wing)(<strong>{{ $role->wing->name }}</strong>)@endif
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-right">
                    {!! Form::submit('Zapisz zmiany', ['class' => 'btn btn-primary off-disable']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $('#periods-container').on('click', '.remove-perion', function(){
            $(this).parent().parent().remove();
        });

        $('#add-period').on('click', function(){
            $.ajax({
                type: "GET",
                url: '/admin/n-manage/timetables/period-add',
                assync:false,
                cache:false,
                dataType: 'html',
                beforeSend: function(){
                    $('#period-loader').show();
                },
                success: function( data ) {
                    $('#period-loader').hide();
                    $('#periods-container').prepend( data );
                }
            });
        });

        $('#periods-container').on('click', '.add-scene', function(){
            var $this = $(this);
            $.ajax({
                type: "GET",
                url: '/admin/n-manage/timetables/scene-add',
                data: {nb: $(this).data('period')},
                assync:false,
                cache:false,
                dataType: 'html',
                beforeSend: function(){
                    $this.parents('.period-container').find('.scene-loader').show();
                },
                success: function( data ) {
                    $this.parents('.period-container').find('.scene-loader').hide();
                    $this.parents('.period-container').find('.collapse').collapse('hide');
                    $this.parents('.period-container').find('.scenes-container').prepend( data );
                }
            });
        });

        $('#periods-container').on('click', '.delete-scene', function(){
            var scene = $(this).data('scene');
            $('.panel[data-scene="'+scene+'"]').remove();

        });

        $('#periods-container').on('click', '.delete-period', function(){
            $(this).parent().parent().remove();
        });

        $('#periods-container').on('change', '.scene-select', function(){
            var scene = $(this).data('scene');
            var name = $(this).find("option:selected").text();
            var scene_id = $(this).val();

            $('.panel[data-scene="'+scene+'"] .accordion-header').html('#Scena ' + name);

            $.ajax({
                type: "GET",
                url: '/admin/n-manage/timetables/roles-add',
                data: { scene_id: scene_id, nb: scene},
                assync:false,
                cache:false,
                dataType: 'html',
                beforeSend: function(){
                    $('.scene-roles-container[data-scene="'+scene+'"]').html('');
                },
                success: function( data ) {
                    $('.scene-roles-container[data-scene="'+scene+'"]').html(data);
                }
            });
        });

        $('#periods-container').on('click', '.check-all', function(){
            var scene = $(this).data('scene');

            $('.scene-role[data-scene="'+scene+'"]').prop('checked', true);
        });

        $('#periods-container').on('click', '.uncheck-all', function(){
            var scene = $(this).data('scene');

            $('.scene-role[data-scene="'+scene+'"]').prop('checked', false);
        });
    </script>
@endsection
