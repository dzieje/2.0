<div class="card marg-top-min period-container">
    <div class="card-header  text-right">
        <span class="btn btn-xs btn-danger delete-period off-disable">
            <i class="fa fa-remove"></i> usuń
        </span>
    </div>
    <div class="card-body">
        <div class="row ">
            <div class="form-group form-group-sm col-md-4">
                <label class="control-label">Przedział od:</label>

                <div class="input-group bootstrap-timepicker timepicker">
                    <input id="timepicker_from{{ $nb }}" name="date_from[{{ $nb }}]" type="text" data-provide="timepicker" data-show-meridian="false" class="form-control required">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                </div>

            </div>
            <div class="form-group form-group-sm col-md-4">
                <label class="control-label">Przedział do:</label><br/>
                <div class="input-group bootstrap-timepicker timepicker">
                    <input id="timepicker_to{{ $nb }}" name="date_to[{{ $nb }}]" type="text" data-provide="timepicker" data-show-meridian="false"  class="form-control required">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                </div>
            </div>
            <div class="form-group form-group-sm col-md-4 d-sm-flex align-items-center justify-content-center">
                <span class="btn btn-sm btn-primary add-scene off-disable" data-period="{{ $nb }}">
                    <i class="fa fa-plus fa-fw"></i>
                    dodaj scenę
                </span>
            </div>

            <div class="col-sm-12">
                <div class="text-center scene-loader" style="display: none;"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>
            </div>
            <div class="col-sm-12">
                <div class="scenes-container panel-group" role="tablist" aria-multiselectable="true">
                </div>
            </div>
        </div>
    </div>
</div>
