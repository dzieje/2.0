<div class="card" data-scene="{{ $nb }}">
    <div class="card-header " role="tab" id="heading{{ $nb }}">
        <h4 class="m-0 font-weight-bold d-sm-flex align-items-center justify-content-between">
            <a class="accordion-header" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $nb }}" aria-expanded="true" aria-controls="collapse{{ $nb }}">
                #Scena {{ $scene->name }}
            </a>
            <span class="btn btn-xs btn-danger delete-scene off-disable" data-scene="{{ $nb }}">
                <i class="fa fa-fw fa-trash-alt"></i> usuń scenę
            </span>
        </h4>
    </div>
    <div id="collapse{{ $nb }}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
        <div class="card-body">
            <div class="row">
                <div class="form-group form-group-sm col-sm-12 col-md-8">
                    <label class="control-label">Wskaż scenę:</label><br/>
                    {{ Form::select('scenes['.$period_nb.']['.$nb.']', $scenes, null, ['class' => 'form-control scene-select', 'data-scene' => $nb]) }}
                </div>
                <div class="col-md-4">
                    <span class="btn btn-xs btn-info marg-top check-all off-disable" data-scene="{{ $nb }}">
                        <i class="fa fa-check-square-o fa-fw"></i> wybierz wszystkie role
                    </span>
                    <span class="btn btn-xs btn-warning marg-top uncheck-all off-disable" data-scene="{{ $nb }}">
                        <i class="fa fa-square-o fa-fw"></i> odznacz wszystkie role
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h5 class="page-header marg-top-min">Role</h5>
                </div>
                <div class="scene-roles-container" data-scene="{{ $nb }}">
                    @foreach($scene->roles as $role)
                        <div class="col-sm-6 col-md-4 col-lg-3">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="scene-role" name="roles[{{ $nb }}][]" data-scene="{{ $nb }}" checked value="{{ $role->id }}">
                                    {{ $role->name }} @if($role->wing)(<strong>{{ $role->wing->name }}</strong>)@endif
                                </label>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
