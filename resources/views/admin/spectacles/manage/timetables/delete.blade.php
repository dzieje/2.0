<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Usuwanie próby </h4>
</div>
<div class="modal-body">
    <div class="panel-body">
        <form action="{{ url('admin/n-manage/timetables/delete', [$rehearsal->id]) }}" method="post" id="dialog-form">
            {!! Form::token() !!}
            <h4>Potwierdź usunięcie próby {{ $rehearsal->name }}</h4>
        </form>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa usuwanie">Usuń</button>
</div>