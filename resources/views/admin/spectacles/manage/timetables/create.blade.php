@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                {!! Form::open(['url' => url('admin/spectacle/'.$project->slug.'/manage/timetables/store'), 'id' => 'page-form']) !!}
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            Tworzenie nowej próby
                        </h6>
                        <a href="{{ URL::to('admin/spectacle/'.$project->slug.'/manage/timetables') }}" class="btn btn-default btn-xs ">
                            <i class="fa fa-remove fa-fw"></i> anuluj
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6 col-lg-4 col-lg-offset-2">
                                <div class="form-group">
                                    <label>Nazwa próby:</label>
                                    {!!  Form::text('name', '', array('class' => 'form-control', 'placeholder' => 'nazwa')) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label>Data próby:</label>
                                    {!!  Form::text('rehearsal', null, array('class' => 'form-control required date dynamic-datepicker', 'placeholder' => 'dzień próby')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Przedziały czasowe
                                    <span id="add-period" class="btn btn-sm btn-primary pull-right off-disable">
                                        <i class="fa fa-plus fa-fw"></i> dodaj przedział
                                    </span>
                                </h4>
                                <hr/>
                                <div class="text-center" id="period-loader" style="display: none;"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>
                                <div id="periods-container">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        {!! Form::submit('Utwórz próbę', ['class' => 'btn btn-primary off-disable']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $('#periods-container').on('click', '.remove-perion', function(){
            $(this).parent().parent().remove();
        });

        $('#add-period').on('click', function(){
            $.ajax({
                type: "GET",
                url: '/admin/spectacle/{{ $project->slug }}/manage/timetables/period-add',
                assync:false,
                cache:false,
                dataType: 'html',
                beforeSend: function(){
                    $('#period-loader').show();
                },
                success: function( data ) {
                    $('#period-loader').hide();
                    $('#periods-container').prepend( data );
                }
            });
        });

        $('#periods-container').on('click', '.add-scene', function(){
            var $this = $(this);
            $.ajax({
                type: "GET",
                url: '/admin/spectacle/{{ $project->slug }}/manage/timetables/scene-add',
                data: {nb: $(this).data('period')},
                assync:false,
                cache:false,
                dataType: 'html',
                beforeSend: function(){
                    $this.parents('.period-container').find('.scene-loader').show();
                },
                success: function( data ) {
                    $this.parents('.period-container').find('.scene-loader').hide();
                    $this.parents('.period-container').find('.collapse').collapse('hide');
                    $this.parents('.period-container').find('.scenes-container').prepend( data );
                }
            });
        });

        $('#periods-container').on('click', '.delete-scene', function(){
            var scene = $(this).data('scene');
            $('.panel[data-scene="'+scene+'"]').remove();

        });

        $('#periods-container').on('click', '.delete-period', function(){
            $(this).parent().parent().remove();
        });

        $('#periods-container').on('change', '.scene-select', function(){
            var scene = $(this).data('scene');
            var name = $(this).find("option:selected").text();
            var scene_id = $(this).val();

            $('.panel[data-scene="'+scene+'"] .accordion-header').html('#Scena ' + name);

            $.ajax({
                type: "GET",
                url: '/admin/spectacle/{{ $project->slug }}/manage/timetables/roles-add',
                data: { scene_id: scene_id, nb: scene},
                assync:false,
                cache:false,
                dataType: 'html',
                beforeSend: function(){
                    $('.scene-roles-container[data-scene="'+scene+'"]').html('');
                },
                success: function( data ) {
                    $('.scene-roles-container[data-scene="'+scene+'"]').html(data);
                }
            });
        });

        $('#periods-container').on('click', '.check-all', function(){
            var scene = $(this).data('scene');

            $('.scene-role[data-scene="'+scene+'"]').prop('checked', true);
        });

        $('#periods-container').on('click', '.uncheck-all', function(){
            var scene = $(this).data('scene');

            $('.scene-role[data-scene="'+scene+'"]').prop('checked', false);
        });
    </script>
@endsection
