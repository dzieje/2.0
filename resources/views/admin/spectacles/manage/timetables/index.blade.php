@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            Zarządzanie harmonogramem prób
                        </h6>
                        <div>
                            <a href="{{ url('admin/spectacle/'.$project->slug.'/manage/timetables/create') }}" class="btn btn-xs btn-primary pull-right">
                                <i class="fa fa-plus fa-fw"></i> dodaj próbę
                            </a>

                            <a href="{{ url('admin/spectacle/'.$project->slug.'/manage/timetables/generate-pdf') }}" class="btn btn-xs btn-primary pull-right marg-right">
                                <i class="fa fa-file-pdf fa-fw" aria-hidden="true"></i>
                                generuj zestawienie prób pdf
                            </a>
                            <a href="{{ url('admin/spectacle/'.$project->slug.'/manage/timetables/generate-xls') }}" class="btn btn-xs btn-success pull-right marg-right">
                                <i class="fa fa-file-excel fa-fw" aria-hidden="true"></i>
                                generuj zestawienie prób xls
                            </a>

                            <a href="{{ url('admin/spectacle/'.$project->slug.'/manage/timetables/attendance-xls') }}" class="btn btn-xs btn-success pull-right marg-right">
                                <i class="fa fa-file-excel fa-fw" aria-hidden="true"></i>
                                generuj zestawienie obecności
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive marg-top">
                            <table class="table table-sm table-hover">
                                <thead>
                                <th>#</th>
                                <th></th>
                                <th></th>
                                <th>Nazwa próby</th>
                                <th>Data próby</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <Th></Th>
                                </thead>
                                @foreach($rehearsals as $rehearsal)
                                    <tr>
                                        <td>{{ ($rehearsals->currentPage() - 1) * $rehearsals->perPage() + $loop->iteration  }}.</td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary btn-xs dropdown-toggle off-disable" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-file fa-fw" aria-hidden="true"></i> generuj <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-left">
                                                    <li>
                                                        <a href="{{ url('admin/spectacle/'.$project->slug.'/manage/timetables/generate-outline', [$rehearsal->id]) }}" >
                                                            <i class="fa fa-file-pdf-o fa-fw" aria-hidden="true"></i> konspekt
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="{{ url('admin/spectacle/'.$project->slug.'/manage/timetables/attendance', [$rehearsal->id]) }}" class="btn btn-success btn-xs">
                                                <i class="fa fa-users fa-fw"></i> lista obecności
                                            </a>
                                        </td>
                                        <td>{{ $rehearsal->name }}</td>
                                        <td>{{ $rehearsal->rehearsal->format('Y-m-d') }}</td>
                                        <td>
                                            <a href="{{ url('admin/spectacle/'.$project->slug.'/manage/timetables/edit', [$rehearsal->id]) }}" class="btn btn-xs btn-warning">
                                                <i class="fa fa-pencil-alt fa-fw"></i> zarządzaj próbą
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ url('admin/spectacle/'.$project->slug.'/manage/timetables/show', [$rehearsal->id]) }}" class="btn btn-xs btn-info">
                                                <i class="fa fa-search fa-fw"></i> szczegóły
                                            </a>
                                        </td>
                                        <td>
                                            <span target="{{ url('admin/spectacle/'.$project->slug.'/manage/timetables/delete', [$rehearsal->id]) }}" class="btn btn-xs btn-danger modal-open" data-toggle="modal" data-target="#modal">
                                                <i class="fa fa-trash-alt fa-fw"></i> usuń próbę
                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>

                    <div class="card-footer text-right">
                        {!! $rehearsals->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>

    </script>
@endsection
