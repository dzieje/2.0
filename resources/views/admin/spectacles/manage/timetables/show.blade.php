@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <a href="{{ url('admin/n-manage/timetables') }}" class="btn btn-sm btn-default pull-right">
                <i class="fa fa-reply fa-fw" aria-hidden="true"></i> wróć
            </a>
            <a href="{{ url('admin/n-manage/timetables/edit', [$rehearsal->id]) }}" class="btn btn-sm btn-warning marg-right pull-right">
                <i class="fa fa-pencil fa-fw"></i> zarządzaj próbą
            </a>
            <div class="page-header marg-top-min">
                <h2>Próba dnia {{ $rehearsal->rehearsal->format('Y-m-d') }} <small>{{ $rehearsal->name }}</small></h2>

            </div>
        </div>
        @foreach($rehearsal->periods as $period)
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                        {{ $period->date_from->format('H:i') }} - {{ $period->date_to->format('H:i') }}
                        </h3>
                    </div>
                    <div class="panel-body">
                        @foreach($period->scenes as $scene)
                            <h3 class="page-header marg-top-min"><small>scena:</small> {{ $scene->name }}</h3>
                            <div class="row">
                                @foreach($period->roles->where('n_scene_id', $scene->id)->all() as $role)
                                    <div class="col-sm-6 col-md-4 col-lg-3">
                                        <i class="fa fa-fw fa-minus"></i> {{ $role->name }} @if($role->wing)(<strong>{{ $role->wing->name }}</strong>)@endif
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection

@section('scripts')
    @parent
    <script>
    </script>
@endsection