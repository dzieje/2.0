<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Edycja sceny </h5>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="panel-body">
        <form action="{{ url('admin/spectacle/'.$project->slug.'/manage/scenes/update', [$scene->id]) }}" method="post" id="dialog-form">
            {!! csrf_field() !!}
            <div class="form-group">
                <label>Nazwa:</label>
                {!!  Form::text('name', $scene->name, array('class' => 'form-control required', 'placeholder' => 'nazwa')) !!}
            </div>
            <div class="form-group">
                <label>Godzina rozpoczęcia:</label>
                {!!  Form::time('time_start', $scene->time_start, array('class' => 'form-control ', 'step' => 1)) !!}
            </div>
            <div class="form-group">
                <label>Godzina zakończenia:</label>
                {!!  Form::time('time_end', $scene->time_end, array('class' => 'form-control ', 'step' => 1)) !!}
            </div>
        </form>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa zapisywanie">Zapisz</button>
</div>
