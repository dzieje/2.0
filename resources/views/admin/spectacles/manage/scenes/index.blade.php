@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                        Zarządzanie scenami
                        </h6>
                        <form action="{{ url('admin/spectacle/'.$project->slug.'/manage/scenes/stage-manager') }}" method="post" >
                            <span target="{{ url('admin/spectacle/'.$project->slug.'/manage/scenes/create') }}" class="btn btn-xs btn-primary modal-open" data-toggle="modal" data-target="#modal">
                                <i class="fa fa-plus fa-fw"></i> dodaj scenę
                            </span>
                            {!! csrf_field() !!}
                            <button type="submit" class="btn btn-xs btn-info ">
                                <i class="fa fa-file-excel fa-fw"></i> inspicjent
                            </button>
                        </form>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-hover">
                                <thead>
                                <th>#</th>
                                <th></th>
                                <th>Nazwa sceny</th>
                                <th>Czas</th>
                                <th>Przypisanych ról</th>
                                <Th></Th>
                                <th></th>
                                <Th></Th>
                                <th></th>
                                <Th></Th>
                                </thead>
                                @foreach($scenes as $scene)
                                    <tr>
                                        <td>{{ ($scenes->currentPage() - 1) * $scenes->perPage() + $loop->iteration  }}.</td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary btn-xs dropdown-toggle off-disable" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-file fa-fw" aria-hidden="true"></i> generuj <span class="caret"></span>
                                                </button>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item" href="{{ url('admin/spectacle/'.$project->slug.'/manage/scenes/generate-outline', [$scene->id, 'pdf']) }}" >
                                                        <i class="fa fa-file-pdf fa-fw" aria-hidden="true"></i> konspekt pdf
                                                    </a>
                                                    <a class="dropdown-item"  href="{{ url('admin/spectacle/'.$project->slug.'/manage/scenes/generate-outline', [$scene->id, 'xls']) }}" >
                                                        <i class="fa fa-file-excel fa-fw" aria-hidden="true"></i> konspekt xls
                                                    </a>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>{{ $scene->name }}</td>
                                        <td>
                                            {{ ($scene->time_start) }} - {{ ($scene->time_end) }}
                                        </td>
                                        <td>
                                            <span target="{{ url('admin/spectacle/'.$project->slug.'/manage/scenes/show-roles', [$scene->id]) }}" class="btn btn-xs btn-info modal-open-lg off-disable" data-toggle="modal" data-target="#modal-lg">
                                                <i class="fa fa-search fa-fw"></i> pokaż role <span class="badge badge-light">{{ $scene->roles->count() }} </span>
                                            </span>
                                        </td>
                                        <td>
                                            <span target="{{ url('admin/spectacle/'.$project->slug.'/manage/scenes/show-wings', [$scene->id]) }}" class="btn btn-xs btn-info modal-open-lg off-disable" data-toggle="modal" data-target="#modal-lg">
                                                <i class="fa fa-search fa-fw"></i> pokaż kulisy <span class="badge badge-light">{{ $scene->roles->pluck('wing.id')->unique()->count() }} </span>
                                            </span>
                                        </td>
                                        <td>
                                            <span target="{{ url('admin/spectacle/'.$project->slug.'/manage/scenes/edit', [$scene->id]) }}" class="btn btn-xs btn-warning modal-open" data-toggle="modal" data-target="#modal">
                                                <i class="fa fa-pencil-alt fa-fw"></i> edytuj
                                            </span>
                                        </td>
                                        <td>
                                            <span target="{{ url('admin/spectacle/'.$project->slug.'/manage/scenes/delete', [$scene->id]) }}" class="btn btn-xs btn-danger modal-open" data-toggle="modal" data-target="#modal">
                                                <i class="fa fa-trash-alt fa-fw"></i> usuń
                                            </span>
                                        </td>
                                        <td>
                                            <a href="{{ url('admin/spectacle/'.$project->slug.'/manage/scenes/export-emails', [$scene->id]) }}" class="btn btn-xs btn-primary">
                                                <i class="fa fa-file-excel fa-fw" aria-hidden="true"></i> exportuj maile xls
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ url('admin/spectacle/'.$project->slug.'/manage/scenes/export-phones', [$scene->id]) }}" class="btn btn-xs btn-primary">
                                                <i class="fa fa-file-excel fa-fw" aria-hidden="true"></i> exportuj tel. csv
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>

                    <div class="card-footer text-right">
                        {!! $scenes->appends(Request::query())->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>

    </script>
@endsection
