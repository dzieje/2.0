<div class="card mt-3">
    <div class="card-body">
        <label>Dostępne wybrane klasy:</label>
        <ul class="list-group">
            <li class="copy-parameter list-group-item text-center" data-clipboard-text="text-center">text-center</li>
            <li class="copy-parameter list-group-item text-danger" title="Skopiowane do schowka!" data-clipboard-text="text-danger">text-danger</li>
            <li class="copy-parameter list-group-item font-weight-bold"  title="Skopiowane do schowka!" data-clipboard-text="font-weight-bold">font-weight-bold</li>
            <li class="copy-parameter list-group-item font-italic"  title="Skopiowane do schowka!" data-clipboard-text="font-italic">font-italic</li>
        </ul>
    </div>
</div>

