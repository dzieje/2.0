@extends('layouts.form')

@section('content')
    <div class="h-100 w-100 position-absolute d-flex justify-content-center align-items-center">
        <div class="jumbotron text-center">
            {!! $survey->submitted_view !!}
        </div>
    </div>
@endsection
@section('scripts')
@endsection
