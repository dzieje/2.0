@extends('layouts.app')

@section('content')
    <link href="{{ asset('css/codemirror.css') }}" rel="stylesheet">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            Edycja formularza <i>{{ $survey->name }}</i>
                        </h6>
                        <a href="{{ URL::previous() }}" class="btn btn-default btn-xs ">
                            <i class="fa fa-remove fa-fw"></i> anuluj
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-5">
                                <form accept-charset="UTF-8" method="POST" id="page-form" action="{{ url('admin/project/'.$project->slug.'/surveys/update', [$survey->id]) }}">
                                    {!! csrf_field() !!}
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4>Pytania ankietowe
                                                        <span id="add-question" class="btn btn-primary btn-xs float-right off-disable">
                                                            <i class="fa fa-plus fa-fw"></i> dodaj
                                                        </span>
                                                    </h4>
                                                    <hr/>
                                                    <div class="text-center" id="question-loader" style="display: none;"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>
                                                    <div id="questions-container">
                                                        @foreach($survey->questions as $question)
                                                            <div class="mt-1">
                                                                <div class="row">
                                                                    <div class="form-group col-md-11">
                                                                        {!! Form::textarea('existing_questions['.$question->id.']', $question->question, ['class' =>'form-control required', 'placeholder' => 'treść pytania']) !!}
                                                                    </div>
                                                                    <div class="col-md-1 btn btn-xs btn-danger remove-question">
                                                                        <i class="fa fa-remove"></i> usuń
                                                                    </div>
                                                                </div>
                                                                <hr/>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 mt-2">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4>Pytania zgodowe
                                                        <span id="add-agreement" class="btn btn-primary btn-xs float-right off-disable">
                                                            <i class="fa fa-plus fa-fw"></i> dodaj
                                                        </span>
                                                    </h4>
                                                    <hr/>
                                                    <div class="text-center" id="agreement-loader" style="display: none;"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>
                                                    <div id="agreements-container">
                                                        @foreach($survey->agreements as $agreement)
                                                            <div class="mt-1">
                                                                <div class="row">
                                                                    <div class="form-group col-md-11">
                                                                        {!! Form::textarea('existing_agreements['.$agreement->id.']', $agreement->question, ['class' =>'form-control required', 'placeholder' => 'treść pytania']) !!}
                                                                    </div>
                                                                    <div class="col-md-1 btn btn-xs btn-danger remove-agreement">
                                                                        <i class="fa fa-remove"></i> usuń
                                                                    </div>
                                                                </div>
                                                                <hr/>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <textarea name="form" id="form-content" class="d-none"></textarea>
                                    <textarea name="css" id="form-css" class="d-none"></textarea>
                                </form>
                            </div>

                            <div class="col-lg-7">
                                <div class="card">
                                    <div class="card-body">
                                        <div id="fb-editor"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-8">
                                <hr>
                                <div class="card mt-3">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>Własne style:</label>
                                            <textarea class="form-control" id="form-css-source" >
                                                {{ $survey->css }}
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <hr>
                                <div class="card mt-3">
                                    <div class="card-body">
                                        <label>Dostępne wybrane klasy:</label>
                                        <ul class="list-group">
                                            <li class="list-group-item text-center">text-center</li>
                                            <li class="list-group-item text-danger">text-danger</li>
                                            <li class="list-group-item font-weight-bold">font-weight-bold</li>
                                            <li class="list-group-item font-italic">font-italic</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        <span class="btn btn-primary off-disable" id="submit">
                            <i class="fas fa-save fa-fw"></i>
                            Zapisz
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('js/form-builder.min.js') }}"></script>
    <script src="{{ asset('js/codemirror.js') }}"></script>
    <script src="{{ asset('js/mode/css.js') }}"></script>
    <script>
        let formBuilder;
        $.ajax({
            type: "GET",
            url: '/admin/project/{{$project->slug}}/surveys/fields',
            assync:false,
            cache:false,
            dataType: 'json',
            success: function( data ) {
                var options = {
                    i18n: {
                        locale: 'pl-PL',
                        location: '/lang'
                    },
                    scrollToFieldOnAdd:false,
                    disableFields: ['autocomplete', 'button','checkbox-group','date','file','hidden','number','radio-group','select','starRating','text','textarea'],
                    disabledAttrs: [
                        'access',
                        'inline',
                        'max',
                        'min',
                        'maxlength',
                        'multiple',
                        'other',
                        'rows',
                        'step',
                        'style',
                        'toggle',
                        'value'
                    ],
                    disabledFieldButtons: {
                        text: ['copy'],
                        select: ['copy'],
                        date: ['copy'],
                        'radio-group': ['copy']
                    },
                    disabledActionButtons: ['data', 'save'],
                    fields: data,
                    formData: '{!! $survey->form !!}'
                };
                formBuilder = $(document.getElementById('fb-editor')).formBuilder(options);
            }
        });

        $(document).ready(function(){
            let code = CodeMirror.fromTextArea(document.getElementById('form-css-source'),{
                lineNumbers: true,
                mode: "css"
            });

            $('#questions-container').on('click', '.remove-question', function(){
                $(this).parent().parent().remove();
            });

            $('#add-question').on('click', function(){
                $.ajax({
                    type: "GET",
                    url: '/admin/project/{{$project->slug}}/surveys/add-question',
                    assync:false,
                    cache:false,
                    dataType: 'html',
                    beforeSend: function(){
                        $('#question-loader').show();
                    },
                    success: function( data ) {
                        $('#question-loader').hide();
                        $('#questions-container').append( data );
                    }
                });
            });

            $('#agreements-container').on('click', '.remove-agreement', function(){
                $(this).parent().parent().remove();
            });

            $('#add-agreement').on('click', function(){
                $.ajax({
                    type: "GET",
                    url: '/admin/project/{{$project->slug}}/surveys/add-agreement',
                    assync:false,
                    cache:false,
                    dataType: 'html',
                    beforeSend: function(){
                        $('#agreement-loader').show();
                    },
                    success: function( data ) {
                        $('#agreement-loader').hide();
                        $('#agreements-container').append( data );
                    }
                });
            });

            $('#submit').on('click', function(e){
                e.preventDefault();
                $('#form-content').val( formBuilder.actions.getData('json') );
                $('#form-css').val( code.getValue());

                $('#page-form').submit();
            });
        });

    </script>
@endsection
