<div class="modal-body">
    {!!  (new \App\Mail\Mailing($setting, $template))->render() !!}
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
</div>
