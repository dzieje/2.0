@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-lg-10 ">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                        Formularze
                        </h6>
                        <span target="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/surveys/create') }}" class="btn btn-xs btn-primary modal-open-lg" data-toggle="modal" data-target="#modal-lg">
                            <i class="fa fa-fw fa-plus"></i> stwórz nowy
                        </span>
                    </div>
                    <table class="table table-sm table-hover">
                        <thead>
                        <th>#</th>
                        <th>nazwa</th>
                        <th>rejestracyjny</th>
                        <th>aktywny od</th>
                        <th>aktywny do</th>
                        <th></th>
                        <th></th>
                        <th></th>

                        <th></th>
                        </thead>
                        @foreach($surveys as $survey)
                            <tr>
                                <td>{{ ($surveys->currentPage() - 1) * $surveys->perPage() + $loop->iteration  }}.</td>
                                <td>{{ $survey->name }}</td>
                                <td>@if( $survey->registration ) <i class="fa fa-check"></i> @else <i class="fa fa-minus"></i> @endif</td>
                                <td>{{ optional($survey->available_from)->format('Y-m-d') }}</td>
                                <td>{{ optional($survey->available_to)->format('Y-m-d') }}</td>
                                <td>
                                    <a href="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/surveys/entries', [$survey->id]) }}" class="btn btn-xs btn-info">
                                        <i class="fa fa-fw fa-eye"></i> wypełniło <span class="badge badge-light">{{ $survey->entries->count() }}/{{ \App\Models\Volunteer::whereNotNull('timesheet_date')->project($project->id)->count() }}</span>
                                    </a>
                                </td>
                                <td>
                                    @if($survey->registration)
                                        <span target="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/surveys/link', [$survey->id]) }}" class="btn btn-xs btn-outline-info modal-open-lg" data-toggle="modal" data-target="#modal-lg">
                                            <i class="fas fa-external-link-alt fa-fw"></i>
                                            link do formularza
                                        </span>
                                    @endif
                                </td>

                                <td>
                                    <a href="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/surveys/manage', [$survey->id]) }}" class="btn btn-xs btn-primary">
                                        <i class="fa fa-tools fa-fw"></i> zarządzaj
                                    </a>
                                </td>
                                <td>
                                    <span target="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/surveys/delete', [$survey->id]) }}" class="btn btn-xs btn-danger modal-open" data-toggle="modal" data-target="#modal">
                                        <i class="fa fa-trash-alt fa-fw"></i> usuń
                                    </span>
                                </td>

                                <td>
                                    <a href="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/surveys/duplicate', [$survey->id]) }}" class="btn btn-outline-dark btn-xs">
                                        <i class="fa fa-fw fa-copy"></i> duplikuj
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </table>

                    <div class="card-footer text-right">
                        {!! $surveys->appends(Request::query())->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endsection
