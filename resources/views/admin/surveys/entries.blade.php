@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-lg-8">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                        Karty formularza <i>{{ $survey->name }}</i>
                        </h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <h4 class="text-success">Wypełnili</h4>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                    <th>#</th>
                                    <th>Aktor</th>
                                    <th>Data wypełnienia</th>
                                    <th></th>
                                    <th></th>
                                    </thead>
                                    @foreach($survey->entries as $k => $entry)
                                        <tr @if( $entry->volunteer->trashed()) class="bg-danger" @endif >
                                            <td>{{ ++$k }}.</td>
                                            <td>{{ $entry->volunteer->full_name }}</td>
                                            <td>{{ $entry->created_at->format('Y-m-d H:i') }}</td>
                                            <td>
                                                <span class="btn btn-info btn-xs modal-open" target="{{ url('admin/surveys/show', [$entry->id]) }}" data-toggle="modal" data-target="#modal">
                                                    <i class="fa fa-fw fa-eye"></i> formularz
                                                </span>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <div class="col-lg-6">
                                <h4 class="text-danger">Zaległe</h4>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                    <th>#</th>
                                    <th>Aktor</th>
                                    <th></th>
                                    </thead>
                                    @foreach($actors as $k => $actor)
                                        <tr>
                                            <td>{{ ++$k }}.</td>
                                            <td>{{ $actor->full_name }}</td>
                                            <td>
                                                <a href="{{ url('surveys/open', [$actor->registration_token]) }}" class="btn btn-primary btn-xs">
                                                    <i class="fa fa-sign-in-alt fa-fw"></i> wypełnij
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endsection
