@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="{{ asset('css/summernote-bs4.min.css') }}">
    <div class="container-fluid">
        <form id="page-form" method="post"
              action="{{ url('admin/project/'.$project->slug.'/surveys/update-response-mail', [$survey->id]) }}">
            {!! csrf_field() !!}
            <div class="card shadow">
                <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                    <h6 class="m-0 font-weight-bold text-info">
                        Mail zwrotny formularza
                    </h6>
                    <a href="{{url('admin/'.$project->type->slug.'/'.$project->slug.'/surveys/manage', [$survey->id]) }}" class="btn btn-default btn-xs ">
                        <i class="fa fa-remove fa-fw"></i> anuluj
                    </a>
                </div>
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-sm-12 col-md-8">
                            <div class="card ">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Tytuł maila:</label>
                                        <input maxlength="255" class="form-control required" required name="response_mail_subject" value="{{ $survey->response_mail_subject }}"
                                               placeholder="tytuł maila">
                                    </div>
                                    <div class="form-group">
                                        <label>Treść maila:</label>
                                        <textarea class="form-control required" name="response_mail_content" id="mail-content">
                                           {{ $survey->response_mail_content }}
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="card ">
                                <div class="card-body">
                                    @include('admin.sending.parameters')
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-footer text-center">
                    <button type="submit" class="btn btm-sm btn-info">
                        <i class="fas fa-save fa-fw"></i>
                        Zapisz
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="/js/clipboard.min.js"></script>
    <script src="/js/summernote-bs4.min.js"></script>
    <script src="/js/summernote-pl-PL.js"></script>
    <script>
        $('.copy-parameter').tooltip();
        var clipboard = new ClipboardJS('.copy-parameter', {
            text: function(trigger) {
                return trigger.getAttribute('key');
            }
        });

        clipboard.on('success', function(e) {
            $(e.trigger).tooltip('show');

            webwizards.sleep(1000).then(() => {
                $(e.trigger).tooltip('hide');
            });

            e.clearSelection();
        });

        const FMButton = function (context) {
            const ui = $.summernote.ui;
            const button = ui.button({
                contents: '<i class="note-icon-picture"></i> ',
                tooltip: 'File Manager',
                click: function () {
                    window.open('/file-manager/summernote', 'fm', 'width=1400,height=800');
                }
            });
            return button.render();
        };

        $('#mail-content').summernote({
            lang: 'pl-PL',
            minHeight: 300,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['misc', ['codeview', 'undo', 'redo', 'fullscreen']],
                ['fm-button', ['fm']],
            ],
            buttons: {
                fm: FMButton
            }
        });

        function fmSetLink($url) {
            $('#mail-content').summernote('insertImage', $url);
        }
    </script>
@endsection
