<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Formularz {{ $entry->volunteer->name }} {{ $entry->volunteer->surname }}</h4>
</div>
<div class="modal-body">
    <div class="panel-body">
        @foreach($entry->answers as $k => $answer)
            <p class="text-center">
                {{ ++$k }}) {{ $answer->question->question }}
                <br>
                @if($answer->answer)
                    <span class="label label-success">TAK</span>
                @else
                    <span class="label label-danger">NIE</span>
                @endif
            </p>
        @endforeach
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
</div>