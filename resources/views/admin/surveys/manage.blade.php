@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            Zarządzanie formularzem <i>{{ $survey->name }}</i>
                        </h6>
                        <div>
                            <a href="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/surveys/preview', [$survey->id]) }}" class="btn btn-outline-info btn-xs" target="_blank">
                                <i class="fas fa-eye fa-fw"></i> podgląd
                            </a>
                            <a href="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/surveys') }}" class="btn btn-default btn-xs ">
                                <i class="fas fa-arrow-alt-circle-left fa-fw"></i> powrót
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12 mb-4">
                                <div class="card border-info">
                                    <div class="card-body">
                                        <p class="h4 border-left-info  pl-2 d-flex justify-content-between align-items-center">
                                            Dane

                                            <span target="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/surveys/edit-base', [$survey->id]) }}" class="btn btn-xs btn-warning modal-open-lg" data-toggle="modal" data-target="#modal-lg">
                                                <i class="fa fa-fw fa-pencil-alt"></i> edytuj
                                            </span>
                                        </p>
                                        <hr>
                                        <dl class="row mb-0">
                                            <dt class="col-sm-2">Nazwa</dt>
                                            <dd class="col-sm-4"> {{ $survey->name }}</dd>
                                            <dt class="col-sm-2">Formularz rejestracyjny</dt>
                                            <dd class="col-sm-4">
                                                @if($survey->registration)
                                                    <i class="fas fa-check fa"></i>
                                                @else
                                                    <i class="fas fa-minus fa-fw"></i>
                                                @endif
                                            </dd>
                                            <dt class="col-sm-2">Dostępny od</dt>
                                            <dd class="col-sm-4"> {{ optional($survey->available_from)->format('Y-m-d') }}</dd>
                                            <dt class="col-sm-2">Dostępny do</dt>
                                            <dd class="col-sm-4"> {{ optional($survey->available_to)->format('Y-m-d') }}</dd>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="card border-info">
                                    <div class="card-body">
                                        <p class="h4 border-left-info  pl-2 d-flex justify-content-between align-items-center">
                                            Formularz
                                            <a href="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/surveys/edit', [$survey->id]) }}" class="btn btn-warning btn-xs">
                                                <i class="fas fa-pencil-alt fa-fw"></i> edytuj
                                            </a>
                                        </p>
                                        <hr>
                                        <div class="row">
                                            @if($survey->form )
                                                @foreach(json_decode( $survey->form ) as $field)
                                                    @include('admin.surveys.fields.'.$field->type)
                                                @endforeach
                                            @endif
                                            <div class="col-sm-12">
                                                <hr>
                                            </div>
                                            @foreach($survey->questions as $question)
                                                <div class="col-sm-12">
                                                    {{ $question->question }}
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio"  disabled>
                                                            <label class="form-check-label" >
                                                                TAK
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio"  disabled>
                                                            <label class="form-check-label" >
                                                                NIE
                                                            </label>
                                                        </div>
                                                </div>
                                            @endforeach
                                            <div class="col-sm-12">
                                                <hr>
                                            </div>
                                            @foreach($survey->agreements as $agreement)
                                                <div class="col-sm-12">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox"  disabled>
                                                        <label class="form-check-label" >
                                                            {{ $agreement->question }}
                                                        </label>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="card border-info mb-4">
                                    <div class="card-body">
                                        <h4 class="border-left-info  pl-2 d-flex justify-content-between align-items-center">
                                            Widok porejestracyjny
                                            <div>
                                                <a class="btn btn-info btn-xs" target="_blank" href="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/surveys/preview-submitted-view', [$survey->id]) }}" >
                                                    <i class="fas fa-eye fa-fw"></i>
                                                    podgląd
                                                </a>
                                                <a href="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/surveys/submitted-view', [$survey->id]) }}" class="btn btn-warning btn-xs">
                                                    <i class="fas fa-pencil-alt fa-fw"></i> edytuj
                                                </a>
                                            </div>
                                        </h4>
                                    </div>
                                </div>
                                <div class="card border-info">
                                    <div class="card-body">
                                        <h4 class="border-left-info  pl-2 d-flex justify-content-between align-items-center">
                                            Mail zwrotny
                                            <div>
                                                <span class="btn btn-info btn-xs  modal-open-lg" data-toggle="modal" data-target="#modal-lg" target="{{ url('admin/volunteers/select-volunteer?url=').url('admin/'.$project->type->slug.'/'.$project->slug.'/surveys/preview-response-mail', [$survey->id]) }}" >
                                                    <i class="fas fa-eye fa-fw"></i>
                                                    podgląd
                                                </span>
                                                <a href="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/surveys/response-mail', [$survey->id]) }}" class="btn btn-warning btn-xs">
                                                    <i class="fas fa-pencil-alt fa-fw"></i> edytuj
                                                </a>
                                            </div>
                                        </h4>
                                        <hr>
                                        <p>
                                            <strong>Tytuł:</strong>
                                             {{ $survey->response_mail_subject }}
                                        </p>
                                        {!! $survey->response_mail_content !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent

@endsection
