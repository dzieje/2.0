<div class="col-sm-12">
    <div class="question">
        @if($field->attribute->multiple_choice)
            <div class="ac-custom ac-checkbox ac-boxfill">
                <div class="row  ">
                    <div class="col-sm-12 col-md-6 col-lg-4 col-md-offset-3 col-lg-offset-4">
                        <ul class="options">
                            @foreach(json_decode( $field->attribute->description ) as $k => $attribute_value )
                                <li class="pt-2 pb-2" >
                                    <input data-instance="{{ $field->name }}" id="{{$field->name}}_{{ $k }}" name="{{ $field->name }}[]" disabled type="checkbox" value="{{ $attribute_value }}" required data-area="{{ $attribute_value }}" />
                                    <label for="{{$field->name}}_{{ $k }}" class="text-uppercase" data-area="{{ $attribute_value }}"/>{{ $attribute_value }}</label>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @else
            <div class="ac-custom ac-radio ac-fill">
                <div class="row  ">
                    <div class="col-sm-12 col-md-6 col-lg-4 col-md-offset-3 col-lg-offset-4">
                        <ul class="options">
                            @foreach(json_decode( $field->attribute->description ) as $k => $attribute_value )
                                <li class="pt-2 pb-2" >
                                    <input data-instance="{{ $field->name }}" id="{{$field->name}}_{{ $k }}" name="{{ $field->name }}[]" type="radio" value="{{ $attribute_value }}" required data-area="{{ $attribute_value }}" />
                                    <label for="{{$field->name}}_{{ $k }}" class="text-uppercase" data-area="{{ $attribute_value }}"/>{{ $attribute_value }}</label>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
