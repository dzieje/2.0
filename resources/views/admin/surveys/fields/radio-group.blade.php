<div class="col-sm-12 my-2">
    @foreach($field->values as $value)
        <div class="form-check">
            <input class="form-check-input {{ property_exists($field, 'className') ? $field->className : null }}" type="radio" name="{{ $field->name }}" disabled>
            <label class="form-check-label">
                {{ $value->label }}
            </label>
        </div>
    @endforeach
</div>
