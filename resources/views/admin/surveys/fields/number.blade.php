<div class="col-sm-12 col-md-6 my-2">
    <input class="{{ property_exists($field, 'className') ? $field->className : null }}" type="number" placeholder="{{ $field->label }}" disabled>
</div>
