<div class="col-sm-12 my-2">
    <{{ $field->subtype }} class="{{ property_exists($field, 'className') ? $field->className : null }}">
        {!!  property_exists($field, 'label') ? $field->label : null !!}
    </{{ $field->subtype }}>
</div>
<div class="col-sm-12">
    <hr>
</div>
