<div class="col-sm-12 col-md-6 my-2">
    <div class="question">
        <input class="{{ property_exists($field, 'className') ? $field->className : null }}" type="date" placeholder="{{ $field->label }}" disabled>
    </div>
</div>
