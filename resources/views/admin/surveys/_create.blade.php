@extends('layouts.app')

@section('content')
    <link href="{{ asset('css/codemirror.css') }}" rel="stylesheet">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                        Tworzenie nowego formularza
                        </h6>
                        <a href="{{ URL::previous() }}" class="btn btn-default btn-xs ">
                            <i class="fa fa-remove fa-fw"></i> anuluj
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-5">
                            <form accept-charset="UTF-8" method="POST" id="page-form" action="{{ url('admin/spectacle/'.$project->slug.'/surveys/store') }}">
                                {!! csrf_field() !!}
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Nazwa:</label>
                                                {!!  Form::text('name', '', array('class' => 'form-control required', 'required', 'placeholder' => 'nazwa')) !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group">
                                                <label>Dostępny od:</label>
                                                {!!  Form::date('available_from', null, array('class' => 'form-control required', 'required')) !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group">
                                                <label>Dostępny do:</label>
                                                {!!  Form::date('available_to', null, array('class' => 'form-control required', 'required' )) !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-check text-center">
                                                <input class="form-check-input" type="checkbox" name="registration" value="1" id="registration">
                                                <label class="form-check-label" for="registration">Formularz rejestracyjny</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <hr/>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4>Pytania ankietowe
                                                        <span id="add-question" class="btn btn-primary btn-xs float-right off-disable">
                                                            <i class="fa fa-plus fa-fw"></i> dodaj
                                                        </span>
                                                    </h4>
                                                    <hr/>
                                                    <div class="text-center" id="question-loader" style="display: none;"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>
                                                    <div id="questions-container">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <textarea name="form" id="form-content" class="d-none"></textarea>
                                <textarea name="css" id="form-css" class="d-none"></textarea>
                            </form>
                            </div>

                            <div class="col-lg-7">
                                <div class="card">
                                    <div class="card-body">
                                        <div id="fb-editor"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-8">
                                <hr>
                                <div class="card mt-3">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>Własne style:</label>
                                            <textarea class="form-control" id="form-css-source" ></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <hr>
                                @include('admin.spectacles.surveys.partials.classes')
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        <span class="btn btn-primary off-disable" id="submit">
                            <i class="fas fa-save fa-fw"></i>
                            Zapisz
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('js/form-builder.min.js') }}"></script>
    <script src="{{ asset('js/codemirror.js') }}"></script>
    <script src="{{ asset('js/mode/css.js') }}"></script>

    <script>
        let formBuilder;
        $.ajax({
            type: "GET",
            url: '/admin/spectacle/{{$project->slug}}/surveys/fields',
            assync:false,
            cache:false,
            dataType: 'json',
            success: function( data ) {
                var options = {
                    i18n: {
                        locale: 'pl-PL',
                        location: '/lang'
                    },
                    scrollToFieldOnAdd:false,
                    disableFields: ['autocomplete', 'button','checkbox-group','date','file','hidden','number','radio-group','select','starRating','text','textarea'],
                    disabledAttrs: [
                        'access',
                        'inline',
                        'max',
                        'min',
                        'maxlength',
                        'multiple',
                        'other',
                        'rows',
                        'step',
                        'style',
                        'toggle',
                        'value'
                    ],
                    disabledFieldButtons: {
                        text: ['copy'],
                        select: ['copy'],
                        date: ['copy'],
                        'radio-group': ['copy']
                    },
                    disabledActionButtons: ['data', 'save'],
                    fields: data
                };
                formBuilder = $(document.getElementById('fb-editor')).formBuilder(options);
            }
        });

        $(document).ready(function(){
            let code = CodeMirror.fromTextArea(document.getElementById('form-css-source'),{
                lineNumbers: true,
                mode: "css"
            });

            $('#questions-container').on('click', '.remove-question', function(){
                $(this).parent().parent().remove();
            });

            $('#add-question').on('click', function(){
                $.ajax({
                    type: "GET",
                    url: '/admin/spectacle/{{$project->slug}}/surveys/add-question',
                    assync:false,
                    cache:false,
                    dataType: 'html',
                    beforeSend: function(){
                        $('#question-loader').show();
                    },
                    success: function( data ) {
                        $('#question-loader').hide();
                        $('#questions-container').append( data );
                    }
                });
            });

            $('#submit').on('click', function(){

                $('#form-content').append( formBuilder.actions.getData('json') );
                $('#form-css').append( code.getValue());
                $('#page-form').submit();
            });
        });

    </script>
@endsection
