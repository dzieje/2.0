<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Tworzenie formularza</h5>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="panel-body">
        <form action="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/surveys/store') }}" method="post" id="dialog-form">
            {!! csrf_field() !!}
            <div class="row">
                <div class="form-group col-12">
                    <label>Nazwa:</label>
                    {!!  Form::text('name', '', array('class' => 'form-control required', 'required', 'placeholder' => 'nazwa')) !!}
                </div>
                <div class="form-group col-6">
                    <label>Dostępny od:</label>
                    {!!  Form::date('available_from', null, array('class' => 'form-control required', 'required')) !!}
                </div>
                <div class="form-group col-6">
                    <label>Dostępny do:</label>
                    {!!  Form::date('available_to', null, array('class' => 'form-control required', 'required' )) !!}
                </div>
                <div class="form-check text-center col-12">
                    <input class="form-check-input" type="checkbox" name="registration" value="1" id="registration">
                    <label class="form-check-label" for="registration">Formularz rejestracyjny</label>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa wykonywanie">Utwórz</button>
</div>
