<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Link do formularza</h5>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body text-center">
    <span id="copy-parameter" data-clipboard-text="{{ url('form/open', [$survey->slug]) }}" class="btn btn-outline-info copy-parameter" data-toggle="tooltip" data-trigger="manual" data-placement="top" title="Skopiowane do schowka!">
        <i class="fa fa-clipboard fa-fw"></i> {{ url('form/open', [$survey->slug]) }}
    </span>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
</div>
<script src="/js/clipboard.min.js"></script>
<script>
    function sleep (time) {
        return new Promise((resolve) => setTimeout(resolve, time));
    }

    $('.copy-parameter').tooltip();

    var clipboard = new ClipboardJS('.copy-parameter', {
        container: document.getElementById('copy-parameter')
    });

    clipboard.on('success', function(e) {
        $(e.trigger).tooltip('show');

        sleep(1000).then(() => {
            $(e.trigger).tooltip('hide');
        });

       e.clearSelection();
    });
</script>
