<div class="mt-2">
    <div class="row">
        <div class="form-group col-md-11">
            {!! Form::textarea('agreements[]', null, ['class' =>'form-control required', 'placeholder' => 'treść pytania']) !!}
        </div>
        <div class="col-md-1 d-flex align-items-center justify-content-center btn btn-xs btn-danger remove-agreement">
            <i class="fa fa-trash-alt fa-fw"></i> usuń
        </div>
    </div>
    <hr/>
</div>
