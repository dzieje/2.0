@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="{{ asset('css/summernote-bs4.min.css') }}">
    <div class="container-fluid">
        <form id="page-form" method="post"
              action="{{ url('admin/project/'.$project->slug.'/surveys/submitted-view', [$survey->id]) }}">
            {!! csrf_field() !!}
            <div class="card shadow">
                <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                    <h6 class="m-0 font-weight-bold text-info">
                        Widok porejestracyjny
                    </h6>
                    <a href="{{url('admin/'.$project->type->slug.'/'.$project->slug.'/surveys/manage', [$survey->id]) }}" class="btn btn-default btn-xs ">
                        <i class="fa fa-remove fa-fw"></i> anuluj
                    </a>
                </div>
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-sm-12">
                            <div class="card ">
                                <div class="card-body">
                                    <div class="form-group">
                                        <textarea class="form-control required" name="submitted_view" id="submitted-content">
                                           {{ $survey->submitted_view }}
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-footer text-center">
                    <button type="submit" class="btn btm-sm btn-info">
                        <i class="fas fa-save fa-fw"></i>
                        Zapisz
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="/js/summernote-bs4.min.js"></script>
    <script src="/js/summernote-pl-PL.js"></script>
    <script>
        const FMButton = function (context) {
            const ui = $.summernote.ui;
            const button = ui.button({
                contents: '<i class="note-icon-picture"></i> ',
                tooltip: 'File Manager',
                click: function () {
                    window.open('/file-manager/summernote', 'fm', 'width=1400,height=800');
                }
            });
            return button.render();
        };

        $('#submitted-content').summernote({
            lang: 'pl-PL',
            minHeight: 300,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['misc', ['codeview', 'undo', 'redo', 'fullscreen']],
                ['fm-button', ['link', 'fm']],
            ],
            buttons: {
                fm: FMButton
            }
        });

        function fmSetLink($url) {
            $('#submitted-content').summernote('insertImage', $url);
        }
    </script>
@endsection
