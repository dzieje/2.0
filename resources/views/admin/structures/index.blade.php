@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-10 col-lg-8">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            Struktury
                        </h6>
                        <span target="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/structures/create') }}" class="btn btn-xs btn-primary modal-open-lg" data-toggle="modal" data-target="#modal-lg">
                            <i class="fa fa-fw fa-plus"></i> stwórz nową
                        </span>
                    </div>
                    <table class="table table-sm table-hover">
                        <thead>
                        <th>#</th>
                        <th>nazwa</th>
                        <th>osoba odpowiedzialna</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        </thead>
                        @foreach($structures as $structure)
                            <tr>
                                <td>{{ $loop->iteration  }}.</td>
                                <td>{{ $structure->name }}</td>
                                <td>
                                    {{ implode(', ', $structure->nodes->first()->leaders->pluck('name')->toArray() ) }}
                                </td>
                                <td>
                                    <span target="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/structures/edit', [$structure->id]) }}" class="btn btn-xs btn-warning modal-open-lg" data-toggle="modal" data-target="#modal-lg">
                                        <i class="fa fa-fw fa-pencil-alt"></i> edytuj </span>
                                    </span>
                                </td>
                                <td>
                                    <a href="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/structures/manage', [$structure->id]) }}" class="btn btn-xs btn-primary">
                                        <i class="fa fa-tools fa-fw"></i> zarządzaj
                                    </a>
                                </td>
                                <td>
                                    <span target="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/structures/delete', [$structure->id]) }}" class="btn btn-xs btn-danger modal-open" data-toggle="modal" data-target="#modal">
                                        <i class="fa fa-trash-alt fa-fw"></i> usuń
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endsection
