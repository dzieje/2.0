<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Dodawanie wolontariuszy</h5>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <form action="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/structures/add-volunteer', [$node->id]) }}" method="post" id="dialog-form">
        {!! csrf_field() !!}
            <div class="form-group">
                <label>Wolontariusz:</label>
                {!! Form::text('volunteer', null, ['class' => 'form-control', 'id' => 'search-volunteers', 'autocomplete' => 'off', 'placeholder' => 'wyszukaj wolontariusza'])!!}
            </div>
            <hr>
            <div class="text-center" id="actor-loader" style="display: none;"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>
            <div id="volunteers-container" >
                <table class="table table-sm table-hover">
                    <thead>
                    <th>imię</th>
                    <th>nazwisko</th>
                    <th>telefon</th>
                    <th>email</th>
                    <th></th>
                    </thead>

                </table>
            </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa wykonywanie">Dodaj</button>
</div>

<script src="/js/bloodhound.js"></script>
<script src="/js/typeahead.jquery.js"></script>
<script>
    var volunteers = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/structures/search-volunteer') }}',
            replace: function(url, query) { return url + "?limit=15&q=" + query; },
            filter: function (data) {
                return $.map(data.volunteers, function (volunteer) {
                    return volunteer
                });
            }
        }
    });

    var volunteersTypeahead = $('#search-volunteers').typeahead({
            minLength: 2,
            highlight: true,
            hint: true
        },
        { name: 'name', displayKey: 'name', minLength: 2, source: volunteers, limit: 15 });

    $('#search-volunteers').on('typeahead:selected', function(ev, datum) {
        var volunteer_id = datum.id;

        $.ajax({
            type: "GET",
            url: '/admin/spectacle/{{ $project->slug }}/manage/groups/load-actor',
            data: { volunteer_id: volunteer_id },
            assync:false,
            cache:false,
            dataType: 'html',
            beforeSend: function(){
                $('#actor-loader').show();
            },
            success: function( data ) {
                $('#actor-loader').hide();
                $('#volunteers-container table').append( data );
                volunteersTypeahead.typeahead('val','');
            }
        });
    });

    $('#search-volunteers').on('typeahead:close', function(){
        $(this).val('');
    });

    $('#volunteers-container').on('click', '.detach-actor', function(){
        var actor_id = $(this).data('actor');
        $('tr[data-actor="'+actor_id+'"]').remove();
    });
</script>
