<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Odpinanie wolontariusza</h5>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="panel-body">
        <form action="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/structures/detach-volunteer', [$node->id, $volunteer->id]) }}" method="post" id="dialog-form">
            {!! csrf_field()!!}
            <p>
                Potwierdź odpięcie <i>{{ $volunteer->full_name }}</i>.
            </p>
        </form>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa wykonywanie">Odepnij</button>
</div>
