@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-10 col-lg-8">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            Wolontariusze w ramach <i class="text-muted">{{ $node->name }}</i>
                        </h6>
                        <div>
                            <span target="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/structures/add-volunteer', [$node->id]) }}" class="btn btn-xs btn-info modal-open" data-toggle="modal" data-target="#modal">
                                <i class="fa fa-fw fa-plus"></i> dodaj
                            </span>
                            <a href="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/structures/manage', [$node->structure_id]) }}" class="btn btn-xs btn-outline-dark">
                                <i class="fas fa-arrow-alt-circle-left fa-fw"></i> powrót
                            </a>
                        </div>
                    </div>
                    <table class="table table-sm table-hover">
                        <thead>
                        <th>#</th>
                        <th>Nazwisko</th>
                        <th>Imię</th>
                        <th>Telefon</th>
                        <Th>Email</Th>
                        <th></th>
                        <th></th>
                        </thead>
                        @foreach($volunteers as $volunteer)
                            <tr>
                                <td>{{ ($volunteers->currentPage() - 1) * $volunteers->perPage() + $loop->iteration  }}.</td>
                                <td>{{ $volunteer->surname }}</td>
                                <td>{{ $volunteer->name }}</td>
                                <td>{{ $volunteer->phone }}</td>
                                <td>{{ $volunteer->email }}</td>
                                <td>
                                    <span target="{{ url('admin/project/'.$project->slug.'/participants/show', [$volunteer->id]) }}" class="btn btn-info btn-xs modal-open-lg off-disable" data-toggle="modal" data-target="#modal-lg" data-backdrop="true">
                                        <i class="fa fa-search fa-fw"></i> szczegóły
                                    </span>
                                </td>
                                <td>
                                    <span target="{{url('admin/'.$project->type->slug.'/'.$project->slug.'/structures/detach-volunteer', [$node->id, $volunteer->id]) }}" class="btn btn-xs btn-danger modal-open" data-toggle="modal" data-target="#modal">
                                        <i class="fa fa-trash-alt fa-fw"></i> odepnij
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <div class="card-footer text-right">
                        {!! $volunteers->appends(Request::query())->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endsection
