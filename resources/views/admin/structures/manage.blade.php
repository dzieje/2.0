@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            Zarządzanie strukturą <i class="text-muted">{{ $structure->name }}</i>
                        </h6>

                        <a href="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/structures') }}" class="btn btn-xs btn-outline-dark">
                            <i class="fa fa-fw fa-arrow-alt-circle-left"></i> powrót
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="structure-chart" id="structure-chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <link rel="stylesheet" href="{{ asset('vendor/treant/Treant.css') }}">
    <style>

    </style>
    <script src="{{ asset('vendor/treant/raphael.js') }}"></script>
    <script src="{{ asset('vendor/treant/Treant.js') }}"></script>

    <script>

        let chart_config = {
            chart: {
                container: "#structure-chart",
                levelSeparation: 45,

                rootOrientation: "NORTH",
                connectors: {
                    type: "step",
                    style: {
                        'arrow-end': 'classic-wide-long'
                    }
                },
                node: {
                    HTMLclass: "border rounded rounded-lg p-2 text-center"
                }
            }
        };


        $.ajax({
            type: "GET",
            url: '/admin/{{$project->type->slug}}/{{$project->slug}}/structures/load-nodes',
            data: {structure_id: '{{ $structure->id }}'},
            assync: false,
            cache: false,
            dataType: 'json',

            success: function (data) {
                chart_config.nodeStructure = data;
                new Treant( chart_config );
            }
        });

    </script>
@endsection
