<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">{{ __('Edycja struktury') }}</h5>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <form action="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/structures/update', [$structure->id]) }}" method="post" id="dialog-form">
        {!! csrf_field() !!}
        <div class="form-group ">
            <label>Nazwa:</label>
            {!!  Form::text('name', $structure->name, array('class' => 'form-control required', 'required', 'placeholder' => 'nazwa')) !!}
        </div>
        <div class="form-group ">
            <label>Liderzy:</label>
            {!! Form::text('user', null, ['class' => 'form-control', 'id' => 'search-users', 'autocomplete' => 'off', 'placeholder' => 'wyszukaj użytkownika'])!!}
        </div>
        <div class="text-center" id="selection-container">
            @foreach($structure->nodes()->root()->leaders as $leader)
                <span class="badge badge-info mx-1">
                    {{ $leader->name }}
                    <input type="hidden" name="users[]" value="{{ $leader->id }}">
                    <i class="remove-leader fas fa-times-circle fa-fw"></i>
                </span>
            @endforeach
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa wykonywanie">Utwórz</button>
</div>

<script src="/js/bloodhound.js"></script>
<script src="/js/typeahead.jquery.js"></script>
<script>
    var users = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/admin/users/search',
            replace: function(url, query) { return url + "?limit=15&q=" + query; },
            filter: function (data) {
                return $.map(data.users, function (user) {
                    return user
                });
            }
        }
    });

    var actorsTypeahead = $('#search-users').typeahead({
            minLength: 2,
            highlight: true,
            hint: true
        },
        { name: 'name', displayKey: 'name', minLength: 2, source: users, limit: 15 });

    $('#search-users').on('typeahead:selected', function(ev, datum) {
        var user_id = datum.id;

        $('#selection-container').append('<span class="badge badge-info mx-1">'+datum.name+'<input type="hidden" name="users[]" value="'+user_id+'"><i class="remove-leader fas fa-times-circle fa-fw"></i></span>');
        actorsTypeahead.typeahead('val','');
    });

    $('#search-users').on('typeahead:close', function(){
        $(this).val('');
    });

    $('#selection-container').on('click', '.remove-leader', function(){
        $(this).parents('.badge').remove();
    });
</script>
