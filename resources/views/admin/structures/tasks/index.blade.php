@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-10 col-lg-8">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            Zadania w ramach <i class="text-muted">{{ $node->name }}</i>
                        </h6>
                        <div>
                            <a href="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/structures/add-task', [$node->id]) }}" class="btn btn-xs btn-info">
                                <i class="fa fa-fw fa-plus"></i> stwórz nowe
                            </a>
                            <a href="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/structures/manage', [$node->structure_id]) }}" class="btn btn-xs btn-outline-dark">
                                <i class="fas fa-arrow-alt-circle-left fa-fw"></i> powrót
                            </a>
                        </div>
                    </div>
                    <table class="table table-sm table-hover">
                        <thead>
                        <th>#</th>
                        <th>Nazwa</th>
                        <th>Zakres czasu</th>
                        <th>Odbiorcy</th>
                        <th></th>
                        <th></th>
                        </thead>
                        @foreach($tasks as $task)
                            <tr>
                                <td>{{ ($tasks->currentPage() - 1) * $tasks->perPage() + $loop->iteration  }}.</td>
                                <td>{{ $task->title }}</td>
                                <td>
                                    {{ optional($task->valid_from)->format('Y-m-d') }} : {{ optional($task->valid_to)->format('Y-m-d') }}
                                </td>
                                <td>{{ $task->read_notifications_count }} / {{ $task->notifications_count }}</td>
                                <td>
                                    <a href="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/structures/edit-task', [$task->id]) }}" class="btn btn-xs btn-warning">
                                        <i class="fa fa-fw fa-pencil-alt"></i> edytuj </span>
                                    </a>
                                </td>
                                <td>
                                    <span target="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/structures/delete-task', [$task->id]) }}" class="btn btn-xs btn-danger modal-open" data-toggle="modal" data-target="#modal">
                                        <i class="fa fa-trash-alt fa-fw"></i> usuń
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <div class="card-footer text-right">
                        {!! $tasks->appends(Request::query())->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endsection
