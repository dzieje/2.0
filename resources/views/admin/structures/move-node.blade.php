<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Przesuwanie węzła</h5>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <form action="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/structures/move-node', [$node->id]) }}" method="post" id="dialog-form">
        {!! csrf_field() !!}

        Wskaż nowy korzeń węzła dla {{ $node->name }}

        <div class="form-group">
            {{ Form::select('node_id', $nodes, null, ['class' =>'form-control']) }}
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa wykonywanie">Zapisz</button>
</div>

<script>

</script>
