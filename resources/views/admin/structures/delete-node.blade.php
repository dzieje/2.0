<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Usuwanie węzła</h5>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <form action="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/structures/delete-node', [$node->id]) }}" method="post" id="dialog-form">
        {!! csrf_field() !!}

        <div class="form-group">
        Potwierdź usnięcie węzła {{ $node->name }}
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="1" name="remove-descendants" id="remove-descendants">
            <label class="form-check-label" for="remove-descendants">
                uwzględnij potomków <span class="badge badge-info"> {{ $node->descendants->count() }} </span>
            </label>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa wykonywanie">Usuń</button>
</div>

