@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            Zadanie <i>{{ $task->title }}</i>
                        </h6>
                        <div>
                            <a href="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/tasks') }}" class="btn btn-default btn-xs ">
                                <i class="fas fa-arrow-alt-circle-left fa-fw"></i> powrót
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <ol class="breadcrumb small bg-gray-100">
                                    @foreach($task->node->ancestors as $ancestor)
                                        <li class="breadcrumb-item">{{ $ancestor->name }}</li>
                                    @endforeach
                                    <li class="breadcrumb-item active" aria-current="page">{{ $task->node->name }}</li>
                                </ol>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="card border-info">
                                    <div class="card-body">
                                        <p class="h4 border-left-info  pl-2 d-flex justify-content-between align-items-center">
                                            Treść
                                        </p>
                                        {!! $task->content !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="card border-info">
                                    <div class="card-body">
                                        <p class="h4 border-left-info  pl-2 d-flex justify-content-between align-items-center">
                                            Osoby
                                        </p>
                                        <table class="table table-sm table-hover">
                                            <thead>
                                            <th>#</th>
                                            <th>Nazwisko</th>
                                            <th>Imię</th>
                                            <th>Telefon</th>
                                            <Th>Email</Th>
                                            <th></th>
                                            </thead>
                                            @foreach($task->node->volunteers as $volunteer)
                                                <tr>
                                                    <td>{{ $loop->iteration  }}.</td>
                                                    <td>{{ $volunteer->surname }}</td>
                                                    <td>{{ $volunteer->name }}</td>
                                                    <td>{{ $volunteer->phone }}</td>
                                                    <td>{{ $volunteer->email }}</td>
                                                    <td>
                                                        <span target="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/participants/show', [$volunteer->id]) }}" class="btn btn-info btn-xs modal-open-lg off-disable" data-toggle="modal" data-target="#modal-lg" data-backdrop="true">
                                                            <i class="fa fa-search fa-fw"></i> szczegóły
                                                        </span>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent

@endsection
