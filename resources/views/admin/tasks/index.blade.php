@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-10 col-lg-9">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            Zadania
                        </h6>
                    </div>
                    <table class="table table-sm table-hover">
                        <thead>
                        <th>#</th>
                        <th>nazwa</th>
                        <th>węzeł</th>
                        <th>aktywny od</th>
                        <th>aktywny do</th>
                        <th>wygasa</th>
                        <th></th>

                        <th></th>
                        </thead>
                        @foreach($tasks as $task)
                            <tr>
                                <td>{{ ($tasks->currentPage() - 1) * $tasks->perPage() + $loop->iteration  }}.</td>
                                <td>{{ $task->title }}</td>
                                <td>
                                    <ol class="breadcrumb small p-0 m-0 " style="background: none;">
                                        @foreach($task->node->ancestors as $ancestor)
                                            <li class="breadcrumb-item">{{ $ancestor->name }}</li>
                                        @endforeach
                                        <li class="breadcrumb-item active" aria-current="page">{{ $task->node->name }}</li>
                                    </ol>
                                </td>
                                <td>{{ optional($task->valid_from)->format('Y-m-d') }}</td>
                                <td>{{ optional($task->valid_to)->format('Y-m-d') }}</td>
                                <td>
                                    @if($task->valid_to->gte(\Carbon\Carbon::now()))
                                        <span class="badge
                                            @if(\Carbon\Carbon::now()->diffInDays($task->valid_to, false) < 5)
                                            badge-danger
                                            @else
                                            badge-info
                                            @endif
                                        ">
                                        {{ \Jenssegers\Date\Date::createFromTimestamp($task->valid_to->timestamp)->diffForHumans() }}
                                        </span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/tasks/show', [$task->id]) }}" class="btn btn-info btn-xs">
                                        <i class="fa fa-fw fa-search"></i> podgląd
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </table>

                    <div class="card-footer text-right">
                        {!! $tasks->appends(Request::query())->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endsection
