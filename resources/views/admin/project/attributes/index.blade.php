@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-lg-8">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            Atrybuty projektu {{ $project->name }}
                        </h6>
                    </div>
                    <table class="table table-sm table-hover">
                        <thead>
                            <th>#</th>
                            <th>Nazwa atrybutu</th>
                            <th>Wartości</th>
                            <th></th>
                        </thead>
                        @foreach($attributes as $k => $attribute)
                            <tr>
                                <td>{{ ++$k  }}.</td>
                                <td>{{ $attribute->name }}</td>
                                <td>
                                    @if($attribute->description)
                                        @foreach(json_decode( $attribute->description ) as $value )
                                            <span class="badge badge-info">{{ $value }}</span>
                                        @endforeach
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ url('admin/project/'.$project->slug.'/attributes/export', [$attribute->id])}}" class="btn btn-outline-primary btn-xs"  >
                                        <i class="fas fa-fw fa-file-excel"></i> eksportuj
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>

    </script>
@endsection
