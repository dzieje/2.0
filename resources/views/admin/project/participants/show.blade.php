<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Informacje o <i>{{ $participant->surname }} {{ $participant->name }}</i></h5>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <nav class="navbar navbar-default">
        <div class="row">
            <div class="col-sm-12">
                <p class="navbar-text navbar-text-small p-0">
                    nazwisko: <strong>{{ $participant->surname }}</strong>
                </p>
                <p class="navbar-text navbar-text-small p-0">
                    imię: <strong>{{ $participant->name }}</strong>
                </p>
                <p class="navbar-text navbar-text-small p-0">
                    telefon: <strong>{{ $participant->phone }}</strong>
                </p>
                <p class="navbar-text navbar-text-small p-0">
                    email: <strong>{{ $participant->email }}</strong>
                </p>
                <p class="navbar-text navbar-text-small p-0">
                    płeć: <strong>@if($participant->gender == 'male') mężczyzna @else kobieta @endif</strong>
                </p>
                <p class="navbar-text navbar-text-small p-0">
                    data urodzin:
                    @if($participant->date_of_birth)
                        <strong>
                            {{ $participant->date_of_birth->format('Y-m-d') }}
                            <span class="badge badge-info">{{ $participant->date_of_birth->diffInYears(Carbon\Carbon::now()) }}</span>
                        </strong>
                    @endif
                </p>
            </div>
        </div>
    </nav>
</div>
<div class="modal-footer">
    <a href="{{ url('admin/project/'.$project->slug.'/participants/edit', [$participant->id]) }}" class="btn btn-warning">
        <i class="fa fa-pencil-alt fa-fw"></i> edytuj
    </a>
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
</div>
