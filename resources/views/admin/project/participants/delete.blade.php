<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Usuwanie uczestnika</h5>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <form action="{{ url('admin/project/'.$project->slug.'/participants/delete', [$participant->id]) }}" method="post" id="dialog-form">
        {!! csrf_field() !!}
        <h4>Potwierdź usunięcie uczestnika {{ $participant->surname }} {{ $participant->name }}</h4>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa usuwanie">Usuń</button>
</div>
