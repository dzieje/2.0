@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                {!! Form::open(['url' => url('admin/project/'.$project->slug.'/participants/update', [$participant->id]), 'id' => 'page-form']) !!}
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                        Edycja danych uczestnika {{ $participant->surname }} {{ $participant->name }}
                        </h6>
                        <a href="{{ URL::previous() }}" class="btn btn-outline-dark btn-sm pull-right">
                            <i class="fa fa-ban fa-fw"></i> anuluj
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label>Nazwisko:</label>
                                    {!! Form::text('surname', $participant->surname, ['class' => 'form-control required', 'placeholder' => 'podaj nazwisko']) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label>Imię:</label>
                                    {!! Form::text('name', $participant->name, ['class' => 'form-control required', 'placeholder' => 'podaj imię']) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label>Telefon:</label>
                                    {!! Form::text('phone', $participant->phone, ['class' => 'form-control', 'placeholder' => 'podaj telefon']) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label>Email:</label>
                                    {!! Form::text('email', $participant->email, ['class' => 'form-control email', 'placeholder' => 'podaj email']) !!}
                                </div>
                            </div>



                            <div class="col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label>Płeć:</label>
                                    {!! Form::select('gender', [null => '---wybierz---', 'male' => 'mężczyzna', 'female' => 'kobieta'], $participant->gender, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label>Data urodzenia:</label>
                                    {!! Form::date('date_of_birth', optional($participant->date_of_birth)->format('Y-m-d'), ['class' => 'form-control dynamic-datepicker', 'placeholder' => 'podaj datę urodzenia']) !!}
                                </div>
                            </div>

                            @foreach($attributes as $attribute)
                                <div class="col-sm-6 col-lg-4">
                                    <div class="form-group">
                                        <label>{{ $attribute->name }}:</label>
                                        @if($attribute->is_collection)
                                            <br>
                                            @foreach(json_decode( $attribute->description ) as $value)
                                                <div class="form-check form-check-inline">
                                                    <input name="{{ $attribute->slug.'[]' }}" type="checkbox" class="form-check-input" value="{{ $value }}" id="{{$attribute->slug}}_{{$value}}" @if($participant->{$attribute->slug}->contains($value)) checked @endif>
                                                    <label class="form-check-label" for="{{$attribute->slug}}_{{$value}}">{{ $value }}</label>
                                                </div>
                                            @endforeach
                                        @else
                                            @if($attribute->type == 'tekstowy')
                                                {!! Form::text($attribute->slug, $participant->{$attribute->slug}, ['class' => 'form-control ']) !!}
                                            @elseif($attribute->type == 'checkbox')
                                                {!! Form::checkbox($attribute->slug, 1, $participant->{$attribute->slug}, ['class' => 'form-control ']) !!}
                                            @elseif($attribute->type == 'liczbowy')
                                                {!! Form::number($attribute->slug, $participant->{$attribute->slug}, ['class' => 'form-control ']) !!}
                                            @else
                                                {!! Form::date($attribute->slug, optional($participant->{$attribute->slug})->format('Y-m-d'), ['class' => 'form-control dynamic-datepicker']) !!}
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        {!! Form::submit('Zapisz zmiany', ['class' => 'btn btn-primary off-disable']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

