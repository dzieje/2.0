@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <form id="page-form" method="post"
              action="{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/settings/update', [$setting->id]) }}">
            {!! csrf_field() !!}

            @include('admin.project.settings.types.'.$setting->key)
        </form>
    </div>
@endsection
