<link rel="stylesheet" href="{{ asset('css/summernote-bs4.min.css') }}">
<div class="card shadow">
    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
        <h6 class="m-0 font-weight-bold text-info">
            {{ $setting->name }}
        </h6>
        <a href="{{ URL::previous()  }}" class="btn btn-default btn-xs ">
            <i class="fa fa-remove fa-fw"></i> anuluj
        </a>
    </div>
    <div class="card-body">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-lg-4 mb-2">
                <div class="card ">
                    <div class="card-body">
                        <div class="form-group">
                            <label>Kolor czcionki:</label>
                            <input id="content_font_color" type="color" class="form-control" name="content_font_color" value="{{ optional($setting->value)->content_font_color }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-4 mb-2">
                <div class="card ">
                    <div class="card-body">
                        <div class="form-group">
                            <label>Kolor tła maila:</label>
                            <input id="mail_background_color" type="color" class="form-control" name="mail_background_color" value="{{ optional($setting->value)->mail_background_color }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-4 mb-2">
                <div class="card ">
                    <div class="card-body">
                        <div class="form-group">
                            <label>Kolor tła zawartości:</label>
                            <input id="content_background_color" type="color" class="form-control" name="content_background_color" value="{{ optional($setting->value)->content_background_color }}">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-lg-6">
                <div class="card ">
                    <div class="card-body">
                        <div class="form-group">
                            <label>Nagłówek:</label>
                            <textarea class="form-control " name="header" id="header-content">
                               {{ optional($setting->value)->header }}
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-6">
                <div class="card ">
                    <div class="card-body">
                        <div class="form-group">
                            <label>Stopka:</label>
                            <textarea class="form-control " name="footer" id="footer-content">
                               {{ optional($setting->value)->footer }}
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <div class="card-footer text-center">
        <button type="submit" class="btn btm-sm btn-info">
            <i class="fas fa-save fa-fw"></i>
            Zapisz
        </button>
    </div>
</div>

@section('scripts')
    @parent
    <script src="/js/summernote-bs4.min.js"></script>
    <script src="/js/summernote-pl-PL.js"></script>
    <script>
        const FMButton = function (context) {
            const ui = $.summernote.ui;
            const button = ui.button({
                contents: '<i class="note-icon-picture"></i> ',
                tooltip: 'File Manager',
                click: function () {
                    window.open('/file-manager/summernote', 'fm', 'width=1400,height=800');
                }
            });
            return button.render();
        };

        $('#header-content').summernote({
            lang: 'pl-PL',
            minHeight: 200,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['table'],
                ['para', ['ul', 'ol', 'paragraph']],
                ['misc', ['codeview', 'undo', 'redo', 'fullscreen']],
                ['fm-button', ['fm']],
            ],
            buttons: {
                fm: FMButton
            }
        });

        $('#footer-content').summernote({
            lang: 'pl-PL',
            minHeight: 200,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['table'],
                ['para', ['ul', 'ol', 'paragraph']],
                ['misc', ['codeview', 'undo', 'redo', 'fullscreen']],
                ['fm-button', ['fm']],
            ],
            buttons: {
                fm: FMButton
            }
        });

        function fmSetLink($url) {
            $('#header-content').summernote('insertImage', $url);
        }
    </script>
@endsection
