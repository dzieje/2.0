<link rel="stylesheet" href="{{ asset('css/summernote-bs4.min.css') }}">
<div class="card shadow">
    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
        <h6 class="m-0 font-weight-bold text-info">
            {{ $setting->name }}
        </h6>
        <a href="{{ URL::previous()  }}" class="btn btn-default btn-xs ">
            anuluj
        </a>
    </div>
    <div class="card-body">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-lg-8">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <span class="btn btn-sm btn-info" id="add-template">
                            <i class="fas fa-plus-square fa-fw"></i> dodaj wzór
                        </span>
                        <hr>
                    </div>
                    <div id="contracts-container" class="w-100">
                        @if($setting->value)
                            @foreach($setting->value->contract_content as $key => $contract)
                                <div class="col-sm-12">
                                    <div class="card ">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Wiek wolontariusza od:</label>
                                                        {!! Form::number('age_from[]', isset($setting->value->age_from[$key]) ? $setting->value->age_from[$key] : null , ['class' => 'form-control required', 'required']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Wiek wolontariusza do:</label>
                                                        {!! Form::number('age_to[]',isset($setting->value->age_to[$key]) ? $setting->value->age_to[$key] : null, ['class' => 'form-control']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Treść umowy:</label>
                                                <textarea class="form-control contract-content" name="contract_content[]" >
                                                   {{ $contract }}
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-4">
                <div class="card ">
                    <div class="card-body">
                        @include('admin.sending.parameters')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer text-center">
        <button type="submit" class="btn btm-sm btn-info">
            <i class="fas fa-save fa-fw"></i>
            Zapisz
        </button>
    </div>
</div>

@section('scripts')
    @parent
    <script src="/js/clipboard.min.js"></script>
    <script src="/js/summernote-bs4.min.js"></script>
    <script src="/js/summernote-pl-PL.js"></script>
    <script>
        $('.copy-parameter').tooltip();
        var clipboard = new ClipboardJS('.copy-parameter', {
            text: function(trigger) {
                return trigger.getAttribute('key');
            }
        });

        clipboard.on('success', function(e) {
            $(e.trigger).tooltip('show');

            webwizards.sleep(1000).then(() => {
                $(e.trigger).tooltip('hide');
            });

            e.clearSelection();
        });

        $('.contract-content').summernote({
            lang: 'pl-PL',
            minHeight: 200,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['table'],
                ['para', ['ul', 'ol', 'paragraph']],
                ['misc', ['codeview', 'undo', 'redo', 'fullscreen']],
                ['fm-button', ['fm']],
            ]
        });

        $('#add-template').on('click', function(){
            $.ajax({
                type: "GET",
                url: '{{ url('admin/'.$project->type->slug.'/'.$project->slug.'/settings/append-template') }}',
                assync:false,
                cache:false,
                dataType: 'html',
                success: function( data ) {
                    $('#contracts-container').append(data);
                    $('.contract-content').summernote({
                        lang: 'pl-PL',
                        minHeight: 200,
                        toolbar: [
                            ['style', ['bold', 'italic', 'underline', 'clear']],
                            ['fontsize', ['fontsize']],
                            ['color', ['color']],
                            ['table'],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['misc', ['codeview', 'undo', 'redo', 'fullscreen']],
                            ['fm-button', ['fm']],
                        ]
                    });
                }
            });
        });
    </script>
@endsection
