<div class="col-sm-12 my-2">
    <div class="card ">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Wiek wolontariusza od:</label>
                        {!! Form::number('age_from[]', null, ['class' => 'form-control required', 'required']) !!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Wiek wolontariusza do:</label>
                        {!! Form::number('age_to[]', null, ['class' => 'form-control required', 'required']) !!}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Treść umowy:</label>
                <textarea class="form-control contract-content" name="contract_content[]" >
                </textarea>
            </div>
        </div>
    </div>
</div>
