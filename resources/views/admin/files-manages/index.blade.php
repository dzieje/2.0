@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="{{ asset('vendor/file-manager/css/file-manager.css') }}">
    <div class="container-fluid">
        <div class="justify-content-center">
            <div style="height: 600px;">
                <div id="fm"></div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    @parent
    <script src="{{ asset('vendor/file-manager/js/file-manager.js') }}"></script>
@endsection
