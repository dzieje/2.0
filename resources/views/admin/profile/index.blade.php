@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            {{ __('Zarządzanie profilem') }}
                        </h6>
                    </div>

                    <div class="text-center card-body">
                        <div class="pt-2 pb-2">
                            <div class="w-25 mx-auto">
                                <img src="{{ $user->avatar ? $user->avatar : asset('images/person.png') }}" class="rounded-circle img-thumbnail" alt="profile-image">
                            </div>
                            <span class="btn btn-warning btn-xs" id="button-avatar-change">
                                <i class="fa fa-fw fa-pencil-alt"></i>
                                edytuj
                            </span>
                            <hr>
                            <div class="p-1">
                                <h4>{{ $user->name }}</h4>
                                <p class="text-muted"><i class="fas fa-fw fa-at"></i>{{ $user->email }} <span>| </span><span>{{ $user->role->name }}</span></p>
                            </div>
                            <hr>
                            <div class="mt-4">
                                <div class="row">
                                    <div class="col-3">
                                        <div class="mt-3">
                                            <h4>{{ $user->news->count() }}</h4>
                                            <p class="mb-0 text-muted">Aktualności</p>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mt-3">
                                            <h4>{{ $user->tasks->count() }}</h4>
                                            <p class="mb-0 text-muted">Zadania</p>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mt-3">
                                            <h4>{{ $user->messages()->mail()->count() }}</h4>
                                            <p class="mb-0 text-muted">Mailings</p>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mt-3">
                                            <h4>{{ $user->messages()->sms()->count() }}</h4>
                                            <p class="mb-0 text-muted">SMS</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        $('#button-avatar-change').on('click', function(event){
            event.preventDefault();

            window.open('/file-manager/fm-button', 'fm', 'width=1400,height=800');
        });

        function fmSetLink($url) {
            console.log($url);
            $.ajax({
                type: "POST",
                url: "{{ url('admin/profile/change-avatar') }}" ,
                data: { url: $url},
                assync: false,
                cache: false,
                success: function (data) {
                    location.reload();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
                dataType: 'json'
            });
        }
    </script>
@endsection
