@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            {{ __('Zarządzanie projektami') }}
                        </h6>

                        <span target="{{ url('admin/projects/create') }}" class="btn btn-xs btn-info modal-open-lg" data-toggle="modal" data-target="#modal-lg">
                            <i class="fa fa-plus fa-fw"></i> {{ __('utwórz projekt') }}
                        </span>
                    </div>

                    <div class="card-body">
                        <table class="table table-sm table-hover">
                            <thead>
                            <th>#</th>
                            <th>Nazwa</th>
                            @if(config('app.show_enable'))
                                <th>Typ</th>
                            @endif
                            <th>Autor</th>
                            <th>Współautorzy</th>
                            <th>Rok startu</th>
                            <th></th>
                            <th></th>
                            </thead>

                            @foreach($projects as $project)
                                <tr>
                                    <td>{{ ++$loop->index }}</td>
                                    <td>{{ $project->name }}</td>
                                    @if(config('app.show_enable'))
                                        <td>{{ $project->type->name }}</td>
                                    @endif
                                    <td>
                                        {{ optional($project->user)->name }}
                                    </td>
                                    <td>
                                        {{ implode(', ', $project->contributors->pluck('name')->toArray()) }}
                                    </td>
                                    <td>
                                        {{ $project->year }}
                                    </td>
                                    <td>
                                        <a target="{{ URL::to('admin/projects/edit', array($project->id)) }}" class="btn btn-warning btn-xs modal-open-lg" data-toggle="modal" data-target="#modal-lg">
                                            <i class="fas fa-pencil-alt fa-fw"></i> edytuj
                                        </a>
                                    </td>
                                    <td>
                                        <span target="{{ URL::to('admin/projects/delete', array($project->id)) }}" class="btn btn-danger btn-xs modal-open" data-toggle="modal" data-target="#modal">
                                            <i class="fas fa-trash-alt fa-fw"></i> usuń
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
