<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Tworzenie projektu</h5>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="panel-body">
        <form action="{{ url('admin/projects/store') }}" method="post" id="dialog-form">
            {!! csrf_field()!!}
            <div class="row">
                <div class="form-group col-md-12">
                    <label>Nazwa projektu:</label>
                    <input type="text" name="name" class="form-control required" required>
                </div>
                <div class="form-group col-md-6">
                    <label>Rok rozpoczęcia</label>
                    <input type="text" name="year" value="{{ \Carbon\Carbon::now()->year }}" class="form-control number required" required>
                </div>
                @if(config('app.show_enable'))
                    <div class="form-group  col-md-6">
                        <label>Typ projektu</label>
                        <select name="project_type_id" class="form-control required" required>
                            @foreach($types as $type_id => $type)
                                <option value="{{ $type_id }}">{{ $type }}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
                <div class="col-sm-12">
                    <hr>
                    <h6 class="text-center" >Współautorzy:</h6>

                </div>
                @foreach($users as $user_id => $user)
                    <div class="form-check col-md-4 text-center">
                        <label>
                            <input type="checkbox" name="users[]" value="{{ $user_id }}">
                            {{ $user }}
                        </label>
                    </div>
                @endforeach
            </div>
        </form>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa wykonywanie">Utwórz projekt</button>
</div>
