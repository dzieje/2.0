@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            {{ __('Zarządzanie aktualnościami') }}
                        </h6>

                        <a href="{{ url('admin/news/create') }}" class="btn btn-xs btn-info">
                            <i class="fa fa-plus fa-fw"></i> {{ __('utwórz wpis') }}
                        </a>
                    </div>

                    <div class="card-body">
                        <table class="table table-sm table-hover">
                            <thead>
                            <th>#</th>
                            <th>Nazwa</th>
                            <th>Projekt</th>
                            <th>Odbiorcy</th>
                            <th>Dodał</th>
                            <th></th>
                            <th></th>
                            </thead>

                            @foreach($news as $news_item)
                                <tr>
                                    <td>{{ ($news->currentPage() - 1) * $news->perPage() + $loop->iteration  }}.</td>
                                    <td>{{ $news_item->title }}</td>
                                    <td>
                                        {{ optional($news_item->project)->name }}
                                    </td>
                                    <td>{{ $news_item->read_notifications_count }} / {{ $news_item->notifications_count }}</td>
                                    <td>
                                        {{ $news_item->user->name }}
                                    </td>
                                    <td>
                                        <a href="{{ URL::to('admin/news/edit', array($news_item->id)) }}" class="btn btn-warning btn-xs">
                                            <i class="fas fa-pencil-alt fa-fw"></i> edytuj
                                        </a>
                                    </td>
                                    <td>
                                        <span target="{{ URL::to('admin/news/delete', array($news_item->id)) }}" class="btn btn-danger btn-xs modal-open" data-toggle="modal" data-target="#modal">
                                            <i class="fas fa-trash-alt fa-fw"></i> usuń
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <div class="card-footer text-right">
                        {!! $news->appends(Request::query())->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
