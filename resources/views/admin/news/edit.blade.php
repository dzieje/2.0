@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="{{ asset('css/summernote-bs4.min.css') }}">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <form id="page-form" method="post" action="{{ url('admin/news/update', [$news->id]) }}">
                    <div class="card shadow">
                        <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                            <h6 class="m-0 font-weight-bold text-info">
                                Edycja wpisu
                            </h6>
                            <a href="{{ URL::previous() }}" class="btn btn-default btn-xs ">
                                <i class="fa fa-remove fa-fw"></i> anuluj
                            </a>
                        </div>
                        <div class="card-body">
                            {!! csrf_field() !!}
                            <div class="row">
                                    <div class="col-md-8">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label>Treść wpisu:</label>
                                                    <textarea class="form-control required" name="content" id="news-content" >
                                                        {{ $news->content }}
                                                    </textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Tytuł wpisu:</label>
                                                            <input maxlength="255" class="form-control required" required name="title" placeholder="tytuł wpisu" value="{{ $news->title }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Dotyczy projektu:</label>
                                                            {!! Form::select('project_id', $projects, $news->project_id, ['class' => 'form-control']) !!}
                                                        </div>
                                                    </div>


                                                    <div class="col-sm-12">
                                                        <hr>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="card border-info mb-4">
                                                            <a href="#collapseCardAttachments" class="d-block card-header text-white  bg-info py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardAttachments">
                                                                <h6 class="m-0">Załączniki</h6>
                                                            </a>
                                                            <div class="collapse " id="collapseCardAttachments">
                                                                <div class="card-body">
                                                                    <div class="list-group list-group-files">
                                                                        @if($news->attachments)
                                                                            @foreach($news->attachments as $attachment)
                                                                                <div class="input-group">
                                                                                    <input type="text" readonly
                                                                                           id="file_'+inputId+'"
                                                                                           class="form-control"
                                                                                           name="attachments[]"
                                                                                           aria-label="Image"
                                                                                           aria-describedby="button-image"
                                                                                           value="{{ $attachment }}">
                                                                                    <div class="input-group-append">
                                                                                        <button
                                                                                            class="btn btn-outline-danger button-remove-file"
                                                                                            type="button">
                                                                                            <i class="fa fa-trash-alt"></i>
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            @endforeach
                                                                        @endif
                                                                    </div>
                                                                    <span class="btn btn-sm btn-outline-info text-center d-block m-3" id="button-file-add">
                                                                        <i class="far fa-plus-square fa-fw"></i> dodaj plik
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="card-footer text-center">
                            <button class="btn btn-primary" type="submit">
                                <i class="fas fa-save fa-fw"></i>
                                Zapisz
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    @parent
    <script src="/js/summernote-bs4.min.js"></script>
    <script src="/js/summernote-pl-PL.js"></script>
    <script>
        const FMButton = function(context) {
            const ui = $.summernote.ui;
            const button = ui.button({
                contents: '<i class="note-icon-picture"></i> ',
                tooltip: 'File Manager',
                click: function() {
                    window.open('/file-manager/summernote', 'fm', 'width=1400,height=800');
                }
            });
            return button.render();
        };

        $('#news-content').summernote({
            lang: 'pl-PL',
            minHeight: 300,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['misc', ['codeview', 'undo', 'redo', 'fullscreen']],
                ['fm-button', ['fm']],
            ],
            buttons: {
                fm: FMButton
            }
        });

        let inputId = false;

        $('#button-file-add').on('click', function(event){
            event.preventDefault();

            inputId = webwizards.makeid(6);

            window.open('/file-manager/fm-button', 'fm', 'width=1400,height=800');
        });

        function fmSetLink($url) {
            if(inputId) {
                $('.list-group-files').append('<div class="input-group">\n' +
                    '    <input type="text" disabled id="file_'+inputId+'" class="form-control" name="attachments[]"\n' +
                    '           aria-label="Image" aria-describedby="button-image" value="{{ url('') }}'+encodeURI($url)+'">\n' +
                    '    <div class="input-group-append">\n' +
                    '        <button class="btn btn-outline-danger button-remove-file" type="button">\n' +
                    '            <i class="fa fa-trash-alt"></i>\n' +
                    '        </button>\n' +
                    '    </div>\n' +
                    '</div>')
            }else {
                $('#news-content').summernote('insertImage', $url);
            }

            inputId = false;
        }

        $(document).on('click', '.button-remove-file', function (){
            $(this).parents('.input-group').remove();
        });



    </script>
@endsection
