@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="{{ asset('css/summernote-bs4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-select.min.css') }}">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <form id="page-form" method="post" action="{{ url('admin/news/store') }}">
                    <div class="card shadow">
                        <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                            <h6 class="m-0 font-weight-bold text-info">
                                Tworzenie nowego wpisu
                            </h6>
                            <a href="{{ URL::previous() }}" class="btn btn-default btn-xs ">
                                <i class="fa fa-remove fa-fw"></i> anuluj
                            </a>
                        </div>
                        <div class="card-body">
                            {!! csrf_field() !!}
                            <div class="row">
                                @include('admin.sending.nav')
                                    <div class="col-md-8">
                                        <div class="card mt-2">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label>Treść wpisu:</label>
                                                    <textarea class="form-control required" name="content" id="news-content">
                                                    </textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card mt-2">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Tytuł wpisu:</label>
                                                            <input maxlength="255" class="form-control required" required name="title" placeholder="tytuł wpisu">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Dotyczy projektu:</label>
                                                            {!! Form::select('project_id', ['' => '--- wybierz projekt ---'] + $projects->flatten()->pluck('name', 'id')->toArray(), '', ['class' => 'form-control']) !!}
                                                        </div>
                                                    </div>


                                                    <div class="col-sm-12">
                                                        <hr>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="card border-info mb-4">
                                                            <a href="#collapseCardAttachments" class="d-block card-header text-white  bg-info py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardAttachments">
                                                                <h6 class="m-0">Załączniki</h6>
                                                            </a>
                                                            <div class="collapse " id="collapseCardAttachments">
                                                                <div class="card-body">
                                                                    <div class="list-group list-group-files"></div>
                                                                    <span class="btn btn-sm btn-outline-info text-center d-block m-3" id="button-file-add">
                                                                        <i class="far fa-plus-square fa-fw"></i> dodaj plik
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="card-footer text-center">
                            <button class="btn btn-primary" type="submit">
                                <i class="fas fa-save fa-fw"></i>
                                Zapisz
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    @parent
    <script src="/js/summernote-bs4.min.js"></script>
    <script src="/js/summernote-pl-PL.js"></script>
    <script src="/js/bootstrap-select.min.js"></script>
    <script>
        var previewedReceivers = true;

        const FMButton = function(context) {
            const ui = $.summernote.ui;
            const button = ui.button({
                contents: '<i class="note-icon-picture"></i> ',
                tooltip: 'File Manager',
                click: function() {
                    window.open('/file-manager/summernote', 'fm', 'width=1400,height=800');
                }
            });
            return button.render();
        };

        $('#news-content').summernote({
            lang: 'pl-PL',
            minHeight: 300,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['misc', ['codeview', 'undo', 'redo', 'fullscreen']],
                ['fm-button', ['fm']],
            ],
            buttons: {
                fm: FMButton
            }
        });

        let inputId = false;

        $('#button-file-add').on('click', function(event){
            event.preventDefault();

            inputId = webwizards.makeid(6);

            window.open('/file-manager/fm-button', 'fm', 'width=1400,height=800');
        });

        function cleanString(input) {
            var output = "";
            for (var i=0; i<input.length; i++) {
                if (input.charCodeAt(i) <= 127 || input.charCodeAt(i) >= 160 && input.charCodeAt(i) <= 255) {
                    output += input.charAt(i);
                }
            }
            return output;
        }

        function fmSetLink($url) {
            if(inputId) {
                var filename = cleanString($url.replace(/^.*[\\\/]/, ''));
                $('.list-group-files').append('<div class="input-group">\n' +
                    '    <input type="text" readonly id="file_'+inputId+'" class="form-control" name="attachments['+inputId+'][path]"\n' +
                    '           aria-label="Image" aria-describedby="button-image" value="{{ url('') }}'+encodeURI($url)+'">\n' +
                    '    <input type="hidden" name="attachments['+inputId+'][filename]" value="'+filename+'"/> '+
                    '    <div class="input-group-append">\n' +
                    '        <button class="btn btn-outline-danger button-remove-file" type="button">\n' +
                    '            <i class="fa fa-trash-alt"></i>\n' +
                    '        </button>\n' +
                    '    </div>\n' +
                    '</div>')
            }else {
                $('#news-content').summernote('insertImage', $url);
            }

            inputId = false;
        }

        $(document).on('click', '.button-remove-file', function (){
            $(this).parents('.input-group').remove();
        });

        $('.groups-filter').selectpicker();

        $('[name="project"]').on('change', function() {
            let project_id = $(this).val();
            $.ajax({
                type: "GET",
                url: "{{ url('admin/sending/mailings/load-project-parameters') }}/" + project_id,
                assync: false,
                cache: false,
                success: function (data) {
                    $('.additional-filters').html(data);
                    $('.groups-filter').selectpicker();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
                dataType: 'html'
            });
        });

        function countReceivers() {
            $.ajax({
                type: "POST",
                url: "{{ url('admin/sending/mailings/count-receivers') }}",
                data: $('#page-form').serialize(),
                assync: false,
                cache: false,
                success: function (data) {
                    $('.receivers-counter').html(data.count);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
                dataType: 'json'
            });
            if(previewedReceivers){
                showReceivers();
            }
        }

        function showReceivers() {
            $.ajax({
                type: "POST",
                url: "{{ url('admin/sending/mailings/show-receivers') }}",
                data: $('#page-form').serialize(),
                assync: false,
                cache: false,
                success: function (data) {
                    $('#receivers-container').html(data);
                    previewedReceivers = true;
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
                dataType: 'html'
            });
        }


    </script>
@endsection
