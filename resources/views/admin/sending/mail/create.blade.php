@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="{{ asset('css/summernote-bs4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-select.min.css') }}">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card shadow">
                    <div class="card-body">
                        <form id="page-form" method="post" action="{{ url('admin/sending/mailings/send') }}">
                            {!! csrf_field() !!}
                            <div class="row">
                                @include('admin.sending.nav')
                                <div class="col-md-8">
                                    <div class="card mt-2">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label>Treść maila:</label>
                                                <textarea class="form-control required" name="content" id="mail-content">
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card mt-2">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>Tytuł maila:</label>
                                                        <input maxlength="255" class="form-control required" name="subject" placeholder="tytuł maila">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 ">
                                                    <div class="form-group">
                                                        <label>Odpowiedź do:</label>
                                                        <input maxlength="255" class="form-control required" name="replyTo" placeholder="Odpowiedź" value="aktorzy@parkdzieje.pl">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <hr>
                                                </div>

                                                @include('admin.sending.parameters')

                                                <div class="col-sm-12">
                                                    <div class="card border-info mb-4">
                                                        <a href="#collapseCardSurveys" class="d-block card-header text-white  bg-info py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardSurveys">
                                                            <h6 class="m-0">Dostępne formularze</h6>
                                                        </a>
                                                        <div class="collapse " id="collapseCardSurveys">
                                                            <div class="card-body">
                                                                <select name="survey_project" class="form-control form-control-sm  ">
                                                                    <option class="text-center" value="0">--- wybierz projekt ---</option>
                                                                    @foreach($projects as $project_type_id => $project_lists)
                                                                        <optgroup label="{{ $project_lists->first()->type->name }}">
                                                                            @foreach($project_lists as $project)
                                                                                <option value="{{ $project->id }}">{{ $project->name }}</option>
                                                                            @endforeach
                                                                        </optgroup>
                                                                    @endforeach
                                                                </select>
                                                                <div id="card-surveys-container" class="mt-3"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="card border-info mb-4">
                                                        <a href="#collapseCardAttachments" class="d-block card-header text-white  bg-info py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardAttachments">
                                                            <h6 class="m-0">Załączniki</h6>
                                                        </a>
                                                        <div class="collapse " id="collapseCardAttachments">
                                                            <div class="card-body">
                                                                <div class="list-group list-group-files"></div>
                                                                <span class="btn btn-sm btn-outline-info text-center d-block m-3" id="button-file-add">
                                                                    <i class="far fa-plus-square fa-fw"></i> dodaj plik
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 text-center mt-5 mb-5">
                                    <span class="btn btn-primary modal-open" target="{{ url('admin/sending/mailings/test') }}" data-toggle="modal" data-target="#modal">
                                        <i class="far fa-fw fa-paper-plane"></i> wyślij próbny mail
                                    </span>
                                    <button type="submit" class="btn  btn-info off-disable">
                                        <i class="fa fa-fw fa-paper-plane"></i> wyślij mailing do <span class="badge" id="receivers-counter"></span> odbiorców
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    @parent
    <script src="/js/clipboard.min.js"></script>
    <script src="/js/summernote-bs4.min.js"></script>
    <script src="/js/summernote-pl-PL.js"></script>
    <script src="/js/bootstrap-select.min.js"></script>
    <script>
        $('.copy-parameter').tooltip();

        var previewedReceivers = true;

        var clipboard = new ClipboardJS('.copy-parameter', {
            text: function(trigger) {
                return trigger.getAttribute('key');
            }
        });

        clipboard.on('success', function(e) {
            $(e.trigger).tooltip('show');

            webwizards.sleep(1000).then(() => {
                $(e.trigger).tooltip('hide');
            });

            e.clearSelection();
        });

        $('.groups-filter').selectpicker();

        const FMButton = function(context) {
            const ui = $.summernote.ui;
            const button = ui.button({
                contents: '<i class="note-icon-picture"></i> ',
                tooltip: 'File Manager',
                click: function() {
                    window.open('/file-manager/summernote', 'fm', 'width=1400,height=800');
                }
            });
            return button.render();
        };

        $('#mail-content').summernote({
            lang: 'pl-PL',
            minHeight: 300,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['misc', ['codeview', 'undo', 'redo', 'fullscreen']],
                ['fm-button', ['fm']],
            ],
            buttons: {
                fm: FMButton
            }
        });

        let inputId = false;

        $('#button-file-add').on('click', function(event){
            event.preventDefault();

            inputId = webwizards.makeid(6);

            window.open('/file-manager/fm-button', 'fm', 'width=1400,height=800');
        });

        function fmSetLink($url) {
            if(inputId) {
                $('.list-group-files').append('<div class="input-group">\n' +
                                            '    <input type="text" disabled id="file_'+inputId+'" class="form-control" name="attachments[]"\n' +
                                            '           aria-label="Image" aria-describedby="button-image" value="{{ url('') }}'+$url+'">\n' +
                                            '    <div class="input-group-append">\n' +
                                            '        <button class="btn btn-outline-danger button-remove-file" type="button">\n' +
                                            '            <i class="fa fa-trash-alt"></i>\n' +
                                            '        </button>\n' +
                                            '    </div>\n' +
                                            '</div>')
            }else {
                $('#mail-content').summernote('insertImage', $url);
            }

            inputId = false;
        }

        $(document).on('click', '.button-remove-file', function (){
            $(this).parents('.input-group').remove();
        });

        $('#modal').on('click', '#send-test', function () {
            var $btn = $(this);
            if ($('#dialog-form').valid()) {
                $btn.button('loading');

                $.ajax({
                    type: "POST",
                    url: $('#dialog-form').prop('action'),
                    data: $('#page-form').serialize() + '&' + $('#dialog-form').serialize(),
                    assync: false,
                    cache: false,
                    success: function (data) {
                        $('#modal').modal('hide');
                        $btn.button('reset');
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $('#modal .modal-body').html('<p class="text-danger">wystąpił błąd w trakcie wysyłki</p>');
                    },
                    dataType: 'json'
                });
            } else {
                $btn.button('reset');
            }
            return false;
        });

        $('[name="project"]').on('change', function() {
            let project_id = $(this).val();
            $.ajax({
                type: "GET",
                url: "{{ url('admin/sending/mailings/load-project-parameters') }}/" + project_id,
                assync: false,
                cache: false,
                success: function (data) {
                    $('.additional-filters').html(data);
                    $('.groups-filter').selectpicker();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
                dataType: 'html'
            });
        });
        $('[name="survey_project"]').on('change', function(){
            let project_id = $(this).val();
            $.ajax({
                type: "GET",
                url: "{{ url('admin/sending/mailings/load-project-surveys') }}/"+project_id,
                assync: false,
                cache: false,
                success: function (data) {
                    $('#collapseCardSurveys .card-body #card-surveys-container').html(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
                dataType: 'html'
            });
        });

        function countReceivers() {
            $.ajax({
                type: "POST",
                url: "{{ url('admin/sending/mailings/count-receivers') }}",
                data: $('#page-form').serialize(),
                assync: false,
                cache: false,
                success: function (data) {
                    $('.receivers-counter').html(data.count);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
                dataType: 'json'
            });
            if(previewedReceivers){
                showReceivers();
            }
        }

        function showReceivers() {
            $.ajax({
                type: "POST",
                url: "{{ url('admin/sending/mailings/show-receivers') }}",
                data: $('#page-form').serialize(),
                assync: false,
                cache: false,
                success: function (data) {
                    $('#receivers-container').html(data);
                    previewedReceivers = true;
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
                dataType: 'html'
            });
        }

    </script>
@endsection
