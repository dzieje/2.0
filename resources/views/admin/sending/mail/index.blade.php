@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">

                        Zarządzanie mailingami
                        </h6>
                        <a href="{{ url('admin/sending/mailings/create') }}" class="btn btn-primary btn-xs ">
                            <i class="fa fa-plus fa-fw"></i>
                            utwórz mailing
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-hover">
                                <thead>
                                <th>#</th>
                                <th>Tytuł mailingu</th>
                                <th>powiadomieni wolontariusze</th>
                                <th>Data wysłania</th>
                                <th>Wysyłający</th>
                                </thead>
                                @foreach($mailings as $mailing)
                                    <tr
                                        class="
                                        @if($mailing->send_time)
                                                 success
                                        @endif
                                                "
                                    >
                                        <td>{{ ($mailings->currentPage() - 1) * $mailings->perPage() + $loop->iteration  }}.</td>
                                        <td>{{ $mailing->subject }}</td>
                                        <td>
                                            <a href="" class="btn btn-info btn-xs">
                                                <i class="fa fa-users fa-fw"></i>

                                                <span class="badge">
                                                    {{ $mailing->entities->count() }}
                                                </span>
                                            </a>
                                        </td>
                                        <td>{{$mailing->send_time}}</td>
                                        <td>{{ $mailing->user->name }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>

                    <div class="card-footer text-right">
                        {!! $mailings->appends(Request::query())->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    @parent
    <script>
    </script>
@endsection
