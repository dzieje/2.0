<div class="col-sm-12 ">
    <div class="form-inline">
        <nav class="navbar navbar-filtering navbar-light bg-light w-100 justify-content-center">
            <div class="form-group form-group-sm flex-column text-center mr-1 ml-1 mb-2">
                <label>Filtrowanie wolontariuszy <span class="badge badge-dark ml-1"></span></label>
                <div>
                    <a class="btn btn-xs btn-danger" href="{{ Request::url() }}">
                        <i class="fa fa-trash-alt fa-fw"></i> usuń filtry
                    </a>
                    <div class="btn btn-xs btn-info" onclick="showReceivers()">
                        <i class="fas fa-fw fa-search"></i> odbiorcy
                        <span class="badge badge-light receivers-counter"></span>
                    </div>
                </div>
            </div>

            <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                <label class="text-center flex-column w-100">płeć:
                <select name="gender" class="form-control form-control-sm ">
                    <option value="0">--- wybierz ---</option>
                    <option value="female">kobieta</option>
                    <option value="male">mężczyzna</option>
                </select>
                </label>
            </div>

            <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                <label class="text-center flex-column w-100">wiek od:
                    <input type="number" class="form-control form-control-sm number " name="age_from" value=""/>
                </label>
            </div>
            <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                <label class="text-center flex-column w-100">wiek do:
                    <input type="number" class="form-control form-control-sm number " name="age_to" value=""/>
                </label>
            </div>

            <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                <label class="text-center flex-column w-100">projekt:
                    <select name="project" class="form-control form-control-sm ">
                        <option value="0">--- wybierz ---</option>
                        @foreach($projects as $project_type_id => $project_lists)
                            <optgroup label="{{ $project_lists->first()->type->name }}">
                                @foreach($project_lists as $project)
                                    <option value="{{ $project->id }}">{{ $project->name }}</option>
                                @endforeach
                            </optgroup>
                        @endforeach
                    </select>
                </label>
            </div>

            <div class="additional-filters d-flex">

            </div>
        </nav>
    </div>
</div>
<div class="col-sm-12 col-lg-10 m-auto" id="receivers-container">

</div>

@section('scripts')
    @parent
    <script>
        countReceivers();
        $('.navbar-filtering').on('change', 'input, select', function (){
            countReceivers();
        });
    </script>
@endsection
