@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">

                            Zarządzanie wysyłką SMS
                        </h6>
                        <a href="{{ url('admin/sending/sms/create') }}" class="btn btn-primary btn-xs ">
                            <i class="fa fa-plus fa-fw"></i>
                            utwórz wysyłkę
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-hover">
                                <thead>
                                <th>#</th>
                                <th>tytuł</th>
                                <th>wiadomość</th>
                                <th>powiadomieni wolontariusze</th>
                                <th>Data wysłania</th>
                                <th>Wysyłający</th>
                                </thead>
                                @foreach($messages as $message)
                                    <tr >
                                        <td>{{ ($messages->currentPage() - 1) * $messages->perPage() + $loop->iteration  }}.</td>
                                        <td>{{ $message->subject }}</td>
                                        <td>
                                            <span style="display: inline-block;
                                                    max-width: 250px;
                                                    white-space: nowrap;
                                                    overflow: hidden !important;
                                                    text-overflow: ellipsis;">
                                                {{ $message->content }}
                                            </span>
                                        </td>
                                        <td>
                                            <a href="" class="btn btn-info btn-xs">
                                                <i class="fa fa-users fa-fw"></i>

                                                <span class="badge">
                                                    {{ $message->entities->count() }}
                                                </span>
                                            </a>
                                        </td>
                                        <td>{{$message->send_time}}</td>
                                        <td>{{ $message->user->name }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>

                    <div class="card-footer text-right">
                        {!! $messages->appends(Request::query())->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    @parent
    <script>
    </script>
@endsection
