<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Wysyłanie sms testowego </h5>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <form action="{{ url('admin/sending/sms/send-test') }}" method="post" id="dialog-form">
        {!! csrf_field() !!}
        <div class="form-group">
            <label>Wybierz odbiorcę</label>
            {!! Form::select('volunteer_id', $volunteers, null, ['class' => 'form-control required']) !!}
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
    <button type="button" class="btn btn-primary" id="send-test" data-loading-text="trwa wysyłanie">Wyślij</button>
</div>
