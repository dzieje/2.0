@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-select.min.css') }}">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card shadow">
                    <div class="card-body">
                        <form id="page-form" method="post" action="{{ url('admin/sending/sms/send') }}">
                            {!! csrf_field() !!}
                            <div class="row">
                                @include('admin.sending.nav')
                                <input type="hidden" name="phone_required" value="1">
                                <div class="col-md-8">
                                    <div class="card mt-2">
                                        <div class="card-body">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Tytuł wiadomości:</label>
                                                    <input maxlength="255" class="form-control required" name="subject" placeholder="tytuł wiadomości">
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Treść wiadomości:</label>
                                                    <textarea class="form-control required" name="content" ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card mt-2">
                                        <div class="card-body">
                                            <div class="row">
                                                @include('admin.sending.parameters')
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 text-center mt-5 mb-5">
                                    <span class="btn btn-primary modal-open" target="{{ url('admin/sending/sms/test') }}" data-toggle="modal" data-target="#modal">
                                        <i class="far fa-fw fa-paper-plane"></i> wyślij próbny sms
                                    </span>
                                    <button type="submit" class="btn  btn-info off-disable">
                                        <i class="fa fa-fw fa-paper-plane"></i> wyślij sms do <span class="badge" id="receivers-counter"></span> odbiorców
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    @parent
    <script src="/js/clipboard.min.js"></script>
    <script src="/js/bootstrap-select.min.js"></script>
    <script>
        $('.copy-parameter').tooltip();

        var previewedReceivers = true;

        var clipboard = new ClipboardJS('.copy-parameter', {
            text: function(trigger) {
                return trigger.getAttribute('key');
            }
        });

        clipboard.on('success', function(e) {
            $(e.trigger).tooltip('show');

            webwizards.sleep(1000).then(() => {
                $(e.trigger).tooltip('hide');
            });

            e.clearSelection();
        });

        $('.groups-filter').selectpicker();

        $('#modal').on('click', '#send-test', function () {
            var $btn = $(this);
            if ($('#dialog-form').valid()) {
                $btn.button('loading');

                $.ajax({
                    type: "POST",
                    url: $('#dialog-form').prop('action'),
                    data: $('#page-form').serialize() + '&' + $('#dialog-form').serialize(),
                    assync: false,
                    cache: false,
                    success: function (data) {
                        $('#modal').modal('hide');
                        $btn.button('reset');
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $('#modal .modal-body').html('<p class="text-danger">wystąpił błąd w trakcie wysyłki</p>');
                    },
                    dataType: 'json'
                });
            } else {
                $btn.button('reset');
            }
            return false;
        });

        $('[name="project"]').on('change', function() {
            let project_id = $(this).val();
            $.ajax({
                type: "GET",
                url: "{{ url('admin/sending/mailings/load-project-parameters') }}/" + project_id,
                assync: false,
                cache: false,
                success: function (data) {
                    $('.additional-filters').html(data);
                    $('.groups-filter').selectpicker();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
                dataType: 'html'
            });
        });

        function countReceivers() {
            $.ajax({
                type: "POST",
                url: "{{ url('admin/sending/mailings/count-receivers') }}",
                data: $('#page-form').serialize(),
                assync: false,
                cache: false,
                success: function (data) {
                    $('.receivers-counter').html(data.count);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
                dataType: 'json'
            });
            if(previewedReceivers){
                showReceivers();
            }
        }

        function showReceivers() {
            $.ajax({
                type: "POST",
                url: "{{ url('admin/sending/mailings/show-receivers') }}",
                data: $('#page-form').serialize(),
                assync: false,
                cache: false,
                success: function (data) {
                    $('#receivers-container').html(data);
                    previewedReceivers = true;
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
                dataType: 'html'
            });
        }

    </script>
@endsection
