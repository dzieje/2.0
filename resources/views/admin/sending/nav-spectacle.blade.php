<div class="form-group form-group-sm mr-1 ml-1 mb-2">
    <label class="text-center flex-column w-100">kulisa:
        <select name="wing" class="form-control form-control-sm ">
            <option value="0">--- wybierz ---</option>
            @foreach($wings as $wing_id => $wing)
                <option value="{{ $wing_id }}">{{ $wing }}</option>
            @endforeach
        </select>
    </label>
</div>
<div class="form-group form-group-sm mr-1 ml-1 mb-2">
    <label class="text-center flex-column w-100">rola:
        <select name="role" class="form-control form-control-sm ">
            <option value="0">--- wybierz ---</option>
            @foreach($roles as $role_id => $role)
                <option value="{{ $role_id }}">{{ $role }}</option>
            @endforeach
        </select>
    </label>
</div>
<div class="form-group form-group-sm mr-1 ml-1 mb-2">
    <label class="text-center flex-column w-100">grupy:
        <select name="groups[]" id="groups-filter" class="groups-filter form-control form-control-sm " data-style="btn-default" multiple="multiple" >
            <option value="0">--- wszyscy ---</option>
            @foreach($groups as $group_id => $group)
                <option value="{{ $group_id }}" >{{ $group }}</option>
            @endforeach
        </select>
    </label>
</div>
