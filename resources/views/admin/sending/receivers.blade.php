<div class="card mt-2" id="accordion-volunteers-filtering" role="tablist">
    <div class="card-header text-white bg-info p-2" role="button" data-toggle="collapse" data-parent="#accordion-volunteers-filtering" data-target="#collapseVolunteersFiltering">
            Odbiorcy <span class="badge badge-light">{{ $volunteers->count() }}</span>
    </div>
    <div class="table-responsive-sm">
    <table class="table table-sm table-condensed table-hover collapse" id="collapseVolunteersFiltering">
        <thead>
            <th><input type="checkbox" name="checkAll" class="mt-1 ml-1" value="1" checked></th>
            <th>#</th>
            <th>Płeć</th>
            <th>Imię</th>
            <th>Nazwisko</th>
            <th>Telefon</th>
            <Th>Email</Th>
            <Th>Data urodzin</Th>
            <th></th>
        </thead>
        @foreach($volunteers as $lp => $volunteer)
            <tr >
                <td>
                    @if(isset($filters['phone_required']) && $filters['phone_required'] && ! $volunteer->phone )

                    @else
                        <input type="checkbox" name="package[]" class="row-checkbox mt-1 ml-1" value="{{ $volunteer->id }}" checked>
                    @endif
                </td>
                <td>{{ ++$lp }}.</td>
                <td>@if($volunteer->gender == 'male') mężczyzna @elseif($volunteer->gender == 'female') kobieta @endif</td>
                <td>{{ $volunteer->name }}</td>
                <td>{{ $volunteer->surname }}</td>
                <td @if(isset($filters['phone_required']) && $filters['phone_required'] && ! $volunteer->phone ) class="bg-danger"  @endif>{{ $volunteer->phone }}</td>
                <td>{{ $volunteer->email }}</td>
                <Td>
                    {{ optional($volunteer->date_of_birth)->format('Y-m-d') }}
                    <span class="badge badge-info">{{ optional($volunteer->date_of_birth)->diffInYears(Carbon\Carbon::now()) }}</span>
                </Td>
                <td>
                    @if($volunteer->guardian)
                        <span class="btn btn-xs btn-info modal-open" data-toggle="modal" data-target="#modal" target="{{ url('admin/volunteers/guardian', [$volunteer->guardian_id]) }}">
                            <i class="fa fa-user fa-fw"></i> opiekun
                        </span>
                    @endif
                </td>
            </tr>
        @endforeach
    </table>
    </div>
</div>
<script>
    $('input[name="checkAll"]').on('change', function(){
        if($(this).is(':checked'))
        {
            $('.row-checkbox').prop('checked', true);
        }else{
            $('.row-checkbox').prop('checked', false);
        }

        let count = $('input.row-checkbox:checked').length;
        $('.receivers-counter').html(count);
    });
    $('.row-checkbox').on('change', function(){
        if( ! $(this).is(':checked'))
        {
            $('input[name="checkAll"]').prop('checked', false);
        }

        let count = $('input.row-checkbox:checked').length;
        $('.receivers-counter').html(count);
    });
</script>
