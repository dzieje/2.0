<div class="list-group">
    @foreach($surveys as $survey)
        <div role="button" class="copy-parameter list-group-item off-disable pointer" key="%%FORM-{{ $survey->slug }}%%" data-toggle="tooltip" data-trigger="manual" data-placement="top" title="Skopiowane do schowka!">
            <i class="fa fa-clipboard fa-fw"></i>
            %%FORM-{{ $survey->slug }}%% - <b>{{ $survey->name }}</b>
        </div>
    @endforeach
</div>
