<div class="col-sm-12">
    <div class="card border-info mb-4">
        <a href="#collapseCardParameters" class="d-block card-header text-white  bg-info py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardParameters">
            <h6 class="m-0 ">Dostępne parametry</h6>
        </a>
        <div class="collapse show" id="collapseCardParameters">
            <div class="card-body">
                <div class="list-group">
                    @foreach($parameters as $key=>$param)
                        <div role="button" class="copy-parameter list-group-item off-disable pointer" key="{{ $key }}" data-toggle="tooltip" data-trigger="manual" data-placement="top" title="Skopiowane do schowka!">
                            <i class="fa fa-clipboard fa-fw"></i>
                            {{ $key }} - <b>{{ $param }}</b>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
