<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Usuwanie roli </h5>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="panel-body">
        <form action="{{ url('admin/settings/roles/delete', [$role->id]) }}" method="post" id="dialog-form">
            {!! Form::token() !!}
            <h4>Potwierdź usunięcie roli {{ $role->name }}</h4>
        </form>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa usuwanie">Usuń</button>
</div>
