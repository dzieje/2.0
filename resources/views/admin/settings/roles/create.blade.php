<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Tworzenie roli</h5>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="panel-body">
        <form action="{{ url('admin/settings/roles/store') }}" method="post" id="dialog-form">
            {!! Form::token() !!}
            <fieldset>
                <div class="form-group">
                    <label>Nazwa roli:</label>
                    {!!  Form::text('name',  null, array('class' => 'form-control required', 'placeholder' => 'nazwa roli'))  !!}
                </div>
                <div class="form-group">
                    <label>Uprawnienia:</label>
                </div>
                <div class="row">
                    @foreach($permissions as $permission_id => $permission)
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="permissions[]" value="{{ $permission_id }}"> {{ $permission }}
                                </label>
                            </div>
                        </div>
                    @endforeach
                </div>
            </fieldset>
        </form>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa wprowadzanie zmian">Zapisz</button>
</div>
