@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            Zarządzanie rolami
                        </h6>

                        <span target="{{ url('admin/settings/roles/create') }}" class="btn btn-xs btn-primary  modal-open-lg" data-toggle="modal" data-target="#modal-lg">
                            <i class="fa fa-plus fa-fw"></i> dodaj rolę
                        </span>
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-hover">
                            <thead>
                            <th>#</th>
                            <th>Rola</th>
                            <th></th>
                            <th></th>
                            </thead>

                            @foreach($roles as $lp => $role)
                                <tr>
                                    <td>{{ ++$loop->index }}.</td>
                                    <td>{{ $role->name }}</td>
                                    <td>
                                        <span target="{{ URL::to('admin/settings/roles/edit', array($role->id)) }}" class="btn btn-warning btn-xs modal-open-lg" data-toggle="modal" data-target="#modal-lg">
                                            <i class="fas fa-pencil-alt fa-fw"></i> edytuj
                                            <span class="badge badge-secondary">
                                                {{ $role->permissions->count() }} uprawnień
                                            </span>
                                        </span>
                                    </td>

                                    <td>
                                        <span target="{{ URL::to('admin/settings/roles/delete', array($role->id)) }}" class="btn btn-danger btn-xs modal-open" data-toggle="modal" data-target="#modal">
                                            <i class="fas fa-trash-alt fa-fw"></i> usuń
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
