<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Zmiana hasła użytkownika</h5>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="panel-body">
        <form action="{{ url('admin/settings/users/update-password', [$user->id])  }}" method="post" id="dialog-form">

            Potwierdź wygenerowanie nowego hasła.
            {!! Form::token() !!}
        </form>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" data-loading-text="Zapisywanie..."  id="set">Potwierdź</button>
</div>
