@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            {{ __('Zarządzanie użytkownikami') }}
                        </h6>

                        <span target="{{ url('admin/settings/users/create') }}" class="btn btn-xs btn-info modal-open" data-toggle="modal" data-target="#modal">
                            <i class="fa fa-plus fa-fw"></i> dodaj użytkownika
                        </span>
                    </div>

                    <div class="card-body">
                        <table class="table table-sm table-hover">
                            <thead>
                            <th>#</th>
                            <th>Nazwisko</th>
                            <th>Login</th>
                            <th>Email</th>
                            <th>Rola</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            </thead>

                            @foreach($users as $user)
                                <tr>
                                    <td>{{ ++$loop->index }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->login }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        {{ ($user->role) ? $user->role->name : '---'}}
                                    </td>
                                    <td>
                                        <span target="{{ URL::to('admin/settings/users/edit', array($user->id)) }}" class="btn btn-warning btn-xs modal-open" data-toggle="modal" data-target="#modal">
                                            <i class="fas fa-pencil-alt fa-fw"></i> edytuj
                                        </span>
                                    </td>
                                    <td>
                                        <span target="{{ URL::to('admin/settings/users/password', array($user->id)) }}" class="btn btn-warning btn-xs modal-open" data-toggle="modal" data-target="#modal">
                                            <i class="fas fa-key fa-fw"></i> hasło
                                        </span>
                                    </td>
                                    <td>
                                        <span target="{{ URL::to('admin/settings/users/delete', array($user->id)) }}" class="btn btn-danger btn-xs modal-open" data-toggle="modal" data-target="#modal">
                                            <i class="fas fa-trash-alt fa-fw"></i> usuń
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
