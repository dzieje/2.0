<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Dodawanie użytkownika</h5>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="panel-body">
        <form action="{{ url('admin/settings/users/store') }}" method="post" id="dialog-form">
            {!! Form::token() !!}
            <fieldset>
                <div class="form-group">
                    <label>Login:</label>
                    {!!  Form::text('login', '', array('class' => 'form-control required', 'required', 'placeholder' => 'login')) !!}
                </div>
                <div class="form-group">
                    <label>Nazwisko:</label>
                    {!!  Form::text('name',  '', array('class' => 'form-control required', 'required', 'placeholder' => 'nazwisko'))  !!}
                </div>
                <div class="form-group">
                    <label>Email:</label>
                    {!! Form::email('email', null, ['class' => 'form-control required', 'required', 'placeholder'=> 'email']) !!}
                </div>
                <div class="form-group">
                    <label>Rola użytkownika</label>
                    {!! Form::select('user_role_id', $roles, null, ['class' => 'form-control required', 'required']) !!}
                </div>
            </fieldset>
        </form>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa dodawanie użytkownika">Dodaj użytkownika</button>
</div>
