<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Edycja użytkownika</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="panel-body">
        <form action="{{ url('admin/settings/users/update', [$user->id]) }}" method="post" id="dialog-form">
            {!! csrf_field() !!}
            <fieldset>
                <div class="form-group">
                    <label>Login:</label>
                    {!!  Form::text('login', $user->login, array('class' => 'form-control required', 'placeholder' => 'login')) !!}
                </div>
                <div class="form-group">
                    <label>Nazwisko:</label>
                    {!!  Form::text('name',  $user->name, array('class' => 'form-control required', 'placeholder' => 'nazwisko'))  !!}
                </div>
                <div class="form-group">
                    <label>Email:</label>
                    {!! Form::email('email', $user->email, ['class' => 'form-control required', 'required', 'placeholder'=> 'email']) !!}
                </div>
                <div class="form-group">
                    <label>Rola użytkownika</label>
                    {!! Form::select('user_role_id', $roles, $user->user_role_id, ['class' => 'form-control required', 'required']) !!}
                </div>
            </fieldset>
        </form>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa wprowadzanie zmian">Zapisz</button>
</div>
