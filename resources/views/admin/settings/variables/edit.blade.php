@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <form id="page-form" method="post"
              action="{{ url('admin/settings/variables/update', [$setting->id]) }}">
            {!! csrf_field() !!}

            @include('admin.settings.variables.types.'.$setting->type)
        </form>
    </div>
@endsection
