<div class="row justify-content-center">
    <div class="col-sm-12 col-lg-8">
        <div class="card shadow">
            <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                <h6 class="m-0 font-weight-bold text-info">
                    {{ $setting->name }}
                </h6>
                <a href="{{url('admin/settings/variables') }}" class="btn btn-default btn-xs ">
                    <i class="fa fa-remove fa-fw"></i> anuluj
                </a>
            </div>
            <input type="hidden" name="sortable" value="1">
            <div class="card-body">
                <div id="items" class="list-group">
                    @foreach($setting->value->attributes as $k => $attribute)
                        <label class="list-group-item list-group-item-action m-0 pl-5">
                            <input class="form-check-input" type="checkbox" name="attributes[{{ $k }}][checked]" value="1"
                                @if( $attribute->checked )
                                   checked
                                @endif
                            >
                            <input type="hidden" name="attributes[{{ $k }}][name]" value="{{ $attribute->name }}">
                            <input type="hidden" name="attributes[{{ $k }}][key]" value="{{ $attribute->key }}">
                            <input type="hidden" name="attributes[{{ $k }}][type]" value="{{ $attribute->type }}">
                            {{ $attribute->name }}
                        </label>
                    @endforeach
                </div>

            </div>
            <div class="card-footer text-center">
                <button type="submit" class="btn btm-sm btn-info">
                    <i class="fas fa-save fa-fw"></i>
                    Zapisz
                </button>
            </div>
        </div>
    </div>
</div>

@section('scripts')
    @parent
    <script src="{{ asset('js/Sortable.min.js') }}"></script>
    <script src="{{asset('js/jquery-sortable.js')}}"></script>
    <script>
        $(document).ready( function(){
            $('#items').sortable();
        });
    </script>
@endsection
