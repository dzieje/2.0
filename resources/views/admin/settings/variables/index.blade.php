@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-lg-10">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info">
                            {{ __('Zarządzanie ustawieniami') }}
                        </h6>
                    </div>

                    <div class="card-body">
                        <table class="table table-sm table-hover">
                            <thead>
                            <th>#</th>
                            <th>Nazwa</th>
                            <th>Wartość</th>
                            <th></th>
                            </thead>

                            @foreach($settings as $setting)
                                <tr>
                                    <td>{{ ++$loop->index }}</td>
                                    <td>{{ $setting->name }}</td>
                                    <td>
                                        @if($setting->type == 'mail')
                                            <span class="btn btn-info btn-xs modal-open-lg" data-toggle="modal" data-target="#modal-lg" target="{{ url('admin/volunteers/select-volunteer?url=').url('admin/settings/variables/preview-mail', [$setting->id]) }}" >
                                                <i class="fas fa-eye fa-fw"></i>
                                                podgląd
                                            </span>
                                        @else
                                            {!!  $setting->preview !!}
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ URL::to('admin/settings/variables/edit', array($setting->id)) }}" class="btn btn-warning btn-xs">
                                            <i class="fas fa-pencil-alt fa-fw"></i> edytuj
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
