@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-lg-10">
                {!! Form::open(['url' => url('admin/volunteers/list/save-projects', [$volunteer->id]), 'id' => 'page-form']) !!}
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6>Projekty wolontariusza {{ $volunteer->surname }} {{ $volunteer->name }}</h6>
                        <a href="{{ URL::previous() }}" class="btn btn-outline-dark btn-sm">
                            <i class="fa fa-ban fa-fw"></i> anuluj
                        </a>
                    </div>
                    <div class="card-body">
                        @foreach($projects as $project_type_id => $groupedProjects)
                        <div class="row">
                            <div class="col-sm-12 bg-info text-white shadow rounded mb-2">
                                <p class="lead mb-0">
                                    {{ $groupedProjects->first()->type->name }}
                                </p>
                            </div>
                            @foreach($groupedProjects as $project)
                                <div class="col-sm-6 col-lg-4 m-1">
                                    <div class="form-group form-check p-4 text-center border-info border">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="projects[]" value="{{ $project->id }}" @if($volunteer->projects->contains($project->id)) checked @endif  >
                                            {{ $project->name }}
                                        </label>
                                        @if($volunteer->projectsWithTrashed->contains($project->id))
                                            <div class="card mt-2 border-left-info">
                                                <div class="card-body py-1">
                                                    <small>umowa: </small>
                                                    @if($volunteer->birth_date)
                                                    <strong>{{ $volunteer->projectsWithTrashed->find($project->id)->pivot->contract }}</strong>
                                                    @else
                                                        <small class="text-danger font-italic">
                                                            brak daty urodzenia
                                                        </small>
                                                    @endif
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        @endforeach
                    </div>
                    <div class="card-footer text-right">
                        {!! Form::submit('Zapisz zmiany', ['class' => 'btn btn-primary off-disable']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>

    </script>
@endsection
