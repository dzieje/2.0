<div class="modal-header">
    <h4 class="modal-title" id="myModalLabel">Usuwanie wolontariusza </h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

</div>
<div class="modal-body">
    <div class="panel-body">
        <form action="{{ url('admin/volunteers/list/delete', [$volunteer->id])  }}" method="post" id="dialog-form">
            {!! csrf_field() !!}
            <h4>Potwierdź usunięcie wolontariusza {{ $volunteer->surname }} {{ $volunteer->name }}</h4>
        </form>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa usuwanie wolontariusza">Usuń</button>
</div>
