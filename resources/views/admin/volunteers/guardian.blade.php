<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Dane opiekuna</h4>
</div>
<div class="modal-body">
    <div class="panel-body">
        <table class="table table-condensed">
            <Tr>
                <td><strong>Imię i Nazwisko</strong></td>
                <Td>{{ $guardian->name }} {{ $guardian->surname }}</Td>
            </Tr>
            <Tr>
                <td><strong>Adres</strong></td>
                <Td>{{ $guardian->street }} {{ $guardian->house_number }}@if($guardian->flat_number != '')/{{ $guardian->flat_number }}@endif {{ $guardian->post_code }} {{ $guardian->city }}</Td>
            </Tr>
            <Tr>
                <td><strong>Telefon</strong></td>
                <Td>{{ $guardian->phone }}</Td>
            </Tr>
            <Tr>
                <td><strong>Email</strong></td>
                <Td>{{ $guardian->email }}</Td>
            </Tr>
            <Tr>
                <td><strong>Data urodzenia</strong></td>
                <Td>{{ $guardian->date_of_birth }}</Td>
            </Tr>
            <tr>
                <td><strong>Wybrane upoważnienie</strong></td>
                <Td>
                    @if($guardian->guardian_permission == 1)
                        Zobowiązuję się do przywożenia oraz odbierania dziecka z prób do widowisk oraz samych widowisk. Miejscem dowozu oraz odbioru będzie zawsze Park Dzieje, zlokalizowany w Murowanej Goślinie przy ul. Brackiej 9.
                    @else
                        Wyrażam zgodę na samodzielne przychodzenie i powrót dziecka z prób do widowisk oraz samych widowisk. Zostałem poinformowany, że powroty z prób do widowisk, a przede wszystkim z widowisk - będą następowały w późnych porach nocnych i jestem tego świadomy.
                    @endif
                </Td>
            </tr>
        </table>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
</div>