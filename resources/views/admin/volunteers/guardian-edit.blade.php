<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Dane opiekuna</h4>
</div>
<div class="modal-body">
{!! Form::open(['url' => url('admin/volunteers/guardian-update', [$guardian->id]), 'id' => 'dialog-form']) !!}
    <div class="row">
    <div class="form-group col-sm-6">
        <label>Nazwisko:</label>
        {!! Form::text('surname', $guardian->surname, ['class' => 'form-control required', 'placeholder' => 'podaj nazwisko']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label>Imię:</label>
        {!! Form::text('name', $guardian->name, ['class' => 'form-control required', 'placeholder' => 'podaj imię']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label>Telefon:</label>
        {!! Form::text('phone', $guardian->phone, ['class' => 'form-control required', 'placeholder' => 'podaj telefon']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label>Email:</label>
        {!! Form::text('email', $guardian->email, ['class' => 'form-control required', 'placeholder' => 'podaj email']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label>Data urodzenia:</label>
        {!! Form::text('date_of_birth', $guardian->date_of_birth, ['class' => 'form-control required dynamic-datepicker']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label>Adres miasto:</label>
        {!! Form::text('city', $guardian->city, ['class' => 'form-control ']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label>Adres kod pocztowy:</label>
        {!! Form::text('post_code', $guardian->post_code, ['class' => 'form-control ']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label>Adres ulica:</label>
        {!! Form::text('street', $guardian->street, ['class' => 'form-control ']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label>Adres nr domu:</label>
        {!! Form::text('house_number', $guardian->house_number, ['class' => 'form-control ']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label>Adres nr mieszkania:</label>
        {!! Form::text('flat_number', $guardian->flat_number, ['class' => 'form-control ']) !!}
    </div>

    <div class="form-group col-sm-12">
        <div class="radio">
            <label>
                <input type="radio" name="guardian_permission" value="1" @if($guardian->guardian_permission == 1) checked @endif>
                Zobowiązuję się do przywożenia
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="guardian_permission" value="2" @if($guardian->guardian_permission == 2) checked @endif>
                Wyrażam zgodę na samodzielne przychodzenie i powrót
            </label>
        </div>
    </div>
    </div>
{!! Form::close() !!}
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" data-dismiss="modal" id="set">Zapisz</button>
</div>