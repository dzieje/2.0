@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                {!! Form::open(['url' => url('admin/volunteers/list/update', [$volunteer->id]), 'id' => 'page-form']) !!}
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6>Edycja danych wolontariusza {{ $volunteer->surname }} {{ $volunteer->name }}</h6>
                        <a href="{{ URL::previous() }}" class="btn btn-outline-dark btn-sm">
                            <i class="fa fa-ban fa-fw"></i> anuluj
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label>Nazwisko:</label>
                                    {!! Form::text('surname', $volunteer->surname, ['class' => 'form-control required', 'placeholder' => 'podaj nazwisko']) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label>Imię:</label>
                                    {!! Form::text('name', $volunteer->name, ['class' => 'form-control required', 'placeholder' => 'podaj imię']) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label>Telefon:</label>
                                    {!! Form::text('phone', $volunteer->phone, ['class' => 'form-control', 'placeholder' => 'podaj telefon']) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label>Email:</label>
                                    {!! Form::text('email', $volunteer->email, ['class' => 'form-control email', 'placeholder' => 'podaj email']) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label>Płeć:</label>
                                    {!! Form::select('gender', [null => '---wybierz---', 'male' => 'mężczyzna', 'female' => 'kobieta'], $volunteer->gender, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label>Data urodzenia:</label>
                                    {!! Form::date('date_of_birth', optional($volunteer->date_of_birth)->format('Y-m-d'), ['class' => 'form-control dynamic-datepicker', 'placeholder' => 'podaj datę urodzenia']) !!}
                                </div>
                            </div>

                            @foreach($attributes as $attribute)
                                <div class="col-sm-6 col-lg-4">
                                    <div class="form-group">
                                        <label>{{ $attribute->name }}:</label>
                                        @if($attribute->is_collection)
                                            <br>
                                            @foreach(json_decode( $attribute->description ) as $value)
                                                <div class="form-check form-check-inline">
                                                    <input name="{{ $attribute->slug.'[]' }}" type="checkbox" class="form-check-input" value="{{ $value }}" id="{{$attribute->slug}}_{{$value}}" @if($volunteer->{$attribute->slug}->contains($value)) checked @endif>
                                                    <label class="form-check-label" for="{{$attribute->slug}}_{{$value}}">{{ $value }}</label>
                                                </div>
                                            @endforeach
                                        @else
                                            @if($attribute->type == 'tekstowy')
                                                {!! Form::text($attribute->slug, $volunteer->{$attribute->slug}, ['class' => 'form-control ']) !!}
                                            @elseif($attribute->type == 'checkbox')
                                                {!! Form::checkbox($attribute->slug, 1, $volunteer->{$attribute->slug}, ['class' => 'form-control ']) !!}
                                            @elseif($attribute->type == 'liczbowy')
                                                {!! Form::number($attribute->slug, $volunteer->{$attribute->slug}, ['class' => 'form-control ']) !!}
                                            @else
                                                {!! Form::date($attribute->slug, optional($volunteer->{$attribute->slug})->format('Y-m-d'), ['class' => 'form-control dynamic-datepicker']) !!}
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        {!! Form::submit('Zapisz zmiany', ['class' => 'btn btn-primary off-disable']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>

    </script>
@endsection
