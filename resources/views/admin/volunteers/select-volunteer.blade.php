<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Wyszukaj wolontariusza </h5>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <form action="{{ url('admin/volunteers/proceed-selection') }}" method="post" id="dialog-form">
        {!! csrf_field() !!}
        <input type="hidden" name="url" value="{{ $redirect_url }}">

        <div class="form-group">
            {!! Form::text('volunteer', null, ['class' => 'form-control', 'id' => 'search-volunteer', 'autocomplete' => 'off', 'placeholder' => 'wyszukaj wolontariusza'])!!}
        </div>
        <input type="hidden" name="volunteer_id">
        <h5 class="text-center text-info" id="selection-container">

        </h5>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa wykonywanie">Potwierdź</button>
</div>

<script src="/js/bloodhound.js"></script>
<script src="/js/typeahead.jquery.js"></script>
<script>
    var volunteers = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/admin/volunteers/search',
            replace: function(url, query) { return url + "?limit=15&q=" + query; },
            filter: function (data) {
                return $.map(data.volunteers, function (volunteer) {
                    return volunteer
                });
            }
        }
    });

    var actorsTypeahead = $('#search-volunteer').typeahead({
            minLength: 2,
            highlight: true,
            hint: true
        },
        { name: 'name', displayKey: 'name', minLength: 2, source: volunteers, limit: 15 });

    $('#search-volunteer').on('typeahead:selected', function(ev, datum) {
        var volunteer_id = datum.id;

        $('#selection-container').html(datum.name);
        $('input[name="volunteer_id"]').val(volunteer_id);
        actorsTypeahead.typeahead('val','');
    });

    $('#search-volunteer').on('typeahead:close', function(){
        $(this).val('');
    });
</script>
