@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-lg-8">
                <form accept-charset="UTF-8" method="POST" id="page-form" action="{{ url('admin/volunteers/attributes/store') }}">
                    <div class="card shadow">
                        <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                            <h6 class="m-0 font-weight-bold text-info">
                                Tworzenie atrybutu
                            </h6>
                            <a href="{{ URL::previous() }}" class="btn btn-default btn-xs ">
                                <i class="fa fa-remove fa-fw"></i> anuluj
                            </a>
                        </div>
                        <div class="card-body">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label>Nazwa atrybutu:</label>
                                {!!  Form::text('name', null, array('class' => 'form-control required', 'placeholder' => 'nazwa atrybutu', 'required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Typ pola</label>
                                {!! Form::select('type', $types, null, array('class' => 'form-control required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Dostępny w projekcie</label>
                            </div>
                            @foreach($projects as $project)
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" name="projects[]" type="checkbox" id="inlineCheckbox{{ $project->id }}" value="{{ $project->id }}">
                                    <label class="form-check-label" for="inlineCheckbox{{ $project->id }}">
                                        <small class="badge badge-dark">{{ $project->type->name }}</small>

                                        {{ $project->name }}
                                    </label>
                                </div>
                            @endforeach
                            <div id="collection-container" class="invisible">
                                <hr>

                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="multiple_choice" value="1" id="multiple_choice" checked>
                                    <label class="custom-control-label" for="multiple_choice">Wielokrotnego wyboru</label>
                                </div>
                                <p class="lead">Dopuszczone wartości:</p>
                                <div class="row" id="values-container">
                                    <div class="input-group col-6 mb-2">
                                        <input type="text" class="form-control" name="values[]" >
                                    </div>
                                </div>
                                <div class="form-group  mt-2">
                                    <div class="btn btn-info btn-block btn-sm w-50 m-auto" id="add-value">
                                        <i class="fa fa-plus fa-fw"></i> dodaj
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-center">
                            <button class="btn btn-primary" type="submit">
                                <i class="fas fa-save fa-fw"></i>
                                Zapisz
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        $('select[name="type"]').on('change', function(){
            if( $('select[name="type"] option:selected').val() == 'collection' ){
                $('#collection-container').removeClass('invisible');
            }else{
                $('#collection-container').addClass('invisible');
            }
        });
        $('#add-value').on('click', function (){
            $('#values-container').append('<div class="input-group col-6 mb-2">\n' +
                '                                        <input type="text" class="form-control" name="values[]" >\n' +
                '                                        <div class="input-group-append">\n' +
                '                                            <button class="btn btn-outline-danger remove-value" type="button">\n' +
                '                                                <i class="fas fa-trash-alt"></i>\n' +
                '                                            </button>\n' +
                '                                        </div>\n' +
                '                                    </div>')
        });

        $('#values-container').on('click', '.remove-value', function (){
            $(this).parents('.input-group').remove();
        });
    </script>
@endsection
