@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-lg-8">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info"> Zarządzanie atrybutami Wolontariusza </h6>
                        <a class="btn btn-info btn-xs" href="{{ url('admin/volunteers/attributes/create') }}">
                            <i class="fas fa-plus fa-fw"></i> utwórz
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive mt-2">
                            <table class="table table-sm table-hover">
                                <thead>
                                <th>#</th>
                                <th>Parametr</th>
                                <th>Typ pola</th>
                                <th>Projekty</th>
                                <th></th>
                                <th></th>
                                </thead>
                                @foreach($attributes as $attribute)
                                    <tr >
                                        <td>{{ $loop->iteration  }}.</td>
                                        <td>
                                            {{ $attribute->name }}
                                        </td>
                                        <td>
                                            @if($attribute->is_collection)
                                                lista
                                            @else
                                                {{ $attribute->type }}
                                            @endif
                                        </td>

                                        <td>
                                            @foreach($attribute->projects as $project)
                                                <span class="badge badge-info">
                                                    {{ $project->name }}
                                                </span>
                                            @endforeach
                                        </td>
                                        <td>
                                            @if(! $attribute->group == 'show')
                                            <a class="btn btn-warning btn-xs" href="{{ url('admin/volunteers/attributes/edit', [$attribute->id]) }}" >
                                                <i class="fas fa-pencil-alt fa-fw"></i> edytuj
                                            </a>
                                            @endif
                                        </td>
                                        <td>
                                            @if(! $attribute->group == 'show')
                                            <span target="{{ url('admin/volunteers/attributes/delete', [$attribute->id]) }}" class="btn btn-xs btn-danger modal-open" data-toggle="modal" data-target="#modal">
                                                <i class="fa fa-trash fa-fw"></i> usuń
                                            </span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    @parent
    <script>
        $('[data-toggle="tooltip"]').tooltip()
    </script>
@endsection
