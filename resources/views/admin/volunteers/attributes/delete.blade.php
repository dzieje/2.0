<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Usuwanie atrybutu</h5>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="panel-body">
        <form action="{{ url('admin/volunteers/attributes/delete', [$attribute->id]) }}" method="post" id="dialog-form">
            {!! csrf_field()!!}
            <p>
                Potwierdź usunięcie atrybutu {{ $attribute->name }}.
            </p>
        </form>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
    <button type="button" class="btn btn-primary" id="set" data-loading-text="trwa wykonywanie">Usuń</button>
</div>
