@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card shadow">
                    <div class="card-header d-sm-flex align-items-center justify-content-between py-3">
                        <h6 class="m-0 font-weight-bold text-info"> Zarządzanie wypełnionymi formularzami Wolontariusza </h6>
                        <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSearchForm" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="btn btn-sm btn-outline-info">
                                <i class="fas fa-search"></i>
                                @if(count( array_filter(Request::all() ) ) > 0)
                                    <span class="badge badge-info">
                                        {{ count( array_filter(Request::all() ) ) }}
                                    </span>
                                @endif
                            </span>
                        </button>
                    </div>
                    <div class="card-body">
                        <nav class="navbar navbar-light bg-light">
                            <div class="collapse navbar-collapse" id="navbarSearchForm">
                                <form class="form-inline" role="search" id="search-form" action="{{ url('admin/volunteers/list') }}" method="get">
                                    <div class="form-group form-group-sm flex-column text-center mr-1 ml-1 mb-2">
                                        <label>Filtrowanie wolontariuszy <span class="badge badge-dark ml-1">{{ $volunteers->total() }}</span></label>
                                        <div>
                                            <a class="btn btn-xs btn-danger" href="{{ url('admin/volunteers/list') }}">
                                                <i class="fa fa-trash-alt fa-fw"></i> usuń filtry
                                            </a>
                                            <span class="btn btn-primary btn-xs" id="export-results">
                                                <i class="fa fa-file-excel fa-fw" aria-hidden="true"></i> eksportuj wyniki
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-sm ">
                                        <div class="divider">|</div>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">Imię:
                                            <input type="text" class="form-control form-control-sm" name="name" value="{{ Request::get('name') }}"/>
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">Nazwisko:
                                            <input type="text" class="form-control form-control-sm" name="surname" value="{{ Request::get('surname') }}"/>
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <div class="divider">|</div>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label  class="text-center flex-column w-100">płeć:
                                            <select name="gender" class="form-control form-control-sm">
                                                <option value="">--- wybierz ---</option>
                                                <option value="female" {{ (Request::get('gender') == 'female') ? 'selected' : '' }}>kobieta</option>
                                                <option value="male" {{ (Request::get('gender') == 'male') ? 'selected' : '' }}>mężczyzna</option>
                                            </select>
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <div class="divider">|</div>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">wiek od:
                                            <input type="text" class="form-control form-control-sm number" name="age_from" value="{{ Request::get('age_from') }}"/>
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label class="text-center flex-column w-100">wiek do:
                                            <input type="text" class="form-control form-control-sm number" name="age_to" value="{{ Request::get('age_to') }}"/>
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <div class="divider">|</div>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label  class="text-center flex-column w-100">profil:
                                            <select name="profile" class="form-control form-control-sm">
                                                <option value="">--- wybierz ---</option>
                                                <option value="yes" {{ (Request::get('profile') == 'yes') ? 'selected' : '' }}>tak</option>
                                                <option value="no" {{ (Request::get('profile') == 'no') ? 'selected' : '' }}>nie</option>
                                            </select>
                                        </label>
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <div class="divider">|</div>
                                    </div>
                                    <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                        <label  class="text-center flex-column w-100">projekt:
                                            <select name="project" class="form-control form-control-sm">
                                                <option value="">--- wybierz ---</option>
                                                @foreach($projects as $project_slug => $project_name)
                                                    <option value="{{ $project_slug }}" {{ (Request::get('project') == $project_slug) ? 'selected' : '' }}>
                                                        {{ $project_name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </label>
                                    </div>

                                    @foreach($attributes as $attribute)
                                        <div class="form-group form-group-sm mr-1 ml-1 mb-2">
                                            <label class="text-center flex-column w-100">
                                                <span data-toggle="tooltip" title="{{ implode(', ', $attribute->projects->pluck('name')->toArray() ) }}"><i class="fas fa-info-circle fa-fw"></i>{{ $attribute->name }}:</span>
                                                @if($attribute->is_collection)
                                                    <select name="{{ $attribute->slug }}" class="form-control form-control-sm">
                                                        <option value="">--- wybierz ---</option>
                                                        @foreach(json_decode( $attribute->description ) as $k => $attribute_value )
                                                            <option value="{{ $attribute_value }}" {{ (Request::get($attribute->slug) == $attribute_value) ? 'selected' : '' }}>
                                                                {{ $attribute_value }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                @else
                                                    @if($attribute->type == 'tekstowy')
                                                        <input type="text" class="form-control form-control-sm" name="{{ $attribute->slug }}" value="{{ Request::get($attribute->slug) }}"/>
                                                    @elseif($attribute->type == 'liczbowy')
                                                        <input type="text" class="form-control form-control-sm number" name="{{ $attribute->slug }}" value="{{ Request::get($attribute->slug) }}"/>
                                                    @elseif($attribute->type == 'data')
                                                        <input type="date" class="form-control form-control-sm" name="{{ $attribute->slug }}" value="{{ Request::get($attribute->slug) }}"/>
                                                    @elseif($attribute->type == 'checkbox')
                                                        <select name="{{ $attribute->slug }}" class="form-control form-control-sm">
                                                            <option value="">--- wybierz ---</option>
                                                            <option value="1" {{ (Request::get($attribute->slug) === 1) ? 'selected' : '' }}>tak</option>
                                                            <option value="0" {{ (Request::get($attribute->slug) === 0) ? 'selected' : '' }}>nie</option>
                                                        </select>
                                                    @endif
                                                @endif
                                            </label>
                                        </div>
                                    @endforeach
                                </form>
                            </div>
                        </nav>
                        <div class="table-responsive mt-2">
                            <table class="table table-sm table-hover">
                                <thead>
                                <th>#</th>
                                <th></th>
                                <th>Profil</th>
                                @foreach($columns as $column)
                                    @if($column->checked && ($column->type == 'embed' || $attributes->where('slug', $column->key)->first() ) )
                                        <th>{{ $column->name }}</th>
                                    @endif
                                @endforeach
                                <Th>Projekty</Th>
                                <th></th>
                                <th></th>
                                </thead>
                                @foreach($volunteers as $volunteer)
                                    <tr >
                                        <td>{{ ($volunteers->currentPage() - 1) * $volunteers->perPage() + $loop->iteration  }}.</td>
                                        <td>
                                            <a href="{{ url('admin/volunteers/list/edit', [$volunteer->id]) }}" class="btn btn-xs btn-warning">
                                                <i class="fa fa-pencil-alt fa-fw"></i> dane
                                            </a>
                                        </td>
                                        <td>
                                            @if($volunteer->login)
                                                <i class="fas fa-fw fa-check " data-toggle="tooltip" title="{{ $volunteer->login }}"></i>

                                            @else
                                                <div class="fas fa-minus fa-fw"></div>
                                            @endif
                                        </td>
                                        @foreach($columns as $column)
                                            @if($column->checked && ($column->type == 'embed' || $attributes->where('slug', $column->key)->first() ) )
                                                <td>
                                                    @if($column->type == 'embed')
                                                        @if( $volunteer->{$column->key} )
                                                            @if($column->key == 'gender')
                                                                {!!   $volunteer->{$column->key} == 'male' ? 'mężczyzna' : 'kobieta' !!}
                                                            @else
                                                                {!!   $volunteer->{$column->key} !!}
                                                            @endif
                                                        @endif
                                                    @else
                                                        @if($attributes->where('slug', $column->key)->first()->is_collection)
                                                            @foreach($volunteer->{$column->key} as $value)
                                                                <span class="badge badge-info">{{ $value }}</span>
                                                            @endforeach
                                                        @else
                                                            @if($attributes->where('slug', $column->key)->first()->type == 'checkbox')
                                                                @if($volunteer->{$column->key} == 1)
                                                                    tak
                                                                @else
                                                                    nie
                                                                @endif
                                                            @else
                                                                {{ $volunteer->{$column->key} }}
                                                            @endif
                                                        @endif
                                                    @endif
                                                </td>
                                            @endif
                                        @endforeach
                                        <td>
                                            <a
                                                href="{{ url('admin/volunteers/list/projects', [$volunteer->id]) }}"

                                                @if($volunteer->date_of_birth)
                                                    class="btn btn-xs btn-info"
                                                @else
                                                    class="btn btn-xs btn-danger"
                                                @endif

                                                data-toggle="tooltip" title="{{ implode(', ', $volunteer->projects->pluck('name')->toArray() ) }}"
                                            >
                                                <i class="fas fa-exchange-alt fa-fw"></i>
                                                {{ __('projekty') }}
                                                <span class="badge badge-light">{{ $volunteer->projects->count() }}</span>
                                            </a>
                                        </td>
                                        <td>
                                            @if($volunteer->guardian)
                                                <span class="btn btn-xs btn-info modal-open" data-toggle="modal" data-target="#modal" target="{{ url('admin/volunteers/list/guardian', [$volunteer->guardian_id]) }}">
                                                    <i class="fa fa-user fa-fw"></i> opiekun
                                                </span>

                                                <span class="btn btn-xs btn-warning modal-open-lg" data-toggle="modal" data-target="#modal-lg" target="{{ url('admin/volunteers/list/guardian-edit', [$volunteer->guardian_id]) }}">
                                                    <i class="fa fa-pencil fa-fw"></i> opiekun
                                                </span>
                                            @endif
                                        </td>
                                        <td>
                                            <span target="{{ url('admin/volunteers/list/delete', [$volunteer->id]) }}" class="btn btn-xs btn-danger modal-open" data-toggle="modal" data-target="#modal">
                                                <i class="fa fa-trash fa-fw"></i> usuń
                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>

                    <div class="card-footer ">
                        {!! $volunteers->appends(Request::query())->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    @parent
    <script>
        $('[data-toggle="tooltip"]').tooltip()
        $('#search-form').validate();
        $('#search-form select, #search-form input[type="checkbox"]').on('change', function () {
            if( $('#search-form').valid() && ! $(this).hasClass('meeting_id')) {
                $('#search-form').submit();
            }
        });

        $('#search-form input').on('keyup keypress', function (e) {
            if(e.which === 13){
                if( $('#search-form').valid() ) {
                    $('#search-form').submit();
                }
            }
        });

        $('#export-results').on('click', function(){
            $.ajax({
                type: "POST",
                url: '/admin/volunteers/list/export',
                data: $('#search-form').serialize(),
                assync:false,
                cache:false,
                success: function( data ) {
                    location.reload();
                }
            });
        });
    </script>
@endsection
