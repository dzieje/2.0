<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @if(Session::has('download.in.the.next.request'))
        <?php
        $redirect_url = Session::get('download.in.the.next.request');
        Session::forget('download.in.the.next.request');
        ?>
        <meta http-equiv="refresh" content="0;url={{ $redirect_url }}">
    @endif

    <title>{{ config('app.name', 'Laravel') }}</title>
    <script src="{{ asset('js/app.js') }}" ></script>
    <link href="{{ asset('css/form.css') }}" rel="stylesheet">
</head>
<body id="app-layout">
@include('layouts.modules.flash')

<div class="container-fluid" id="page-container">
    @yield('content')
</div>

@include('layouts.modules.modals')

@yield('scripts')
</body>
</html>
