<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <script src="{{ asset('js/app.js') }}" ></script>
    <!-- Custom fonts for this template-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>

<body class="d-flex flex-column vh-100" style="background-color: #f8f9fc;">

    @yield('content')
<footer class="sticky-footer mt-auto bg-white">
    <div class="container my-auto">
        <div class=" text-center my-auto d-flex justify-content-center align-items-center">
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="switch_contrast" name="contrast_mode" value="1">
                <label class="custom-control-label" for="switch_contrast">Wysoki kontrast</label>
            </div>
            <div class="ml-2" style="transform: scale(0.75);">
                <button type="button" class="btn btn-sm btn-outline-dark zoomed" data-zoom="none"><i class="fas fa-font fa-sm"></i></button>
                <button type="button" class="btn btn-sm btn-outline-dark zoomed" data-zoom="sm"><i class="fas fa-font fa-lg"></i></button>
                <button type="button" class="btn btn-sm btn-outline-dark zoomed" data-zoom="md"><i class="fas fa-font fa-2x"></i></button>
            </div>
        </div>

        <img src="{{ asset('images/2020-NCK_dofinans_kulturawsieci-mono.jpg') }}"  class="mx-auto d-block" style="max-height: 100px; max-width: 100%;">

        <div class="copyright text-center my-auto">
            <span>Made By <a class="text-muted" href="https://webwizards.pl/">WEBWIZARDS</a></span>
        </div>
    </div>
</footer>
@yield('scripts')
<script>
    $(document).ready(function(){
        if(window.localStorage.getItem('contrast-mode') == 1){
            $('#switch_contrast').prop("checked", true);
            $('body').addClass('inverted');
        }

        $('#switch_contrast').on("change", function (){
            if($(this).prop('checked')){
                $('body').addClass('inverted');
                window.localStorage.setItem('contrast-mode', 1);
            }else{
                $('body').removeClass('inverted');
                window.localStorage.setItem('contrast-mode', 0);
            }
        });

        if(window.localStorage.getItem('zoom-mode') == 1){
            $('body').addClass('zoomed');
            if(window.localStorage.getItem('zoom-value') == 'sm'){
                $('body').addClass('zoomed-sm');
                $('button.zoomed[data-zoom="sm"]').addClass('active');
            }else if(window.localStorage.getItem('zoom-value') == 'md'){
                $('body').addClass('zoomed-md');
                $('button.zoomed[data-zoom="md"]').addClass('active');
            }
        }else{
            $('button.zoomed[data-zoom="none"]').addClass('active');
        }

        $('button.zoomed').on('click', function(){
            $('button.zoomed').removeClass('active');
            let zoomed = $(this).data('zoom');

            if(zoomed === 'none'){
                $('body').removeClass('zoomed');
                window.localStorage.setItem('zoom-mode', 0);
                $('button.zoomed[data-zoom="none"]').addClass('active');
            }else{
                window.localStorage.setItem('zoom-mode', 1);
                window.localStorage.setItem('zoom-value', zoomed);
                $('body').addClass('zoomed');

                $('body').removeClass('zoomed-sm');
                $('body').removeClass('zoomed-md');

                if(zoomed === 'sm'){
                    $('body').addClass('zoomed-sm');
                    $('button.zoomed[data-zoom="sm"]').addClass('active');
                }else if(zoomed === 'md'){
                    $('body').addClass('zoomed-md');
                    $('button.zoomed[data-zoom="md"]').addClass('active');
                }
            }
        });
    });
</script>
</body>

</html>
