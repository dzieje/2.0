<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <script src="{{ asset('js/app.js') }}" ></script>
    <!-- Custom fonts for this template-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">
    @include('layouts.modules.sidebar')

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>

                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="switch_contrast" name="contrast_mode" value="1">
                    <label class="custom-control-label" for="switch_contrast">Wysoki kontrast</label>
                </div>
                <div class="ml-2" style="transform: scale(0.75);">
                    <button type="button" class="btn btn-sm btn-outline-dark zoomed" data-zoom="none"><i class="fas fa-font fa-sm"></i></button>
                    <button type="button" class="btn btn-sm btn-outline-dark zoomed" data-zoom="sm"><i class="fas fa-font fa-lg"></i></button>
                    <button type="button" class="btn btn-sm btn-outline-dark zoomed" data-zoom="md"><i class="fas fa-font fa-2x"></i></button>
                </div>

                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">

                    <!-- Nav Item - Alerts -->
{{--                    <li class="nav-item dropdown no-arrow mx-1">--}}
{{--                        <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                            <i class="fas fa-bell fa-fw"></i>--}}
{{--                            <!-- Counter - Alerts -->--}}
{{--                            <span class="badge badge-danger badge-counter">3+</span>--}}
{{--                        </a>--}}
{{--                        <!-- Dropdown - Alerts -->--}}
{{--                        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">--}}
{{--                            <h6 class="dropdown-header">--}}
{{--                                Alerts Center--}}
{{--                            </h6>--}}
{{--                            <a class="dropdown-item d-flex align-items-center" href="#">--}}
{{--                                <div class="mr-3">--}}
{{--                                    <div class="icon-circle bg-primary">--}}
{{--                                        <i class="fas fa-file-alt text-white"></i>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div>--}}
{{--                                    <div class="small text-gray-500">December 12, 2019</div>--}}
{{--                                    <span class="font-weight-bold">A new monthly report is ready to download!</span>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a class="dropdown-item d-flex align-items-center" href="#">--}}
{{--                                <div class="mr-3">--}}
{{--                                    <div class="icon-circle bg-success">--}}
{{--                                        <i class="fas fa-donate text-white"></i>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div>--}}
{{--                                    <div class="small text-gray-500">December 7, 2019</div>--}}
{{--                                    $290.29 has been deposited into your account!--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a class="dropdown-item d-flex align-items-center" href="#">--}}
{{--                                <div class="mr-3">--}}
{{--                                    <div class="icon-circle bg-warning">--}}
{{--                                        <i class="fas fa-exclamation-triangle text-white"></i>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div>--}}
{{--                                    <div class="small text-gray-500">December 2, 2019</div>--}}
{{--                                    Spending Alert: We've noticed unusually high spending for your account.--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>--}}
{{--                        </div>--}}
{{--                    </li>--}}

{{--                    <!-- Nav Item - Messages -->--}}
{{--                    <li class="nav-item dropdown no-arrow mx-1">--}}
{{--                        <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                            <i class="fas fa-envelope fa-fw"></i>--}}
{{--                            <!-- Counter - Messages -->--}}
{{--                            <span class="badge badge-danger badge-counter">7</span>--}}
{{--                        </a>--}}
{{--                        <!-- Dropdown - Messages -->--}}
{{--                        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">--}}
{{--                            <h6 class="dropdown-header">--}}
{{--                                Message Center--}}
{{--                            </h6>--}}
{{--                            <a class="dropdown-item d-flex align-items-center" href="#">--}}
{{--                                <div class="dropdown-list-image mr-3">--}}
{{--                                    <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="">--}}
{{--                                    <div class="status-indicator bg-success"></div>--}}
{{--                                </div>--}}
{{--                                <div class="font-weight-bold">--}}
{{--                                    <div class="text-truncate">Hi there! I am wondering if you can help me with a problem I've been having.</div>--}}
{{--                                    <div class="small text-gray-500">Emily Fowler · 58m</div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a class="dropdown-item d-flex align-items-center" href="#">--}}
{{--                                <div class="dropdown-list-image mr-3">--}}
{{--                                    <img class="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60" alt="">--}}
{{--                                    <div class="status-indicator"></div>--}}
{{--                                </div>--}}
{{--                                <div>--}}
{{--                                    <div class="text-truncate">I have the photos that you ordered last month, how would you like them sent to you?</div>--}}
{{--                                    <div class="small text-gray-500">Jae Chun · 1d</div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a class="dropdown-item d-flex align-items-center" href="#">--}}
{{--                                <div class="dropdown-list-image mr-3">--}}
{{--                                    <img class="rounded-circle" src="https://source.unsplash.com/CS2uCrpNzJY/60x60" alt="">--}}
{{--                                    <div class="status-indicator bg-warning"></div>--}}
{{--                                </div>--}}
{{--                                <div>--}}
{{--                                    <div class="text-truncate">Last month's report looks great, I am very happy with the progress so far, keep up the good work!</div>--}}
{{--                                    <div class="small text-gray-500">Morgan Alvarez · 2d</div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a class="dropdown-item d-flex align-items-center" href="#">--}}
{{--                                <div class="dropdown-list-image mr-3">--}}
{{--                                    <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60" alt="">--}}
{{--                                    <div class="status-indicator bg-success"></div>--}}
{{--                                </div>--}}
{{--                                <div>--}}
{{--                                    <div class="text-truncate">Am I a good boy? The reason I ask is because someone told me that people say this to all dogs, even if they aren't good...</div>--}}
{{--                                    <div class="small text-gray-500">Chicken the Dog · 2w</div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>--}}
{{--                        </div>--}}
{{--                    </li>--}}

                    <div class="topbar-divider d-none d-sm-block"></div>

                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                                {{ auth()->user()->name }}
                            </span>
                            <img src="{{ auth()->user()->avatar ? auth()->user()->avatar : asset('images/person.png') }}" class="img-profile rounded-circle" >
                        </a>
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="{{ url('admin/profile') }}">
                                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                {{ __('Profil') }}
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                {{ __('Wyloguj się') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>

                </ul>

            </nav>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            @include('layouts.modules.flash')
           @yield('content')

            <!-- /.container-fluid -->

            @include('layouts.modules.modals')
        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="d-flex justify-content-between align-items-center">

                <img src="{{ asset('images/2020-NCK_dofinans_kulturawsieci-mono.jpg') }}" style="max-height: 100px;max-width: 100%;">

                <div class="copyright pr-5 ">
                    <span>Made By <a class="text-muted" href="https://webwizards.pl/">WEBWIZARDS</a></span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

@yield('scripts')
<script>
    $(document).ready(function(){
        if(window.localStorage.getItem('contrast-mode') == 1){
            $('#switch_contrast').prop("checked", true);
            $('body').addClass('inverted');
        }

        $('#switch_contrast').on("change", function (){
            if($(this).prop('checked')){
                $('body').addClass('inverted');
                window.localStorage.setItem('contrast-mode', 1);
            }else{
                $('body').removeClass('inverted');
                window.localStorage.setItem('contrast-mode', 0);
            }
        });

        if(window.localStorage.getItem('zoom-mode') == 1){
            $('body').addClass('zoomed');
            if(window.localStorage.getItem('zoom-value') == 'sm'){
                $('body').addClass('zoomed-sm');
                $('button.zoomed[data-zoom="sm"]').addClass('active');
            }else if(window.localStorage.getItem('zoom-value') == 'md'){
                $('body').addClass('zoomed-md');
                $('button.zoomed[data-zoom="md"]').addClass('active');
            }
        }else{
            $('button.zoomed[data-zoom="none"]').addClass('active');
        }

        $('button.zoomed').on('click', function(){
            $('button.zoomed').removeClass('active');
            let zoomed = $(this).data('zoom');

            if(zoomed === 'none'){
                $('body').removeClass('zoomed');
                window.localStorage.setItem('zoom-mode', 0);
                $('button.zoomed[data-zoom="none"]').addClass('active');
            }else{
                window.localStorage.setItem('zoom-mode', 1);
                window.localStorage.setItem('zoom-value', zoomed);
                $('body').addClass('zoomed');

                $('body').removeClass('zoomed-sm');
                $('body').removeClass('zoomed-md');

                if(zoomed === 'sm'){
                    $('body').addClass('zoomed-sm');
                    $('button.zoomed[data-zoom="sm"]').addClass('active');
                }else if(zoomed === 'md'){
                    $('body').addClass('zoomed-md');
                    $('button.zoomed[data-zoom="md"]').addClass('active');
                }
            }
        });
    });
</script>
</body>

</html>
