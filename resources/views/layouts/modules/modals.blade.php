<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog ">
        <div class="modal-content">

        </div>
    </div>
</div>

@section('scripts')
    @parent
    <script type="text/javascript">
            $(document).ready(function () {
                $('#dialog-form').validate();
                $('#wrapper').on('click', '.btn-modal-open, .modal-open, .modal-open-lg, .modal-open-sm, .modal-open-xlg', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    var url = $(this).attr('target');
                    var data_string = $(this).attr('form-data');
                    var data = {};
                    if (data_string !== undefined) {
                        data = $.parseJSON(data_string);
                    }
                    var size = 'md';
                    if ($(this).hasClass('modal-open-sm')) size = 'sm';
                    if ($(this).hasClass('modal-open-lg')) size = 'lg';
                    if ($(this).hasClass('modal-open-xlg')) size = 'xlg';
                    webwizards.open_dialog(url, size, data);
                });
                $('#modal').on('click', '#set, .set, #btn-modal-set', function () {
                    webwizards.send_dialog_form($(this));
                    return false;
                });
                $('#modal').on('click', '#btn-modal-undo', function () {
                    if (webwizards.dialog_undo_url != null) {
                        let undo = webwizards.dialog_undo_url;
                        webwizards.dialog_undo_url = null;
                        webwizards.dialog_url = null;
                        webwizards.open_dialog(undo, null, null);
                    } else {
                        $('#modal').modal('hide');
                    }
                });
                $('#modal').on('hidden.bs.modal', function (e) {
                    $('#modal .modal-content').html('');
                    $('#modal .modal-dialog').removeClass('modal-xlg');
                    $('#modal .modal-dialog').removeClass('modal-lg');
                    $('#modal .modal-dialog').removeClass('modal-sm');
                    webwizards.dialog_undo_url = null;
                    webwizards.dialog_url = null;
                    if (webwizards.dialog_close_refresh) {
                        window.location.reload();
                    }
                });
            });
            $(document).on("keypress", ":input:not(textarea)", function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                }
            });
    </script>
@stop
