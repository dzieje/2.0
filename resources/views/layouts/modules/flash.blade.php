    <div class="container-fluid">
        @if (Session::has('flash_notification.message'))
            <div class="mb-2 flash-message alert alert-{{ Session::get('flash_notification.level', 'primary') }} alert-hideable" >
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {!! Session::get('flash_notification.message') !!}
            </div>
        @endif
        @if (Session::has('flash_notification.error'))
            <div class="mb-2 flash-message alert alert-{{ Session::get('flash_notification.level', 'danger') }} alert-hideable" >
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {!! Session::get('flash_notification.error') !!}
            </div>
        @endif

    </div>
