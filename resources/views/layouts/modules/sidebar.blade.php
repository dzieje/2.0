<ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('admin') }}">
        <div class="sidebar-brand-text mx-3">{{ config('app.name', 'Laravel') }}</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <li class="nav-item ">
        <a class="nav-link" href="{{ url('admin') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>{{ __('Panel główny') }}</span></a>
    </li>
    <li class="nav-item @if(Request::segment(2) == 'volunteers') active @endif">
        <a class="nav-link " href="#" data-toggle="collapse" data-target="#collapseVolunteers" aria-expanded="true" aria-controls="collapseVolunteers">
            <i class="fas fa-fw fa-users"></i>
            <span>{{ __('Wolontariusze') }}</span>
        </a>
        <div id="collapseVolunteers" class="collapse @if(Request::segment(2) == 'volunteers') show @endif" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item @if(Request::segment(3) == 'list') active @endif" href="{{ url('admin/volunteers/list') }}">
                    <i class="fa fa-users fa-fw"></i>
                    {{ __('Lista') }}</a>

                <a class="collapse-item @if(Request::segment(3) == 'attributes') active @endif" href="{{ url('admin/volunteers/attributes') }}">
                    <i class="fa fa-list-alt fa-fw"></i>
                    {{ __('Atrybuty') }}</a>
            </div>
        </div>
    </li>

    @if(config('app.show_enable'))
        @if(\App\Models\Project::where('project_type_id', 1)->count())

            <!-- Divider -->
            <hr class="sidebar-divider">

            <div class="sidebar-heading">
                {{ __('Widowiska') }}
            </div>
        @endif

        @foreach(\App\Models\Project::where('project_type_id', 1)->orderBy('id', 'desc')->get()->groupBy('year') as $year => $projects)
            <li class="nav-item ">
                <small class="d-block badge badge-light ml-3 mr-3 mt-2">{{ $year }}</small>
            </li>
            @foreach($projects as $project)
                <li class="nav-item ">
                    <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapseProject{{ $project->id }}" aria-controls="collapseProject{{ $project->id }}">
                        <i class="fas fa-theater-masks fa-fw"></i>
                        <span>{{ $project->name }}</span>
                    </a>
                    <div id="collapseProject{{ $project->id }}" class="collapse @if(Request::segment(3) == $project->slug) show @endif "  data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">

                            <ul class="navbar-nav" id="accordionProjectManage{{ $project->id }}">
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapseProjectManage{{ $project->id }}" >
                                        <i class="fas fa-fw fa-list"></i>
                                        <span>{{ __('Zarządzanie') }}</span>
                                    </a>
                                    <div id="collapseProjectManage{{ $project->id }}" class="collapse @if(Request::segment(3) == $project->slug && Request::segment(4) == 'manage') show @endif" data-parent="#accordionProjectManage{{ $project->id }}">
                                        <div class="bg-light collapse-inner rounded">
                                            <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'manage' && Request::segment(4) == 'actors') active @endif" href="{{ url('admin/spectacle/'.$project->slug.'/manage/actors') }}">
                                                <i class="fas fa-list-alt fa-fw"></i>
                                                {{ __('Aktorami') }}</a>
                                            <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'manage' && Request::segment(4) == 'scenes') active @endif" href="{{ url('admin/spectacle/'.$project->slug.'/manage/scenes') }}">
                                                <i class="fas fa-list-alt fa-fw"></i>
                                                {{ __('Scenami') }}</a>
                                            <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'manage' && Request::segment(4) == 'roles') active @endif" href="{{ url('admin/spectacle/'.$project->slug.'/manage/roles') }}">
                                                <i class="fas fa-list-alt fa-fw"></i>
                                                {{ __('Rolami') }}</a>
                                            <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'manage' && Request::segment(4) == 'groups') active @endif" href="{{ url('admin/spectacle/'.$project->slug.'/manage/groups') }}">
                                                <i class="fas fa-list-alt fa-fw"></i>
                                                {{ __('Grupami') }}</a>
                                            <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'manage' && Request::segment(4) == 'wings') active @endif" href="{{ url('admin/spectacle/'.$project->slug.'/manage/wings') }}">
                                                <i class="fas fa-list-alt fa-fw"></i>
                                                {{ __('Kulisami') }}</a>
                                            <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'manage' && Request::segment(4) == 'timetables') active @endif" href="{{ url('admin/spectacle/'.$project->slug.'/manage/timetables') }}">
                                                <i class="fas fa-list-alt fa-fw"></i>
                                                {{ __('Harm. prób') }}</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <ul class="navbar-nav" id="accordionProjectPanel{{ $project->id }}">
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapseProjectPanel{{ $project->id }}" >
                                        <i class="fas fa-fw fa-home"></i>
                                        <span>{{ __('Panel') }}</span>
                                    </a>
                                    <div id="collapseProjectPanel{{ $project->id }}" class="collapse @if(Request::segment(3) == $project->slug && Request::segment(4) == 'panel') show @endif" data-parent="#accordionProjectPanel{{ $project->id }}">
                                        <div class="bg-light collapse-inner rounded">
                                            <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'panel' && Request::segment(5) == 'actors') active @endif" href="{{ url('admin/spectacle/'.$project->slug.'/panel/actors') }}">
                                                <i class="fas fa-list-alt fa-fw"></i>
                                                {{ __('Aktorów') }}</a>
                                            <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'panel' && Request::segment(5) == 'scenes') active @endif" href="{{ url('admin/spectacle/'.$project->slug.'/panel/scenes') }}">
                                                <i class="fas fa-list-alt fa-fw"></i>
                                                {{ __('Scen') }}</a>
                                            <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'panel' && Request::segment(5) == 'timetables') active @endif" href="{{ url('admin/spectacle/'.$project->slug.'/panel/timetables') }}">
                                                <i class="fas fa-list-alt fa-fw"></i>
                                                {{ __('Prób') }}</a>
                                            <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'panel' && Request::segment(5) == 'costumes') active @endif" href="{{ url('admin/spectacle/'.$project->slug.'/panel/costumes') }}">
                                                <i class="fas fa-list-alt fa-fw"></i>
                                                {{ __('Kostiumów') }}</a>
                                            <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'panel' && Request::segment(5) == 'props') active @endif" href="{{ url('admin/spectacle/'.$project->slug.'/panel/props') }}">
                                                <i class="fas fa-list-alt fa-fw"></i>
                                                {{ __('Rekwizytów') }}</a>
                                            <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'panel' && Request::segment(5) == 'absences') active @endif" href="{{ url('admin/spectacle/'.$project->slug.'/panel/absences/list') }}">
                                                <i class="fas fa-list-alt fa-fw"></i>
                                                {{ __('Nieobecności') }}</a>
                                            <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'panel' && Request::segment(5) == 'shows') active @endif" href="{{ url('admin/spectacle/'.$project->slug.'/panel/shows') }}">
                                                <i class="fas fa-list-alt fa-fw"></i>
                                                {{ __('Widowisk') }}</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <hr class="sidebar-divider">
                            <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'volunteers') active @endif" href="{{ url('admin/spectacle/'.$project->slug.'/volunteers') }}">
                                <i class="fas fa-users fa-fw"></i>
                                {{ __('Wolontariusze') }}</a>
                            <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'changes') active @endif" href="{{ url('admin/spectacle/'.$project->slug.'/changes') }}">
                                <i class="fas fa-exchange-alt fa-fw"></i>
                                {{ __('Zmiany Widowisk') }}</a>
                            <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'surveys') active @endif" href="{{ url('admin/spectacle/'.$project->slug.'/surveys') }}">
                                <i class="fas fa-address-card fa-fw"></i>
                                {{ __('Formularze') }}</a>
                            <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'stage-manager') active @endif" href="{{ url('admin/spectacle/'.$project->slug.'/stage-manager') }}">
                                <i class="fas fa-film fa-fw"></i>
                                {{ __('Inspicjent') }}</a>
                            <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'structures') active @endif" href="{{ url('admin/spectacle/'.$project->slug.'/structures') }}">
                                <i class="fas fa-network-wired fa-fw"></i>
                                {{ __('Struktury') }}</a>
                            <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'tasks') active @endif" href="{{ url('admin/spectacle/'.$project->slug.'/tasks') }}">
                                <i class="fas fa-tasks fa-fw"></i>
                                {{ __('Zadania') }}</a>
                            <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'settings') active @endif" href="{{ url('admin/spectacle/'.$project->slug.'/settings') }}">
                                <i class="fas fa-cogs fa-fw"></i>
                                {{ __('Ustawienia') }}</a>
                        </div>

                    </div>

                </li>
            @endforeach
        @endforeach
    @endif

    @if(\App\Models\Project::where('project_type_id', 2)->count())
        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            {{ __('Projekty') }}
        </div>
    @endif

    @foreach(\App\Models\Project::where('project_type_id', 2)->orderBy('id', 'desc')->get()->groupBy('year') as $year => $projects)
        <li class="nav-item ">
            <small class="d-block badge badge-light ml-3 mr-3 mt-2">{{ $year }}</small>
        </li>
        @foreach($projects as $project)
            <li class="nav-item ">
                <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapseProject{{ $project->id }}"  aria-controls="collapseProject{{ $project->id }}">
                    <i class="fas fa-theater-masks fa-fw"></i>
                    <span>{{ $project->name }}</span>
                </a>
                <div id="collapseProject{{ $project->id }}" class="collapse @if(Request::segment(3) == $project->slug) show @endif "  data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'participants') active @endif" href="{{ url('admin/project/'.$project->slug.'/participants') }}">
                            <i class="fas fa-users fa-fw"></i>
                            {{ __('Uczestnicy') }}</a>
                        <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'surveys') active @endif" href="{{ url('admin/project/'.$project->slug.'/surveys') }}">
                            <i class="fas fa-address-card fa-fw"></i>
                            {{ __('Formularze') }}</a>
                        <hr class="sidebar-divider">
                        <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'structures') active @endif" href="{{ url('admin/project/'.$project->slug.'/structures') }}">
                            <i class="fas fa-network-wired fa-fw"></i>
                            {{ __('Struktury') }}</a>
                        <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'tasks') active @endif" href="{{ url('admin/project/'.$project->slug.'/tasks') }}">
                            <i class="fas fa-tasks fa-fw"></i>
                            {{ __('Zadania') }}</a>
                        <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'attributes') active @endif" href="{{ url('admin/project/'.$project->slug.'/attributes') }}">
                            <i class="fa fa-list-alt fa-fw"></i>
                            {{ __('Atrybuty') }}</a>
                        <a class="collapse-item @if(Request::segment(3) == $project->slug && Request::segment(4) == 'settings') active @endif" href="{{ url('admin/project/'.$project->slug.'/settings') }}">
                            <i class="fas fa-cogs fa-fw"></i>
                            {{ __('Ustawienia') }}</a>
                    </div>
                </div>

            </li>
        @endforeach
    @endforeach
    <!-- Divider -->
    <hr class="sidebar-divider">


    <li class="nav-item @if(Request::segment(2) == 'files-manager') active @endif">
        <a class="nav-link" href="{{ url('admin/files-manager') }}">
            <i class="fas fa-fw fa-copy"></i>
            <span>{{ __('Menadżer plików') }}</span></a>
    </li>


    <li class="nav-item @if(Request::segment(2) == 'sending') active @endif">
        <a class="nav-link " href="#" data-toggle="collapse" data-target="#collapseSending" aria-expanded="true" aria-controls="collapseSending">
            <i class="fas fa-fw fa-paper-plane"></i>
            <span>{{ __('Wysyłka') }}</span>
        </a>
        <div id="collapseSending" class="collapse @if(Request::segment(2) == 'sending') show @endif" aria-labelledby="headingSending" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item @if(Request::segment(3) == 'mailings') active @endif" href="{{ url('admin/sending/mailings') }}">
                    <i class="fa fa-at fa-fw"></i>
                    {{ __('Mailing') }}</a>

                <a class="collapse-item @if(Request::segment(3) == 'sms') active @endif" href="{{ url('admin/sending/sms') }}">
                    <i class="fa fa-sms fa-fw"></i>
                    {{ __('SMS') }}</a>
            </div>
        </div>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">

    <li class="nav-item @if(Request::segment(2) == 'news') active @endif">
        <a class="nav-link" href="{{ url('admin/news') }}">
            <i class="fas fa-fw fa-envelope"></i>
            <span>{{ __('Aktualności') }}</span></a>
    </li>

    <li class="nav-item @if(Request::segment(2) == 'projects') active @endif">
        <a class="nav-link" href="{{ url('admin/projects') }}">
            <i class="fas fa-fw fa-list-alt"></i>
            <span>{{ __('Projekty') }}</span></a>
    </li>

    <li class="nav-item @if(Request::segment(2) == 'settings') active @endif">
        <a class="nav-link " href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-cogs"></i>
            <span>{{ __('Ustawienia') }}</span>
        </a>
        <div id="collapsePages" class="collapse @if(Request::segment(2) == 'settings') show @endif" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item @if(Request::segment(3) == 'users') active @endif" href="{{ url('admin/settings/users') }}">
                    <i class="fa fa-users fa-fw"></i>
                    {{ __('Użytkownicy') }}</a>

                <a class="collapse-item @if(Request::segment(3) == 'roles') active @endif" href="{{ url('admin/settings/roles') }}">
                    <i class="fa fa-list-alt fa-fw"></i>
                    {{ __('Role') }}</a>

                <a class="collapse-item @if(Request::segment(3) == 'variables') active @endif" href="{{ url('admin/settings/variables') }}">
                    <i class="fas fa-cogs fa-fw"></i>
                    {{ __('System') }}</a>
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
