<div class="col-sm-12">
    <div class="question">
        <div class="ac-custom ac-radio ac-fill">
            <ul class="row">
                @foreach($field->values as $value)
                    <li class="ml-2 mr-2">
                        <input class="{{ property_exists($field, 'className') ? $field->className : null }}" id="{{ $field->name }}_{{ $value->value }}" name="{{ $field->name }}" type="radio" value="{{ $value->value }}" @if($field->required) required @endif @if($volunteer && $volunteer->{$field->name} == $value->value) checked @endif>
                        <label for="{{ $field->name }}_{{ $value->value }}">{{ $value->label }}</label>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
