<div class="col-sm-12">
    <p class="description description-small {{ property_exists($field, 'className') ? $field->className : null }}">
    {!!  property_exists($field, 'label') ? $field->label : null !!}
    </p>
</div>
