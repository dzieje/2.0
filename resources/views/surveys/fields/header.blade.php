<div class="col-sm-12">
    <{{ $field->subtype }} class="page-header {{ property_exists($field, 'className') ? $field->className : null }}">
        {{ $field->label }}
    </{{ $field->subtype }}>
</div>
<div class="col-sm-12">
    <hr>
</div>
