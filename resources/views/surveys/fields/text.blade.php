<div class="col-sm-12 col-md-6">
    <div class="question">
        @if(property_exists($field, 'description'))
            <label for="{{ $field->name }}">{{ $field->description }}</label>
        @endif
        <input class="{{ property_exists($field, 'className') ? $field->className : null }}" id="{{ $field->name }}" name="{{ $field->name }}" type="{{ $field->subtype }}" placeholder="{{ $field->label }}" @if($field->required) required @endif @if($volunteer) value="{{ $volunteer->{$field->name} }}" @endif>
    </div>
</div>
