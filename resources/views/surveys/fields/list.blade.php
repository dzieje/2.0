<div class="col-sm-12">
    <div class="question">
        <div class="ac-custom ac-checkbox ac-boxfill">
            <div class="row  area-container">
                <div class="col-sm-12 col-md-6 col-lg-4 col-md-offset-3 col-lg-offset-4">
                    <ul class="row">
                        <li class="col-sm-12 area" style="padding: 2em 0 !important;">
                            <input id="area_1" name="primary_area_id[]" type="checkbox" value="1" required data-area="1" />
                            <label for="area_1" class="text-uppercase" data-area="1"/>akrobatyczne</label>
                        </li>
                        <li class="col-sm-12 area" style="padding: 2em 0 !important;">
                            <input id="area_5" name="primary_area_id[]" type="checkbox" value="5" required data-area="5" />
                            <label for="area_5" class="text-uppercase" data-area="5"/>fechtunku</label>
                        </li>
                        <li class="col-sm-12 area" style="padding: 2em 0 !important;">
                            <input id="area_2" name="primary_area_id[]" type="checkbox" value="2" required data-area="2" />
                            <label for="area_2" class="text-uppercase" data-area="2"/>krawieckie</label>
                        </li>
                        <li class="col-sm-12 area" style="padding: 2em 0 !important;">
                            <input id="area_3" name="primary_area_id[]" type="checkbox" value="3" required data-area="3" />
                            <label for="area_3" class="text-uppercase" data-area="3"/>rekwizytorskie</label>
                        </li>
                        <li class="col-sm-12 area" style="padding: 2em 0 !important;">
                            <input id="area_4" name="primary_area_id[]" type="checkbox" value="4" required data-area="4" />
                            <label for="area_4" class="text-uppercase" data-area="4"/>taneczne</label>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
