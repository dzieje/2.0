@extends('layouts.form')

@section('content')
    <style>
        {!!  $survey->css !!}
    </style>
    <form action="" id="page-form" >
        <div class="row justify-content-center mt-5">
            {!! csrf_field() !!}
            <div class="col-lg-8 ">
                <div class="row justify-content-center">
                    @foreach($fields as $field)
                        @include('admin.surveys.fields.'.$field->type)
                    @endforeach
                </div>
            </div>
            @if($survey->questions->count() > 0)
                <div class="col-lg-8 ">
                    <div class="row">
                        @foreach($survey->questions->chunk(2) as $k => $chunk)
                            @foreach($chunk as $question)
                                <div class="col-sm-12 ">
                                    <div class="d-flex align-items-center justify-content-center">
                                        <p class="description description-small m-0">
                                            {{ $loop->iteration + ($k * 2) }}) {{ $question->question }}
                                        </p>
                                        <div class="question m-0">
                                            <div class="ac-custom ac-radio ac-fill">
                                                <ul class="row">
                                                    <li class="ml-2 mr-2">
                                                        <input id="question_{{ $question->id }}_1" name="question[{{ $question->id }}]" type="radio" value="1">
                                                        <label for="question_{{ $question->id }}_1">Tak</label>
                                                    </li>
                                                    <li class="ml-2 mr-2">
                                                        <input id="question_{{ $question->id }}_2" name="question[{{ $question->id }}]" type="radio" value="2">
                                                        <label for="question_{{ $question->id }}_2">Nie</label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                </div>
            @endif

            @if($survey->agreements->count() > 0)
                <div class="col-lg-8 ">
                    <div class="row">
                        @foreach($survey->agreements->chunk(2) as $k => $chunk)
                            @foreach($chunk as $agreement)
                                <div class="col-sm-12 ">
                                    <div class="d-flex align-items-center justify-content-center">
                                        <div class="question m-0">
                                            <div class="ac-custom ac-checkbox ac-boxfill">
                                                <ul class="row">
                                                    <li class="pt-2 pb-2" >
                                                        <input id="agreement_{{ $agreement->id }}" name="agreement[{{ $agreement->id }}]" type="checkbox" value="1" required>
                                                        <label for="agreement_{{ $agreement->id }}">{{ $agreement->question }}</label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                </div>
            @endif

            <div class="col-lg-8">
                <div class="row">
                    <div class="col-sm-12 text-center marg-btm">
                        <span class="btn btn-primary"><i class="fas fa-save fa-fw"></i> Zatwierdzam i wysyłam!</span>
                    </div>
                </div>
            </div>
        </div>
    </form>


@endsection
@section('scripts')
    <script src="/js/svgcheckbx.js"></script>
@endsection
