@extends('layouts.form')

@section('content')
    <style>
        {!!  $survey->css !!}
    </style>
    <form action="{{ url('form/submit', [$survey->slug, optional($volunteer)->uuid]) }}" id="page-form" method="POST">
        <div class="row justify-content-center mt-5">
            {!! csrf_field() !!}
            <div class="col-lg-8 ">
                <div class="row justify-content-center">
                    @foreach($fields as $field)
                        @include('surveys.fields.'.$field->type)
                    @endforeach
                </div>
            </div>
        @if($survey->questions->count() > 0)
            <div class="col-lg-8 ">
                <div class="row">
                    @foreach($survey->questions->chunk(2) as $k => $chunk)
                        @foreach($chunk as $question)
                            <div class="col-sm-12 ">
                                <div class="d-flex align-items-center justify-content-center">
                                    <p class="description description-small m-0">
                                        {{ $loop->iteration + ($k * 2) }}) {{ $question->question }}
                                    </p>
                                    <div class="question m-0">
                                        <div class="ac-custom ac-radio ac-fill">
                                            <ul class="row">
                                                <li class="ml-2 mr-2">
                                                    <input id="question_{{ $question->id }}_1" name="question[{{ $question->id }}]" type="radio" value="1">
                                                    <label for="question_{{ $question->id }}_1">Tak</label>
                                                </li>
                                                <li class="ml-2 mr-2">
                                                    <input id="question_{{ $question->id }}_2" name="question[{{ $question->id }}]" type="radio" value="2">
                                                    <label for="question_{{ $question->id }}_2">Nie</label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endforeach
                </div>
            </div>
        @endif

        <div class="col-lg-8">
            <div class="row guardian-container">
                <div class="col-sm-12">
                    <h2 class="page-header colour-white">Dane Twojego opiekuna prawnego:</h2>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="question">
                        <input id="guardian_name" name="guardian_name" type="text" placeholder="Imię" >
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="question">
                        <input id="guardian_surname" name="guardian_surname" type="text" placeholder="Nazwisko" >
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="question" >
                        <input type="date" class="date adult" id="guardian_date_of_birth" name="guardian_date_of_birth"  placeholder="Data urodzenia" >
                    </div>
                </div>
                <div class="col-sm-12">
                    <hr>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="question">
                        <input id="guardian_city" name="guardian_city" type="text" placeholder="Miasto zamieszkania" >
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="question">
                        <input id="guardian_phone" name="guardian_phone" type="text"  class="phone digit"  placeholder="Numer telefonu"  maxlength="9" minlength="9">
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="question">
                        <input class="email" id="guardian_email" name="guardian_email" type="email" placeholder="Adres email" >
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="ac-custom ac-checkbox ac-checkmark ac-block">
                        <ul>
                            <li>
                                <input id="permission_guardian_participation" name="permission_guardian_participation" type="checkbox" >
                                <label for="permission_guardian_participation" class="f-16 f-light colour-white">
                                    Wyrażam zgodę na udział dziecka w plenerowym widowisku historycznym "Orzeł i Krzyż", realizowanym przez Stowarzyszenie Dzieje oraz biorę odpowiedzialność za zorganizowanie opieki dziecku (poniżej 10roku życia) podczas prób oraz widowisk. Wyrażam również zgodę na przetwarzanie przez Stowarzyszenie Dzieje, jako administratora danych osobowych, danych osobowych umieszczonych
                                    w formularzu zgłoszeniowym, w zakresie prowadzenia i realizacji wolontariatu. Jednocześnie oświadczam, że jestem świadoma/świadomy dobrowolności podania danych oraz, że zostałam/zostałem poinformowany o prawie wglądu do podanych danych oraz możliwości ich poprawiania. Wyrażam zgodę na wykorzystywanie wizerunku dziecka poprzez publikację zdjęć z widowiska „Orzeł i Krzyż” w celu promocji ww. widowiska, również w kolejnych latach.
                                </label>
                            </li>
                        </ul>
                    </div>
                    <div class="ac-custom ac-checkbox ac-checkmark ac-block" id="permission_guardian_container">
                        <ul>
                            <li>
                                <input id="permission_guardian" name="guardian_permission_single" type="checkbox"  value="1">
                                <label for="permission_guardian" class="f-16 f-light colour-white">Zobowiązuję się do przywożenia oraz odbierania dziecka z prób do widowisk oraz samych widowisk. Miejscem dowozu oraz odbioru będzie zawsze Park Dzieje, zlokalizowany w Murowanej Goślinie przy ul. Brackiej 9. (Według prawa: dziecko, które nie ukończyło 7 lat - nie może samodzielnie poruszać się bez dorosłego opiekuna poza szkołą i domem.)</label>
                            </li>
                        </ul>
                    </div>
                    <div class="ac-custom ac-radio ac-fill ac-block" id="permission_multi_guardian_container" style="display: none;">
                        <ul>
                            <li>
                                <input id="guardian_permission_multi_1" name="guardian_permission_multi" type="radio" value="1"  />
                                <label for="guardian_permission_multi_1" class="f-16 f-light colour-white" />Zobowiązuję się do przywożenia oraz odbierania dziecka z prób do widowisk oraz samych widowisk. Miejscem dowozu oraz odbioru będzie zawsze Park Dzieje, zlokalizowany w Murowanej Goślinie przy ul. Brackiej 9.</label>
                            </li>
                            <li>
                                <input id="guardian_permission_multi_2" name="guardian_permission_multi" type="radio" value="2"  />
                                <label for="guardian_permission_multi_2" class="f-16 f-light colour-white"/>Wyrażam zgodę na samodzielne przychodzenie i powrót dziecka z prób do widowisk oraz samych widowisk. Zostałem poinformowany, że powroty z prób do widowisk, a przede wszystkim z widowisk - będą następowały w późnych porach nocnych i jestem tego świadomy.</label>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

        @if($survey->agreements->count() > 0)
            <div class="col-lg-8 ">
                <div class="row agreements-container">
                    @foreach($survey->agreements->chunk(2) as $k => $chunk)
                        @foreach($chunk as $agreement)
                            <div class="col-sm-12 ">
                                <div class="d-flex align-items-center justify-content-center">
                                    <div class="question m-0">
                                        <div class="ac-custom ac-checkbox ac-boxfill">
                                            <ul class="row">
                                                <li class="pt-2 pb-2" >
                                                    <input id="agreement_{{ $agreement->id }}" name="agreement[{{ $agreement->id }}]" type="checkbox" value="1" required>
                                                    <label for="agreement_{{ $agreement->id }}">{{ $agreement->question }}</label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endforeach
                </div>
            </div>
        @endif

        <div class="col-lg-8">
            <div class="row">
                <div class="col-sm-12 text-center marg-btm">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-save fa-fw"></i> Zatwierdzam i wysyłam!</button>
                </div>
            </div>
        </div>
        </div>
    </form>


@endsection
@section('scripts')
    <script src="/js/svgcheckbx.js"></script>
    <script src="/js/moment-with-locales.js"></script>
    <script>
        $('document').ready(function (){
            // let requiredCheckboxes = $(this).find('.options :checkbox[required]');
            // requiredCheckboxes.on('change', function(){
            //     let instance = $(this).data('instance');
            //     if(requiredCheckboxes.is(':checked')) {
            //         requiredCheckboxes.add('[data-instance="'+instance+'"]').removeAttr('required');
            //     } else {
            //         requiredCheckboxes.add('[data-instance="'+instance+'"]').attr('required', 'required');
            //     }
            // });

            $('input').each(function(){
                $(this).attr('autocomplete', Math.random().toString(36).substr(2, 5));
            });

            $('input[name="date_of_birth"]').on('change', function(){
                $('#date_of_birth-error-custom').remove();

                var date = moment($(this).val());
                var limit = moment().subtract(18, 'years');

                if(! date.isValid() ) return;

                var years = moment().diff(date, 'years');

                if (date < limit) {
                    $('.guardian-container').slideUp();
                } else {
                    $('.guardian-container').slideDown();
                    $('.guardian-container').css('display', 'flex');

                    if(years >= 7){
                        $('#permission_guardian_container').hide();
                        $('#permission_multi_guardian_container').show();
                    }else{
                        $('#permission_multi_guardian_container').hide();
                        $('#permission_guardian_container').show();
                    }

                    $('html, body').animate({
                        scrollTop: $(".guardian-container").offset().top
                    }, 1000);
                }

                date.isValid();
                //$(this).datepicker('hide');
            }).change();
        });
    </script>
@endsection
