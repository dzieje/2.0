require('./bootstrap');
require('./plugins/validate');

(function($) {
    "use strict"; // Start of use strict

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // Toggle the side navigation
    $(document).on('click', '#sidebarToggle, #sidebarToggleTop', function(e) {
        $("body").toggleClass("sidebar-toggled");
        $(".sidebar").toggleClass("toggled");
        if ($(".sidebar").hasClass("toggled")) {
            $('.sidebar .collapse').collapse('hide');
        };
    });

    // Close any open menu accordions when window is resized below 768px
    $(window).resize(function() {
        if ($(window).width() < 768) {
            $('.sidebar .collapse').collapse('hide');
        };

        // Toggle the side navigation when window is resized below 480px
        if ($(window).width() < 480 && !$(".sidebar").hasClass("toggled")) {
            $("body").addClass("sidebar-toggled");
            $(".sidebar").addClass("toggled");
            $('.sidebar .collapse').collapse('hide');
        };
    });

    // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
    $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function(e) {
        if ($(window).width() > 768) {
            var e0 = e.originalEvent,
                delta = e0.wheelDelta || -e0.detail;
            this.scrollTop += (delta < 0 ? 1 : -1) * 30;
            e.preventDefault();
        }
    });

    // Scroll to top button appear
    $(document).on('scroll', function() {
        var scrollDistance = $(this).scrollTop();
        if (scrollDistance > 100) {
            $('.scroll-to-top').fadeIn();
        } else {
            $('.scroll-to-top').fadeOut();
        }
    });

    // Smooth scrolling using jQuery easing
    $(document).on('click', 'a.scroll-to-top', function(e) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top)
        }, 1000, 'easeInOutExpo');
        e.preventDefault();
    });

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('.flash-message.alert').delay(4000).slideUp(400);
    });


    window.webwizards = (function() {
        var my = {};
        my.dialog_close_refresh = false;
        my.dialog_undo_url = null;
        my.dialog_url = null;
        my.error_msg = 'Wystąpił błąd w trakcie wykonywania polecenia!';

        my.open_dialog = function(url, size, data) {
            if (my.dialog_url != null) {
                my.dialog_undo_url = my.dialog_url;
            }
            my.dialog_url = url;
            //console.log(my.dialog_undo_url+' - '+my.dialog_url);
            $('#modal .modal-content').html('');
            $('#modal .modal-dialog').removeClass('modal-sm');
            $('#modal .modal-dialog').removeClass('modal-lg');
            $('#modal .modal-dialog').removeClass('modal-xlg');
            if (size == 'sm') $('#modal .modal-dialog').addClass('modal-sm');
            if (size == 'lg') $('#modal .modal-dialog').addClass('modal-lg');
            if (size == 'xlg') $('#modal .modal-dialog').addClass('modal-xlg');
            $('#modal').modal('show');
            $.get(url, data, function(data) {
                $('#modal .modal-content').html(data);
                $('input.datepicker').prop('autocomplete', 'off');
            });
        }
        my.send_dialog_form = function(btn) {
            $('#modal .modal-footer .btn').prop('disabled', true);
            $('#modal .modal-footer .btn').addClass('disabled');
            if ($('#dialog-form').valid()) {
                $.ajax({
                    type: "POST",
                    url: $('#dialog-form').prop('action'),
                    data: $('#dialog-form').serialize(),
                    cache: false,
                    dataType: 'json',
                    success: function (data) {
                        if (data.code == '0') {
                            window.location.reload();
                        } else if (data.code == '1') {
                            window.location = data.url;
                        } else if (data.code == '3') {
                            $('#modal .modal-body').html(data.msg);
                            $('#modal .modal-footer').html('<button type="button" class="btn btn-default"'
                                +' data-dismiss="modal">Ok</button>');
                            $('#modal').on('hidden.bs.modal', function (e) {
                                window.location.reload();
                            });
                        } else if (data.code == '4') {
                            my.open_dialog(data.url, 'lg', null);
                        } else if (data.code == '2') {
                            my.alert(data.error);
                            delay(function() {
                                $('#modal .modal-footer .btn').prop('disabled', false);
                                $('#modal .modal-footer .btn').removeClass('disabled');
                            }, 5000);
                        } else {
                            $('#modal .modal-body').html(data.error);
                            if (isset(data.url) && data.url != '') {
                                btn.button('reset');
                                $('#modal').on('hidden.bs.modal', function (e) {
                                    window.location = data.url;
                                });
                            }
                        }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        my.alert(my.error_msg);
                    }
                });
            } else {
                $('#modal .modal-footer .btn').prop('disabled', false);
                $('#modal .modal-footer .btn').removeClass('disabled');
            }
            return 0;
        }
        my.copy_to_clipboard = function (text) {
            if (window.clipboardData && window.clipboardData.setData) {
                // IE specific code path to prevent textarea being shown while dialog is visible.
                return clipboardData.setData("Text", text);
            } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
                var textarea = document.createElement("textarea");
                textarea.textContent = text;
                textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
                document.body.appendChild(textarea);
                textarea.select();
                try {
                    return document.execCommand("copy");  // Security exception may be thrown by some browsers.
                } catch (ex) {
                    console.warn("Copy to clipboard failed.", ex);
                    return false;
                } finally {
                    document.body.removeChild(textarea);
                }
            }
        }
        my.alert = function (text) {
            $('#alert-message span').text(text);
            $('#alert-message').fadeIn(200).delay(4000).fadeOut(400);
        }
        my.success = function (text) {
            $('#success-message span').text(text);
            $('#success-message').fadeIn(200).delay(4000).fadeOut(400);
        }
        my.set_dialog_size = function (size) {
            $('#modal .modal-dialog').removeClass('modal-sm');
            $('#modal .modal-dialog').removeClass('modal-lg');
            $('#modal .modal-dialog').removeClass('modal-xlg');
            $('#modal .modal-dialog').addClass('modal-'+size);
        }

        my.makeid = function(length) {
            var result           = '';
            var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for ( var i = 0; i < length; i++ ) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
        }

        my.sleep = function(time) {
            return new Promise((resolve) => setTimeout(resolve, time));
        }

        return my;
    }());

    $("form").validate();

    /*
    * Translated default messages for the jQuery validation plugin.
    * Language: PL
    */
    jQuery.extend(jQuery.validator.messages, {
        required: "To pole jest wymagane.",
        remote: "Proszę o wypełnienie tego pola.",
        email: "Proszę o podanie prawidłowego adresu email.",
        url: "Proszę o podanie prawidłowego URL.",
        date: "Proszę o podanie prawidłowej daty.",
        dateISO: "Proszę o podanie prawidłowej daty (ISO).",
        number: "Proszę o podanie prawidłowej liczby.",
        digits: "Proszę o podanie samych cyfr.",
        creditcard: "Proszę o podanie prawidłowej karty kredytowej.",
        equalTo: "Proszę o podanie tej samej wartości ponownie.",
        accept: "Proszę o podanie wartości z prawidłowym rozszerzeniem.",
        maxlength: jQuery.validator.format("Proszę o podanie nie więcej niż {0} znaków."),
        minlength: jQuery.validator.format("Proszę o podanie przynajmniej {0} znaków."),
        rangelength: jQuery.validator.format("Proszę o podanie wartości o długości od {0} do {1} znaków."),
        range: jQuery.validator.format("Proszę o podanie wartości z przedziału od {0} do {1}."),
        max: jQuery.validator.format("Proszę o podanie wartości mniejszej bądź równej {0}."),
        min: jQuery.validator.format("Proszę o podanie wartości większej bądź równej {0}.")
    });
})(jQuery); // End of use strict
