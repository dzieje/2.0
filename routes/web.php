<?php

use App\Models\News;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::redirect('/', 'panel');
Route::redirect('/home', 'panel');


Route::prefix('form')->group(function(){
    Route::get('open/{slug}/{volunteer_uuid?}', 'FormController@open');
    Route::post('submit/{slug}/{volunteer_uuid?}', 'FormController@submit');
});

Route::prefix('panel')->namespace('Panel')->group(function(){
    Route::get('complete/{volunteer_uuid}', 'AuthController@complete');
    Route::post('submit/{volunteer_uuid}','AuthController@submit');

    Route::get('login', 'AuthController@showLoginForm')->name('panel.login');
    Route::post('login', 'AuthController@login');


    Route::middleware(['auth:panel'])->group(function (){
        Route::redirect('/', 'panel/news');
        Route::get('news', 'PanelController@news');
        Route::get('tasks', 'PanelController@tasks');
        Route::get('calendar', 'PanelController@calendar');
        Route::get('calendar/feed', 'PanelController@calendarFeed');
        Route::post('logout', 'AuthController@logout')->name('panel.logout');
        Route::get('edit-profile', 'PanelController@editProfile');
        Route::post('update-profile', 'PanelController@updateProfile');
        Route::get('projects', 'PanelController@projects');
    });
});


Route::prefix('admin')->group(function(){
    Auth::routes([
        'login'    => true,
        'logout'   => true,
        'register' => false,
        'reset'    => false,
        'confirm'  => false,
        'verify'   => false,
    ]);

    Route::middleware(['auth'])->group(function() {
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('/', 'HomeController@index')->name('home');
        Route::get('users/search',  'Settings\UserController@searchUser');

        Route::prefix('profile')->group(function(){
           Route::get('/', 'ProfileController@index');
           Route::post('change-avatar', 'ProfileController@changeAvatar');
        });

        Route::prefix('settings')->group(function () {
            Route::prefix('users')->group(function () {
                Route::get('/', 'Settings\UserController@index');
                Route::get('edit/{user_id}', 'Settings\UserController@edit');
                Route::post('update/{user_id}', 'Settings\UserController@update');
                Route::get('create', 'Settings\UserController@create');
                Route::post('store', 'Settings\UserController@store');
                Route::get('password/{user_id}', 'Settings\UserController@password');
                Route::post('update-password/{user_id}', 'Settings\UserController@updatePassword');
                Route::get('delete/{user_id}', 'Settings\UserController@delete');
                Route::post('delete/{user_id}', 'Settings\UserController@remove');
            });

            Route::prefix('roles')->group( function () {
                Route::get('/', 'Settings\RoleController@getIndex');
                Route::get('index', 'Settings\RoleController@getIndex');
                Route::get('edit/{role_id}', 'Settings\RoleController@getEdit');
                Route::post('update/{role_id}', 'Settings\RoleController@postUpdate');
                Route::get('delete/{role_id}', 'Settings\RoleController@getDelete');
                Route::post('delete/{role_id}', 'Settings\RoleController@postDelete');
                Route::get('create', 'Settings\RoleController@getCreate');
                Route::post('store', 'Settings\RoleController@postStore');
            });

            Route::namespace('Settings')->prefix('variables')->group(function (){
                Route::get('/', 'SettingController@index');
                Route::get('edit/{setting_id}', 'SettingController@edit');
                Route::post('update/{setting_id}', 'SettingController@update');
                Route::get('preview-mail/{setting_id}', 'SettingController@previewMail');
            });
        });

        Route::prefix('projects')->group(function () {
            Route::get('/', 'ProjectController@index');
            Route::get('create', 'ProjectController@create');
            Route::post('store', 'ProjectController@store');
            Route::get('edit/{project_id}', 'ProjectController@edit');
            Route::post('update/{project_id}', 'ProjectController@update');
            Route::get('delete/{project_id}', 'ProjectController@delete');
            Route::post('delete/{project_id}', 'ProjectController@remove');
        });

        Route::prefix('volunteers')->namespace('Volunteers')->group(function () {
            Route::prefix('list')->group(function () {
                Route::get('/', 'ListController@index');
                Route::get('edit/{volunteer_id}', 'ListController@edit');
                Route::post('update/{volunteer_id}', 'ListController@update');
                Route::get('projects/{volunteer_id}', 'ListController@projects');
                Route::post('save-projects/{volunteer_id}', 'ListController@saveProjects');
                Route::get('delete/{volunteer_id}', 'ListController@delete');
                Route::post('delete/{volunteer_id}', 'ListController@remove');
            });

            Route::prefix('attributes')->group(function () {
                Route::get('/', 'AttributeController@index');
                Route::get('edit/{attribute_id}', 'AttributeController@edit');
                Route::post('update/{attribute_id}', 'AttributeController@update');
                Route::get('create', 'AttributeController@create');
                Route::post('store', 'AttributeController@store');
                Route::get('delete/{attribute_id}', 'AttributeController@delete');
                Route::post('delete/{attribute_id}', 'AttributeController@remove');
            });

            Route::get('select-volunteer', 'ManageController@selectVolunteer');
            Route::post('proceed-selection', 'ManageController@proceedSelection');
            Route::get('search',  'ManageController@searchVolunteer');
        });

        Route::prefix('spectacle/{project_slug}')->group(function () {
            Route::prefix('manage')->namespace('Spectacles\Manage')->group(function () {
                Route::prefix('actors')->group(function () {
                    Route::get('/', 'ActorController@index');
                    Route::get('index', 'ActorController@index');
                    Route::post('export', 'ActorController@export');
                    Route::get('show/{actor_id}', 'ActorController@show');
                    Route::get('edit/{actor_id}', 'ActorController@edit');
                    Route::get('wing-info', 'ActorController@wingInfo');
                    Route::get('scene-add', 'ActorController@sceneAdd');
                    Route::get('group-add', 'ActorController@groupAdd');
                    Route::get('scene-load-roles', 'ActorController@sceneLoadRoles');
                    Route::get('scene-load-role-info', 'ActorController@sceneLoadRoleInfo');
                    Route::post('update/{actor_id}', 'ActorController@update');
                    Route::get('delete/{actor_id}', 'ActorController@delete');
                    Route::post('delete/{actor_id}', 'ActorController@remove');
                    Route::get('show-roles/{actor_id}', 'ActorController@showRoles');
                    Route::get('show-groups/{actor_id}', 'ActorController@showGroups');
                });
                Route::prefix('roles')->group(function () {
                    Route::get('/', 'RoleController@index');
                    Route::get('create', 'RoleController@create');
                    Route::get('costume-add', 'RoleController@costumeAdd');
                    Route::get('prop-add', 'RoleController@propAdd');
                    Route::post('store', 'RoleController@store');
                    Route::get('edit/{role_id}', 'RoleController@edit');
                    Route::post('update/{role_id}', 'RoleController@update');
                    Route::get('delete/{role_id}', 'RoleController@delete');
                    Route::post('delete/{role_id}', 'RoleController@remove');
                    Route::get('show/{role_id}', 'RoleController@show');
                    Route::get('show-props/{role_id}', 'RoleController@showProps');
                    Route::get('show-costumes/{role_id}', 'RoleController@showCostumes');
                    Route::get('show-actors/{role_id}', 'RoleController@showActors');
                    Route::get('show-groups/{role_id}', 'RoleController@showGroups');
                    Route::get('export-phone/{role_id}', 'RoleController@exportPhones');
                    Route::get('export-emails/{role_id}', 'RoleController@exportEmails');
                    Route::get('search-actor', 'RoleController@searchActor');
                    Route::get('load-actor', 'RoleController@loadActor');
                    Route::get('search-group', 'RoleController@searchGroup');
                    Route::get('load-group', 'RoleController@loadGroup');
                    Route::post('detach-actor/{role_id}/{actor_id}', 'RoleController@detachActor');
                    Route::get('manage-actors/{role_id}', 'RoleController@manageActors');
                    Route::post('update-actors/{role_id}', 'RoleController@updateActors');
                });
                Route::prefix('scenes')->group(function () {
                    Route::get('/', 'SceneController@index');
                    Route::get('index', 'SceneController@index');
                    Route::get('create', 'SceneController@create');
                    Route::post('store', 'SceneController@store');
                    Route::get('edit/{scene_id}', 'SceneController@edit');
                    Route::post('update/{scene_id}', 'SceneController@update');
                    Route::get('delete/{scene_id}', 'SceneController@delete');
                    Route::post('delete/{scene_id}', 'SceneController@remove');
                    Route::get('show-roles/{scene_id}', 'SceneController@showRoles');
                    Route::get('show-wings/{scene_id}', 'SceneController@showWings');
                    Route::get('export-phones/{scene_id}', 'SceneController@exportPhones');
                    Route::get('export-emails/{scene_id}', 'SceneController@exportEmails');
                    Route::get('generate-outline/{rehearsal_id}/{file_type}', 'SceneController@generateOutline');
                    Route::get('download-generator/{filename}/{file_type?}', 'SceneController@downloadGenerator');
                    Route::post('stage-manager', 'SceneController@stageManager');
                });
                Route::prefix('groups')->group(function () {
                    Route::get('/', 'GroupController@index');
                    Route::get('index', 'GroupController@index');
                    Route::get('show-actors/{group_id}', 'GroupController@showActors');
                    Route::get('attach-role/{group_id}', 'GroupController@attachRole');
                    Route::get('roles', 'GroupController@roles');
                    Route::post('attach-role/{group_id}', 'GroupController@attachRole');
                    Route::get('create', 'GroupController@create');
                    Route::post('store', 'GroupController@store');
                    Route::get('edit/{group_id}', 'GroupController@edit');
                    Route::post('update/{group_id}', 'GroupController@update');
                    Route::get('show-scenes/{group_id}', 'GroupController@ahowScenes');
                    Route::get('show-roles/{group_id}', 'GroupController@ahowRoles');
                    Route::get('manage-actors/{group_id}', 'GroupController@manageActors');
                    Route::get('search-actor', 'GroupController@searchActor');
                    Route::get('load-actor', 'GroupController@loadActor');
                    Route::post('update-actors/{group_id}', 'GroupController@updateActors');
                    Route::post('delete-role/{role_id}', 'GroupController@deleteRole');
                    Route::get('export', 'GroupController@export');
                });
                Route::prefix('wings')->group(function () {
                    Route::get('/', 'WingController@index');
                    Route::get('index', 'WingController@index');
                    Route::get('create', 'WingController@create');
                    Route::post('store', 'WingController@store');
                    Route::get('export/{wing_id}', 'WingController@export');
                    Route::post('stage-manager', 'WingController@stageManager');
                });
                Route::prefix('timetables')->group(function () {
                    Route::get('/', 'TimetableController@index');
                    Route::get('index', 'TimetableController@gindex');
                    Route::get('period-add', 'TimetableController@periodAdd');
                    Route::get('scene-add', 'TimetableController@sceneAdd');
                    Route::get('roles-add', 'TimetableController@rolesAdd');
                    Route::get('create', 'TimetableController@create');
                    Route::post('store', 'TimetableController@store');
                    Route::get('show/{rehearsal_id}', 'TimetableController@show');
                    Route::get('delete/{rehearsal_id}', 'TimetableController@delete');
                    Route::post('delete/{rehearsal_id}', 'TimetableController@remove');
                    Route::get('edit/{rehearsal_id}', 'TimetableController@edit');
                    Route::post('update/{rehearsal_id}', 'TimetableController@update');
                    Route::get('generate-pdf', 'TimetableController@generatePdf');
                    Route::get('download-pdf', 'TimetableController@downloadPdf');
                    Route::get('generate-xls', 'TimetableController@generateXls');
                    Route::get('download-xls', 'TimetableController@downloadXls');
                    Route::get('generate-outline/{rehearsal_id}', 'TimetableController@generateOutline');
                    Route::get('download-generator/{filename}', 'TimetableController@downloadGenerator');
                    Route::get('attendance/{rehearsal_id}', 'TimetableController@attendance');
                    Route::post('set-attendance', 'TimetableController@setAttendance');
                    Route::post('export-attendance', 'TimetableController@exportAttendance');
                    Route::get('download-doc/{filename}', 'TimetableController@downloadDoc');
                    Route::get('attendance-xls', 'TimetableController@attendanceXls');
                });
            });

            Route::prefix('panel')->namespace('Spectacles\Panel')->group(function () {
                Route::group(['prefix' => 'actors'], function () {
                    Route::get('/', 'ActorController@index');
                    Route::get('index', 'ActorController@index');
                    Route::get('assign-role/{actor_id}/{scene_id}', 'ActorController@assignRole');
                    Route::post('update-roles/{actor_id}', 'ActorController@updateRoles');
                    Route::get('detach-role/{actor_id}/{role_id}', 'ActorController@detachRole');
                    Route::post('detach-role/{actor_id}/{role_id}', 'ActorController@postDetachRole');
                    Route::get('export', 'ActorController@export');
                });

                Route::group(['prefix' => 'scenes'], function () {
                    Route::get('/', 'SceneController@index');
                    Route::get('index', 'SceneController@index');
                    Route::get('generate-outline/{rehearsal_id}/{file_type}', 'SceneController@generateOutline');
                    Route::get('show-roles/{scene_id}', 'SceneController@showRoles');
                    Route::get('show-actors/{role_id}', 'SceneController@showActors');
                    Route::get('show-wings/{scene_id}', 'SceneController@showWings');
                    Route::get('download-generator/{filename}/{file_type}', 'SceneController@downloadGenerator');
                });

                Route::group(['prefix' => 'timetables'], function () {
                    Route::get('/', 'TimetableController@index');
                    Route::get('index', 'TimetableController@index');
                    Route::get('generate-pdf', 'TimetableController@generatePdf');
                    Route::get('download-pdf', 'TimetableController@downloadPdf');
                    Route::get('generate-xls', 'TimetableController@generateXls');
                    Route::get('download-xls', 'TimetableController@downloadXls');
                    Route::get('generate-outline/{rehearsal_id}', 'TimetableController@generateOutline');
                    Route::get('download-generator/{filename}', 'TimetableController@downloadGenerator');
                    Route::get('attendance/{rehearsal_id}', 'TimetableController@attendance');
                    Route::post('set-attendance', 'TimetableController@setAttendance');
                    Route::post('export-attendance', 'TimetableController@exportAttendance');
                    Route::get('show/{rehearsal_id}', 'TimetableController@show');
                    Route::get('download-doc/{filename}', 'TimetableController@downloadDoc');
                });

                Route::group(['prefix' => 'absences'], function () {
                    Route::get('list    ', 'AbsenceController@list');
                    Route::get('roles', 'AbsenceController@roles');
                    Route::get('roles-show/{show_id}', 'AbsenceController@rolesShow');
                    Route::get('show/{actor_id}', 'AbsenceController@show');
                    Route::get('edit/{actor_id}', 'AbsenceController@edit');
                    Route::post('update/{actor_id}', 'AbsenceController@update');
                    Route::get('show-actors/{show_id}/{role_id}', 'AbsenceController@showActors');
                    Route::get('export-roles/{show_id}', 'AbsenceController@exportRoles');
                    Route::get('export-actors', 'AbsenceController@exportActors');
                });

                Route::group(['prefix' => 'shows'], function () {
                    Route::get('/', 'ShowController@index');
                    Route::get('index', 'ShowController@index');
                    Route::get('generate-outline/{show_id}/{type}', 'ShowController@generateOutline');
                    Route::get('download-generator/{filename}', 'ShowController@downloadGenerator');
                    Route::get('create', 'ShowController@create');
                    Route::post('store', 'ShowController@store');
                });

                Route::group(['prefix' => 'costumes'], function () {
                    Route::get('/', 'CostumeController@index');
                    Route::get('index', 'CostumeController@index');
                    Route::post('export', 'CostumeController@export');
                    Route::get('download/{filename}', 'CostumeController@download');
                });

                Route::group(['prefix' => 'props'], function () {
                    Route::get('/', 'PropController@index');
                    Route::get('index', 'PropController@index');
                    Route::post('export', 'PropController@export');
                    Route::get('download/{filename}', 'PropController@download');
                });
            });

            Route::prefix('volunteers')->namespace('Spectacles')->group(function () {
                Route::get('/', 'VolunteerController@index');
                Route::get('index', 'VolunteerController@index');
            });

            Route::prefix('changes')->namespace('Spectacles')->group(function () {
                Route::get('/', 'ChangesController@index');
                Route::get('index', 'ChangesController@index');
                Route::get('roles/{show_id}', 'ChangesController@roles');
                Route::get('actors/{show_id}/{role_id}', 'ChangesController@actors');
                Route::get('add-replacement/{show_id}/{role_id}', 'ChangesController@addReplacement');
                Route::get('search-actor', 'ChangesController@searchActor');
                Route::get('load-actor', 'ChangesController@loadActor');
                Route::post('store-replacement/{show_id}/{role_id}', 'ChangesController@storeReplacement');
                Route::get('delete-replacement/{change_id}', 'ChangesController@deleteReplacement');
                Route::post('remove-replacement/{change_id}', 'ChangesController@removeReplacement');
            });

            Route::prefix('stage-manager')->namespace('Spectacles')->group(function () {
                Route::get('/', 'StageManagerController@index');
                Route::post('upload', 'StageManagerController@upload');
                Route::get('run/{script_id}', 'StageManagerController@run');
                Route::get('load/{script_id}', 'StageManagerController@load');
            });
        });

        Route::prefix('{project_type}/{project_slug}')->where(['project_type' => 'spectacle|project'])->group(function () {
            Route::prefix('surveys')->group(function(){
                Route::get('/', 'SurveyController@index');
                Route::get('create', 'SurveyController@create');
                Route::post('store', 'SurveyController@store');
                Route::get('manage/{survey_id}', 'SurveyController@manage');
                Route::get('add-question', 'SurveyController@addQuestion');
                Route::get('add-agreement', 'SurveyController@addAgreement');
                Route::get('edit/{survey_id}', 'SurveyController@edit');
                Route::post('update/{survey_id}', 'SurveyController@update');
                Route::get('edit-base/{survey_id}', 'SurveyController@editBase');
                Route::post('update-base/{survey_id}', 'SurveyController@updateBase');
                Route::get('delete/{survey_id}', 'SurveyController@deleteModal');
                Route::post('delete/{survey_id}', 'SurveyController@delete');
                Route::get('duplicate/{survey_id}', 'SurveyController@duplicate');
                Route::get('entries/{survey_id}', 'SurveyController@entries');
                Route::get('show/{survey_entry_id}', 'SurveyController@show');
                Route::get('fields', 'SurveyController@fields');
                Route::get('preview/{survey_id}', 'SurveyController@preview');
                Route::get('link/{survey_id}', 'SurveyController@link');
                Route::get('response-mail/{survey_id}', 'SurveyController@responseMail');
                Route::post('update-response-mail/{survey_id}', 'SurveyController@updateResponseMail');
                Route::get('preview-response-mail/{survey_id}', 'SurveyController@previewResponseMail');
                Route::get('submitted-view/{survey_id}', 'SurveyController@submittedView');
                Route::post('submitted-view/{survey_id}', 'SurveyController@updateSubmittedView');
                Route::get('preview-submitted-view/{survey_id}', 'SurveyController@previewSubmittedView');
            });

            Route::prefix('settings')->group(function(){
                Route::get('/', 'SettingController@index');
                Route::get('edit/{setting_id}', 'SettingController@edit');
                Route::post('update/{setting_id}', 'SettingController@update');
                Route::get('preview-mail/{setting_id}', 'SettingController@previewMail');
                Route::get('append-template', 'SettingController@appendTemplate');
            });

            Route::prefix('structures')->group(function (){
                Route::get('/', 'StructureController@index');
                Route::get('create', 'StructureController@create');
                Route::post('store', 'StructureController@store');
                Route::get('edit/{structure_id}', 'StructureController@edit');
                Route::post('update/{structure_id}', 'StructureController@update');
                Route::get('manage/{structure_id}', 'StructureController@manage');
                Route::get('load-nodes', 'StructureController@loadNodes');
                Route::get('append/{node_id}', 'StructureController@appendModal');
                Route::post('append/{node_id}', 'StructureController@append');
                Route::get('tasks/{node_id}', 'StructureController@tasks');
                Route::get('add-task/{node_id}',  'StructureController@addTask');
                Route::post('store-task',  'StructureController@storeTask');
                Route::get('edit-task/{task_id}',  'StructureController@editTask');
                Route::post('update-task/{task_id}',  'StructureController@updateTask');
                Route::get('volunteers/{node_id}', 'StructureController@volunteers');
                Route::get('add-volunteer/{node_id}',  'StructureController@addVolunteer');
                Route::get('search-volunteer', 'StructureController@searchVolunteer');
                Route::post('add-volunteer/{node_id}',  'StructureController@attachVolunteer');
                Route::get('detach-volunteer/{node_id}/{volunteer_id}', 'StructureController@detachVolunteerModal');
                Route::post('detach-volunteer/{node_id}/{volunteer_id}',  'StructureController@detachVolunteer');
                Route::get('move-node/{node_id}', 'StructureController@moveNodeModal');
                Route::post('move-node/{node_id}', 'StructureController@moveNode');
                Route::get('edit-node/{node_id}', 'StructureController@editNode');
                Route::post('update-node/{node_id}', 'StructureController@updateNode');
                Route::get('delete-node/{node_id}', 'StructureController@deleteNodeModal');
                Route::post('delete-node/{node_id}', 'StructureController@deleteNode');
            });

            Route::prefix('tasks')->group(function(){
                Route::get('/', 'TaskController@index');
                Route::get('show/{task_id}', 'TaskController@show');
            });
        });

        Route::prefix('project/{project_slug}')->namespace('Project')->group(function () {
            Route::prefix('participants')->group(function () {
                Route::get('/', 'ParticipantController@index');
                Route::post('export', 'ParticipantController@export');
                Route::get('show/{participant_id}', 'ParticipantController@show');
                Route::get('edit/{participant_id}', 'ParticipantController@edit');
                Route::post('update/{participant_id}', 'ParticipantController@update');
                Route::get('delete/{participant_id}', 'ParticipantController@delete');
                Route::post('delete/{participant_id}', 'ParticipantController@remove');
                Route::get('generate-contract/{participant_id}','ParticipantController@generateContract');
            });

            Route::prefix('attributes')->group(function(){
                Route::get('/', 'AttributeController@index');
                Route::get('export/{attribute_id}', 'AttributeController@export');
            });
        });

        Route::prefix('files-manager')->group(function () {
            Route::get('/', 'FilesManagerController@index');
        });

        Route::prefix('sending')->namespace('Sending')->group(function () {
            Route::prefix('mailings')->group(function () {
                Route::get('/', 'MailingController@index');
                Route::get('create', 'MailingController@create');
                Route::post('show-receivers', 'MailingController@showReceivers');
                Route::post('count-receivers', 'MailingController@countReceivers');
                Route::post('send', 'MailingController@send');
                Route::get('test', 'MailingController@test');
                Route::post('send-test', 'MailingController@sendTest');
                Route::get('load-project-parameters/{project_id}', 'MailingController@loadProjectParameters');
                Route::get('load-project-surveys/{project_id}', 'MailingController@loadProjectSurveys');
            });

            Route::prefix('sms')->group(function () {
                Route::get('/', 'SmsController@index');
                Route::get('create', 'SmsController@create');
                Route::get('test', 'SmsController@test');
                Route::post('send-test', 'SmsController@sendTest');
                Route::post('send', 'SmsController@send');
            });
        });

        Route::prefix('news')->group(function(){
            Route::get('/', 'NewsController@index');
            Route::get('create', 'NewsController@create');
            Route::post('store', 'NewsController@store');
            Route::get('edit/{news_id}', 'NewsController@edit');
            Route::post('update/{news_id}', 'NewsController@update');
            Route::get('delete/{news_id}', 'NewsController@delete');
            Route::post('remove/{news_id}', 'NewsController@remove');
        });
    });
});

