<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->middleware('auth:api')->group(function(){

    Route::prefix('user')->group(function(){
        Route::get('info', 'UserController@info');
        Route::post('firebase-uuid', 'UserController@firebaseUiid');
        Route::post('logout', 'UserController@logout');
        Route::post('avatar', 'UserController@avatar');
    });

    Route::prefix('news')->group(function(){
        Route::get('/', 'NewsController@index');
        Route::get('show/{news_id}', 'NewsController@show');
    });

    Route::prefix('tasks')->group(function(){
        Route::get('/', 'TaskController@index');
        Route::get('show/{task_id}', 'TaskController@show');
    });

});
