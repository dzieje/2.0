<?php

namespace Webwizards\WebSMS;

use Illuminate\Support\ServiceProvider;

class WebSMSServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $configPath = __DIR__ . '/../config/web-sms.php';
        $this->mergeConfigFrom($configPath, 'web-sms');

        $this->app->bind('websms', function (){
            return new WebSMS();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $configPath = __DIR__ . '/../config/web-sms.php';
        if (function_exists('config_path')) {
            $publishPath = config_path('web-sms.php');
        } else {
            $publishPath = base_path('config/web-sms.php');
        }
        $this->publishes([$configPath => $publishPath], 'websms-config');
    }



}
