<?php


namespace Webwizards\WebSMS;


class WebSMS
{
    protected $from;
    protected $content;

    private $error_codes = [
        8 =>	'Błąd w odwołaniu (Prosimy zgłosić)',
        11 =>	'Zbyt długa lub brak wiadomości lub ustawiono parametr nounicode i pojawiły się znaki specjalne w wiadomości. Dla wysyłki VMS błąd oznacz brak pliku WAV ,lub błąd tekstu TTS (brak tekstu lub inne niż UTF-8 kodowanie).',
        12 =>	'Wiadomość składa się z większej ilości części niż określono w parametrze &max_parts',
        13 =>	'Brak prawidłowych numerów telefonów (numer błędny, stacjonarny (w przypadku wysyłki SMS) lub znajdujący się na czarnej liście)',
        14 =>	'Nieprawidłowe pole nadawcy',
        17 =>	'Nie można wysłać FLASH ze znakami specjalnymi',
        18 =>	'Nieprawidłowa liczba parametrów',
        19 =>	'Za dużo wiadomości w jednym odwołaniu (zbyt duża liczba odbiorców, w przypadku użycia skróconego linku, limit wynosi 100)',
        20 =>	'Nieprawidłowa liczba parametrów IDX',
        21 =>	'Wiadomość MMS ma za duży rozmiar (maksymalnie 300kB)',
        22 =>	'Błędny format SMIL',
        23 =>	'Błąd pobierania pliku dla wiadomości MMS lub VMS',
        24 =>	'Błędny format pobieranego pliku',
        25 =>	'Parametry &normalize oraz &datacoding nie mogą być używane jednocześnie.',
        26 =>	'Za długi temat wiadomości. Temat może zawierać maksymalnie 30 znaków.',
        27 =>	'Parametr IDX za długi. Maksymalnie 255 znaków',
        28 =>	'Błędna wartość parametru time_restriction. Dostępne wartości to follow, ignore lub nearest_available',
        30 =>	'Brak parametru UDH jak podany jest datacoding=bin',
        31 =>	'Błąd konwersji TTS',
        32 =>	'Nie można wysyłać wiadomości MMS i VMS na zagraniczne numery lub wysyłka na zagranicę wyłączona na koncie.',
        33 =>	'Brak poprawnych numerów',
        35 =>	'Błędna wartość parametru tts_lector. Dostępne wartości: agnieszka, ewa, jacek, jan, maja',
        36 =>	'Nie można wysyłać wiadomości binarnych z ustawioną stopką.',
        40 =>	'Brak grupy o podanej nazwie',
        41 =>	'Wybrana grupa jest pusta (brak kontaktów w grupie)',
        50 =>	'Nie można zaplanować wysyłki na więcej niż 3 miesiące w przyszłość',
        51 =>	'Ustawiono błędną godzinę wysyłki, wiadomość VMS mogą być wysyłane tylko pomiędzy godzinami 8 a 22 lub ustawiono kombinację parametrów try i interval powodującą możliwość próby połączenia po godzinie 22.',
        52 =>	'Za dużo prób wysyłki wiadomości do jednego numeru (maksymalnie 10 prób w przeciągu 60sek do jednego numeru)',
        53 =>	'Nieunikalny parametr idx. Wiadomość o podanym idx została wysłana w ostatnich czterech dniach lub jest zaplanowana do wysyłki w przyszłości przy wykorzystaniu parametru &check_idx=1.',
        54 =>	'Błędny format daty. Ustawiono sprawdzanie poprawności daty &date_validate=1',
        55 =>	'Brak numerów stacjonarnych w wysyłce i ustawiony parametr skip_gsm',
        56 =>	'Różnica pomiędzy datą wysyłki, a datą wygaśnięcia nie może być mniejsza niż 15 minut i większa niż 72 godzin',
        57 =>	'Numer znajduje się na czarnej liście dla danego użytkownika.',
        59 =>	'Numer znajduje się na liście "Wypisani"',
        60 =>	'Grupa kodów o podanej nazwie nie istnieje.',
        61 =>	'Data ważności grupy kodów minęła.',
        62 =>	'Brak wolnych kodów w podanej grupie (wszystkie kody zostały już wykorzystane).',
        65 =>	'Brak wystarczającej liczby kodów rabatowych dla wysyłki. Liczba niewykorzystanych kodów w grupie musi być co najmniej równa liczbie numerów w wysyłce.',
        66 =>	'W treści wiadomości brak jest znacznika [%kod%] dla wysyłki z parametrem &discount_group (znacznik taki jest wymagany).',
        70 =>	'Błędny adres CALLBACK w parametrze notify_url.',
        74 =>	'Data wysyłki nie spełnia ograniczeń czasowych ustawionych na koncie',
        76 =>	'Nieprawidłowe znaki w parametrach żądania',
        94 =>	'Wysyłka wiadomości z linkiem jest zablokowana',
        101 =>	'Niepoprawne lub brak danych autoryzacji',
        102 =>	'Nieprawidłowy login lub hasło',
        103 =>	'Brak punków dla tego użytkownika',
        104 =>	'Brak szablonu',
        105 =>	'Błędny adres IP (włączony filtr IP dla interfejsu API)',
        106 =>	'Błędny adres URL do skrócenia w parametrze [%idzdo:adres_URL%]',
        110 =>	'Usługa (SMS, MMS, VMS lub HLR) nie jest dostępna na danym koncie.',
        200 =>	'Nieudana próba wysłania wiadomości, prosimy ponowić odwołanie',
        201 =>	'Wewnętrzny błąd systemu (prosimy zgłosić)',
        202 =>	'Zbyt duża ilość jednoczesnych odwołań do serwisu, wiadomość nie została wysłana (prosimy odwołać się ponownie)',
        203 =>	'Zbyt wiele odwołań do serwisu. Spróbuj ponownie później.',
        300 =>	'Nieprawidłowa wartość pola points (przy użyciu pola points jest wymagana wartość 1)',
        301 =>	'Wiadomość o podanym ID nie istnieje lub jest zaplanowana do wysłania w przeciągu najbliższych 60 sekund (nie można usunąć takiej wiadomości).',
        400 =>	'Nieprawidłowy ID statusu wiadomości.',
        401 =>	'Token nie ma uprawnień do wykonywanej akcji.',
        409 =>	'Podana wartość już istnieje.',
        999 =>	'Wewnętrzny błąd systemu (prosimy zgłosić)',
        1000 =>	'Akcja dostępna tylko dla użytkownika głównego',
        1001 =>	'Nieprawidłowa akcja (oczekiwane jedna z add_user, set_user, get_user, credits)',
        1010 =>	'Błąd dodawania użytkownika',
        1020 =>	'Błąd edycji konta użytkownika',
        1021 =>	'Brak danych do edycji, przynajmniej jeden parametr musi być edytowany',
        1030 =>	'Błąd pobierania danych użytkownika',
        1032 =>	'Nie istnieje użytkownik o podanej nazwie dla danego użytkownika głównego',
        1100 =>	'Błąd danych użytkownika',
        1110 =>	'Błędna nazwa tworzonego użytkownika',
        1111 =>	'Nie podano nazwy tworzonego konta użytkownika',
        1112 =>	'Nazwa konta użytkownika za krótka (minimum 3 znaki)',
        1113 =>	'Nazwa konta użytkownika za długa, łączna długość nazwy użytkownika wraz z prefiksem użytkownika głównego może mieć maksymalnie 32 znaki',
        1114 =>	'W nazwie użytkownika pojawiły się nidozwolone znaki, dozwolone są litery [A – Z], cyfry [0 – 9] oraz znaki @, -, _ i',
        1115 =>	'Istnieje już użytkownik o podanej nazwie',
        1120 =>	'Błąd hasła dla tworzonego konta użytkownika',
        1121 =>	'Hasło dla tworzonego konta użytkownika za krótkie',
        1122 =>	'Hasło dla tworzonego konta użytkownika za długie',
        1123 =>	'Hasło powinno być zakodowane w MD5',
        1130 =>	'Błąd limitu punktów przydzielanego użytkownikowi',
        1131 =>	'Parametr limit powinno zawierać wartość numeryczną',
        1140 =>	'Błąd limitu miesięcznego punktów przydzielanego użytkownikowi',
        1141 =>	'Parametr month_limit powinno zawierać wartość numeryczną',
        1150 =>	'Błędna wartość parametru senders, dopuszczalne wartości dla tego parametru to 0 lub 1',
        1160 =>	'Błędna wartość parametru phonebook, dopuszczalne wartości dla tego parametru to 0 lub 1',
        1170 =>	'Błędna wartość parametru active, dopuszczalne wartości dla tego parametru to 0 lub 1',
        1180 =>	'Błąd parametru info',
        1183 =>	'Zawartość parametru info jest za długa',
        1190 =>	'Błąd hasła do interfejsu API dla konta użytkownika',
        1192 =>	'Błędna długość hasła do interfejsu API dla konta użytkownika (hasło zakodowane w md5 powinno mieć 32 znaki)',
        1193 =>	'Hasło do interfejsu powinno zostać podane w formie zakodowanej w md5',
        2001 =>	'Nieprawidłowa akcja (oczekiwane jedna z add, status, delete, list)',
        2010 =>	'Błąd dodawania pola nadawcy',
        2030 =>	'Błąd sprawdzania statusu pola nadawcy',
        2031 =>	'Nie istnieje pole nadawcy o podanej nazwie',
        2060 =>	'Błąd dodawania domyślnego pola nadawcy',
        2061 =>	'Pole nadawcy musi być aktywne, żeby ustawić je jako domyślne',
        2062 =>	'Pole nadawcy już jest ustawione jako domyślne',
        2100 =>	'Błąd przesyłanych danych',
        2110 =>	'Błąd nazwy pola nadawcy',
        2111 =>	'Brak nazwy dodawanego pola nadawcy (parametr &add jest pusty)',
        2112 =>	'Niepoprawna nazwa pola nadawcy (np. numer telefonu, zawierająca polskie i/lub specjalne znaki lub za długie), pole nadawcy może mieć maksymalnie 11 znaków, dopuszczalne znaki: a-z A-Z 0-9 - . [spacja]',
        2115 =>	'Pole o podanej nazwie już istnieje'
    ];

    public function from($phone_number)
    {
        $phone_number = str_replace([' ', '-', ':'], '', $phone_number);

        $this->from = trim($phone_number);

        return $this;
    }

    public function content($content)
    {
        $this->content = $this->cleanString($content);

        return $this;
    }

    public function send($log = true)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, config('web-sms.api_address'));
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $params = [];
        $params["serwis"] = config('web-sms.serwis');
        $params["odbiorca"] = $this->from;
        $params["wiadomosc"] = $this->content;

        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        $result = curl_exec($ch);
        curl_close($ch);

        if($log){
            \Log::info('wysłanie sms', ['params' => $params, 'response' => $result]);
        }

        $result = json_decode($result, true);

        return $result;
    }

    public function error($error_code)
    {
        if(isset($this->error_codes[$error_code])){
            return $this->error_codes[$error_code];
        }

        return null;
    }

    private function cleanString($text) {
        $replace = [
            '&lt;' => '', '&gt;' => '', '&#039;' => '', '&amp;' => '',
            '&quot;' => '', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'Ae',
            '&Auml;' => 'A', 'Å' => 'A', 'Ā' => 'A', 'Ą' => 'A', 'Ă' => 'A', 'Æ' => 'Ae',
            'Ç' => 'C', 'Ć' => 'C', 'Č' => 'C', 'Ĉ' => 'C', 'Ċ' => 'C', 'Ď' => 'D', 'Đ' => 'D',
            'Ð' => 'D', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ē' => 'E',
            'Ę' => 'E', 'Ě' => 'E', 'Ĕ' => 'E', 'Ė' => 'E', 'Ĝ' => 'G', 'Ğ' => 'G',
            'Ġ' => 'G', 'Ģ' => 'G', 'Ĥ' => 'H', 'Ħ' => 'H', 'Ì' => 'I', 'Í' => 'I',
            'Î' => 'I', 'Ï' => 'I', 'Ī' => 'I', 'Ĩ' => 'I', 'Ĭ' => 'I', 'Į' => 'I',
            'İ' => 'I', 'Ĳ' => 'IJ', 'Ĵ' => 'J', 'Ķ' => 'K', 'Ł' => 'K', 'Ľ' => 'K',
            'Ĺ' => 'K', 'Ļ' => 'K', 'Ŀ' => 'K', 'Ñ' => 'N', 'Ń' => 'N', 'Ň' => 'N',
            'Ņ' => 'N', 'Ŋ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O',
            'Ö' => 'Oe', '&Ouml;' => 'Oe', 'Ø' => 'O', 'Ō' => 'O', 'Ő' => 'O', 'Ŏ' => 'O',
            'Œ' => 'OE', 'Ŕ' => 'R', 'Ř' => 'R', 'Ŗ' => 'R', 'Ś' => 'S', 'Š' => 'S',
            'Ş' => 'S', 'Ŝ' => 'S', 'Ș' => 'S', 'Ť' => 'T', 'Ţ' => 'T', 'Ŧ' => 'T',
            'Ț' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'Ue', 'Ū' => 'U',
            '&Uuml;' => 'Ue', 'Ů' => 'U', 'Ű' => 'U', 'Ŭ' => 'U', 'Ũ' => 'U', 'Ų' => 'U',
            'Ŵ' => 'W', 'Ý' => 'Y', 'Ŷ' => 'Y', 'Ÿ' => 'Y', 'Ź' => 'Z', 'Ž' => 'Z',
            'Ż' => 'Z', 'Þ' => 'T', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a',
            'ä' => 'ae', '&auml;' => 'ae', 'å' => 'a', 'ā' => 'a', 'ą' => 'a', 'ă' => 'a',
            'æ' => 'ae', 'ç' => 'c', 'ć' => 'c', 'č' => 'c', 'ĉ' => 'c', 'ċ' => 'c',
            'ď' => 'd', 'đ' => 'd', 'ð' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e',
            'ë' => 'e', 'ē' => 'e', 'ę' => 'e', 'ě' => 'e', 'ĕ' => 'e', 'ė' => 'e',
            'ƒ' => 'f', 'ĝ' => 'g', 'ğ' => 'g', 'ġ' => 'g', 'ģ' => 'g', 'ĥ' => 'h',
            'ħ' => 'h', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ī' => 'i',
            'ĩ' => 'i', 'ĭ' => 'i', 'į' => 'i', 'ı' => 'i', 'ĳ' => 'ij', 'ĵ' => 'j',
            'ķ' => 'k', 'ĸ' => 'k', 'ł' => 'l', 'ľ' => 'l', 'ĺ' => 'l', 'ļ' => 'l',
            'ŀ' => 'l', 'ñ' => 'n', 'ń' => 'n', 'ň' => 'n', 'ņ' => 'n', 'ŉ' => 'n',
            'ŋ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'oe',
            '&ouml;' => 'oe', 'ø' => 'o', 'ō' => 'o', 'ő' => 'o', 'ŏ' => 'o', 'œ' => 'oe',
            'ŕ' => 'r', 'ř' => 'r', 'ŗ' => 'r', 'š' => 's', 'ù' => 'u', 'ú' => 'u',
            'û' => 'u', 'ü' => 'ue', 'ū' => 'u', '&uuml;' => 'ue', 'ů' => 'u', 'ű' => 'u',
            'ŭ' => 'u', 'ũ' => 'u', 'ų' => 'u', 'ŵ' => 'w', 'ý' => 'y', 'ÿ' => 'y',
            'ŷ' => 'y', 'ž' => 'z', 'ż' => 'z', 'ź' => 'z', 'þ' => 't', 'ß' => 'ss',
            'ſ' => 'ss', 'ый' => 'iy', 'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G',
            'Д' => 'D', 'Е' => 'E', 'Ё' => 'YO', 'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I',
            'Й' => 'Y', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
            'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F',
            'Х' => 'H', 'Ц' => 'C', 'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SCH', 'Ъ' => '',
            'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA', 'а' => 'a',
            'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo',
            'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l',
            'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's',
            'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch',
            'ш' => 'sh', 'щ' => 'sch', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e',
            'ю' => 'yu', 'я' => 'ya'
        ];
        $text = str_replace(array_keys($replace), $replace, $text);
        // 1) convert á ô => a o
        $text = preg_replace("/[áàâãªä]/u","a",$text);
        $text = preg_replace("/[ÁÀÂÃÄ]/u","A",$text);
        $text = preg_replace("/[ÍÌÎÏ]/u","I",$text);
        $text = preg_replace("/[íìîï]/u","i",$text);
        $text = preg_replace("/[éèêë]/u","e",$text);
        $text = preg_replace("/[ÉÈÊË]/u","E",$text);
        $text = preg_replace("/[óòôõºö]/u","o",$text);
        $text = preg_replace("/[ÓÒÔÕÖ]/u","O",$text);
        $text = preg_replace("/[úùûü]/u","u",$text);
        $text = preg_replace("/[ÚÙÛÜ]/u","U",$text);
        $text = preg_replace("/[’‘‹›‚]/u","'",$text);
        $text = preg_replace("/[“”«»„]/u",'"',$text);
        $text = str_replace("–","-",$text);
        $text = str_replace(" "," ",$text);
        $text = str_replace("ç","c",$text);
        $text = str_replace("Ç","C",$text);
        $text = str_replace("ñ","n",$text);
        $text = str_replace("Ñ","N",$text);

        //2) Translation CP1252. &ndash; => -
        $trans = get_html_translation_table(HTML_ENTITIES);
        $trans[chr(130)] = '&sbquo;';    // Single Low-9 Quotation Mark
        $trans[chr(131)] = '&fnof;';    // Latin Small Letter F With Hook
        $trans[chr(132)] = '&bdquo;';    // Double Low-9 Quotation Mark
        $trans[chr(133)] = '&hellip;';    // Horizontal Ellipsis
        $trans[chr(134)] = '&dagger;';    // Dagger
        $trans[chr(135)] = '&Dagger;';    // Double Dagger
        $trans[chr(136)] = '&circ;';    // Modifier Letter Circumflex Accent
        $trans[chr(137)] = '&permil;';    // Per Mille Sign
        $trans[chr(138)] = '&Scaron;';    // Latin Capital Letter S With Caron
        $trans[chr(139)] = '&lsaquo;';    // Single Left-Pointing Angle Quotation Mark
        $trans[chr(140)] = '&OElig;';    // Latin Capital Ligature OE
        $trans[chr(145)] = '&lsquo;';    // Left Single Quotation Mark
        $trans[chr(146)] = '&rsquo;';    // Right Single Quotation Mark
        $trans[chr(147)] = '&ldquo;';    // Left Double Quotation Mark
        $trans[chr(148)] = '&rdquo;';    // Right Double Quotation Mark
        $trans[chr(149)] = '&bull;';    // Bullet
        $trans[chr(150)] = '&ndash;';    // En Dash
        $trans[chr(151)] = '&mdash;';    // Em Dash
        $trans[chr(152)] = '&tilde;';    // Small Tilde
        $trans[chr(153)] = '&trade;';    // Trade Mark Sign
        $trans[chr(154)] = '&scaron;';    // Latin Small Letter S With Caron
        $trans[chr(155)] = '&rsaquo;';    // Single Right-Pointing Angle Quotation Mark
        $trans[chr(156)] = '&oelig;';    // Latin Small Ligature OE
        $trans[chr(159)] = '&Yuml;';    // Latin Capital Letter Y With Diaeresis
        $trans['euro'] = '&euro;';    // euro currency symbol
        ksort($trans);

        foreach ($trans as $k => $v) {
            $text = str_replace($v, $k, $text);
        }

        // 3) remove <p>, <br/> ...
        $text = strip_tags($text);

        // 4) &amp; => & &quot; => '
        $text = html_entity_decode($text);

        // 5) remove Windows-1252 symbols like "TradeMark", "Euro"...
        $text = preg_replace('/[^(\x20-\x7F)]*/','', $text);

        $targets=array('\r\n','\n','\r','\t');
        $results=array(" "," "," ","");
        $text = str_replace($targets,$results,$text);

        //XML compatible
        /*
        $text = str_replace("&", "and", $text);
        $text = str_replace("<", ".", $text);
        $text = str_replace(">", ".", $text);
        $text = str_replace("\\", "-", $text);
        $text = str_replace("/", "-", $text);
        */

        return ($text);
    }
}
