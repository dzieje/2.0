<?php

namespace Webwizards\WebSMS;

class Facade extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return 'websms';
    }
}
