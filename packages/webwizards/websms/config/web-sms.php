<?php

return [
    'api_address' => env('SMS_API_ADDRESS'),
    'serwis' => env('SMS_SERVICE')
];
