<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Show extends Model
{
    protected $fillable = ['project_id', 'show'];
    protected $dates = ['show'];

    public function volunteers()
    {
        return $this->belongsToMany(Volunteer::class, 'volunteer_show', 'show_id', 'volunteer_id');
    }

    public function changes()
    {
        return $this->hasMany(ShowVolunteerChange::class, 'show_id');
    }


    public function scopeProject($query, $project_id)
    {
        $query->where('project_id', $project_id);
    }
}
