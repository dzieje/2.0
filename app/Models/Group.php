<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    use SoftDeletes;

    protected $fillable = ['project_id', 'if_children', 'name', ];

    public function actors()
    {
        return $this->belongsToMany(Volunteer::class,  'volunteer_group', 'group_id', 'volunteer_id');
    }

    public function scopeProject($query, $project_id)
    {
        $query->where('project_id', $project_id);
    }
}
