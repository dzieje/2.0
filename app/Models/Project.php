<?php

namespace App\Models;

use App\Services\Traits\Projectable;
use App\Services\Traits\Slug;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rinvex\Attributes\Traits\Attributable;

class Project extends Model
{
    use SoftDeletes, Slug, Attributable, Projectable;

    protected $fillable = ['user_id', 'project_type_id', 'name', 'slug', 'year'];

    protected $with = ['eav'];

    protected static function boot()
    {
        parent::boot();

        static::created(function (Project $project){
            $project->settings()->create( [
                'key' => 'mail_template',
                'name' => 'szablon maila',
                'value' => [
                    'header' => '<table style="border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 100%;color: #7c7e7f;">
                        <tbody><tr>
                            <td style="padding: 0;vertical-align: middle;mso-line-height-rule: at-least;width: 600px;padding-top: 10px;padding-bottom: 20px;">
                                <div style="font-family: lato,tahoma,sans-serif;background-color: #FFFFFF;font-weight: 100;padding: 20px;Margin-bottom: 0;text-align: center;font-size: 26px !important;line-height: 34px !important;" align="center" >
                                    '.config('app.name').'
                                </div>
                            </td>
                        </tr>
                        </tbody></table>',
                    'footer' => '<table style="border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 100%;color: #7c7e7f; background-color: #262626;">
                        <tbody><tr>
                            <td style="padding: 0;vertical-align: middle;">&nbsp;</td>
                            <td style="padding: 0;vertical-align: middle;mso-line-height-rule: at-least;width: 600px;padding-top: 10px;padding-bottom: 20px;">
                                <div style="font-family: lato,tahoma,sans-serif;color: #FFFFFF;font-weight: 100;Margin-bottom: 0;text-align: center;font-size: 26px !important;line-height: 34px !important;" align="center">
                                    &copy;  '.config('app.name').'
                                </div>
                            </td>
                            <td style="padding: 0;vertical-align: middle;">&nbsp;</td>
                        </tr>
                        </tbody></table>',
                    'mail_background_color' => '#262626',
                    'content_background_color' => '#ffffff',
                    'content_font_color' => '#7c7e7f'
                ]
            ]);

            $project->settings()->create( [
                'key' => 'contract_template',
                'name' => 'wzory umów'
            ]);
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function contributors()
    {
        return $this->belongsToMany(User::class, 'project_user');
    }

    public function type()
    {
        return $this->belongsTo(ProjectType::class, 'project_type_id');
    }

    public function attributes()
    {
        return $this->belongsToMany(VolunteerAttribute::class, 'project_attribute', 'project_id', 'attribute_id');
    }

    public function surveys()
    {
        return $this->hasMany(Survey::class);
    }

    public function settings()
    {
        return $this->hasMany(ProjectSetting::class);
    }

    public function settingMailTemplate()
    {
        return $this->settings()->where('key', 'mail_template')->first();
    }

    public function settingContractTemplate()
    {
        return $this->settings()->where('key', 'contract_template')->first();
    }

    public function projectVolunteers()
    {
        return $this->hasMany(ProjectVolunteer::class);
    }

    public function volunteers()
    {
        return $this->belongsToMany(Volunteer::class, 'project_volunteers')
            ->withTimestamps()
            ->withPivot('contract')
            ->whereNull('project_volunteers.deleted_at');
    }
}
