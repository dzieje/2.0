<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Costume extends Model
{
    use SoftDeletes;

    protected $fillable = ['role_id', 'name', 'type'];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

}
