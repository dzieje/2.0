<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StageManager extends Model
{
    protected $fillable = [
        'project_id',
        'user_id',
        'filename',
        'original_filename'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function scopeProject($query, $project_id)
    {
        $query->where('project_id', $project_id);
    }
}
