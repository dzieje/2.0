<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SurveyAgreement extends Model
{
    use SoftDeletes;

    protected $fillable = ['survey_id', 'question'];
    protected $dates = ['deleted_at'];

    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }

    public function answers()
    {
        return $this->morphMany(SurveyEntryAnswer::class, 'question');
    }
}
