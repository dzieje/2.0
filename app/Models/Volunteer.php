<?php

namespace App\Models;

use App\Services\Traits\WithoutEvents;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Laravel\Passport\HasApiTokens;
use Rinvex\Attributes\Traits\Attributable;
use Illuminate\Support\Str;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Volunteer extends Authenticatable
{
    use SoftDeletes, Attributable, WithoutEvents, HasApiTokens;

    protected $guarded = 'panel';

    protected $fillable = [
        'firebase_uuid',
        'password',
        'uuid',
        'avatar_filename',
        'gender',
        'name',
        'surname',
        'phone',
        'email',
        'login',
        'date_of_birth',
        'guardian_id',
        'received_date',
        'registration_token',
        'short_hash',
        'timesheet_date',
        'timesheet_all',
        'contract_type'
    ];

    protected $dates = ['date_of_birth', 'received_date', 'timesheet_date'];

    protected $with = ['eav'];

    public $viewable = [
        'gender' => 'płeć',
        'name' => 'imię',
        'surname' => 'nazwisko',
        'phone' => 'telefon',
        'email' => 'email',
        'date_of_birth_formatted' => 'data urodzenia',
        'created_at_formatted' => 'data wypełnienia'
    ];

    protected static function booted()
    {
        self::creating(function ($model) {
            $model->uuid = (string) Str::uuid();
        });

        static::created(function (Volunteer $volunteer) {
            $volunteer->updateContract();
        });

        static::updated(function (Volunteer $volunteer){
            if($volunteer->isDirty('date_of_birth')){
                $volunteer->updateContract();

                foreach($volunteer->projectVolunteers as $projectVolunteer)
                {
                    ProjectVolunteer::updateContract($projectVolunteer);
                }
            }
        });
    }

    public function projects()
    {
        return $this->belongsToMany(Project::class, 'project_volunteers')
            ->withTimestamps()
            ->withPivot('contract')
            ->whereNull('project_volunteers.deleted_at');
    }

    public function projectsWithTrashed()
    {
        return $this->belongsToMany(Project::class, 'project_volunteers')
            ->withTimestamps()
            ->withPivot('contract');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'volunteer_role', 'volunteer_id', 'role_id');
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'volunteer_group', 'volunteer_id', 'group_id');
    }

    public function projectVolunteers()
    {
        return $this->hasMany(ProjectVolunteer::class);
    }

    public function nodes()
    {
        return $this->belongsToMany(StructureNode::class, 'structure_node_volunteer');
    }


    /**
     * Scope a query to filtr by project.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  integer  $project_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeProject($query, $project_id, $filtered_show = true)
    {
        $return_query = $query->whereHas('projects', function ($query) use($project_id){
            $query->where('project_id', $project_id);
        });

        if(Cache::remember('project_types.'.$project_id, 3600, function () use($project_id){
            return Project::find($project_id)->project_type_id;
        }) == 1 && $filtered_show){
            $query->hasAttribute('show_if_actor', 1);
        }

        return  $return_query;
    }

    public function updateContract()
    {
        if($this->date_of_birth) {
            $birth_18 = Carbon::now()->subYears(18);
            $birth_16 = Carbon::now()->subYears(16);

            if ($this->date_of_birth->lte($birth_18)) {
                $this->contract_type = 'adult';
            } elseif ($this->date_of_birth->lte($birth_16)) {
                $this->contract_type = '16_17';
            }

            $this->contract_type = 'under_16';
            $this->saveWithoutEvents();
        }
    }

    public function getFullNameAttribute()
    {
        return $this->name.' '.$this->surname;
    }

    public static function getDocParamList() {
        $out = [];
        $out['%%IMIE%%'] = 'Imię wolontariusza';
        $out['%%NAZWISKO%%'] = 'Nazwisko wolontariusza';
        $out['%%INICJALY%%'] = 'Inicjały wolontariusza';
        $out['%%TELEFON%%'] = 'Telefon wolontariusza';
        $out['%%EMAIL%%'] = 'Email wolontariusza';
        $out['%%PROFILE_BUTTON%%'] = 'Link do profilu';
        $out['%%APP_NAME%%'] = 'Nazwa systemu';
        $out['%%CONTRACT_NUMBER%%'] = 'Numer umowy';
        $out['%%CURRENT_DATE%%'] = 'Dzisiejsza data';

        return $out;
    }
    public function getDocParamValues() {
        $out = [];
        $out['%%IMIE%%'] = $this->name;
        $out['%%NAZWISKO%%'] = $this->surname;
        $out['%%INICJALY%%'] = $this->name[0].'.'.$this->surname[0].'.';
        $out['%%TELEFON%%'] = $this->phone;
        $out['%%EMAIL%%'] = $this->email;
        $out['%%PROFILE_BUTTON%%'] = '<a style="color: #fff; background-color: #c3193d; border-color: #ac1636; display: inline-block; margin-bottom: 0;font-weight: normal; text-align: center; vertical-align: middle; -ms-touch-action: manipulation; touch-action: manipulation; cursor: pointer;background-image: none; border: 1px solid transparent; white-space: nowrap; padding: 6px 12px;font-size: 14px;line-height: 1.428571429; border-radius: 4px; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none;user-select: none;" href="'.url('panel/complete', $this->uuid).'">Przejdź by dokończyć</a>';
        $out['%%APP_NAME%%'] = config('app.name');
        $out['%%CURRENT_DATE%%'] = Carbon::now()->format('Y-m-d');
        return $out;
    }

    public function setPasswordAttribute($password)
    {
        if ( $password !== null & $password !== "" )
        {
            $this->attributes['password'] = bcrypt($password);
        }
    }

    public function getDateOfBirthFormattedAttribute()
    {
        if($this->date_of_birth)
        {
            return '<div class="d-flex align-items-center text-nowrap">
                '.$this->date_of_birth->format('Y-m-d').'
               <span class="badge badge-primary ml-1">'.$this->date_of_birth->diffInYears(Carbon::now()).'</span>
            </div>';
        }
    }

    public function getCreatedAtFormattedAttribute()
    {
        return $this->created_at->format('Y-m-d H:i');
    }

    public static function initColumnFields()
    {
        $json = [];

        $attributes = VolunteerAttribute::pluck('name', 'slug')->toArray();
        foreach($attributes as $slug => $attribute)
        {
            $json[] = [
                'name' => $attribute,
                'key' => $slug,
                'checked' => true,
                'type' => 'extended'
            ];
        }

        $attributes = (new Volunteer())->viewable;
        foreach($attributes as $slug => $attribute)
        {
            $json[] = [
                'name' => $attribute,
                'key' => $slug,
                'checked' => true,
                'type' => 'embed'
            ];
        }

        return ['attributes' => $json];
    }

    /**
     * Find the user instance for the given username.
     *
     * @param  string  $username
     */
    public function findForPassport($username)
    {
        return $this->where('login', $username)->first();
    }

    public function news()
    {
        return $this->hasMany(Notification::class)->where('notifiable_type', 'App\Models\News');
    }

    public function tasks()
    {
        return $this->hasMany(Notification::class)->where('notifiable_type', 'App\Models\Task');
    }
}
