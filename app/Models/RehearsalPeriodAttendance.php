<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RehearsalPeriodAttendance extends Model
{
    protected $fillable = ['rehearsal_period_id', 'user_id', 'volunteer_id', 'status'];

    public function period()
    {
        return $this->belongsTo(RehearsalPeriod::class, 'rehearsal_period_id')->withTrashed();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function actor()
    {
        return $this->belongsTo(Volunteer::class, 'volunteer_id');
    }
}
