<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SurveyEntry extends Model
{
    use SoftDeletes;

    protected $fillable = ['survey_id', 'volunteer_id'];
    protected $dates = ['deleted_at'];

    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }

    public function volunteer()
    {
        return $this->belongsTo(Volunteer::class)->withTrashed();
    }

    public function answers()
    {
        return $this->hasMany(SurveyEntryAnswer::class);
    }
}
