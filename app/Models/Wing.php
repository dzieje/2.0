<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wing extends Model
{
    use SoftDeletes;

    protected $fillable = ['name'];

    public function roles()
    {
        return $this->hasMany(Role::class, 'wing_id');
    }

}
