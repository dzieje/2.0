<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $fillable = ['name'];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'user_role_permission', 'user_role_id', 'permission_id');
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
