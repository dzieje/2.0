<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Scene extends Model
{
    use SoftDeletes;

    protected $fillable = ['project_id', 'name', 'time_start', 'time_end'];
    protected $dates = ['deleted_at'];

    public function roles()
    {
        return $this->hasMany(Role::class);
    }

    public function scopeProject($query, $project_id)
    {
        $query->where('project_id', $project_id);
    }
}
