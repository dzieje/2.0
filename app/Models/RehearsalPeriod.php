<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RehearsalPeriod extends Model
{
    use SoftDeletes;

    protected $fillable = ['rehearsal_id', 'date_from', 'date_to'];
    protected $dates = ['date_from', 'date_to', 'deleted_at'];

    public function rehearsal()
    {
        return $this->belongsTo(Rehearsal::class);
    }

    public function scenes()
    {
        return $this->belongsToMany(Scene::class, 'rehearsal_periods_scenes', 'rehearsal_period_id', 'scene_id');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'rehearsal_periods_roles', 'rehearsal_period_id', 'role_id');
    }

    public function attendances()
    {
        return $this->hasMany(RehearsalPeriodAttendance::class);
    }
}
