<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rehearsal extends Model
{
    use SoftDeletes;

    protected $fillable = ['project_id', 'if_general', 'name', 'rehearsal'];
    protected $dates = ['rehearsal', 'deleted_at'];

    public function periods()
    {
        return $this->hasMany(RehearsalPeriod::class);
    }

    public function scopeProject($query, $project_id)
    {
        $query->where('project_id', $project_id);
    }
}
