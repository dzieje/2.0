<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectSetting extends Model
{
    protected $fillable = [
        'project_id',
        'key',
        'name',
        'value'
    ];

    protected $casts = [
        'value' => 'object'
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
