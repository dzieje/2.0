<?php

namespace App\Models;

use App\Services\Traits\Slug;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

class Survey extends Model
{
    use SoftDeletes, Slug;

    protected $fillable = ['registration', 'project_id', 'name', 'slug', 'form', 'submitted_view', 'response_mail_subject', 'response_mail_content', 'css', 'available_from', 'available_to'];
    protected $dates = ['deleted_at', 'available_from', 'available_to'];

    protected $casts = [
        'registration' => 'boolean',
        'form' => 'json'
    ];

    protected static function booted()
    {
        static::creating(function ($model) {
            $model->slug = self::createSlug($model->name);
        });

        static::saving(function ($model) {
            $model->slug = self::createSlug($model->name, $model->id);
        });
        static::saved(function (Survey $survey) {
            Cache::forget('surveys.'.$survey->slug);
        });
    }

    public function questions()
    {
        return $this->hasMany(SurveyQuestion::class);
    }

    public function agreements()
    {
        return $this->hasMany(SurveyAgreement::class);
    }

    public function entries()
    {
        return $this->hasMany(SurveyEntry::class);
    }

    public function scopeProject($query, $project_id)
    {
        $query->where('project_id', $project_id);
    }

    public function relatedProject()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }
}
