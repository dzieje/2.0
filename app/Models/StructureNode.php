<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class StructureNode extends Model
{
    use NodeTrait;

    protected $fillable = [
        'structure_id',
        'name'
    ];

    protected static function booted()
    {
        static::deleting(function ($node) {
            $node->tasks()->delete();
        });
    }

    protected function getScopeAttributes()
    {
        return [ 'structure_id' ];
    }

    public function structure()
    {
        return $this->belongsTo(Structure::class);
    }

    public function leaders()
    {
        return $this->belongsToMany(User::class, 'structure_node_user');
    }

    public function volunteers()
    {
        return $this->belongsToMany(Volunteer::class, 'structure_node_volunteer');
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
}
