<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'title',
        'content',
        'attachments',
        'filters',
        'project_id'
    ];

    protected $casts =[
        'filters' => 'json',
        'attachments' => 'json'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function notifications()
    {
        return $this->morphMany(Notification::class, 'notifiable');
    }

    public function unreadNotifications()
    {
        return $this->notifications()->unread();
    }

    public function readNotifications()
    {
        return $this->notifications()->read();
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function scopeOwner($query)
    {
        if(auth()->user()) {
            if(\Auth::guard('web')->check()){
                $query->where('user_id', auth()->user()->id);
            }
        }
    }
}
