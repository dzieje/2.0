<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = ['name', 'ability'];

    public function roles()
    {
        return $this->belongsToMany(UserRole::class, 'user_role_permission', 'permission_id', 'user_role_id');
    }
}
