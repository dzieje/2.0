<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Guardian extends Model
{
    protected $fillable = [
        'name',
        'surname',
        'gender',
        'date_of_birth',
        'street',
        'house_number',
        'flat_number',
        'post_code',
        'city',
        'phone',
        'email',
        'guardian_permission'
    ];
}
