<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'user_id',
        'message_type_id',
        'title',
        'subject',
        'content',
        'reply_to',
        'send_time'
    ];
    protected $dates = ['deleted_at', 'send_time'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function messageType()
    {
        return $this->belongsTo(MessageType::class);
    }

    public function entities()
    {
        return $this->hasMany(MessageEntity::class);
    }

    public function scopeSms($query)
    {
        return $query->where('message_type_id', 2);
    }

    public function scopeMail($query)
    {
        return $query->where('message_type_id', 1);
    }
}
