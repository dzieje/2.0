<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MessageEntity extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'message_id',
        'volunteer_id',
        'uuid',
        'send_time',
        'response'
    ];

    protected $dates = [
        'deleted_at',
        'send_time'
    ];

    protected $casts = [
        'response' => 'json'
    ];

    public function message()
    {
        return $this->belongsTo(Message::class);
    }

    public function volunteer()
    {
        return $this->belongsTo(Volunteer::class)->withTrashed();
    }
}
