<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShowVolunteerChange extends Model
{
    protected $fillable = [
        'user_id',
        'show_id',
        'volunteer_id',
        'role_id',
        'type'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function show()
    {
        return $this->belongsTo(Show::class);
    }

    public function volunteer()
    {
        return $this->belongsTo(Volunteer::class);
    }

    public function actor()
    {
        return $this->belongsTo(Volunteer::class, 'volunteer_id');
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }
}
