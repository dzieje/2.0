<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login', 'name', 'email', 'avatar', 'password', 'user_role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    public function role()
    {
        return $this->belongsTo(UserRole::class, 'user_role_id');
    }

    public function projects()
    {
        return $this->belongsToMany(Project::class, 'project_user');
    }

    public function news()
    {
        return $this->hasMany(News::class);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function hasAbility($ability)
    {
        if(is_array($ability)){
            $abilities = $ability;

            foreach ($abilities as $ability)
            {
                foreach ($this->role->permissions as $permission) {
                    if ($permission->ability == $ability) {
                        return true;
                    }
                }
            }
        }else {
            foreach ($this->role->permissions as $permission) {
                if ($permission->ability == $ability) {
                    return true;
                }
            }
        }


        return false;
    }
}
