<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Structure extends Model
{

    protected $fillable = [
        'project_id',
        'name'
    ];

    public function scopeProject($query, $project_id)
    {
        $query->where('project_id', $project_id);
    }

    public function relatedProject()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function nodes()
    {
        return $this->hasMany(StructureNode::class);
    }

}
