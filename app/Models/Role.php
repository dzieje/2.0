<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'project_id',
        'scene_id',
        'wing_id',
        'name',
        'min_actors',
        'max_actors',
        'time_start',
        'time_end',
        'if_single_actor',
        'if_understudy'
    ];

    public function scene()
    {
        return $this->belongsTo(Scene::class);
    }

    public function wing()
    {
        return $this->belongsTo(Wing::class);
    }

    public function actors()
    {
        return $this->belongsToMany(Volunteer::class, 'volunteer_role', 'role_id', 'volunteer_id');
    }

    public function costumes()
    {
        return $this->hasMany(Costume::class);
    }

    public function props()
    {
        return $this->hasMany(Prop::class);
    }

    public function periods()
    {
        return $this->belongsToMany(RehearsalPeriod::class, 'rehearsal_periods_roles', 'n_role_id', 'rehearsal_period_id');
    }

    public function changes()
    {
        return $this->hasMany(ShowVolunteerChange::class);
    }

    public function scopeProject($query, $project_id)
    {
        $query->where('project_id', $project_id);
    }
}
