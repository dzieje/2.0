<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyEntryAnswer extends Model
{
    protected $fillable = ['survey_entry_id', 'answer', 'question_id', 'question_type'];
    protected $casts = ['answer' => 'boolean'];

    public function surveyEntry(){
        return $this->belongsTo(SurveyEntry::class);
    }

    public function question()
    {
        return $this->morphTo();
    }
}
