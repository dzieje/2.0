<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'type',
        'key',
        'name',
        'value'
    ];

    protected $casts = [
        'value' => 'object'
    ];

    public function getPreviewAttribute()
    {
        $view = '';
        if($this->value) {
            foreach ($this->value->attributes as $attribute) {
                if( $attribute->checked ) {
                    $view .= '<span class="badge badge-info mx-1">' . $attribute->name . '</span>';
                }
            }
        }

        return $view;
    }

}
