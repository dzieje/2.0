<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectVolunteer extends Model
{
    use SoftDeletes;

    protected $fillable = ['project_id', 'volunteer_id', 'contract'];

    protected static function booted()
    {
        static::created(function ($projectVolunteer) {
            self::updateContract($projectVolunteer);
        });
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function volunteer()
    {
        return $this->belongsTo(Volunteer::class);
    }

    public static function updateContract(ProjectVolunteer $projectVolunteer)
    {
        if($projectVolunteer->volunteer->contract_type && ! $projectVolunteer->contract) {
            $lastContract = ProjectVolunteer::where('project_id', $projectVolunteer->project_id)->whereHas('volunteer', function ($query) use ($projectVolunteer) {
                $query->where('contract_type', $projectVolunteer->contract_type);
            })->whereNotNull('contract')->orderBy('id', 'desc')->first();

            $year = $projectVolunteer->created_at->format('Y');

            if (!$lastContract) $lastContractNumber = 0;
            else {
                $partials = explode('/', $lastContract->contract);
                if ($year == $partials[1]) {
                    $lastContractNumber = $partials[0];
                } else {
                    $lastContractNumber = 0;
                }
            }

            $contract = ($lastContractNumber + 1) . '/' . $year;

            if ($projectVolunteer->volunteer->contract_type == 'adult') {
                $contract .= '/P';
            } elseif ($projectVolunteer->volunteer->contract_type == '16_17') {
                $contract .= '/N';
            } elseif ($projectVolunteer->volunteer->contract_type == 'under_16') {
                $contract .= '/D';
            }

            $projectVolunteer->update([
                'contract' => $contract
            ]);
        }
    }
}
