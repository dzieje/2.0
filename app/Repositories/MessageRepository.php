<?php


namespace App\Repositories;


use App\Models\Group;
use App\Models\Project;
use App\Models\Role;
use App\Models\Volunteer;
use App\Models\Wing;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MessageRepository
{
    public function docParameters()
    {
        return Volunteer::getDocParamList();
    }

    public function filteredVolunteers(Request $request)
    {
        return Volunteer::where(function($query) use ($request){
            if($request->get('gender') == 'female')
            {
                $query->where('gender', 'female');
            }elseif($request->get('gender') == 'male'){
                $query->where('gender', 'male');
            }

            if($request->get('age_from') > 0)
            {
                $age_from = Carbon::now()->subYears($request->get('age_from'))->format('Y-m-d');
                $query->where('date_of_birth', '<=', $age_from);
            }

            if($request->get('age_to') > 0)
            {
                $age_to = Carbon::now()->subYears($request->get('age_to'))->format('Y-m-d');
                $query->where('date_of_birth', '>=', $age_to);
            }

            if($request->get('project') > 0)
            {
                $query->whereHas('projects', function($query) use($request){
                    $query->where('project_id', $request->get('project'));
                });
            }

            if($request->get('wing') > 0)
            {
                $query->whereHas('group', function($query) use($request){
                    $query->where('n_wing_id', $request->get('wing'));
                });
            }

            if($request->get('groups') > 0 && !in_array(0, $request->get('groups')))
            {
                $query->whereHas('groups', function($query) use($request) {
                    $query->whereIn('n_group_id', $request->get('groups'));
                });
            }

            if($request->get('role') > 0)
            {
                $query->whereHas('roles', function($query) use($request) {
                    $query->where('n_role_id', $request->get('role'));
                });
            }
        })->orderBy('surname')->get();
    }

    public function projectParameters(Project $project)
    {
        if ($project->project_type_id == 1)
        {
            $groups = Group::orderBy('name')->pluck('name', 'id');
            $roles = Role::orderBy('name')->pluck('name',   'id');
            $wings = Wing::orderBy('name')->pluck('name', 'id');

            return view('admin.sending.nav-spectacle', compact('groups', 'roles', 'wings'));
        }else{
            return '';
        }
    }

    public function projectSurveys(Project $project)
    {
        $surveys = $project->surveys()->where('available_from', '<=', Carbon::now()->startOfDay())->where(function ($query){
            $query->whereNull('available_to')->orWhere('available_to', '>=', Carbon::now()->endOfDay());
        })->get();

        return view('admin.sending.surveys', compact('surveys'));
    }
}
