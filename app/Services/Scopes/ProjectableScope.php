<?php


namespace App\Services\Scopes;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class ProjectableScope implements Scope
{

    public function apply(Builder $builder, Model $model)
    {
        if(auth()->user()) {
            if(\Auth::guard('panel')->check()){

            }else{
                $builder->where(function ($query) {
                    $query->where('user_id', auth()->user()->id)->orWhereHas('contributors', function ($query) {
                        $query->where('user_id', auth()->user()->id);
                    });
                });
            }
        }
    }
}
