<?php


namespace App\Services\Traits;


use App\Services\Scopes\ProjectableScope;

trait Projectable
{
    public static function bootProjectable()
    {
        static::addGlobalScope(new ProjectableScope);
    }
}
