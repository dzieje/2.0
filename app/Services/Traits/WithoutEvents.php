<?php


namespace App\Services\Traits;


trait WithoutEvents
{
    public function saveWithoutEvents(array $options=[])
    {
        return static::withoutEvents(function() use ($options) {
            return $this->save($options);
        });
    }
}
