<?php

namespace App\Providers;

use App\Models\Project;
use App\Models\Volunteer;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use Jenssegers\Date\Date;
use Rinvex\Attributes\Models\Attribute;
use Rinvex\Attributes\Models\Type\Varchar;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Carbon::setLocale('pl');
        Date::setLocale('pl');

        Attribute::typeMap([
            'tekstowy' => Varchar::class,
            'checkbox' =>  \Rinvex\Attributes\Models\Type\Boolean::class,
            'liczbowy' => \Rinvex\Attributes\Models\Type\Integer::class,
            'data' => \Rinvex\Attributes\Models\Type\Datetime::class
        ]);

        app('rinvex.attributes.entities')->push(Volunteer::class);
        app('rinvex.attributes.entities')->push(Project::class);
    }
}
