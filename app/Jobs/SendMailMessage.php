<?php

namespace App\Jobs;

use App\Mail\Template;
use App\Models\Message;
use App\Models\Survey;
use App\Models\Volunteer;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendMailMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    /**
     * @var Message
     */
    private $mail;
    /**
     * @var Volunteer
     */
    private $volunteer;

    /**
     * Create a new job instance.
     *
     * @param Message $mail
     * @param Volunteer $volunteer
     */
    public function __construct(Message $mail, Volunteer $volunteer)
    {
        $this->mail = $mail;
        $this->volunteer = $volunteer;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $params = $this->volunteer->getDocParamValues();

        $template = str_replace(array_keys($params), $params, $this->mail->content);

        preg_match_all('/\%\%FORM-[^\%]*\%\%/', $template, $forms);

        if(isset($forms[0]) > 0) {
            foreach ($forms[0] as $form) {
                $slug = str_replace(['%%FORM-', '%%'], '', $form);
                $survey = Survey::where('slug', $slug)->first();
                if ($survey) {
                    $link = '<p class="font-lato" style="color: #7c7e7f;font-family: Lato,Tahoma,sans-serif;font-size: 13px; line-height: 22px;Margin-top: 14px;Margin-bottom: 0; text-align: center"><a style="color: #fff; background-color: #c3193d; border-color: #ac1636; display: inline-block; margin-bottom: 0;font-weight: normal; text-align: center; vertical-align: middle; -ms-touch-action: manipulation; touch-action: manipulation; cursor: pointer;background-image: none; border: 1px solid transparent; white-space: nowrap; padding: 6px 12px;font-size: 14px;line-height: 1.428571429; border-radius: 4px; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none;user-select: none;" href="' . url('form/open', [$survey->slug, $this->volunteer->uuid]) . '">wypełnij formularz ' . $survey->name . '</a></p>';
                    $template = str_replace($form, $link, $template);
                }
            }
        }

        $template = preg_replace('/%%(.*)%%/', '', $template);
        $template = str_replace('<!-- pagebreak -->', '', $template);

        $replyTo = $this->mail->replyTo;
        $subject = $this->mail->subject;

        \Mail::to($this->volunteer->email)->send(new Template( $template , $subject, $replyTo ));

        $this->mail->entities()->create([
            'volunteer_id' => $this->volunteer->id,
            'send_time' => Carbon::now()
        ]);
    }
}
