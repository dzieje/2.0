<?php

namespace App\Jobs;

use App\Models\Message;
use App\Models\Volunteer;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendSmsMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    /**
     * @var Message
     */
    private $sms;
    /**
     * @var Volunteer
     */
    private $volunteer;

    /**
     * Create a new job instance.
     *
     * @param Message $sms
     * @param Volunteer $volunteer
     */
    public function __construct(Message $sms, Volunteer $volunteer)
    {
        $this->sms = $sms;
        $this->volunteer = $volunteer;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $params = $this->volunteer->getDocParamValues();

        $template = str_replace(array_keys($params), $params, $this->sms->content);
        $template = preg_replace('/%%(.*)%%/', '', $template);

        $result = \WebSMS::from($this->volunteer->phone)->content($template)->send();

        if($result && $result['status'] == 0){
            $this->sms->entities()->create([
                'volunteer_id' => $this->volunteer->id,
                'uuid' => isset($result['output'][0]) ? $result['output'][0]['id'] : null,
                'send_time' => Carbon::now(),
                'response' => $result
            ]);
        }else{
            $this->sms->entities()->create([
                'volunteer_id' => $this->volunteer->id,
                'response' => $result
            ]);
        }
    }
}
