<?php

namespace App\Exports;

use App\Exports\Sheets\ProjectParticipantAttribute;
use App\Models\VolunteerAttribute;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ProjectParticipantAttributes implements WithMultipleSheets
{
    /**
     * @var VolunteerAttribute
     */
    private $attribute;

    /**
     * ProjectParticipantAttributes constructor.
     */
    public function __construct(VolunteerAttribute $attribute)
    {
        $this->attribute = $attribute;
    }


    public function sheets(): array
    {
        $sheets = [];

        if($this->attribute->is_collection) {
            foreach (json_decode($this->attribute->description) as $item) {
                $sheets[] = new ProjectParticipantAttribute($item, $this->attribute);
            }
        }else{
            $sheets[] = new ProjectParticipantAttribute($this->attribute->slug, $this->attribute);
        }

        return $sheets;
    }
}
