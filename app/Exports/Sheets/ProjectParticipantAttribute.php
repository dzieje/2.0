<?php

namespace App\Exports\Sheets;

use App\Models\Volunteer;
use App\Models\VolunteerAttribute;
use Illuminate\Database\Eloquent\Builder;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class ProjectParticipantAttribute implements FromCollection, WithTitle, WithHeadings
{
    private $item;
    /**
     * @var VolunteerAttribute
     */
    private $attribute;

    /**
     * ProjectParticipantAttribute constructor.
     * @param $item
     * @param VolunteerAttribute $attribute
     */
    public function __construct($item, VolunteerAttribute $attribute)
    {
        $this->item = $item;
        $this->attribute = $attribute;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data = [];
        foreach(Volunteer::whereHas($this->attribute->slug, function (Builder $builder) {
            $builder->where('content', $this->item);
        })->get() as $volunteer){
            $data[] = [
                $volunteer->name,
                $volunteer->surname,
                $volunteer->email,
                $volunteer->phone
            ];
        }

        return collect($data);
    }

    public function headings(): array
    {
        return [
            'imię',
            'nazwisko',
            'email',
            'telefon',
        ];
    }

    public function title(): string
    {
        return $this->item;
    }
}
