<?php

namespace App\Http\Controllers\Spectacles;

use App\Http\Controllers\ExtendedController;
use App\Models\Role;
use App\Models\Scene;
use App\Models\Wing;
use App\Models\Show;
use App\Models\ShowVolunteerChange;
use App\Models\Volunteer;
use Illuminate\Http\Request;

class ChangesController extends ExtendedController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    public function index()
    {
        $shows = Show::project($this->project->id)->orderBy('show')->get();

        return view('admin.spectacles.changes.index', compact('shows'));
    }

    public function roles(Request $request, $show_id)
    {
        $show = Show::project($this->project->id)->with('volunteers', 'volunteers.roles')->find($show_id);

        $roles = Role::
        project($this->project->id)->where(function($query) use($request){
            if($request->has('name') && $request->get('name') != ''){
                $query->where('name', 'like', '%'.$request->get('name').'%');
            }
            if($request->has('scene') && $request->get('scene') != '0'){
                $query->where('n_scene_id',  $request->get('scene'));
            }
            if($request->has('wing') && $request->get('wing') != '0'){
                $query->where('n_wing_id',  $request->get('wing'));
            }
            if($request->has('if_single_actor')){
                $query->where('if_single_actor', 1);
            }
            if($request->has('if_multi_actor')){
                $query->where('if_single_actor', 0);
            }
            if($request->has('if_understudy')){
                $query->where('if_understudy', 1);
            }
            if($request->has('if_nogroup')){
                $query->has('groups', '<', 1);
            }
            if($request->has('if_noactor')){
                $query->has('actors', '<', 1);
            }
        })->with(['actors', 'wing', 'scene', 'changes' => function($query) use($show_id){
            $query->where('show_id', $show_id);
        }])->orderBy('name')->paginate(100);
        $scenes = Scene::pluck('name', 'id');
        $wings = Wing::pluck('name', 'id');

        $absences = [];
        foreach($show->volunteers as $volunteer)
        {
            foreach ($volunteer->roles as $role)
            {
                if(!isset($absences[$role->id])) $absences[$role->id] = 1;
                else $absences[$role->id] += 1;
            }
        }

        return view('admin.spectacles.changes.roles', compact('show', 'roles', 'scenes', 'wings', 'absences'));
    }

    public function actors($show_id, $role_id)
    {
        $show = Show::project($this->project->id)->with(['changes' => function($query) use($role_id){
            $query->where('n_role_id', $role_id);
        }])->find($show_id);
        $role = Role::project($this->project->id)->with('actors', 'actors.groups')->find($role_id);
        return view('admin.spectacles.changes.actors', compact('role', 'show'));
    }

    public function addReplacement($show_id, $role_id)
    {
        $show = Show::find($show_id);
        $role = Role::find($role_id);

        return view('admin.spectacles.changes.add-replacement', compact('role', 'show'));
    }

    public function searchActor(Request $request)
    {
        $q = $request->get('q');
        $actors = Volunteer::project($this->project->id)->where(function($query) use($q){
            $query->where('name', 'like', $q.'%')->orWhere('surname', 'like', $q.'%');
        })->get();

        $response = ['actors' => []];
        foreach ($actors as $actor)
        {
            $response['actors'][] = ['id' => $actor->id,  'name' => $actor->name.' '.$actor->surname];
        }

        return response()->json($response);
    }

    public function loadActor(Request $request)
    {
        $volunteer_id = $request->get('volunteer_id');
        $actor = Volunteer::with('roles')->find($volunteer_id);

        $role_id = $request->get('role_id');
        $nrole = Role::find($role_id);
        $n_scene_id = $nrole->n_scene_id;

        $roles = [];
        foreach($actor->roles as $role)
        {
            if($role->n_scene_id == $n_scene_id)
            {
                $roles[] = $role;
            }
        }

        return view('admin.spectacles.changes.actor-row', compact('actor', 'roles'));
    }

    public function storeReplacement(Request $request, $show_id, $role_id)
    {

        foreach ($request->get('actors', []) as $volunteer_id) {
            $replacement = ShowVolunteerChange::where('show_id', $show_id)->where('n_role_id', $role_id)->where('volunteer_id', $volunteer_id)->first();

            if(!$replacement)
            {
                ShowVolunteerChange::create([
                    'user_id' => auth()->user()->id,
                    'show_id' => $show_id,
                    'volunteer_id' => $volunteer_id,
                    'n_role_id' => $role_id,
                    'type'  =>  1
                ]);
            }
        }

        flash('Zastępstwa zostały utworzony.');
        return json_encode(['code' => 0]);
    }

    public function deleteReplacement($change_id)
    {
        $change = ShowVolunteerChange::find($change_id);
        return view('admin.spectacles.changes.delete-replacement', compact('change'));
    }

    public function removeReplacement($change_id)
    {
        $change = ShowVolunteerChange::find($change_id);
        $change->delete();
        flash('Zastępstwa zostało usunięte.');
        return json_encode(['code' => 0]);
    }
}
