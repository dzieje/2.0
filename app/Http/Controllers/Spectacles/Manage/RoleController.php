<?php

namespace App\Http\Controllers\Spectacles\Manage;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ExtendedController;
use App\Jobs\ExportEmailsRolesXls;
use App\Jobs\ExportPhonesRolesCsv;
use App\Models\Group;
use App\Models\Costume;
use App\Models\Prop;
use App\Models\RehearsalPeriod;
use App\Models\Role;
use App\Models\Scene;
use App\Models\Wing;
use App\Models\Project;
use App\Models\Volunteer;
use Illuminate\Http\Request;


class RoleController extends ExtendedController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    public function index(Request $request)
    {
        $query = Role::query();
        if($request->has('name') && $request->get('name') != ''){
            $query->where('name', 'like', '%'.$request->get('name').'%');
        }
        if($request->has('scene') && $request->get('scene') != '0'){
            $query->where('ć∂scene_id',  $request->get('scene'));
        }
        if($request->has('wing') && $request->get('wing') != '0'){
            $query->where('ćwing_id',  $request->get('wing'));
        }
        if($request->has('if_single_actor')){
            $query->where('if_single_actor', 1);
        }
        if($request->has('if_multi_actor')){
            $query->where('if_single_actor', 0);
        }
        if($request->has('if_understudy')){
            $query->where('if_understudy', 1);
        }
        if($request->has('if_nogroup')){
            $query->has('groups', '<', 1);
        }
        if($request->has('if_noactor')){
            $query->has('actors', '<', 1);
        }
        $query->where('project_id', $this->project->id);

        $roles = $query->with('scene', 'actors', 'costumes', 'props')->orderBy('name')->paginate(100);
        $scenes = Scene::pluck('name', 'id');
        $wings = Wing::pluck('name', 'id');

        return view('admin.spectacles.manage.roles.index', compact('roles', 'scenes', 'wings'));
    }

    public function create()
    {
        $scenes = Scene::pluck('name', 'id');
        $scenes[''] = '--- wybierz scenę ---';

        $wings = Wing::pluck('name', 'id');
        $wings[''] = '--- wybierz kulisę ---';

        return view('admin.spectacles.manage.roles.create', compact('scenes', 'wings'));
    }

    public function costumeAdd()
    {
        return view('admin.spectacles.manage.roles.add-costume');
    }

    public function propAdd()
    {
        return view('admin.spectacles.manage.roles.add-prop');
    }


    public function store(Request $request)
    {
        $role = Role::create($request->all());

        foreach ($request->get('costumes', []) as $k => $costume_name)
        {
            Costume::create([
                'role_id' => $role->id,
                'name' => $costume_name,
                'type' => $request->get('costume_types')[$k]
            ]);
        }

        foreach ($request->get('props', []) as $k => $prop_name)
        {
            Prop::create([
                'role_id' => $role->id,
                'name' => $prop_name,
                'type' => $request->get('prop_types')[$k]
            ]);
        }

        $scene_id = $role->n_scene_id;
        $periodsWithScene = RehearsalPeriod::whereHas('scenes', function($query) use($scene_id){
            $query->where('scene_id', $scene_id);
        })->get();
        foreach ($periodsWithScene as $period)
        {
            $period->roles()->attach($role->id);
        }

        return redirect()->to(url('admin/spectacle/'.$this->project->slug.'/manage/roles'));
    }

    public function edit(Request  $request)
    {
        $scenes = Scene::pluck('name', 'id');
        $scenes[0] = '--- wybierz scenę ---';

        $wings = Wing::pluck('name', 'id');
        $wings[0] = '--- wybierz kulisę ---';

        $role = Role::with('props', 'costumes')->find($request->route('role_id'));

        return view('admin.spectacles.manage.roles.edit', compact('role', 'scenes', 'wings'));
    }

    public function update(Request $request)
    {
        $role = Role::find($request->route('role_id'));

        if($role->scene_id != $request->get('scene_id'))
        {
            $scene_id = $role->scene_id;
            $new_scene_id = $request->get('scene_id');
            $periodsWithScene = RehearsalPeriod::whereHas('scenes', function($query) use($scene_id){
                $query->where('scene_id', $scene_id);
            })->get();
            foreach ($periodsWithScene as $period)
            {
                $period->roles()->detach($role->id);
            }

            $periodsWithScene = RehearsalPeriod::whereHas('scenes', function($query) use($new_scene_id){
                $query->where('scene_id', $new_scene_id);
            })->get();
            foreach ($periodsWithScene as $period)
            {
                $period->roles()->attach($role->id);
            }
        }

        $role->update($request->all());

        if(! $request->has('if_single_actor')) $role->update(['if_single_actor' => 0]);
        if(! $request->has('if_understudy')) $role->update(['if_understudy' => 0]);

        $role->costumes()->delete();
        if($request->has('costumes')){
            foreach ($request->get('costumes') as $k => $costume_name)
            {
                Costume::create([
                    'role_id' => $role->id,
                    'name' => $costume_name,
                    'type' => $request->get('costume_types')[$k]
                ]);
            }
        }

        $role->props()->delete();
        if($request->has('props')){
            foreach ($request->get('props') as $k => $prop_name)
            {
                Prop::create([
                    'role_id' => $role->id,
                    'name' => $prop_name,
                    'type' => $request->get('prop_types')[$k]
                ]);
            }
        }

        return redirect()->to(url('admin/spectacle/'.$this->project->slug.'/manage/roles'));
    }

    public function delete(Request  $request)
    {
        $role = Role::find($request->route('role_id'));

        return view('admin.spectacles.manage.roles.delete', compact('role'));
    }

    public function remove(Request  $request){
        $role = Role::find($request->route('role_id'));
        $role->delete();

        flash('Rola została usunięta.');
        return json_encode(['code' => 0]);
    }

    public function show(Request $request)
    {
        $role = Role::with('props', 'costumes')->find($request->route('role_id'));

        return view('admin.spectacles.manage.roles.show', compact('role'));
    }

    public function showProps(Request $request)
    {
        $role = Role::with('props')->find($request->route('role_id'));
        return view('admin.spectacles.manage.roles.show-props', compact('role'));
    }

    public function showCostumes(Request $request)
    {
        $role = Role::with('costumes')->find($request->route('role_id'));
        return view('admin.spectacles.manage.roles.show-costumes', compact('role'));
    }

    public function showActors(Request $request)
    {
        $role = Role::with('actors')->find($request->route('role_id'));
        return view('admin.spectacles.manage.roles.show-actors', compact('role'));
    }

    public function showGroups(Request $request)
    {
        $role = Role::with('groups')->find($request->route('role_id'));
        return view('admin.spectacles.manage.roles.show-groups', compact('role'));
    }

    public function exportPhones($role_id)
    {
        $role = Role::with('actors')->find($role_id);
        return $this->dispatch(new ExportPhonesRolesCsv($role));
    }

    public function exportEmails($role_id)
    {
        $role = Role::with('actors')->find($role_id);
        return $this->dispatch(new ExportEmailsRolesXls($role));
    }

    public function searchActor(Request $request)
    {
        $q = $request->get('q');
        $actors = Volunteer::whereIn('if_actor', [1, 2])->where('if_registered', 1)->where(function($query) use($q){
            $query->where('name', 'like', $q.'%')->orWhere('surname', 'like', $q.'%');
        })->get();

        $response = ['actors' => []];
        foreach ($actors as $actor)
        {
            $response['actors'][] = ['id' => $actor->id,  'name' => $actor->name.' '.$actor->surname];
        }

        return response()->json($response);
    }

    public function loadActor(Request $request)
    {
        $volunteer_id = $request->get('volunteer_id');

        $actor = Volunteer::find($volunteer_id);

        return view('admin.spectacles.manage.roles.actor-row', compact('actor'));
    }

    public function searchGroup(Request $request)
    {
        $q = $request->get('q');
        $groups = Group::where(function($query) use($q){
            $query->where('name', 'like', $q.'%');
        })->get();

        $response = ['groups' => []];
        foreach ($groups as $group)
        {
            $response['groups'][] = ['id' => $group->id,  'name' => $group->name];
        }

        return response()->json($response);
    }

    public function loadGroup(Request $request)
    {
        $group_id = $request->get('group_id');

        $group = Group::with('actors', 'actors.groups')->find($group_id);

        return view('admin.spectacles.manage.roles.group-rows', compact('group'));
    }

    public function detachActor(Request $request)
    {
        $role = Role::find($request->route('role_id'));
        $actor = Volunteer::find($request->route('actor_id'));

        $actor->roles()->detach($request->route('role_id'));

        if($role->if_understudy == 0) {
            $roles = $actor->group->roles()->where('n_scene_id', $role->n_scene_id)->get();
            foreach ($roles as $role_a) {
                $actor->roles()->attach($role_a->id);
            }
        }

        flash('Rola została zaktualizowana.');
        return json_encode(['code' => 0]);
    }

    public function manageActors(Request  $request)
    {
        $role = Role::with('actors')->find($request->route('role_id'));

        return view('admin.spectacles.manage.roles.manage-actors', compact('role'));
    }

    public function updateActors(Request $request)
    {
        $role = Role::find($request->route('role_id'));
        $request_actors = $request->get('actors', []);
        $role->actors()->sync($request_actors);

        return redirect()->to(url('admin/spectacle/'.$this->project->slug.'/manage/roles'));
    }
}
