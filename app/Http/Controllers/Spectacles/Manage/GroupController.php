<?php

namespace App\Http\Controllers\Spectacles\Manage;

use App\Http\Controllers\ExtendedController;
use App\Models\Group;
use App\Models\Role;
use App\Models\Scene;
use App\Models\Volunteer;
use Illuminate\Http\Request;

class GroupController extends ExtendedController
{
    public function __construct(Request  $request)
    {
        parent::__construct($request);
        $this->middleware('ability:groups');
    }

    public function index(Request $request)
    {
        $groups = Group::with('actors')->orderBy('name')->paginate(100);
        return view('admin.spectacles.manage.groups.index', compact('groups'));
    }

    public function showActors(Request $request)
    {
        $group = Group::find($request->route('group_id'));
        $actors = $group->actors;
        return view('admin.spectacles.manage.groups.show-actors', compact('actors', 'group'));
    }

    public function attachRole(Request $request)
    {
        $group = Group::find($request->route('group_id'));
        $scenes = Scene::whereHas('roles', function($query) {
            $query->where('if_single_actor', 0);
        })->orderBy('name')->pluck('name', 'id');
        return view('admin.spectacles.manage.groups.attach-role', compact('scenes', 'group'));
    }

    public function roles(Request $request)
    {
        $scene_id = $request->get('scene_id');
        $group_id = $request->get('group_id');
        $group = Group::find($group_id);
        $roles = Role::where('scene_id', $scene_id)->where('if_single_actor', 0)->whereNotIn('id', $group->roles->pluck('id')->toArray())->orderBy('name')->pluck('name', 'id');
        return view('admin.spectacles.manage.groups.roles', compact('roles'));
    }

    public function postAttachRole(Request $request)
    {
        $group = Group::with('actors')->find($request->route('group_id'));
        if($request->has('role_id') && $request->get('role_id') != 0) {
            $role_id = $request->get('role_id') ;
            $role = Role::find($role_id);

            foreach ($group->actors as $actor) {
                $roles = $actor->roles()->where('scene_id', $role->scene_id)->where('if_single_actor', 1)->where('if_understudy', 0)->first();

                if(! $roles) {
                    if (!$actor->roles->contains($role_id)) {
                        $actor->roles()->attach($role_id);
                    }
                }
            }
            if(! $group->roles->contains($role_id)){
                $group->roles()->attach($role_id);
            }
        }

        return json_encode(['code' => 0]);
    }

    public function create()
    {
        return view('admin.spectacles.manage.groups.create');
    }

    public function store(Request $request)
    {
        Group::create($request->all());
        flash('Grupa została utworzona.');
        return json_encode(['code' => 0]);
    }

    public function edit(Request $request)
    {
        $group = Group::find($request->route('group_id'));

        return view('admin.spectacles.manage.groups.edit', compact('group'));
    }

    public function update(Request $request)
    {
        $group = Group::find($request->route('group_id'));
        $group->update($request->all());

        flash('Grupa została zaktualizowana.');
        return json_encode(['code' => 0]);
    }

    public function showScenes(Request $request)
    {
        $group = Group::with('roles.scene')->find($request->route('group_id'));
        $scenes = $group->roles->pluck('scene_id')->unique();
        $scenes = Scene::whereIn('id', $scenes)->get();

        return view('admin.spectacles.manage.groups.show-scenes', compact('group', 'scenes'));
    }

    public function showRoles(Request $request)
    {
        $group = Group::with('roles', 'roles.scene')->find($request->route('group_id'));

        return view('admin.spectacles.manage.groups.show-roles', compact('group'));
    }


    public function manageActors(Request $request)
    {
        $group = Group::with('actors')->find($request->route('group_id'));

        return view('admin.spectacles.manage.groups.manage-actors', compact('group'));
    }

    public function searchActor(Request $request)
    {
        $q = $request->get('q');
        $query = Volunteer::project($this->project->id)->where(function($query) use($q){
            $query->where('name', 'like', $q.'%')->orWhere('surname', 'like', $q.'%');
        });

        if( $request->has('role_id')){
            $role = Role::find($request->get('role_id'));
            $query->whereHas('roles', function($query) use($role){
                $query->where('scene_id', $role->scene_id);
            }, '<', 1);
        }

        $actors = $query->get();

        $response = ['actors' => []];
        foreach ($actors as $actor)
        {
            $response['actors'][] = ['id' => $actor->id,  'name' => $actor->name.' '.$actor->surname];
        }

        return response()->json($response);
    }

    public function loadActor(Request $request)
    {
        $volunteer_id = $request->get('volunteer_id');

        $actor = Volunteer::find($volunteer_id);

        return view('admin.spectacles.manage.groups.actor-row', compact('actor'));
    }

    public function updateActors(Request $request)
    {
        $group = Group::find($request->route('group_id'));
        $request_actors = $request->get('actors', []);

        $group->actors()->detach();
        foreach ($request_actors as $actor_id)
        {
            $group->actors()->attach($actor_id);
        }

        return redirect()->to(url('admin/spectacle/'.$this->project->slug.'/manage/groups'));
    }

    public function deleteRole($group_id, $role_id)
    {
        $group = Group::find($group_id);

        foreach ($group->actors as $actor) {
            if ($actor->roles->contains($role_id)) {
                $actor->roles()->detach($role_id);
            }
        }

        $group->roles()->detach($role_id);

        flash('Grupa została zaktualizowana.');
        return json_encode(['code' => 0]);
    }

    public function getExport(Request $request)
    {
        $groups = Group::where(function($query) use($request){
            if($request->has('wing') && $request->get('wing') != '0'){
                $query->where('wing_id',  $request->get('wing'));
            }
        })->with('actors')->orderBy('name')->get();

        $view = view('pdf.groups', compact('groups'))->render();
        $pdf = \PDF::loadHTML($view)->setPaper('a3')
            ->setOrientation('landscape')
            ->setOption('margin-top', '2.5cm')
            ->setOption('margin-right', '1cm')
            ->setOption('margin-bottom', '1cm')
            ->setOption('margin-left', '1cm')
            ->setOption('header-html', 'http://aktorzy.parkdzieje.pl/templates/header.html')
            ->setOption('header-spacing', 5);

        return $pdf->download('grupy.pdf');
    }
}
