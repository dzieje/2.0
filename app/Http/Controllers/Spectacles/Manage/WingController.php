<?php

namespace App\Http\Controllers\Spectacles\Manage;

use App\Http\Controllers\ExtendedController;
use App\Jobs\ExportNWingActorsXls;
use App\Models\Group;
use App\Models\NGroup;
use App\Models\NRole;
use App\Models\NScene;
use App\Models\NWing;
use App\Models\Role;
use App\Models\Scene;
use App\Models\Volunteer;
use App\Models\Wing;
use File;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Spectacles\Controller;
use Illuminate\Support\Facades\Storage;
use Response;

class WingController extends ExtendedController
{
    public function __construct(Request  $request)
    {
        parent::__construct($request);
        $this->middleware('ability:wings');
    }

    public function index()
    {
        $wings = Wing::orderBy('name')->paginate(100);
        return view('admin.spectacles.manage.wings.index', compact('wings'));
    }

	public function create()
	{
		return view('admin.spectacles.manage.wings.create');
	}

	public function store(Request $request)
	{
		Wing::create($request->all());
		flash('Kulisa została utworzona.');
		return json_encode(['code' => 0]);
	}

    public function export($wing_id)
    {
        $wing = Wing::with('roles.actors')->find($wing_id);
        return $this->dispatch(new ExportNWingActorsXls($wing));
    }

    public function stageManager()
    {
        $json = [];

        $wings = Wing::with(['roles' => function($query){
            $query->orderBy('time_start');
        }, 'roles.actors'])->get();


        foreach ($wings as $wing)
        {
            $scenes = Scene::orderBy('time_start')->pluck( 'name', 'time_start')->toArray();
            $scene_time = $this->array_key_first($scenes);

            foreach($wing->roles as $role)
            {
                if($scene_time && strtotime($scene_time)<=strtotime($role->time_start)) {
                    $json[] = [
                        'time' => $scene_time,
                        'scene' => $scenes[$scene_time],
                    ];

                    unset($scenes[$scene_time]);
                    if(count($scenes) > 0) {
                        $scene_time = $this->array_key_first($scenes);
                    }else{
                        $scene_time = null;
                    }
                }

                $actors = [];
                foreach($role->actors as $actor)
                {
                    $actors[] = $actor->name. ' ' . $actor->surname;
                }
                $json[] = [
                    'time' => $role->time_start,
                    'role' => $role->name,
                    'actors' => $actors,
                    'exit' => $wing->name
                ];
            }
        }

        $collection = collect($json);
        $sorted = $collection->sortBy('time');

        $fileName = time() . '_wings.json';
        Storage::put('jsons/'.$fileName,json_encode($sorted->values()->toArray()) );
        return Storage::download('jsons/'.$fileName);
    }

    private function array_key_first(array $arr) {
        foreach($arr as $key => $unused) {
            return $key;
        }
        return NULL;
    }
}
