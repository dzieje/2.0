<?php

namespace App\Http\Controllers\Spectacles\Manage;

use App\Http\Controllers\ExtendedController;
use App\Jobs\ExportRehearsalAttendances;
use App\Models\NScene;
use App\Models\NWing;
use App\Models\Rehearsal;
use App\Models\RehearsalPeriod;
use App\Models\RehearsalPeriodAttendance;
use App\Models\Scene;
use App\Models\Volunteer;
use App\Services\Generator\RehearsalOutline;
use App\Services\Generator\RehearsalsTimetableStats;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Spectacles\Controller;
use PHPExcel_Style_Fill;
use Session;

class TimetableController extends ExtendedController
{
    private $columns = ['D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' ,'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ'];

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->middleware('ability:timetables');
    }

    public function index()
    {
        $rehearsals = Rehearsal::latest('rehearsal')->paginate(100);

        return view('admin.spectacles.manage.timetables.index', compact('rehearsals'));
    }

    public function create()
    {
        return view('admin.spectacles.manage.timetables.create');
    }

    public function periodAdd()
    {
        $nb = rand(10000000, 99999999);
        return view('admin.spectacles.manage.timetables.add-period', compact('nb'));
    }

    public function sceneAdd(Request $request)
    {
        $scenes = Scene::orderBy('name')->pluck('name','id')->toArray();
        $nb = rand(10000000, 99999999);
        $period_nb = $request->get('nb');

        $scene = Scene::with('roles', 'roles.wing')->find(key($scenes));

        return view('admin.spectacles.manage.timetables.add-scene', compact('scenes', 'nb', 'period_nb', 'scene'));
    }

    public function rolesAdd(Request $request)
    {
        $scene_id = $request->get('scene_id');
        $nb = $request->get('nb');
        $scene = NScene::with('roles', 'roles.wing')->find($scene_id);

        return view('admin.spectacles.manage.timetables.scene-roles', compact('scene', 'nb'));
    }

    public function store(Request $request){
        $rehearsal = Rehearsal::create($request->all());

        foreach($request->get('date_from', []) as $period_unique => $date_from)
        {
            $period = RehearsalPeriod::create(['rehearsal_id' => $rehearsal->id, 'date_from' => $rehearsal->rehearsal->format('Y-m-d').' '.$date_from.':00', 'date_to' => $rehearsal->rehearsal->format('Y-m-d').' '.$request->get('date_to')[$period_unique].':00' ]);

            if($request->has('scenes') && isset($request->get('scenes')[$period_unique])) {
                foreach ($request->get('scenes')[$period_unique] as $scene_unique => $scene_id) {
                    $period->scenes()->attach($scene_id);

                    if ($request->has('roles') && isset($request->get('roles')[$scene_unique]) ){
                        $period->roles()->attach($request->get('roles')[$scene_unique]);
                    }
                }
            }
        }

        return redirect()->to(url('admin/n-manage/timetables'));
    }

    public function show($rehearsal_id){
        $rehearsal = Rehearsal::with(['periods' => function($query){
            $query->orderBy('date_from');
        }, 'periods.scenes', 'periods.roles', 'periods.roles.wing'])->find($rehearsal_id);

        return view('admin.spectacles.manage.timetables.show', compact('rehearsal'));
    }

    public function delete($rehearsal_id){
        $rehearsal = Rehearsal::find($rehearsal_id);
        return view('admin.spectacles.manage.timetables.delete', compact('rehearsal'));
    }

    public function remove($rehearsal_id)
    {
        $rehearsal = Rehearsal::find($rehearsal_id);
        $rehearsal->delete();

        flash('Próba została usunięta.');
        return json_encode(['code' => 0]);
    }

    public function edit($rehearsal_id)
    {
        $rehearsal = Rehearsal::with(['periods' => function($query){
            $query->orderBy('date_from');
        }, 'periods.scenes','periods.scenes.roles', 'periods.roles', 'periods.roles.wing'])->find($rehearsal_id);


        $scenes = NScene::orderBy('name')->pluck('name','id');

        return view('admin.spectacles.manage.timetables.edit', compact('rehearsal', 'scenes'));
    }

    public function update(Request $request, $rehearsal_id){
        $rehearsal = Rehearsal::find($rehearsal_id);
        $rehearsal->update($request->all());


        foreach ($rehearsal->periods as $period)
        {
            $period->scenes()->detach();
            $period->roles()->detach();
            $period->delete();
        }


        foreach($request->get('date_from', []) as $period_unique => $date_from)
        {
            $period = RehearsalPeriod::create(['rehearsal_id' => $rehearsal->id, 'date_from' => $rehearsal->rehearsal->format('Y-m-d').' '.$date_from.':00', 'date_to' => $rehearsal->rehearsal->format('Y-m-d').' '.$request->get('date_to')[$period_unique].':00' ]);

            if($request->has('scenes') && isset($request->get('scenes')[$period_unique])) {
                foreach ($request->get('scenes')[$period_unique] as $scene_unique => $scene_id) {
                    $period->scenes()->attach($scene_id);

                    if ($request->has('roles') && isset($request->get('roles')[$scene_unique]) ){
                        $period->roles()->attach($request->get('roles')[$scene_unique]);
                    }
                }
            }
        }

        return redirect()->to(url('admin/n-manage/timetables'));
    }

    public function generatePdf()
    {
        $generator = new RehearsalsTimetableStats('pdf');
        $generator->generate();
        Session::put('download.in.the.next.request', url('admin/n-manage/timetables/download-pdf' ));

        return redirect()->back();
    }

    public function downloadPdf()
    {
        $filename = 'Zestawienie prob.pdf';

        $file = storage_path('app') . '/' . $filename;
        $headers = array(
            'Content-Description' => 'File Transfer',
            'Content-Type' => File::mimeType($file),
        );
        return response()->download($file, $filename, $headers);
    }

    public function generateXls()
    {
        $generator = new RehearsalsTimetableStats('xls');
        $generator->generate();
        Session::put('download.in.the.next.request', url('admin/n-manage/timetables/download-xls' ));

        return redirect()->back();
    }

    public function downloadXls()
    {
        $filename = 'Zestawienie prob.xls';

        $file = storage_path('app') . '/' . $filename;
        $headers = array(
            'Content-Description' => 'File Transfer',
            'Content-Type' => File::mimeType($file),
        );
        return response()->download($file, $filename, $headers);
    }

    public function generateOutline($rehearsal_id)
    {
        $generator = new RehearsalOutline($rehearsal_id);
        $filename = $generator->generate();
        //return $filename;

        Session::put('download.in.the.next.request', url('admin/n-manage/timetables/download-generator', [$filename]));
        return redirect()->back();
    }

    public function downloadGenerator($filename)
    {
        $filename = $filename.'.pdf';

        $file = storage_path('app/generates') . '/' . $filename;
        $headers = array(
            'Content-Description' => 'File Transfer',
            'Content-Type' => File::mimeType($file),
        );
        return response()->download($file, $filename, $headers);
    }

    public function attendance(Request $request, $rehearsal_id)
    {
        $rehearsal = Rehearsal::with('periods', 'periods.roles', 'periods.roles.actors')->find($rehearsal_id);

        $actors = Volunteer::whereHas('roles', function ($query) use ($rehearsal) {
            $query->whereHas('periods', function ($query) use ($rehearsal) {
                $query->where('rehearsal_id', $rehearsal->id);
            });
        })->where(function($query) use($request){
            if($request->has('name') && $request->get('name') != ''){
                $query->where('name', 'like', '%'.$request->get('name').'%');
            }
            if($request->has('surname') && $request->get('surname') != ''){
                $query->where('surname', 'like', '%'.$request->get('surname').'%');
            }
            if($request->has('phone') && $request->get('phone') != ''){
                $query->where('phone', 'like', '%'.$request->get('phone').'%');
            }
            if($request->has('email') && $request->get('email') != ''){
                $query->where('email', 'like', '%'.$request->get('email').'%');
            }
            if($request->has('wing') && $request->get('wing') != '0')
            {
                $query->whereHas('roles', function ($query) use ($request){
                    $query->where('n_wing_id', $request->get('wing'));
                });
            }
        })->orderBy('surname')
        ->with(['groups', 'roles' => function($query){
            $query->orderBy('name');
        }, 'roles.periods', 'attendances' => function($query)use ($rehearsal) {
            $query->whereHas('period', function ($query) use ($rehearsal) {
                $query->where('rehearsal_id', $rehearsal->id);
            });
        }])->whereIn('if_actor', [1,2])->paginate(100);

        $attendances = [];
        foreach ($actors as $actor)
        {
            foreach ($actor->attendances as $attendance)
            {
                $attendances[$actor->id][$attendance->rehearsal_period_id] = $attendance->status;
            }
        }

        $wings = NWing::pluck('name', 'id');

        return view('admin.spectacles.manage.timetables.attendance', compact('rehearsal', 'actors', 'attendances', 'wings'));
    }

    public function setAttendance(Request $request)
    {
        $actor_id = $request->get('actor_id');
        $period_id = $request->get('period_id');
        $status = $request->get('status');

        $attendance = RehearsalPeriodAttendance::where('volunteer_id', $actor_id)->where('rehearsal_period_id', $period_id)->first();
        if(!$attendance)
        {
            RehearsalPeriodAttendance::create([
                 'volunteer_id' => $actor_id,
                'user_id' => auth()->user()->id,
                'rehearsal_period_id' => $period_id,
                'status' => $status,
            ]);
        }else{
            $attendance->update([
                'status' => $status,
                'user_id' => auth()->user()->id
            ]);
        }

        return response()->json(['status' => 'ok']);
    }

    public function exportAttendance(Request $request)
    {
        $rehearsal = Rehearsal::with('periods', 'periods.roles', 'periods.roles.actors')->find($request->get('rehearsal_id'));

        $filename = 'Zestawienie obecnosci '.$rehearsal->rehearsal->format('d-m');
        \Excel::create($filename, function($excel) use($request, $rehearsal){

            $wings = NWing::get();
            foreach($wings as $wing)
            {
                $excel->sheet('kulisa '.$wing->name, function ($sheet) use($wing, $rehearsal){
                    $thead = [];
                    $thead[] = 'nazwisko i imię';
                    $thead[] = 'grupy';
                    $thead[] = 'telefon';

                    foreach ($rehearsal->periods as $lp => $period){
                        $thead[] = $period->date_from->format('H:i').'-'.$period->date_to->format('H:i');
                        $sheet->getStyle($this->columns[$lp].'1')->getAlignment()->setWrapText(true);
                        $sheet->getColumnDimension($this->columns[$lp])
                            ->setAutoSize(false);
                        $sheet->getColumnDimension($this->columns[$lp])
                            ->setWidth(6);
                    }
                    $sheet->appendRow($thead);

                    $actors = Volunteer::whereHas('roles', function ($query) use ($rehearsal, $wing) {
                        $query->whereHas('periods', function ($query) use ($rehearsal) {
                            $query->whereHas('rehearsal', function ($query) use ($rehearsal) {
                                $query->where('id', $rehearsal->id);
                            });
                        });
                        $query->where('n_wing_id', $wing->id);
                    })->whereHas('groups', function($query) {
                        $query->where('if_children', 0);

                    })->whereIn('if_actor', [1,2])->orderBy('surname')->with('groups', 'roles', 'roles.periods', 'attendances')->get();

                    foreach ($actors as $lp => $actor) {
                        $attendances = [];
                        foreach ($actor->attendances as $attendance)
                        {
                            $attendances[$attendance->rehearsal_period_id] = $attendance->status;
                        }

                        $row = [];
                        $row[] = $actor->surname.' '.$actor->name;
                        $row[] = implode(', ', $actor->groups->pluck('name')->toArray());
                        $row[] = $actor->phone;
                        $attendance = [];
                        foreach ($rehearsal->periods as $period_lp => $rehearsal_period){
                            $if_period = false;
                            foreach ($actor->roles as $role)
                            {
                                foreach ($role->periods as $period)
                                {
                                    if($period->id == $rehearsal_period->id)
                                    {
                                        $if_period = true;
                                    }
                                }
                            }
                            if($if_period ){
                                if(isset($attendances[$rehearsal_period->id]))
                                {
                                    if($attendances[$rehearsal_period->id] == 1) {
                                        $row[] = 'tak';
                                    }else{
                                        $row[] = 'nie';
                                    }
                                }else {
                                    $row[] = '?';
                                }
                                $attendance[] = $this->columns[$period_lp];
                            }else{
                                $row[] = '';
                            }
                        }

                        $sheet->appendRow($row);
                        $lp++;
                        foreach($attendance as $column)
                        {
                            $sheet->getStyle($column.($lp+1))->applyFromArray(
                                array(
                                    'fill' => array(
                                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                        'color' => array('rgb' => 'c5c5c5')
                                    )
                                )
                            );
                        }
                    }
                });
            }

            $excel->sheet('dzieci', function ($sheet) use( $rehearsal){
                $thead = [];
                $thead[] = 'nazwisko i imię';
                $thead[] = 'grupy';
                $thead[] = 'telefon';

                foreach ($rehearsal->periods as $lp => $period){
                    $thead[] = $period->date_from->format('H:i').'-'.$period->date_to->format('H:i');
                    $sheet->getStyle($this->columns[$lp].'1')->getAlignment()->setWrapText(true);
                    $sheet->getColumnDimension($this->columns[$lp])
                        ->setAutoSize(false);
                    $sheet->getColumnDimension($this->columns[$lp])
                        ->setWidth(6);
                }
                $sheet->appendRow($thead);

                $actors = Volunteer::whereHas('roles', function ($query) use ($rehearsal) {
                    $query->whereHas('periods', function ($query) use ($rehearsal) {
                        $query->whereHas('rehearsal', function ($query) use ($rehearsal) {
                            $query->where('id', $rehearsal->id);
                        });
                    });
                })->whereHas('groups', function($query){
                    $query->where('if_children', 1);
                })->whereIn('if_actor', [1,2])->orderBy('surname')->with('groups', 'roles', 'roles.periods', 'attendances')->get();

                foreach ($actors as $lp => $actor) {
                    $attendances = [];
                    foreach ($actor->attendances as $attendance)
                    {
                        $attendances[$attendance->rehearsal_period_id] = $attendance->status;
                    }

                    $row = [];
                    $row[] = $actor->surname.' '.$actor->name;
                    $row[] = implode(', ', $actor->groups->pluck('name')->toArray());
                    $row[] = $actor->phone;

                    $attendance = [];
                    foreach ($rehearsal->periods as $period_lp => $rehearsal_period){
                        $if_period = false;
                        foreach ($actor->roles as $role)
                        {
                            foreach ($role->periods as $period)
                            {
                                if($period->id == $rehearsal_period->id)
                                {
                                    $if_period = true;
                                }
                            }
                        }
                        if($if_period ){
                            if(isset($attendances[$rehearsal_period->id]))
                            {
                                if($attendances[$rehearsal_period->id] == 1) {
                                    $row[] = 'tak';
                                }else{
                                    $row[] = 'nie';
                                }
                            }else {
                                $row[] = '?';
                            }
                            $attendance[] = $this->columns[$period_lp];
                        }else{
                            $row[] = '';
                        }
                    }

                    $sheet->appendRow($row);
                    $lp++;
                    foreach($attendance as $column)
                    {
                        $sheet->getStyle($column.($lp+1))->applyFromArray(
                            array(
                                'fill' => array(
                                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                    'color' => array('rgb' => 'c5c5c5')
                                )
                            )
                        );
                    }
                }
            });

        })->store('xlsx', storage_path('excel'));

        Session::put('download.in.the.next.request', url('admin/n-manage/timetables/download-doc', array($filename) ));
        return response()->json([
            'code'  => 0
        ]);

    }

    public function downloadDoc($filename)
    {
        $filename = $filename.'.xlsx'; //

        $file = storage_path('excel') . '/' . $filename;
        $headers = array(
            'Content-Description' => 'File Transfer',
            'Content-Type' => File::mimeType($file),
        );
        return response()->download($file, $filename, $headers);
    }

    public function attendanceXls()
    {
        set_time_limit(360);
        return $this->dispatch(new ExportRehearsalAttendances());
    }
}
