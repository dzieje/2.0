<?php

namespace App\Http\Controllers\Spectacles\Manage;

use App\Http\Controllers\ExtendedController;
use App\Jobs\ExportEmailsXls;
use App\Jobs\ExportPhonesCsv;
use App\Models\Scene;
use App\Models\Wing;
use App\Services\Generator\SceneOutline;
use File;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Spectacles\Controller;
use Response;
use Session;

class SceneController extends ExtendedController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->middleware('ability:scenes');
    }

    public function index()
    {
        $scenes = Scene::project($this->project->id)->with('roles', 'roles.wing')->paginate(100);

        return view('admin.spectacles.manage.scenes.index', compact('scenes'));
    }

    public function create()
    {
        return view('admin.spectacles.manage.scenes.create');
    }

    public function store(Request $request)
    {
        $request->request->add(['project_id' => $this->project->id]);
        Scene::create($request->all());
        flash('Scena została utworzona.');
        return json_encode(['code' => 0]);
    }

    public function edit(Request $request )
    {
        $scene = Scene::find($request->route('scene_id'));
        return view('admin.spectacles.manage.scenes.edit', compact('scene'));
    }

    public function update(Request $request )
    {
        $scene = Scene::find($request->route('scene_id'));
        $scene->update($request->all());

        flash('Scena została zaktualizowana.');
        return json_encode(['code' => 0]);
    }

    public function delete(Request $request )
    {
        $scene = Scene::find($request->route('scene_id'));

        return view('admin.spectacles.manage.scenes.delete', compact('scene'));
    }

    public function remove(Request $request )
    {
        $scene = Scene::find($request->route('scene_id'));
        $scene->delete();

        flash('Scena została usunięta.');
        return json_encode(['code' => 0]);
    }

    public function showRoles(Request $request )
    {
        $scene = Scene::with('roles', 'roles.actors', 'roles.wing')->find($request->route('scene_id'));
        return view('admin.spectacles.manage.scenes.show-roles', compact('scene'));
    }

    public function showWings(Request $request )
    {
        $scene = Scene::with('roles.wing')->find($request->route('scene_id'));
        $wings = $scene->roles->pluck('wing.id')->unique();
        $wings = Wing::whereIn('id', $wings)->get();

        return view('admin.spectacles.manage.scenes.show-wings', compact('scene', 'wings'));
    }

    public function exportPhones($scene_id)
    {
        $scene = Scene::with('roles', 'roles.actors')->find($scene_id);
        return $this->dispatch(new ExportPhonesCsv($scene));
    }

    public function exportEmails($scene_id)
    {
        $scene = Scene::with('roles', 'roles.actors')->find($scene_id);
        return $this->dispatch(new ExportEmailsXls($scene));
    }

    public function generateOutline($rehearsal_id, $file_type)
    {
        $generator = new SceneOutline($rehearsal_id);
        $filename = $generator->generate($file_type);
        //return $filename;

        Session::put('download.in.the.next.request', url('admin/n-manage/scenes/download-generator', [$filename, $file_type]));
        return redirect()->back();
    }

    public function getDownloadGenerator($filename, $file_type = 'pdf')
    {
        $filename = $filename.'.'.$file_type;

        $file = storage_path('app/generates') . '/' . $filename;
        $headers = array(
            'Content-Description' => 'File Transfer',
            'Content-Type' => File::mimeType($file),
        );
        return response()->download($file, $filename, $headers);
    }

    public function postStageManager()
    {
        $json = [];

        $scenes = Scene::orderBy('time_start')->get();

        foreach ($scenes as $scene)
        {
            $json['sceny'][] = [
                'time' => $scene->time_start,
                'scene' => $scene->name,
            ];
        }

        $fileName = time() . '_scenes.json';
        File::put(storage_path('jsons/'.$fileName),json_encode($json) );
        return Response::download(storage_path('jsons/'.$fileName));
    }
}
