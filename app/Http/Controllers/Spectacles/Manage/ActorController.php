<?php

namespace App\Http\Controllers\Spectacles\Manage;

use App\Http\Controllers\ExtendedController;
use App\Jobs\ExportEmailsActorsXls;
use App\Jobs\ExportPhonesActorsCsv;
use App\Jobs\GenerateActorRehearsalsTimetable;
use App\Models\Group;
use App\Models\Role;
use App\Models\Volunteer;
use App\Models\VolunteerAttribute;
use App\Services\Generator\ActorRehearsalsTimetable;
use App\Services\Generator\ActorRoles;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class ActorController extends ExtendedController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->middleware('ability:actors');
    }

    public function index(Request $request)
    {
        $query = Volunteer::project($this->project->id);

        if($request->has('name') && $request->get('name') != ''){
            $query->where('name', 'like', '%'.$request->get('name').'%');
        }
        if($request->has('surname') && $request->get('surname') != ''){
            $query->where('surname', 'like', '%'.$request->get('surname').'%');
        }
        if($request->has('gender') && $request->get('gender') != '0')
        {
            $query->where('gender', $request->get('gender'));
        }

        if($request->has('age_from') && $request->get('age_from') != ''){
            $birth = Carbon::now()->subYears($request->get('age_from'))->format('Y-m-d');

            $query->where('date_of_birth', '<=', $birth);
        }

        if($request->has('age_to') && $request->get('age_to') != ''){
            $birth = Carbon::now()->subYears($request->get('age_to'))->subYear()->addDay()->format('Y-m-d');

            $query->where('date_of_birth', '>=', $birth);
        }

        if($request->has('group') && $request->get('group') != 0){
            if($request->get('group') == '-1')
            {
                $query->whereDoesntHave('groups');
            }else {
                $group_id = $request->get('group');
                $query->whereHas('groups', function ($query) use($group_id){
                    $query->where('n_group_id', $group_id);
                });
            }
        }

        if($request->has('role') && $request->get('role') != 0){
            $role_id = $request->get('role');
            $query->whereHas('roles', function ($query) use($role_id){
                $query->where('n_role_id', $role_id);
            });
        }

        $actors = $query->with('groups', 'roles' )->orderBy('surname')->paginate(100);

        $groups = Group::orderBy('name')->pluck('name', 'id');
        $roles = Role::orderBy('name')->pluck('name',   'id');

        $attributes = VolunteerAttribute::whereHas('projects', function ($query){
            $query->where('project_id', $this->project->id);
        })->get();

        return view('admin.spectacles.manage.actors.index', compact('actors', 'groups', 'roles', 'attributes'));
    }

    public function postExport(Request $request)
    {
        $filename = 'Zestawienie aktorow';

        \Excel::create($filename, function($excel) use($request){
            $excel->sheet('zestawienie', function($sheet) use($request) {

                $sheet->appendRow(array(
                    'nazwisko i imię',
                    'wiek',
                    'telefon',
                    'email',
                    'wzrost'
                ));

                $volunteers = Volunteer::where('if_registered', 1)->whereIn('if_actor', [1,2])
                    ->where(function($query) use($request){
                        if($request->has('name') && $request->get('name') != ''){
                            $query->where('name', 'like', '%'.$request->get('name').'%');
                        }
                        if($request->has('surname') && $request->get('surname') != ''){
                            $query->where('surname', 'like', '%'.$request->get('surname').'%');
                        }

                        if($request->has('gender') && $request->get('gender') != '0')
                        {
                            $query->where('gender', $request->get('gender'));
                        }

                        if($request->has('age_from') && $request->get('age_from') != ''){
                            $birth = Carbon::now()->subYears($request->get('age_from'))->format('Y-m-d');

                            $query->where('date_of_birth', '<=', $birth);
                        }

                        if($request->has('age_to') && $request->get('age_to') != ''){
                            $birth = Carbon::now()->subYears($request->get('age_to'))->subYear()->addDay()->format('Y-m-d');

                            $query->where('date_of_birth', '>=', $birth);
                        }

                        if($request->has('group') && $request->get('group') != 0){
                            if($request->get('group') == '-1')
                            {
                                $query->whereDoesntHave('groups');
                            }else {
                                $group_id = $request->get('group');
                                $query->whereHas('groups', function ($query) use($group_id){
                                    $query->where('n_group_id', $group_id);
                                });
                            }
                        }

                        if($request->has('role') && $request->get('role') != 0){
                            $role_id = $request->get('role');
                            $query->whereHas('roles', function ($query) use($role_id){
                                $query->where('n_role_id', $role_id);
                            });
                        }

                    })
                    ->orderBy('surname')->with( 'guardian')->get();

                foreach ($volunteers as $volunteer)
                {
                    $age = $volunteer->date_of_birth->diffInYears(Carbon::now());
                    $sheet->appendRow(array(
                        $volunteer->surname.' '.$volunteer->name,
                        $age,
                        str_replace(' ', '', ltrim($volunteer->phone) ),
                        $volunteer->email,
                        $volunteer->height
                    ));
                }

            });
        })->store('xlsx', storage_path('excel'));

        Session::put('download.in.the.next.request', url('admin/volunteers/download-doc', array($filename) ));
        return response()->json([
            'code'  => 0
        ]);
    }

    public function show(Request $request)
    {
        $actor = Volunteer::with(['roles' => function($query){
            $query->orderBy('scene_id');
        }, 'roles.scene', 'roles.props', 'roles.costumes'])->find($request->route('actor_id'));

        return view('admin.spectacles.manage.actors.show', compact('actor'));
    }

    public function edit(Request $request)
    {
        $actor = Volunteer::find($request->route('actor_id'));

        return view('admin.spectacles.manage.actors.edit', compact('actor'));
    }

    public function getWingInfo(Request $request)
    {
        $group = NGroup::find($request->get('group_id'));

        return $group->wing->name;
    }

    public function getSceneAdd()
    {
        $scenes = NScene::pluck('name', 'id');
        $scenes[0] = '--- wybierz scenę ---';

        return view('n-manage.actors.scene-add', compact('scenes'));
    }

    public function getGroupAdd()
    {
        $groups = NGroup::pluck('name', 'id');
        $groups[0] = '--- wybierz grupę ---';

        return view('n-manage.actors.group-add', compact('groups'));
    }

    public function getSceneLoadRoles(Request $request)
    {
        if($request->get('scene_id') == 0)
            return '';

        $scene = NScene::find($request->get('scene_id'));
        $roles = $scene->roles()->orderBy('name')->pluck('name', 'id');
        $roles[0] = '--- wybierz rolę ---';
        return view('n-manage.actors.scene-load-roles', compact('roles'));
    }

    public function getSceneLoadRoleInfo(Request $request)
    {
        if($request->get('role_id') == 0)
            return '';

        $role = NRole::with('props', 'costumes')->find($request->get('role_id'));
        return view('n-manage.actors.scene-load-role-info', compact('role'));
    }

    public function update(Request $request)
    {
        $actor = Volunteer::find($request->route('actor_id'));
        $actor->update($request->all());

        return redirect()->to(url('admin/spectacle/'.$this->project->slug.'/manage/actors'));
    }

    public function delete(Request $request)
    {
        $actor = Volunteer::find($request->route('actor_id'));

        return view('admin.spectacles.manage.actors.delete', compact('actor'));
    }

    public function remove(Request $request){
        $actor = Volunteer::find($request->route('actor_id'));
        $actor->projects()->detach($this->project->id);

        flash('Aktor '.$actor->surname.' '.$actor->name.' został odpięty.');
        return json_encode(['code' => 0]);
    }

    public function showRoles(Request $request)
    {
        $actor = Volunteer::with(['roles' => function($query){
            $query->orderBy('scene_id');
        }, 'roles.scene'])->find($request->route('actor_id'));
        return view('admin.spectacles.manage.actors.show-roles', compact('actor'));
    }

    public function showGroups(Request $request)
    {
        $actor = Volunteer::with(['groups' => function($query){
            $query->orderBy('group_id');
        }, 'groups'])->find($request->route('actor_id'));
        return view('admin.spectacles.manage.actors.show-groups', compact('actor'));
    }

    public function getExportPhones(Request $request)
    {
        $actors = Volunteer::whereIn('if_actor', [1,2])->where('if_registered', 1)->where(function($query) use($request){
            if($request->has('name') && $request->get('name') != ''){
                $query->where('name', 'like', '%'.$request->get('name').'%');
            }
            if($request->has('surname') && $request->get('surname') != ''){
                $query->where('surname', 'like', '%'.$request->get('surname').'%');
            }

            if($request->has('group') && $request->get('group') != 0){
                if($request->get('group') == '-1')
                {
                    $query->whereNull('n_group_id');
                }else {
                    $group_id = $request->get('group');
                    $query->where('n_group_id', $group_id);
                }
            }

            if($request->has('role') && $request->get('role') != 0){
                $role_id = $request->get('role');
                $query->whereHas('roles', function ($query) use($role_id){
                    $query->where('n_role_id', $role_id);
                });
            }

        })->get();
        return $this->dispatch(new ExportPhonesActorsCsv($actors));
    }

    public function getExportEmails(Request $request)
    {
        $actors = Volunteer::whereIn('if_actor', [1,2])->where('if_registered', 1)->where(function($query) use($request){
            if($request->has('name') && $request->get('name') != ''){
                $query->where('name', 'like', '%'.$request->get('name').'%');
            }
            if($request->has('surname') && $request->get('surname') != ''){
                $query->where('surname', 'like', '%'.$request->get('surname').'%');
            }

            if($request->has('group') && $request->get('group') != 0){
                if($request->get('group') == '-1')
                {
                    $query->whereNull('n_group_id');
                }else {
                    $group_id = $request->get('group');
                    $query->where('n_group_id', $group_id);
                }
            }

            if($request->has('role') && $request->get('role') != 0){
                $role_id = $request->get('role');
                $query->whereHas('roles', function ($query) use($role_id){
                    $query->where('n_role_id', $role_id);
                });
            }

        })->get();
        return $this->dispatch(new ExportEmailsActorsXls($actors));
    }

    public function getEditGroup($actor_id)
    {
        $actor = Volunteer::with('group')->find($actor_id);
        $groups = NGroup::orderBy('name')->pluck('name', 'id');
        $groups[''] = '--- wybierz grupę ---';
        return view('n-manage.actors.edit-group', compact('actor', 'groups'));
    }

    public function postUpdateGroup(Request $request, $actor_id)
    {
        $actor = Volunteer::find($actor_id);
        $group_id = $request->get('group_id');
        if($group_id == '') $group_id = null;

        if($group_id != $actor->n_group_id)
        {
            if($actor->group)
            {
                $group_roles = $actor->group->roles->pluck('id')->toArray();
                $actor->roles()->detach($group_roles);
            }
            $group = Group::find($group_id);
            if($group) {
                $group_roles = $group->roles->pluck('id')->toArray();
                $actor->roles()->attach($group_roles);
            }
        }

        $actor->update(['n_group_id' => $group_id]);

        flash('Grupa '.$actor->surname.' '.$actor->name.' została zaktualizowana.');
        return json_encode(['code' => 0]);
    }

    public function getEditRoles($actor_id)
    {
        $actor = Volunteer::with('roles')->find($actor_id);
        $roles = Role::orderBy('name')->pluck('name', 'id');
        return view('n-manage.actors.edit-roles', compact('actor', 'roles'));
    }

    public function postUpdateRoles(Request $request, $actor_id)
    {
        $actor = Volunteer::find($actor_id);
        $actor->roles()->sync($request->get('role_id', []));

        flash('Role '.$actor->surname.' '.$actor->name.' zostały zaktualizowane.');
        return json_encode(['code' => 0]);
    }

    public function getGenerateRehearsalsTimetable($actor_id)
    {
        $generator = new ActorRehearsalsTimetable($actor_id);
        $filename = $generator->generate();
        //return $filename;
        Session::put('download.in.the.next.request', url('admin/n-manage/actors/download-timetable', array($filename) ));

        return redirect()->back();
    }

    public function getDownloadTimetable($filename)
    {
        $filename = $filename.'.pdf';

        $file = storage_path('timetables') . '/' . $filename;
        $headers = array(
            'Content-Description' => 'File Transfer',
            'Content-Type' => File::mimeType($file),
        );
        return response()->download($file, $filename, $headers);
    }

    public function getGenerateRoles($actor_id)
    {
        $generator = new ActorRoles($actor_id);
        $filename = $generator->generate();
        Session::put('download.in.the.next.request', url('admin/n-manage/actors/download-roles', array($filename) ));

        return redirect()->back();
    }

    public function getDownloadRoles($filename)
    {
        $filename = $filename.'.pdf';

        $file = storage_path('roles') . '/' . $filename;
        $headers = array(
            'Content-Description' => 'File Transfer',
            'Content-Type' => File::mimeType($file),
        );
        return response()->download($file, $filename, $headers);
    }

    public function getConnect($actor_id){
        $actor = Volunteer::find($actor_id);

        return view('n-manage.actors.connect', compact('actor'));
    }

    public function getSearchLojalni(Request $request)
    {
        $lojalni = new Lojalni();

        $response= $lojalni->searchUsers($request->get('attribute'), $request->get('value'));

        $accounts = $response['accounts'];

        return view('n-manage.actors.lojalni-list', compact('accounts'));
    }

    public function getLojalniAvatar($user_identifier)
    {
        $lojalni = new Lojalni();

        return $lojalni->getImageSrc($user_identifier);
    }

    public function postUpdateConnection(Request $request, $actor_id)
    {
        $actor = Volunteer::find($actor_id);
        $lojalni = new Lojalni();

        $actor->update([
            'identifier' => $request->get('identifier'),
            'avatar_filename' => $lojalni->getImage($request->get('identifier'))
        ]);

        return redirect()->to('/admin/n-manage/actors');
    }
}
