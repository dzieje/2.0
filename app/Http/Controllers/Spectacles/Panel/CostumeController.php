<?php

namespace App\Http\Controllers\Spectacles\Panel;

use App\Http\Controllers\ExtendedController;
use App\Models\Costume;
use App\Models\Guardian;
use App\Models\Group;
use App\Models\Role;
use App\Models\Scene;
use App\Models\Wing;
use App\Models\Rehearsal;
use App\Models\RehearsalPeriodAttendance;
use App\Models\Volunteer;
use App\Services\Generator\ActorRehearsalsTimetable;
use App\Services\Generator\ActorRoles;
use App\Services\Generator\RehearsalKidsGuardianOutline;
use App\Services\Generator\RehearsalKidsList;
use App\Services\Generator\RehearsalKidsOutline;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;

use PHPExcel_Style_Border;
use Session;

class CostumeController extends ExtendedController
{
    public function __construct(Request  $request)
    {
        parent::__construct($request);
        $this->middleware('ability:costumes_panel');
    }

    public function index(Request $request)
    {
        $roles = Role::project($this->project->id)->orderBy('name')->with('scene')->get();
        $scenes = Scene::project($this->project->id)->orderBy('name')->get();

        $costumes = Costume::where(function($query) use ($request){
            if($request->has('role') && $request->get('role') > 0){
                $query->where('role_id', $request->get('role'));
            }
            if($request->has('type') && $request->get('type') < 2){
                $query->where('type', $request->get('type'));
            }
            if($request->has('scene') && $request->get('scene') > 0){
                $query->whereHas('role', function($query) use($request){
                    $query->where('scene_id', $request->get('scene'));
                });
            }
        })->with('role', 'role.actors', 'role.scene')->orderBy('name')->paginate(100);

        return view('admin.spectacles.panel.costumes.index', compact('roles', 'scenes', 'costumes'));
    }

    public function export(Request $request)
    {
        $filename = 'Zestawienie kostiumow';
        \Excel::create($filename, function($excel) use($request) {

            $excel->sheet('Zestawienie', function($sheet) use($request)  {

                $sheet->appendRow(array(
                    'ilość',
                    'kostium',
                    'scena',
                    'rola',
                    'przygotowuje',
                    'aktorzy'
                ));

                $costumes = NCostume::where(function($query) use ($request){
                    if($request->has('role') && $request->get('role') > 0){
                        $query->where('n_role_id', $request->get('role'));
                    }
                    if($request->has('scene') && $request->get('scene') > 0){
                        $query->whereHas('role', function($query) use($request){
                            $query->where('n_scene_id', $request->get('scene'));
                        });
                    }
                    if($request->has('type') && $request->get('type') < 2){
                        $query->where('type', $request->get('type'));
                    }
                })->with('role', 'role.actors', 'role.scene')->orderBy('name')->get();

                foreach ($costumes as $costume)
                {
                    $sheet->appendRow(array(
                        ($costume->role) ? $costume->role->actors->count() : 0 ,
                        $costume->name ,
                        ($costume->role) ? $costume->role->scene->name : '',
                        ($costume->role) ? $costume->role->name : '',
                        ($costume->type == 0) ? 'dzieje' : 'statysta',
                        ($costume->role) ? implode(', ', $costume->role->actors->pluck('full_name')->toArray()) : '',
                    ));
                }
            });

        })->store('xlsx', storage_path('excel'));

        Session::put('download.in.the.next.request', url('admin/panel/costumes/download', array($filename) ));
        return response()->json([
            'code'  => 0
        ]);
    }

    public function download($filename)
    {
        $filename = $filename.'.xlsx'; //

        $file = storage_path('excel') . '/' . $filename;
        $headers = array(
            'Content-Description' => 'File Transfer',
            'Content-Type' => File::mimeType($file),
        );
        return response()->download($file, $filename, $headers);
    }
}
