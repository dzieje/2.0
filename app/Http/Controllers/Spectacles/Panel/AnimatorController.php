<?php

namespace App\Http\Controllers\Spectacles\Panel;

use App\Models\Guardian;
use App\Models\NGroup;
use App\Models\NWing;
use App\Models\Rehearsal;
use App\Models\RehearsalPeriodAttendance;
use App\Models\Volunteer;
use App\Services\Generator\ActorRehearsalsTimetable;
use App\Services\Generator\ActorRoles;
use App\Services\Generator\RehearsalKidsGuardianOutline;
use App\Services\Generator\RehearsalKidsList;
use App\Services\Generator\RehearsalKidsOutline;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Spectacles\Controller;
use PHPExcel_Style_Border;
use Session;

class AnimatorController extends Controller
{
    public function __construct()
    {
        $this->middleware('ability:animators');
    }

    public function getIndex(Request $request)
    {
        $birth = Carbon::now()->subYears(13)->format('Y-m-d');

        $actors = Volunteer::whereIn('if_actor', [1,2])->where('if_registered', 1)->where(function($query) use($request){
            if($request->has('name') && $request->get('name') != ''){
                $query->where('name', 'like', '%'.$request->get('name').'%');
            }
            if($request->has('surname') && $request->get('surname') != ''){
                $query->where('surname', 'like', '%'.$request->get('surname').'%');
            }
            if($request->has('group') && $request->get('group') != 0){
                if($request->get('group') == '-1')
                {
                    $query->has('groups', '<', 1);
                }else {
                    $group_id = $request->get('group');
                    $query->whereHas('groups', function($query) use( $group_id){
                        $query->where('n_group_id', $group_id);
                    });
                }
            }
            if($request->has('guardian_permission') && $request->get('guardian_permission') != 0){
                $query->whereHas('guardian', function($query) use($request){
                    $query->where('guardian_permission', $request->get('guardian_permission'));
                });
            }
            if($request->has('children') && $request->get('children') != '-1')
            {
                $query->whereHas('group', function($query) use($request){
                    $query->where('if_children', $request->get('children'));
                });
            }
            if($request->has('rehearsal') && $request->get('rehearsal') != 0)
            {
                $query->whereHas('roles', function($query) use($request){
                    $query->whereHas('periods', function ($query) use($request){
                        $query->where('rehearsal_id', $request->get('rehearsal'));
                    });
                });
            }
        })->where('date_of_birth', '>=', $birth)->has('guardian')->with('groups', 'roles' )->orderBy('surname')->paginate(100);


        $groups = NGroup::orderBy('name')->pluck('name', 'id');
        $rehearsals = Rehearsal::orderBy('rehearsal')->pluck('name', 'id');

        return view('panel.animators.index', compact('actors', 'groups', 'rehearsals'));
    }

    public function postExport(Request $request)
    {
        $filename = 'Zestawienie dzieci';
        \Excel::create($filename, function($excel) use($request) {

            $excel->sheet('Zestawienie', function($sheet) use($request)  {

                $sheet->appendRow(array(
                    'nazwisko i imię',
                    'wiek',
                    'telefon',
                    'email',
                    'grupy',
                    'opiekun imię',
                    'opiekun nazwisko',
                    'opiekun telefon',
                    'opiekun email',
                    'zgoda'
                ));
                $birth = Carbon::now()->subYears(13)->format('Y-m-d');
                $volunteers = Volunteer::where('if_registered', 1)
                    ->where('date_of_birth', '>=', $birth)
                    ->where(function($query) use($request){
                        if($request->has('name') && $request->get('name') != ''){
                            $query->where('name', 'like', '%'.$request->get('name').'%');
                        }
                        if($request->has('surname') && $request->get('surname') != ''){
                            $query->where('surname', 'like', '%'.$request->get('surname').'%');
                        }
                        if($request->has('group') && $request->get('group') != 0){
                            if($request->get('group') == '-1')
                            {
                                $query->has('groups', '<', 1);
                            }else {
                                $group_id = $request->get('group');
                                $query->whereHas('groups', function($query) use( $group_id){
                                    $query->where('n_group_id', $group_id);
                                });
                            }
                        }
                        if($request->has('guardian_permission') && $request->get('guardian_permission') != 0){
                            $query->whereHas('guardian', function($query) use($request){
                                $query->where('guardian_permission', $request->get('guardian_permission'));
                            });
                        }
                        if($request->has('children') && $request->get('children') != '-1')
                        {
                            $query->whereHas('group', function($query) use($request){
                                $query->where('if_children', $request->get('children'));
                            });
                        }
                        if($request->has('rehearsal') && $request->get('rehearsal') != 0)
                        {
                            $query->whereHas('roles', function($query) use($request){
                                $query->whereHas('periods', function ($query) use($request){
                                    $query->where('rehearsal_id', $request->get('rehearsal'));
                                });
                            });
                        }
                    })
                    ->has('guardian')
                    ->whereIn('if_actor', [1,2])
                    ->with('groups', 'guardian')
                    ->orderBy('surname')->get();

                $lp = 1;
                foreach ($volunteers as $volunteer)
                {
                    if($volunteer->group && $volunteer->group->if_children == 1) {
                        $lp++;
                        $sheet->appendRow(array(
                            $volunteer->surname.' '.$volunteer->name,
                            $volunteer->date_of_birth->diffInYears(Carbon::now()),
                            $volunteer->phone,
                            $volunteer->email,
                            ($volunteer->groups->count() > 0) ? implode(', ', $volunteer->groups->pluck('name')->toArray()) : '',
                            $volunteer->guardian->name,
                            $volunteer->guardian->surname,
                            $volunteer->guardian->phone,
                            $volunteer->guardian->email,
                            ($volunteer->guardian->guardian_permission == 1) ? 'odbiór' : 'sam'
                        ));
                    }
                }

                $sheet->getStyle('A'.$lp.':L'.$lp)->getBorders()->getBottom()->applyFromArray(
                    [
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => [
                            'rgb' => '#00000'
                        ]
                    ]
                );

                foreach ($volunteers as $volunteer)
                {
                    if(! $volunteer->group || $volunteer->group->if_children == 0) {
                        $sheet->appendRow(array(
                            $volunteer->surname.' '.$volunteer->name,
                            $volunteer->date_of_birth->diffInYears(Carbon::now()),
                            $volunteer->phone,
                            $volunteer->email,
                            ($volunteer->groups->count() > 0) ?implode(', ', $volunteer->groups->pluck('name')->toArray()) : '',
                            $volunteer->guardian->name,
                            $volunteer->guardian->surname,
                            $volunteer->guardian->phone,
                            $volunteer->guardian->email,
                            ($volunteer->guardian->guardian_permission == 1) ? 'odbiór' : 'sam'
                        ));
                    }
                }

            });

        })->store('xlsx', storage_path('excel'));

        Session::put('download.in.the.next.request', url('admin/panel/animators/download-doc', array($filename) ));
        return response()->json([
            'code'  => 0
        ]);
    }

    public function getDownload($filename)
    {
        $filename = $filename.'.xlsx'; //

        $file = storage_path('excel') . '/' . $filename;
        $headers = array(
            'Content-Description' => 'File Transfer',
            'Content-Type' => File::mimeType($file),
        );
        return response()->download($file, $filename, $headers);
    }

    public function getDownloadDoc($filename)
    {
        $filename = $filename.'.xlsx'; //

        $file = storage_path('excel') . '/' . $filename;
        $headers = array(
            'Content-Description' => 'File Transfer',
            'Content-Type' => File::mimeType($file),
        );
        return response()->download($file, $filename, $headers);
    }

    public function getGenerateRehearsalsTimetable($actor_id)
    {
        $generator = new ActorRehearsalsTimetable($actor_id);
        $filename = $generator->generate();
        //return $filename;
        Session::put('download.in.the.next.request', url('admin/panel/animators/download-timetable', array($filename) ));

        return redirect()->back();
    }

    public function getDownloadTimetable($filename)
    {
        $filename = $filename.'.pdf';

        $file = storage_path('timetables') . '/' . $filename;
        $headers = array(
            'Content-Description' => 'File Transfer',
            'Content-Type' => File::mimeType($file),
        );
        return response()->download($file, $filename, $headers);
    }

    public function getGenerateRoles($actor_id)
    {
        $generator = new ActorRoles($actor_id);
        $filename = $generator->generate();
        Session::put('download.in.the.next.request', url('admin/panel/animators/download-roles', array($filename) ));

        return redirect()->back();
    }

    public function getDownloadRoles($filename)
    {
        $filename = $filename.'.pdf';

        $file = storage_path('roles') . '/' . $filename;
        $headers = array(
            'Content-Description' => 'File Transfer',
            'Content-Type' => File::mimeType($file),
        );
        return response()->download($file, $filename, $headers);
    }

    public function getShow($actor_id)
    {
        $actor = Volunteer::with(['roles' => function($query){
            $query->orderBy('n_scene_id');
        }, 'roles.scene', 'roles.props', 'roles.costumes'])->whereIn('if_actor', [1,2])->find($actor_id);

        return view('panel.animators.show', compact('actor'));
    }

    public function getShowRoles($actor_id)
    {
        $actor = Volunteer::with(['roles' => function($query){
            $query->orderBy('n_scene_id');
        }, 'roles.scene'])->whereIn('if_actor', [1,2])->find($actor_id);
        return view('panel.animators.show-roles', compact('actor'));
    }

    public function getGuardian($guardian_id)
    {
        $guardian = Guardian::find($guardian_id);

        return view('volunteers.guardian', compact('guardian'));
    }

    public function getTimetables()
    {
        $rehearsals = Rehearsal::whereHas('periods', function($query){
            $query->whereHas('roles', function($query){
                $query->whereHas('actors', function($query){
                    $birth = Carbon::now()->subYears(18)->format('Y-m-d');
                    $query->where('date_of_birth', '>=', $birth)->has('guardian');
                });
            });
        })->latest('rehearsal')->paginate(100);

        return view('panel.animators.timetables', compact('rehearsals'));
    }

    public function getDownloadGenerator($filename)
    {
        $filename = $filename.'.pdf';

        $file = storage_path('app/generates') . '/' . $filename;
        $headers = array(
            'Content-Description' => 'File Transfer',
            'Content-Type' => File::mimeType($file),
        );
        return response()->download($file, $filename, $headers);
    }

    public function getGenerateOutline($rehearsal_id)
    {
        $generator = new RehearsalKidsOutline($rehearsal_id);
        $filename = $generator->generate();
        //return $filename;

        Session::put('download.in.the.next.request', url('admin/panel/animators/download-generator', [$filename]));
        return redirect()->back();
    }

    public function getGenerateOutlineGuardian($rehearsal_id, $guardian_permission_id)
    {
        $generator = new RehearsalKidsGuardianOutline($rehearsal_id, $guardian_permission_id);
        $filename = $generator->generate();
        //return $filename;

        Session::put('download.in.the.next.request', url('admin/panel/animators/download-generator', [$filename]));
        return redirect()->back();
    }

    public function getGenerateList($rehearsal_id, $guardian_permission_id)
    {
        $generator = new RehearsalKidsList($rehearsal_id, $guardian_permission_id);
        $filename = $generator->generate();
        //return $filename;

        Session::put('download.in.the.next.request', url('admin/panel/animators/download-doc', array($filename) ));
        return redirect()->back();
    }

    public function getAttendance(Request $request, $rehearsal_id)
    {
        $birth = Carbon::now()->subYears(18)->format('Y-m-d');
        $rehearsal = Rehearsal::with(['periods' => function($query){
                                    $query->whereHas('roles', function($query){
                                        $query->whereHas('actors', function($query){
                                            $birth = Carbon::now()->subYears(18)->format('Y-m-d');
                                            $query->where('date_of_birth', '>=', $birth)->has('guardian')->whereHas('group', function ($query){
                                                $query->where('if_children', 1);
                                            });
                                        });
                                    });
                                }, 'periods.scenes' => function($query){
                                    $query->whereHas('roles', function($query){
                                        $query->whereHas('actors', function($query){
                                            $birth = Carbon::now()->subYears(18)->format('Y-m-d');
                                            $query->where('date_of_birth', '>=', $birth)->has('guardian')->whereHas('group', function ($query){
                                                $query->where('if_children', 1);
                                            });
                                        });
                                    });
                                }, 'periods.roles', 'periods.roles.actors'])->find($rehearsal_id);

        $actors = Volunteer::whereHas('roles', function ($query) use ($rehearsal) {
                $query->whereHas('periods', function ($query) use ($rehearsal) {
                    $query->where('rehearsal_id', $rehearsal->id);
                });
            })
            ->whereIn('if_actor', [1,2])
            ->whereHas('group', function ($query){
                $query->where('if_children', 1);
            })
            ->where(function($query) use($request){
                if($request->has('name') && $request->get('name') != ''){
                    $query->where('name', 'like', '%'.$request->get('name').'%');
                }
                if($request->has('surname') && $request->get('surname') != ''){
                    $query->where('surname', 'like', '%'.$request->get('surname').'%');
                }
                if($request->has('phone') && $request->get('phone') != ''){
                    $query->where('phone', 'like', '%'.$request->get('phone').'%');
                }
                if($request->has('email') && $request->get('email') != ''){
                    $query->where('email', 'like', '%'.$request->get('email').'%');
                }
                if($request->has('wing') && $request->get('wing') != '0')
                {
                    $query->whereHas('group', function ($query) use ($request){
                        $query->where('n_wing_id', $request->get('wing'));
                    });
                }
                if($request->has('group') && $request->get('group') != '0')
                {
                    $query->whereHas('group', function ($query) use ($request){
                        $query->where('id', $request->get('group'));
                    });
                }
            })->where('date_of_birth', '>=', $birth)->has('guardian')->orderBy('surname')
            ->with(['group', 'roles' => function($query){
                $query->orderBy('name');
            }, 'roles.periods', 'attendances' => function($query)use ($rehearsal) {
                $query->whereHas('period', function ($query) use ($rehearsal) {
                    $query->where('rehearsal_id', $rehearsal->id);
                });
            }])->paginate(100);

        $attendances = [];
        foreach ($actors as $actor)
        {
            foreach ($actor->attendances as $attendance)
            {
                $attendances[$actor->id][$attendance->rehearsal_period_id] = $attendance->status;
            }
        }

        $wings = NWing::pluck('name', 'id');
        $groups = NGroup::where('if_children', 1)->pluck('name', 'id');

        return view('panel.animators.attendance', compact('rehearsal', 'actors', 'attendances', 'wings', 'groups'));
    }

    public function postSetAttendance(Request $request)
    {
        $actor_id = $request->get('actor_id');
        $period_id = $request->get('period_id');
        $status = $request->get('status');

        $attendance = RehearsalPeriodAttendance::where('volunteer_id', $actor_id)->where('rehearsal_period_id', $period_id)->first();
        if(!$attendance)
        {
            RehearsalPeriodAttendance::create([
                'volunteer_id' => $actor_id,
                'user_id' => auth()->user()->id,
                'rehearsal_period_id' => $period_id,
                'status' => $status,
            ]);
        }else{
            $attendance->update([
                'status' => $status,
                'user_id' => auth()->user()->id
            ]);
        }

        return response()->json(['status' => 'ok']);
    }
}
