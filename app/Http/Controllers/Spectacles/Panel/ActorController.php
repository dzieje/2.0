<?php

namespace App\Http\Controllers\Spectacles\Panel;

use App\Http\Controllers\ExtendedController;
use App\Models\Group;
use App\Models\Role;
use App\Models\Scene;
use App\Models\Volunteer;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ActorController extends ExtendedController
{
    public function __construct(Request  $request)
    {
        parent::__construct($request);
        $this->middleware('ability:actors_panel');
    }

    public function index(Request $request)
    {
        $actors = Volunteer::project($this->project->id)->where(function($query) use($request){
            if($request->has('name') && $request->get('name') != ''){
                $query->where('name', 'like', '%'.$request->get('name').'%');
            }
            if($request->has('surname') && $request->get('surname') != ''){
                $query->where('surname', 'like', '%'.$request->get('surname').'%');
            }

            if($request->has('group') && $request->get('group') != 0){
                if($request->get('group') == '-1')
                {
                    $query->whereDoesntHave('groups');
                }else {
                    $group_id = $request->get('group');
                    $query->whereHas('groups', function ($query) use($group_id){
                        $query->where('n_group_id', $group_id);
                    });
                }
            }

            if($request->has('gender') && $request->get('gender') != '0'){
                $query->where('gender', $request->get('gender'));
            }

            if($request->has('age_from') && $request->get('age_from') != ''){
                $birth = Carbon::now()->subYears($request->get('age_from'))->format('Y-m-d');

                $query->where('date_of_birth', '<=', $birth);
            }


            if($request->has('age_to') && $request->get('age_to') != ''){
                $birth = Carbon::now()->subYears($request->get('age_to'))->subYear()->addDay()->format('Y-m-d');

                $query->where('date_of_birth', '>=', $birth);
            }

            if($request->has('role') && $request->get('role') != 0){
                $role_id = $request->get('role');
                $query->whereHas('roles', function ($query) use($role_id){
                    $query->where('n_role_id', $role_id);
                });
            }

        })->with('groups', 'roles')->orderBy('surname')->paginate(100);

        $scenes = Scene::with('roles.wing')->get();
        $groups = Group::orderBy('name')->pluck('name', 'id');

        return view('admin.spectacles.panel.actors.index', compact('actors', 'scenes', 'groups'));
    }

    public function assignRole(Request $request )
    {
        $actor = Volunteer::find( $request->route('actor_id') );

        $scene = Scene::find(  $request->route('scene_id') );

        return view('admin.spectacles.panel.actors.assign-role', compact('actor', 'scene'));
    }

    public function updateRoles(Request $request)
    {
        $actor = Volunteer::find($request->route('actor_id'));

        foreach($request->get('roles', []) as $role)
        {
            $actor->roles()->attach($role);
        }

        return response()->json([
            'code'  => 0
        ]);
    }

    public function detachRole(Request $request )
    {
        $actor = Volunteer::find($request->route('actor_id'));
        $role = Role::find($request->route('role_id'));

        return view('admin.spectacles.panel.actors.detach-role', compact('actor', 'role'));
    }

    public function postDetachRole(Request $request )
    {
        $actor = Volunteer::find($request->route('actor_id'));

        $actor->roles()->detach($request->route('role_id'));

        return response()->json([
            'code'  => 0
        ]);
    }

    public function export(Request $request)
    {
        $filename = 'Zestawienie aktorów';

        \Excel::create($filename, function($excel) use($request) {
            $excel->sheet('zestawienie', function ($sheet) use ($request) {
                $header = ['aktor', 'grupy', 'wiek', 'wzrost'];
                $scenes = NScene::with('roles.wing')->get();
                foreach($scenes as $scene){
                    $header[] = $scene->name;
                }
                $sheet->appendRow($header);

                $actors = Volunteer::whereIn('if_actor', [1,2])
                    ->where('if_registered', 1)->where(function($query) use($request){
                        if($request->has('name') && $request->get('name') != ''){
                            $query->where('name', 'like', '%'.$request->get('name').'%');
                        }
                        if($request->has('surname') && $request->get('surname') != ''){
                            $query->where('surname', 'like', '%'.$request->get('surname').'%');
                        }

                        if($request->has('group') && $request->get('group') != 0){
                            if($request->get('group') == '-1')
                            {
                                $query->whereDoesntHave('groups');
                            }else {
                                $group_id = $request->get('group');
                                $query->whereHas('groups', function ($query) use($group_id){
                                    $query->where('n_group_id', $group_id);
                                });
                            }
                        }

                        if($request->has('gender') && $request->get('gender') != '0'){
                            $query->where('gender', $request->get('gender'));
                        }

                        if($request->has('age_from') && $request->get('age_from') != ''){
                            $birth = Carbon::now()->subYears($request->get('age_from'))->format('Y-m-d');

                            $query->where('date_of_birth', '<=', $birth);
                        }


                        if($request->has('age_to') && $request->get('age_to') != ''){
                            $birth = Carbon::now()->subYears($request->get('age_to'))->subYear()->addDay()->format('Y-m-d');

                            $query->where('date_of_birth', '>=', $birth);
                        }

                        if($request->has('role') && $request->get('role') != 0){
                            $role_id = $request->get('role');
                            $query->whereHas('roles', function ($query) use($role_id){
                                $query->where('n_role_id', $role_id);
                            });
                        }
                    })->with('groups', 'roles')->orderBy('surname')->get();

                foreach($actors as $actor)
                {
                    $row = [
                        $actor->name.' '.$actor->surname,
                        ($actor->groups->count() > 0) ? implode(', ', $actor->groups->pluck('name')->toArray() ): '',
                        $actor->date_of_birth->diffInYears(Carbon::now()),
                        $actor->height
                    ];

                    foreach($scenes as $scene){
                        $roles = [];
                        foreach($scene->roles as $role)
                        {
                            if($actor->roles->contains($role->id)){
                                $roles[] = $role->wing->name.' - '.$role->name;
                            }
                        }
                        $row[] = implode('; ', $roles);
                    }
                    $sheet->appendRow($row);
                }

            });
        })->download('xlsx');
    }
}
