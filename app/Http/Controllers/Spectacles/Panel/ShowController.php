<?php

namespace App\Http\Controllers\Spectacles\Panel;

use App\Http\Controllers\ExtendedController;
use App\Models\Show;
use App\Services\Generator\ShowOutline;
use File;
use Illuminate\Http\Request;

use Session;

class ShowController extends ExtendedController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->middleware('ability:shows_panel');
    }

    public function index()
    {
        $shows = Show::project($this->project->id)->orderBy('show')->get();

        return view('admin.spectacles.panel.shows.index', compact('shows'));
    }

    public function generateOutline($show_id, $type)
    {
        $generator = new ShowOutline($show_id);
        $filename = $generator->generate($type);

        Session::put('download.in.the.next.request', url('admin/panel/shows/download-generator', [$filename]));
        return redirect()->back();
    }

    public function downloadGenerator($filename)
    {
        $filename = $filename.'.pdf';

        $file = storage_path('app/generates') . '/' . $filename;
        $headers = array(
            'Content-Description' => 'File Transfer',
            'Content-Type' => File::mimeType($file),
        );
        return response()->download($file, $filename, $headers);
    }

    public function create()
    {
        return view('admin.spectacles.panel.shows.create');
    }

    public function store(Request $request)
    {
        Show::create([
            'project_id' => $this->project->id,
            'show' => $request->get('show_date').' '.$request->get('show_time')
        ]);

        return response()->json(['code' => 0]);
    }
}
