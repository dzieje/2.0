<?php

namespace App\Http\Controllers\Spectacles\Panel;

use App\Http\Controllers\ExtendedController;
use App\Models\Group;
use App\Models\Role;
use App\Models\Scene;
use App\Models\Wing;
use App\Models\Show;
use App\Models\Volunteer;
use Illuminate\Http\Request;

use App\Http\Controllers\Spectacles\Controller;
use Jenssegers\Date\Date;

class AbsenceController extends ExtendedController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->middleware('ability:absences');
    }

    public function list(Request $request)
    {
        $query = Volunteer::project($this->project->id)->whereNotNull('timesheet_date'); // whereIn('if_actor', [1,2])->where('if_registered', 1)->
        if($request->has('name') && $request->get('name') != ''){
            $query->where('name', 'like', '%'.$request->get('name').'%');
        }
        if($request->has('surname') && $request->get('surname') != ''){
            $query->where('surname', 'like', '%'.$request->get('surname').'%');
        }

        if($request->has('group') && $request->get('group') != 0){
            if($request->get('group') == '-1')
            {
                $query->doesnthave('groups');
            }else {
                $group_id = $request->get('group');

                $query->whereHas('groups', function($query) use($group_id) {
                    $query->where('group_id', $group_id);
                });
            }
        }

        if($request->has('role') && $request->get('role') != 0){
            $role_id = $request->get('role');
            $query->whereHas('roles', function ($query) use($role_id){
                $query->where('role_id', $role_id);
            });
        }

        $actors = $query->with('shows')->orderBy('timesheet_date', 'DESC')->paginate(300);

        $actorsWaiting = Volunteer::project($this->project->id)->whereNull('timesheet_date')->inRandomOrder()->take(10)->get();
        $allactorsWaiting = Volunteer::project($this->project->id)->whereNull('timesheet_date')->count();
        $groups = Group::project($this->project->id)->orderBy('name')->pluck('name', 'id');
        $roles = Role::project($this->project->id)->orderBy('name')->pluck('name',   'id');

        return view('admin.spectacles.panel.absences.index', compact('actors', 'groups', 'roles', 'actorsWaiting', 'allactorsWaiting'));
    }

    public function roles()
    {
        $shows = Show::project($this->project->id)->get();

        return view('admin.spectacles.panel.absences.shows', compact('shows'));
    }

    public function rolesShow(Request $request, $show_id)
    {
        $show = Show::project($this->project->id)->with('volunteers', 'volunteers.roles')->find($show_id);

        $roles = Role::
        project($this->project->id)->where(function($query) use($request){
            if($request->has('name') && $request->get('name') != ''){
                $query->where('name', 'like', '%'.$request->get('name').'%');
            }
            if($request->has('scene') && $request->get('scene') != '0'){
                $query->where('n_scene_id',  $request->get('scene'));
            }
            if($request->has('wing') && $request->get('wing') != '0'){
                $query->where('n_wing_id',  $request->get('wing'));
            }
            if($request->has('if_single_actor')){
                $query->where('if_single_actor', 1);
            }
            if($request->has('if_multi_actor')){
                $query->where('if_single_actor', 0);
            }
            if($request->has('if_understudy')){
                $query->where('if_understudy', 1);
            }
            if($request->has('if_nogroup')){
                $query->has('groups', '<', 1);
            }
            if($request->has('if_noactor')){
                $query->has('actors', '<', 1);
            }
        })->orderBy('n_scene_id')->orderBy('name')->paginate(100);
        $scenes = Scene::project($this->project->id)->pluck('name', 'id');
        $wings = Wing::pluck('name', 'id');

        $absences = [];
        foreach($show->volunteers as $volunteer)
        {
            foreach ($volunteer->roles as $role)
            {
                if(!isset($absences[$role->id])) $absences[$role->id] = 1;
                else $absences[$role->id] += 1;
            }
        }

        return view('admin.spectacles.panel.absences.roles', compact('roles', 'show', 'scenes', 'wings', 'absences'));
    }

    public function show($actor_id)
    {
        $actor = Volunteer::find($actor_id);

        return view('admin.spectacles.panel.absences.show', compact('actor'));
    }

    public function edit($actor_id)
    {
        $actor = Volunteer::find($actor_id);

        $shows = Show::project($this->project->id)->get();

        return view('admin.spectacles.panel.absences.edit', compact('actor', 'shows'));
    }

    public function update(Request $request, $actor_id)
    {
        $actor = Volunteer::find($actor_id);
        $actor->shows()->sync($request->get('shows', []));

        return redirect()->to(url('admin/panel/absences/list'));
    }

    public function showActors($show_id, $role_id)
    {
        $show = Show::find($show_id);

        $role = Role::project($this->project->id)->with('actors', 'actors.groups')->find($role_id);
        return view('admin.spectacles.panel.absences.show-actors', compact('role', 'show'));
    }

    public function exportRoles(Request $request, $show_id)
    {
        $show = Show::project($this->project->id)->with('volunteers', 'volunteers.roles')->find($show_id);
        $filename = 'Zestawienie nieobecnosci '.$show->show->format('Y-m-d');

        \Excel::create($filename, function($excel) use($request, $show_id){
            $excel->sheet('zestawienie', function($sheet) use($request, $show_id) {

                $sheet->appendRow(array(
                    'nazwa roli',
                    'scena',
                    'kulisa',
                    'obecnych',
                    'na możliwych'
                ));



                $roles = NRole::
                where(function($query) use($request){
                    if($request->has('name') && $request->get('name') != ''){
                        $query->where('name', 'like', '%'.$request->get('name').'%');
                    }
                    if($request->has('scene') && $request->get('scene') != '0'){
                        $query->where('n_scene_id',  $request->get('scene'));
                    }
                    if($request->has('wing') && $request->get('wing') != '0'){
                        $query->where('n_wing_id',  $request->get('wing'));
                    }
                    if($request->has('if_single_actor')){
                        $query->where('if_single_actor', 1);
                    }
                    if($request->has('if_multi_actor')){
                        $query->where('if_single_actor', 0);
                    }
                    if($request->has('if_understudy')){
                        $query->where('if_understudy', 1);
                    }
                    if($request->has('if_nogroup')){
                        $query->has('groups', '<', 1);
                    }
                    if($request->has('if_noactor')){
                        $query->has('actors', '<', 1);
                    }
                })->orderBy('n_scene_id')->orderBy('name')->get();

                $absences = [];
                $show = Show::with('volunteers', 'volunteers.roles')->find($show_id);
                foreach($show->volunteers as $volunteer)
                {
                    foreach ($volunteer->roles as $role)
                    {
                        if(!isset($absences[$role->id])) $absences[$role->id] = 1;
                        else $absences[$role->id] += 1;
                    }
                }

                foreach ($roles as $role)
                {
                    if(isset($absences[$role->id])){
                        $present = $role->actors->count()-$absences[$role->id];
                    }else{
                        $present = $role->actors->count();
                    }

                    $sheet->appendRow(array(
                        $role->name,
                        ($role->scene) ? $role->scene->name : '---' ,
                        ($role->wing) ? $role->wing->name : '---',
                        $present,
                        $role->actors->count()
                    ));
                }

            });
        })->download('xlsx');
    }

    public function exportActors(Request $request)
    {
        $filename = 'Zestawienie nieobecnosci';

        \Excel::create($filename, function($excel) use($request){
            $excel->sheet('zestawienie', function($sheet) use($request) {

                $shows = Show::get();


                $row = [
                    'imię',
                    'nazwisko',
                    'telefon',
                    'email',
                    'grupa',
                ];
                foreach ($shows as $show)
                {
                    $row[] = Date::createFromFormat('Y-m-d',  $show->show->format('Y-m-d'))->format('j F Y')  ;
                }
                $sheet->appendRow($row);



                $actors = Volunteer::whereIn('if_actor', [1,2])->where('if_registered', 1)->whereNotNull('timesheet_date')
                    ->where(function($query) use($request){
                        if($request->has('name') && $request->get('name') != ''){
                            $query->where('name', 'like', '%'.$request->get('name').'%');
                        }
                        if($request->has('surname') && $request->get('surname') != ''){
                            $query->where('surname', 'like', '%'.$request->get('surname').'%');
                        }

                        if($request->has('group') && $request->get('group') != 0){
                            if($request->get('group') == '-1')
                            {
                                $query->doesnthave('groups');
                            }else {
                                $group_id = $request->get('group');

                                $query->whereHas('groups', function($query) use($group_id) {
                                    $query->where('n_group_id', $group_id);
                                });
                            }
                        }

                        if($request->has('role') && $request->get('role') != 0){
                            $role_id = $request->get('role');
                            $query->whereHas('roles', function ($query) use($role_id){
                                $query->where('n_role_id', $role_id);
                            });
                        }

                    })
                    ->has('shows')->with('shows', 'groups')->orderBy('surname')->get();


                foreach ($actors as $actor)
                {
                    $row = [
                        $actor->name,
                        $actor->surname,
                        $actor->phone,
                        $actor->email,
                        ($actor->groups->count() > 0) ?implode(', ', $actor->groups->pluck('name')->toArray()) : '',
                    ];

                    foreach ($shows as $show)
                    {
                        $row[] = ($actor->shows->contains($show->id)) ? 'x' : '';
                    }

                    $sheet->appendRow($row);
                }

            });
        })->download('xlsx');
    }
}
