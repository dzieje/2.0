<?php

namespace App\Http\Controllers\Spectacles\Panel;

use App\Http\Controllers\ExtendedController;
use App\Models\Role;
use App\Models\Scene;
use App\Models\Wing;
use App\Services\Generator\SceneOutline;
use File;
use Illuminate\Http\Request;

use Session;

class SceneController extends ExtendedController
{
    public function __construct(Request  $request)
    {
        parent::__construct($request);
        $this->middleware('ability:scenes_panel');
    }

    public function index()
    {
        $scenes = Scene::project($this->project->id)->with('roles', 'roles.wing')->paginate(100);

        return view('admin.spectacles.panel.scenes.index', compact('scenes'));
    }

    public function generateOutline(Request  $request)
    {
        $generator = new SceneOutline($request->route('rehearsal_id'));
        $filename = $generator->generate($request->route('file_type'));

        Session::put('download.in.the.next.request', url('admin/panel/scenes/download-generator', [$filename, $file_type]));
        return redirect()->back();
    }

    public function showRoles(Request  $request)
    {
        $scene = Scene::project($this->project->id)->with('roles', 'roles.actors', 'roles.wing')->find($request->route('scene_id'));
        return view('admin.spectacles.panel.scenes.show-roles', compact('scene'));
    }

    public function showActors(Request  $request)
    {
        $role = Role::project($this->project->id)->with('actors', 'actors.group')->find($request->route('role_id'));
        return view('admin.spectacles.panel.scenes.show-actors', compact('role'));
    }

    public function showWings(Request  $request)
    {
        $scene = Scene::project($this->project->id)->with('roles.wing')->find($request->route('scene_id'));
        $wings = $scene->roles->pluck('wing.id')->unique();
        $wings = Wing::whereIn('id', $wings)->get();

        return view('admin.spectacles.panel.scenes.show-wings', compact('scene', 'wings'));
    }


    public function downloadGenerator(Request  $request)
    {
        $filename = $request->route('filename').'.'.$request->route('file_type');

        $file = storage_path('app/generates') . '/' . $filename;
        $headers = array(
            'Content-Description' => 'File Transfer',
            'Content-Type' => File::mimeType($file),
        );
        return response()->download($file, $filename, $headers);
    }
}
