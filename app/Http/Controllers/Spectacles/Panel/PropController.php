<?php

namespace App\Http\Controllers\Spectacles\Panel;

use App\Http\Controllers\ExtendedController;
use App\Models\Guardian;
use App\Models\Costume;
use App\Models\Group;
use App\Models\Prop;
use App\Models\Role;
use App\Models\Scene;
use App\Models\Wing;
use App\Models\Rehearsal;
use App\Models\RehearsalPeriodAttendance;
use App\Models\Volunteer;
use App\Services\Generator\ActorRehearsalsTimetable;
use App\Services\Generator\ActorRoles;
use App\Services\Generator\RehearsalKidsGuardianOutline;
use App\Services\Generator\RehearsalKidsList;
use App\Services\Generator\RehearsalKidsOutline;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;

use PHPExcel_Style_Border;
use Session;

class PropController extends ExtendedController
{
    public function __construct(Request  $request)
    {
        parent::__construct($request);
        $this->middleware('ability:costumes_panel');
    }

    public function index(Request $request)
    {
        $roles = Role::project($this->project->id)->orderBy('name')->with('scene')->get();
        $scenes = Scene::project($this->project->id)->orderBy('name')->get();
        $wings = Wing::orderBy('name')->get();
        $props = Prop::where(function($query) use ($request){
            if($request->has('role') && $request->get('role') > 0){
                $query->where('role_id', $request->get('role'));
            }
            if($request->has('scene') && $request->get('scene') > 0){
                $query->whereHas('role', function($query) use($request){
                    $query->where('scene_id', $request->get('scene'));
                });
            }
            if($request->has('wing') && $request->get('wing') > 0){
                $query->whereHas('role', function($query) use($request){
                    $query->where('wing_id', $request->get('wing'));
                });
            }
            if($request->has('type') && $request->get('type') < 2){
                $query->where('type', $request->get('type'));
            }
        })->with('role.scene', 'role.actors', 'role.wing')->orderBy('name')->paginate(100);

        return view('admin.spectacles.panel.props.index', compact( 'roles', 'scenes', 'props', 'wings'));
    }

    public function export(Request $request)
    {
        $filename = 'Zestawienie rekwizytów';
        \Excel::create($filename, function($excel) use($request) {

            $excel->sheet('Zestawienie', function($sheet) use($request)  {

                $sheet->appendRow(array(
                    'ilość',
                    'rekwizyt',
                    'rola',
                    'scena',
                    'kulisa',
                    'przygotowuje'
                ));

                $props = NProp::where(function($query) use ($request){
                    if($request->has('role') && $request->get('role') > 0){
                        $query->where('n_role_id', $request->get('role'));
                    }
                    if($request->has('scene') && $request->get('scene') > 0){
                        $query->whereHas('role', function($query) use($request){
                            $query->where('n_scene_id', $request->get('scene'));
                        });
                    }
                    if($request->has('wing') && $request->get('wing') > 0){
                        $query->whereHas('role', function($query) use($request){
                            $query->where('n_wing_id', $request->get('wing'));
                        });
                    }
                    if($request->has('type') && $request->get('type') < 2){
                        $query->where('type', $request->get('type'));
                    }
                })->with('role.scene', 'role.actors', 'role.wing')->orderBy('name')->get();

                foreach ($props as $prop)
                {
                    $sheet->appendRow(array(
                        ($prop->role) ? $prop->role->actors->count() : 0,
                        $prop->name,
                        ($prop->role) ? $prop->role->name : '',
                        ($prop->role) ? $prop->role->scene->name : '',
                        $prop->role ? ($prop->role->wing ? $prop->role->wing->name : '') : '',
                        ($prop->type == 0) ? 'dzieje' : 'statysta'
                    ));
                }
            });

        })->store('xlsx', storage_path('excel'));

        Session::put('download.in.the.next.request', url('admin/panel/props/download', array($filename) ));
        return response()->json([
            'code'  => 0
        ]);
    }

    public function download($filename)
    {
        $filename = $filename.'.xlsx'; //

        $file = storage_path('excel') . '/' . $filename;
        $headers = array(
            'Content-Description' => 'File Transfer',
            'Content-Type' => File::mimeType($file),
        );
        return response()->download($file, $filename, $headers);
    }
}
