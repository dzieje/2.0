<?php

namespace App\Http\Controllers\Spectacles\Panel;

use App\Models\Cleanup;
use App\Models\CleanupPeriod;
use App\Models\Volunteer;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Spectacles\Controller;

class CleanupController extends Controller
{
    public function __construct()
    {
        $this->middleware('ability:cleanups');
    }

    public function getIndex(Request $request)
    {
        $volunteers = Volunteer::where('if_registered', 1)->whereIn('if_actor', [1,2])->whereNotNull('cleanup_date')
            ->where(function($query) use($request){
                if($request->has('name') && $request->get('name') != ''){
                    $query->where('name', 'like', '%'.$request->get('name').'%');
                }
                if($request->has('surname') && $request->get('surname') != ''){
                    $query->where('surname', 'like', '%'.$request->get('surname').'%');
                }
            })
            ->with('cleanupPeriods')->orderBy('surname')->paginate(100);

        return view('panel.cleanups.index', compact('volunteers'));
    }

    public function getShow($volunteer_id)
    {
        $volunteer = Volunteer::with('cleanupPeriods', 'cleanupPeriods.cleanup')->find($volunteer_id);

        return view('panel.cleanups.show', compact('volunteer'));
    }

    public function getEdit($volunteer_id)
    {
        $volunteer = Volunteer::with('cleanupPeriods')->find($volunteer_id);
        $cleanups = Cleanup::with('periods')->get();

        $times = CleanupPeriod::pluck('cleaning_time', 'cleaning_time');
        return view('panel.cleanups.edit', compact('volunteer','cleanups', 'times'));
    }

    public function postUpdate(Request $request, $volunteer_id)
    {
        $volunteer = Volunteer::find($volunteer_id);
        $volunteer->cleanupPeriods()->sync($request->get('cleanups', []));

        return redirect()->to(url('admin/panel/cleanups'));
    }

    public function getExport(Request $request)
    {
        $filename = 'Zestawienie porządków';

        \Excel::create($filename, function($excel) use($request){
            $excel->sheet('zestawienie', function($sheet) use($request) {

                $cleanups = Cleanup::with('periods')->get();



                $row = [
                    'imię',
                    'nazwisko',
                    'telefon',
                    'email'
                ];
                foreach ($cleanups as $cleanup)
                {
                    foreach ($cleanup->periods as $period) {
                        $row[] = $period->cleanup->cleaning_day->format('Y-m-d').' '.substr($period->cleaning_time, 0, -3);
                    }
                }
                $sheet->appendRow($row);



                $volunteers = Volunteer::where('if_registered', 1)->whereNotNull('cleanup_date')
                    ->where(function($query) use($request){
                        if($request->has('name') && $request->get('name') != ''){
                            $query->where('name', 'like', '%'.$request->get('name').'%');
                        }
                        if($request->has('surname') && $request->get('surname') != ''){
                            $query->where('surname', 'like', '%'.$request->get('surname').'%');
                        }
                    })->whereIn('if_actor', [1,2])
                    ->has('cleanupPeriods')->with('cleanupPeriods')->orderBy('surname')->get();


                foreach ($volunteers as $volunteer)
                {
                    $row = [
                        $volunteer->name,
                        $volunteer->surname,
                        $volunteer->phone,
                        $volunteer->email
                    ];

                    foreach ($cleanups as $cleanup)
                    {
                        foreach ($cleanup->periods as $period) {
                            $row[] = ($volunteer->cleanupPeriods->contains($period->id)) ? 'x' : '';
                        }
                    }

                    $sheet->appendRow($row);
                }

            });
        })->download('xlsx');
    }
}
