<?php

namespace App\Http\Controllers\Spectacles;

use App\Http\Controllers\ExtendedController;
use App\Models\Volunteer;
use App\Models\VolunteerAttribute;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VolunteerController extends ExtendedController
{
    public function index(Request  $request)
    {
        $query = Volunteer::project($this->project->id, false);

        if($request->has('name') && $request->get('name') != ''){
            $query->where('name', 'like', '%'.$request->get('name').'%');
        }
        if($request->has('surname') && $request->get('surname') != ''){
            $query->where('surname', 'like', '%'.$request->get('surname').'%');
        }
        if($request->has('gender') && $request->get('gender') != '0')
        {
            $query->where('gender', $request->get('gender'));
        }

        if($request->has('age_from') && $request->get('age_from') != ''){
            $birth = Carbon::now()->subYears($request->get('age_from'))->format('Y-m-d');

            $query->where('date_of_birth', '<=', $birth);
        }

        if($request->has('age_to') && $request->get('age_to') != ''){
            $birth = Carbon::now()->subYears($request->get('age_to'))->subYear()->addDay()->format('Y-m-d');

            $query->where('date_of_birth', '>=', $birth);
        }

        $volunteers = $query->orderBy('surname')->paginate(100);

        $attributes = VolunteerAttribute::whereHas('projects', function ($query){
            $query->where('project_id', $this->project->id);
        })->get();

        return view('admin.spectacles.volunteers.index', compact('volunteers',  'attributes'));
    }
}
