<?php

namespace App\Http\Controllers\Spectacles;

use App\Http\Controllers\ExtendedController;
use App\Models\StageManager;
use App\Models\Survey;
use App\Models\SurveyQuestion;
use App\Models\Volunteer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class StageManagerController extends ExtendedController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    public function index()
    {
        $scripts = StageManager::orderBy('id','desc')->paginate(100);

        return view('admin.spectacles.stage-manager.index', compact('scripts'));
    }

    public function upload(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'file' => 'file'
        ]);

        if ($validator->fails())
        {
            flash('błędny format pliku', 'danger');
            return;
        }

        $filename = Str::random(4).time().'.json';

        $original_filename = $request->file('file')->getClientOriginalName();
        $request->file('file')->move(storage_path('app/stage_manager'),$filename);

        $data = json_decode( Storage::get('stage_manager/'.$filename) );
        if (is_null($data)) {
            flash('błędny struktura pliku json', 'danger');
            return;
        }

        StageManager::create([
            'project_id' => $this->project-id,
            'user_id' => auth()->user()->id,
            'filename' => $filename,
            'original_filename' => $original_filename
        ]);

        return;
    }

    public function run(Request $request)
    {
        $script = StageManager::find($request->route('script_id'));

        return view('admin.spectacles.stage-manager.countdown', compact('script'));
    }

    public function load(Request $request)
    {
        $script = StageManager::find($request->route('script_id'));

        return Storage::get('stage_manager/'.$script->filename, array('Content-type'=> 'application/json'));
    }
}
