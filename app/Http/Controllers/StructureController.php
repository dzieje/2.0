<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\Structure;
use App\Models\StructureNode;
use App\Models\Task;
use App\Models\User;
use App\Models\Volunteer;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

class StructureController extends ExtendedController
{
    public function index()
    {
        $structures = Structure::project($this->project->id)->with('nodes')->get();

        return view('admin.structures.index', compact('structures'));
    }

    public function create()
    {
        return view('admin.structures.create');
    }

    public function store(Request $request)
    {
        $structure = Structure::create([
            'project_id' => $this->project->id,
            'name' => $request->get('name')
        ]);

        $node = $structure->nodes()->create(['name' => $request->get('name')]);
        $node->leaders()->sync($request->get('users', []));

        return response()->json(['code' => 0]);
    }

    public function edit(Request  $request)
    {
        $structure = Structure::find($request->route('structure_id'));
        return view('admin.structures.edit', compact('structure'));
    }

    public function update(Request $request)
    {
        $structure = Structure::find($request->route('structure_id'));
        $structure->update([
            'name' => $request->get('name')
        ]);

        $node = $structure->nodes()->create(['name' => $request->get('name')]);
        $node->leaders()->sync($request->get('users', []));

        return response()->json(['code' => 0]);
    }

    public function manage(Request $request)
    {
        $structure = Structure::find($request->route('structure_id'));

        return view('admin.structures.manage', compact('structure'));
    }

    public function loadNodes(Request $request)
    {
        $structure = Structure::find($request->get('structure_id'));
        $nodes = $structure->nodes->toTree();

        $nodeStructure = $this->traverse($nodes);

        return response()->json($nodeStructure);
    }

    private function traverse($nodes){
        $structure = [];
        foreach ($nodes as $node) {
            $leaders = [];
            foreach($node->leaders as $leader)
            {
                $leaders[] = '<span class="badge badge-info mx-1">'.$leader->name.'</span>';
            }
            $nodeStructure = [
                'text' => ['name' => $node->name],
                'innerHTML' => '<div class="d-flex justify-content-between align-items-center mb-2">
                                    '.$node->name.'
                                    <div>
                                    '.(! $node->isRoot() ? '

                                            <span target="'.url('admin/'.$this->project->type->slug.'/'.$this->project->slug.'/structures/move-node', [$node->id]).'" class="btn btn-xs btn-outline-dark modal-open" data-toggle="modal" data-target="#modal">
                                                <i class="fas fa-arrows-alt-v"></i>
                                            </span>
                                            <span target="'.url('admin/'.$this->project->type->slug.'/'.$this->project->slug.'/structures/delete-node', [$node->id]).'" class="btn btn-xs btn-outline-dark modal-open" data-toggle="modal" data-target="#modal">
                                                <i class="fas fa-trash-alt"></i>
                                            </span>
                                        ' : '').'
                                        <span target="'.url('admin/'.$this->project->type->slug.'/'.$this->project->slug.'/structures/edit-node', [$node->id]).'" class="btn btn-xs btn-outline-dark modal-open-lg" data-toggle="modal" data-target="#modal-lg">
                                            <i class="fas fa-pencil-alt"></i>
                                        </span>
                                    </div>
                                </div>

                                <div class="small">'.implode('', $leaders).'</div>
                                <hr class="my-2">
                                <a class="btn btn-xs btn-outline-dark" href="'.url('admin/'.$this->project->type->slug.'/'.$this->project->slug.'/structures/tasks', [$node->id]).'"><i class="fas fa-fw fa-tasks"></i> zadania <span class="badge badge-dark">'.$node->tasks->count().'</span></a>
                                <a class="btn btn-xs btn-outline-dark" href="'.url('admin/'.$this->project->type->slug.'/'.$this->project->slug.'/structures/volunteers', [$node->id]).'"><i class="fas fa-fw fa-users"></i> osoby <span class="badge badge-dark">'.$node->volunteers->count().'</span></a>
                                <span target="'.url('admin/'.$this->project->type->slug.'/'.$this->project->slug.'/structures/append', [$node->id]).'" class="btn btn-xs btn-info modal-open" data-toggle="modal" data-target="#modal"><i class="fas fa-fw fa-plus"></i> </span>',
                'children' => $this->traverse($node->children)
            ];
            if($node->isRoot()){
                $structure = $nodeStructure;
            }else{
                $structure[] = $nodeStructure;
            }
        }
        return $structure;
    }

    public function appendModal(Request $request)
    {
        $node = StructureNode::find($request->route('node_id'));

        return view('admin.structures.append', compact('node'));
    }

    public function append(Request $request)
    {
        $parent = StructureNode::find($request->route('node_id'));

        $node = StructureNode::create([
            'structure_id' => $parent->structure_id,
            'name' => $request->get('name')
        ]);

        $node->leaders()->sync($request->get('users', []));

        $node->appendToNode($parent)->save();

        if($request->get('duplicate-volunteers') == 1){
            $node->volunteers()->sync($parent->volunteers->pluck('id')->toArray());
        }

        return response()->json(['code' => 0]);
    }

    public function tasks(Request  $request)
    {
        $node = StructureNode::with('tasks')->find($request->route('node_id'));
        $tasks = $node->tasks()->withCount('notifications', 'readNotifications')->paginate(100);

        return view('admin.structures.tasks.index', compact('node', 'tasks'));
    }

    public function addTask(Request  $request)
    {
        $node = StructureNode::find($request->route('node_id'));

        return view('admin.structures.tasks.create', compact('node'));
    }

    public function storeTask(Request  $request)
    {
        $request->request->add(['user_id' => auth()->user()->id]);

        if($request->has('content')){
            $content = $request->get("content");
            $dom = new \DOMDocument();
            $content = str_replace('o:p', 'p', $content);
            @$dom->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
            $images = $dom->getElementsByTagName('img');

            foreach ($images as $image) {
                $old_src = $image->getAttribute('src');
                if(is_absolute_path($old_src));
                {
                    $new_src = url($old_src);
                }
                $image->setAttribute('src', $new_src);
            }

            $content = $dom->saveHTML();
            $request->request->set("content", $content);
        }

        $task = Task::create($request->all());

        foreach($request->get('attachments', []) as $attachment)
        {
            $task->attachments()->create(['filename' => $attachment]);
        }

        foreach ($task->node->volunteers as $volunteer)
        {
            if(config('fcm.enabled')) {
                $optionBuilder = new OptionsBuilder();
                $optionBuilder->setTimeToLive(60 * 10);

                $notificationBuilder = new PayloadNotificationBuilder($task->title);
                $notificationBuilder->setBody(str_replace(array("\n","\r\n","\r"), '', Str::limit(strip_tags( getbody($task->content) ), 180) ))
                    ->setSound('default');

                $dataBuilder = new PayloadDataBuilder();
                $dataBuilder->addData([
                    'id' => $task->id,
                    'type' => 'task',
                    'title' => $task->title,
                    'content' => str_replace(array("\n","\r\n","\r"), '', Str::limit(strip_tags( getbody($task->content) ), 180) ),
                    'unread' => true,
                    'date' => $task->created_at,
                    'author' => $task->user->name,
                    'photo' => $task->user->avatar ? $task->user->avatar : asset('images/person.png')
                ]);

                $option = $optionBuilder->build();
                $data = $dataBuilder->build();
            }

            $task->notifications()->create([
                'volunteer_id' => $volunteer->id
            ]);

            if(config('fcm.enabled') && $volunteer->firebase_uuid)
            {
                $notificationBuilder->setBadge($volunteer->news()->unread()->count() + $volunteer->tasks()->unread()->count() );
                $notification = $notificationBuilder->build();

                FCM::sendTo($volunteer->firebase_uuid, $option, $notification, $data);
            }
        }

        return redirect()->to(url('admin/'.$this->project->type->slug.'/'.$this->project->slug.'/structures/tasks', [$task->structure_node_id]));
    }

    public function editTask(Request  $request)
    {
        $task = Task::find($request->route('task_id'));

        return view('admin.structures.tasks.edit', compact('task'));
    }

    public function updateTask(Request  $request)
    {
        $task = Task::find($request->route('task_id'));

        if($request->has('content')){
            $content = $request->get("content");
            $dom = new \DOMDocument();
            $content = str_replace('o:p', 'p', $content);
            @$dom->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
            $images = $dom->getElementsByTagName('img');

            foreach ($images as $image) {
                $old_src = $image->getAttribute('src');
                if(is_absolute_path($old_src));
                {
                    $new_src = url($old_src);
                }
                $image->setAttribute('src', $new_src);
            }

            $content = $dom->saveHTML();
            $request->request->set("content", $content);
        }

        $task->update($request->all());

        foreach($request->get('attachments', []) as $attachment)
        {
            $task->attachments()->create(['filename' => $attachment]);
        }

        return redirect()->to(url('admin/'.$this->project->type->slug.'/'.$this->project->slug.'/structures/tasks', [$task->structure_node_id]));
    }

    public function volunteers(Request  $request)
    {
        $node = StructureNode::with('tasks')->find($request->route('node_id'));

        $volunteers = $node->volunteers()->paginate(100);

        return view('admin.structures.volunteers.index', compact('node', 'volunteers'));
    }

    public function addVolunteer(Request  $request)
    {
        $node = StructureNode::find($request->route('node_id'));

        return view('admin.structures.volunteers.add', compact('node'));
    }

    public function searchVolunteer(Request $request)
    {
        $q = $request->get('q');
        $query = Volunteer::project($this->project->id)->where(function($query) use($q){
            $query->where('name', 'like', $q.'%')->orWhere('surname', 'like', $q.'%');
        });

        $volunteers = $query->get();

        $response = ['volunteers' => []];
        foreach ($volunteers as $volunteer)
        {
            $response['volunteers'][] = ['id' => $volunteer->id,  'name' => $volunteer->name.' '.$volunteer->surname];
        }

        return response()->json($response);
    }

    public function attachVolunteer(Request  $request)
    {
        $node = StructureNode::find($request->route('node_id'));

        foreach($request->get('actors', []) as $volunteer_id)
        {
            if(! $node->volunteers->contains($volunteer_id)) {
                $node->volunteers()->attach($volunteer_id);
            }
        }

        return response()->json(['code' => 0]);
    }

    public function detachVolunteerModal(Request  $request)
    {
        $node = StructureNode::find($request->route('node_id'));
        $volunteer = Volunteer::find($request->route('volunteer_id'));

        return view('admin.structures.volunteers.detach', compact('node', 'volunteer'));
    }

    public function detachVolunteer(Request $request)
    {
        $node = StructureNode::find($request->route('node_id'));
        $volunteer = Volunteer::find($request->route('volunteer_id'));

        $node->volunteers()->detach($volunteer->id);

        return response()->json(['code' => 0]);
    }

    public function moveNodeModal(Request $request)
    {
        $node = StructureNode::find($request->route('node_id'));

        $nodes = $node->structure->nodes()->root()->descendants->where('id', '!=', $node->id)->pluck('name', 'id')->toArray();
        $nodes[$node->structure->nodes()->root()->id] = $node->structure->nodes()->root()->name;

        return view('admin.structures.move-node', compact('node', 'nodes'));
    }

    public function moveNode(Request $request)
    {
        $node = StructureNode::find($request->route('node_id'));
        $node->parent_id = $request->get('node_id');
        $node->save();

        return response()->json(['code' => 0]);
    }

    public function editNode(Request $request)
    {
        $node = StructureNode::find($request->route('node_id'));

        return view('admin.structures.edit-node', compact('node'));
    }

    public function updateNode(Request $request)
    {
        $node = StructureNode::find($request->route('node_id'));

        $node->update([
            'name' => $request->get('name')
        ]);

        $node->leaders()->sync($request->get('users', []));

        return response()->json(['code' => 0]);
    }

    public function deleteNodeModal(Request $request)
    {
        $node = StructureNode::find($request->route('node_id'));
        return view('admin.structures.delete-node', compact('node'));
    }

    public function deleteNode(Request $request)
    {
        $node = StructureNode::find($request->route('node_id'));

        if($request->get('remove-descendants') != 1)
        {
            foreach($node->children as $child)
            {
                $node->parent->appendNode($child);
            }
        }

        $node->delete();

        return response()->json(['code' => 0]);
    }
}
