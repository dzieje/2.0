<?php

namespace App\Http\Controllers;

use App\Charts\ProjectSubmissions;
use App\Models\News;
use App\Models\Project;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('landing');
    }

    public function landing()
    {
        return view('welcome');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $projects = Project::get();
        $charts=[];
        foreach($projects as $project){

            $period = CarbonPeriod::create(Carbon::now()->subWeek(), 8);
            $labels = [];

            foreach ($period as $key => $date) {
                $labels[] = $date->format('Y-m-d');
            }

            $chart = new ProjectSubmissions;
            $chart->labels($labels);

            $data = $project->projectVolunteers()->where('created_at', '>=', Carbon::now()->subWeek()->startOfDay())
                ->select('id', 'created_at', DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d') as date"))
                ->get()
                ->groupBy('date')
                ->map(function ($item) {
                    return count($item);
                })->toArray();

            $dataset = [];

            foreach ($period as $key => $date) {
                if(!isset($data[$date->format('Y-m-d')])){
                    $dataset[$key] = 0;
                }else{
                    $dataset[$key] = $data[$date->format('Y-m-d')];
                }
            }

            $chart->dataset('Nowi wolontariusze', 'line',$dataset);

            $charts[] = [
                'project' => $project,
                'chart' => $chart
            ];
        }
        return view('home', compact('charts'));
    }
}
