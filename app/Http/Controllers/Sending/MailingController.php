<?php

namespace App\Http\Controllers\Sending;

use App\Http\Controllers\Controller;
use App\Jobs\SendMailMessage;
use App\Mail\Mailing;
use App\Mail\Template;
use App\Models\Group;
use App\Models\Message;
use App\Models\Project;
use App\Models\Role;
use App\Models\Survey;
use App\Models\User;
use App\Models\Volunteer;
use App\Models\Wing;
use App\Repositories\MessageRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MailingController extends Controller
{
    /**
     * @var MessageRepository
     */
    private $message;

    /**
     * MailingController constructor.
     * @param MessageRepository $message
     */
    public function __construct(MessageRepository $message)
    {
        $this->message = $message;
    }

    public function index(){
        $mailings = Message::mail()->with(['user', 'entities'])->latest()->paginate(100);

        return view('admin.sending.mail.index', compact('mailings'));
    }

    public function create()
    {
        $projects = Project::with('type')->get()->groupBy('project_type_id');

        $parameters = $this->message->docParameters();

        return view('admin.sending.mail.create', compact('parameters', 'projects'));
    }

    public function countReceivers(Request $request)
    {
        $volunteers = $this->message->filteredVolunteers($request);

        return response()->json(['count' => $volunteers->count()]);
    }

    public function showReceivers(Request $request)
    {
        $volunteers = $this->message->filteredVolunteers($request);

        $filters = $request->except('_token');

        return view('admin.sending.receivers', compact('volunteers', 'filters'));
    }

    public function loadProjectParameters($project_id)
    {
        $project = Project::find($project_id);
        return $project ? $this->message->projectParameters($project) : '';
    }

    public function loadProjectSurveys($project_id)
    {
        $project = Project::find($project_id);
        return $project ? $this->message->projectSurveys($project) : '';
    }

    public function test()
    {
        $volunteers = Volunteer::orderBy('surname')->select('name', 'surname', 'id')->get()->pluck('fullName','id')->toArray();

        return view('admin.sending.mail.send-test', compact('volunteers'));
    }


    public function sendTest(Request $request)
    {
        $template = $request->get('content');

        $dom = new \DOMDocument();
        $template = str_replace('o:p', 'p', $template);
        @$dom->loadHTML(mb_convert_encoding($template, 'HTML-ENTITIES', 'UTF-8'));
        $images = $dom->getElementsByTagName('img');

        foreach ($images as $image) {
            $old_src = $image->getAttribute('src');
            if(is_absolute_path($old_src));
            {
                $new_src = url($old_src);
            }
            $image->setAttribute('src', $new_src);
        }

        $template = $dom->saveHTML();

        $volunteer = Volunteer::find($request->get('volunteer_id'));
        $params = $volunteer->getDocParamValues();

        $template = str_replace(array_keys($params), $params, $template);

        preg_match_all('/\%\%FORM-[^\%]*\%\%/', $template, $forms);

        if(isset($forms[0]) > 0) {
            foreach ($forms[0] as $form) {
                $slug = str_replace(['%%FORM-', '%%'], '', $form);
                $survey = Survey::where('slug', $slug)->first();
                if ($survey) {
                    $link = '<p class="font-lato" style="color: #7c7e7f;font-family: Lato,Tahoma,sans-serif;font-size: 13px; line-height: 22px;Margin-top: 14px;Margin-bottom: 0; text-align: center"><a style="color: #fff; background-color: #c3193d; border-color: #ac1636; display: inline-block; margin-bottom: 0;font-weight: normal; text-align: center; vertical-align: middle; -ms-touch-action: manipulation; touch-action: manipulation; cursor: pointer;background-image: none; border: 1px solid transparent; white-space: nowrap; padding: 6px 12px;font-size: 14px;line-height: 1.428571429; border-radius: 4px; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none;user-select: none;" href="' . url('form/open', [$survey->slug, $volunteer->uuid]) . '">wypełnij formularz ' . $survey->name . '</a></p>';
                    $template = str_replace($form, $link, $template);
                }
            }
        }

        $template = preg_replace('/%%(.*)%%/', '', $template);
        $template = str_replace('<!-- pagebreak -->', '', $template);

        $replyTo = $request->get('replyTo');
        $subject = $request->get('subject');

        \Mail::to($volunteer->email)->send(new Template( $template , $subject, $replyTo ));

        return response()->json(['success']);
    }

    public function send(Request  $request)
    {
        $volunteers = Volunteer::whereIn('id', $request->get('package', []))->get();

        $replyTo = $request->get('replyTo');
        $subject = $request->get('subject');
        $content = $request->get('content');

        $dom = new \DOMDocument();
        $content = str_replace('o:p', 'p', $content);
        @$dom->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
        $images = $dom->getElementsByTagName('img');

        foreach ($images as $image) {
            $old_src = $image->getAttribute('src');
            if(is_absolute_path($old_src));
            {
                $new_src = url($old_src);
            }
            $image->setAttribute('src', $new_src);
        }
        $content = $dom->saveHTML();

        $mail = Message::create([
            "user_id" => auth()->user()->id,
            'message_type_id' => 1,
            "subject" => $subject,
            "content" => $content,
            "reply_to" => $replyTo,
            "send_time" => date("Y-m-d H:i:s"),
        ]);

        foreach($volunteers as $volunteer) {
            $this->dispatch(new SendMailMessage($mail, $volunteer));
        }

        return redirect()->to(url('admin/sending/mailings'));
    }
}
