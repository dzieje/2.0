<?php

namespace App\Http\Controllers\Sending;

use App\Http\Controllers\Controller;
use App\Jobs\SendSmsMessage;
use App\Models\Message;
use App\Models\Project;
use App\Models\Volunteer;
use App\Repositories\MessageRepository;
use Illuminate\Http\Request;

class SmsController extends Controller
{
    /**
     * @var MessageRepository
     */
    private $message;

    /**
     * MailingController constructor.
     * @param MessageRepository $message
     */
    public function __construct(MessageRepository $message)
    {
        $this->message = $message;
    }

    public function index(){
        $messages = Message::sms()->with(['user', 'entities'])->latest()->paginate(100);

        return view('admin.sending.sms.index', compact('messages'));
    }

    public function create()
    {
        $projects = Project::with('type')->get()->groupBy('project_type_id');

        $parameters = $this->message->docParameters();

        return view('admin.sending.sms.create', compact('parameters', 'projects'));
    }

    public function test()
    {
        $volunteers = Volunteer::whereNotNull('phone')->orderBy('surname')->select('name', 'surname', 'id')->get()->pluck('fullName','id')->toArray();

        return view('admin.sending.sms.send-test', compact('volunteers'));
    }


    public function sendTest(Request $request)
    {
        $template = $request->get('content');

        $volunteer = Volunteer::find($request->get('volunteer_id'));
        $params = $volunteer->getDocParamValues();

        $template = str_replace(array_keys($params), $params, $template);
        $template = preg_replace('/%%(.*)%%/', '', $template);

        \WebSMS::from($volunteer->phone)->content($template)->send();

        return response()->json(['success']);
    }

    public function send(Request  $request)
    {
        $volunteers = Volunteer::whereIn('id', $request->get('package', []))->get();

        $subject = $request->get('subject');
        $content = $request->get('content');

        $sms = Message::create([
            "user_id" => auth()->user()->id,
            'message_type_id' => 2,
            "subject" => $subject,
            "content" => strip_tags($content),
            "send_time" => date("Y-m-d H:i:s"),
        ]);

        foreach($volunteers as $volunteer) {
            $this->dispatch(new SendSmsMessage($sms, $volunteer));
        }

        return redirect()->to(url('admin/sending/sms'));
    }
}
