<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\Project;
use App\Models\Volunteer;
use App\Repositories\MessageRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

class NewsController extends Controller
{
    public function index()
    {
        $news = News::owner()->with( 'user', 'project')->withCount('notifications', 'readNotifications')->latest()->paginate(100);

        return view('admin.news.index', compact('news'));
    }

    public function create()
    {
        $projects = Project::with('type')->get()->groupBy('project_type_id');

        return view('admin.news.create', compact('projects'));
    }

    public function store(Request $request)
    {
        $request->request->add(['user_id' => \Auth::user()->id]);

        if($request->has('content')){
            $content = $request->get("content");
            $dom = new \DOMDocument();
            $content = str_replace('o:p', 'p', $content);
            @$dom->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
            $images = $dom->getElementsByTagName('img');

            foreach ($images as $image) {
                $old_src = $image->getAttribute('src');
                if(is_absolute_path($old_src));
                {
                    $new_src = url($old_src);
                }
                $image->setAttribute('src', $new_src);

                $image_style = $image->getAttribute('style');
                $image->setAttribute("style", $image_style.' max-width: 100%;');
            }

            $content = $dom->saveHTML();
            $request->request->set("content", $content);
        }

        $request->request->add(['filters' => $request->except(['_token', 'user_id', 'content', 'package', 'title', 'attachments'])]);

        if($request->has('attachments')){
            $attachments = $request->get('attachments');
            $attachments = array_values($attachments);
            $request->request->set('attachments', $attachments);
        }

        $news = News::create($request->all());

        if(config('fcm.enabled')) {
            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60 * 10);

            $notificationBuilder = new PayloadNotificationBuilder($news->title);
            $notificationBuilder->setBody(str_replace(array("\n","\r\n","\r"), '', Str::limit(strip_tags( getbody($news->content) ), 180) ))
                ->setSound('default');

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData([
                'id' => $news->id,
                'type' => 'news',
                'title' => $news->title,
                'content' => getbody($news->content),
                'unread' => true,
                'date' => $news->created_at,
                'author' => $news->user->name,
                'photo' => $news->user->avatar ? $news->user->avatar : asset('images/person.png')
            ]);

            $option = $optionBuilder->build();
            $data = $dataBuilder->build();
        }

        foreach($request->get('package', []) as $volunteer_id)
        {
            $volunteer = Volunteer::find($volunteer_id);
            $news->notifications()->create([
                'volunteer_id' => $volunteer->id
            ]);

            if(config('fcm.enabled') && $volunteer->firebase_uuid)
            {
                $notificationBuilder->setBadge($volunteer->news()->unread()->count() + $volunteer->tasks()->unread()->count());
                $notification = $notificationBuilder->build();

                FCM::sendTo($volunteer->firebase_uuid, $option, $notification, $data);
            }
        }

        return redirect()->to(url('admin/news'));
    }

    public function edit($news_id)
    {
        $news = News::owner()->find($news_id);

        $projects = ['' => '--- wybierz projekt ---'] + Project::pluck('name', 'id')->toArray();

        return view('admin.news.edit', compact('news', 'projects'));
    }

    public function update(Request $request, $news_id)
    {
        $news = News::owner()->find($news_id);

        if($request->has('content')){
            $content = $request->get("content");
            $dom = new \DOMDocument();
            $content = str_replace('o:p', 'p', $content);
            @$dom->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
            $images = $dom->getElementsByTagName('img');

            foreach ($images as $image) {
                $old_src = $image->getAttribute('src');
                if(is_absolute_path($old_src));
                {
                    $new_src = url($old_src);
                }
                $image->setAttribute('src', $new_src);

                $image_style = $image->getAttribute('style');
                $image->setAttribute("style", $image_style.' max-width: 100%;');
            }

            $content = $dom->saveHTML();
            $request->request->set("content", $content);
        }

        $news->update($request->all());

        return redirect()->to(url('admin/news'));
    }

    public function delete($news_id)
    {
        $news = News::owner()->find($news_id);

        return view('admin.news.delete', compact('news'));
    }

    public function remove($news_id)
    {
        $news = News::owner()->find($news_id);
        $news->delete();

        return response()->json(['code' => 0]);
    }
}
