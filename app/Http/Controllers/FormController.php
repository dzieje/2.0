<?php

namespace App\Http\Controllers;

use App\Mail\Mailing;
use App\Models\Guardian;
use App\Models\ProjectVolunteer;
use App\Models\Setting;
use App\Models\Survey;
use App\Models\Volunteer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;

class FormController extends Controller
{
    protected $survey;

    /**
     * FormController constructor.
     */
    public function __construct(Request $request)
    {
        $this->survey = Cache::remember('surveys.'.$request->route('slug'), 3600, function () use($request){
            return Survey::where('slug', $request->route('slug'))->first();
        });
    }

    public function open($slug, $volunteer_uuid = null)
    {
        $volunteer = Volunteer::where('uuid', $volunteer_uuid)->whereNotNull('uuid')->first();

        $fields = [];
        foreach(json_decode($this->survey->form) as $field)
        {
            if(!in_array($field->type, ['date', 'header', 'paragraph', 'radio-group'])) {
                $attribute = $this->survey->relatedProject->attributes()->where('slug', $field->name)->first();
                if ($attribute && $attribute->is_collection) {
                    $field->type = 'collection';
                    $field->attribute = $attribute;
                }
            }

            $fields[] = $field;
        }


        return view('surveys.open', ['survey' => $this->survey, 'volunteer' => $volunteer, 'fields' => $fields]);
    }

    public function submit(Request $request, $slug, $volunteer_uuid = null)
    {
        if($request->get('date_of_birth')) {
            $request->request->set('date_of_birth', new Carbon($request->get('date_of_birth')));
        }

        \DB::beginTransaction();
        if($volunteer_uuid){
            $volunteer = Volunteer::where('uuid', $volunteer_uuid)->whereNotNull('uuid')->first();
            if($volunteer) {
                $volunteer->update($request->all());
            }else{
                $volunteer = Volunteer::create($request->all());
            }
        }else{
            $volunteer = Volunteer::create($request->all());

        }

        if($this->survey->registration)
        {
            ProjectVolunteer::create([
                'project_id' => $this->survey->project_id,
                'volunteer_id' => $volunteer->id
            ]);
        }

        $entry = $this->survey->entries()->create([
            'volunteer_id' => $volunteer->id
        ]);

        foreach($request->get('question', []) as $question_id => $answer)
        {
            $entry->answers()->create([
                'question_id' => $question_id,
                'question_type' => 'App\Models\SurveyQuestion',
                'answer' => $answer
            ]);
        }


        foreach($request->get('agreements', []) as $question_id => $answer)
        {
            $entry->answers()->create([
                'question_id' => $question_id,
                'question_type' => 'App\Models\SurveyAgreement',
                'answer' => $answer
            ]);
        }

        if($request->has('guardian_name') && $request->get('guardian_name') != ''){
            if($request->has('guardian_permission_single') && $request->get('guardian_permission_single') == 1)
            {
                $permission = 1;
            }elseif($request->has('guardian_permission_multi')) {
                $permission = $request->get('guardian_permission_multi');
            }else{
                $permission = null;
            }
            $guardian = Guardian::create([
                'name' => $request->get('guardian_name'),
                'surname' => $request->get('guardian_surname'),
                'date_of_birth' => $request->get('guardian_date_of_birth') ? new Carbon($request->get('guardian_date_of_birth')) : null,
                'street' => $request->get('guardian_street'),
                'house_number' => $request->get('guardian_house_number'),
                'flat_number' => $request->get('guardian_flat_number'),
                'post_code' => $request->get('guardian_post_code'),
                'city' => $request->get('guardian_city'),
                'phone' => $request->get('guardian_phone'),
                'email' => $request->get('guardian_email'),
                'guardian_permission'   =>  $permission
            ]);
            $volunteer->update(['guardian_id' => $guardian->id]);
        }

        $template = $this->survey->response_mail_content;

        $params = $volunteer->getDocParamValues();

        $template = str_replace(array_keys($params), $params, $template);

        Mail::to($volunteer->email)->send(new Mailing($this->survey->relatedProject->settingMailTemplate(), $template, $this->survey->response_mail_subject));

        \DB::commit();

        if(! $volunteer->login) {
            $setting = Setting::where('key', 'registration_email')->first();
            if($setting && $setting->value) {
                $template = $setting->value->content;
                $params = $volunteer->getDocParamValues();
                $template = str_replace(array_keys($params), $params, $template);

                Mail::to($volunteer->email)->send(new Mailing($this->survey->relatedProject->settingMailTemplate(), $template, $setting->value->subject));
            }
        }

        $survey = $this->survey;
        return view('surveys.complete', compact('volunteer', 'survey'));
    }
}
