<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index()
    {
        $user = \Auth::user();
        return view('admin.profile.index', compact('user'));
    }

    public function changeAvatar(Request  $request)
    {
        $url = $request->get('url');
        $user = \Auth::user();
        $user->update(['avatar' => url($url) ]);

        return response()->json([]);
    }
}
