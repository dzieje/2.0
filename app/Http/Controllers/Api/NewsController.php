<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\News;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class NewsController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::guard('api')->user();

        $news = News::whereHas('notifications', function($query) use($user){
            $query->where('volunteer_id', $user->id);
        })->with('user', 'notifications')->latest()->paginate(20);

        foreach($news as $news_item)
        {
            $news_item->content = str_replace(array("\n","\r\n","\r"), '',  Str::limit(strip_tags( getbody($news_item->content) ), 180));
            $news_item->unread = $news_item->notifications->where('volunteer_id', $user->id)->first()->read_at ? false : true;
            $news_item->date = $news_item->created_at;
            $news_item->author = $news_item->user->name;
            $news_item->photo = $news_item->user->avatar ? $news_item->user->avatar : asset('images/person.png');
            $news_item->project = $news_item->project ? $news_item->project->name : null;
        }

        $custom = collect(
            [
                'tasks_unread_count' => $user->tasks()->unread()->count(),
                'news_unread_count' => $user->news()->unread()->count(),
            ]
        );

        $data = $custom->merge($news);

        return response()->json($data);
    }

    public function show(Request $request, $news_id)
    {
        $user = Auth::guard('api')->user();

        $news = News::find($news_id);

        if(! $news->notifications->where('volunteer_id', $user->id)->first()->read_at )
        {
            $news->notifications->where('volunteer_id', $user->id)->first()->update(['read_at'=> Carbon::now()]);
        }

        $news = [
            'title' => $news->title,
            'content' => getbody($news->content),
            'unread' => false,
            'date' => $news->created_at,
            'author' => $news->user->name,
            'project' => $news->project ? $news->project->name : null,
            'photo' => $news->user->avatar ? $news->user->avatar : asset('images/person.png'),
            'attachments' => $news->attachments
        ];

        return response()->json($news);
    }
}
