<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function info(Request $request)
    {
        $user = Auth::guard('api')->user();

        return [
            'avatar' => $user->avatar_filename ? asset('avatars/'.$user->avatar_filename) : null,
            'gender' => $user->gender,
            'name' => $user->name,
            'surname' => $user->surname,
            'phone' => $user->phone,
            'email' => $user->email,
            'login' => $user->login,
            'date_of_birth' => $user->date_of_birth
        ];
    }

    public function firebaseUiid(Request $request)
    {
        $user = Auth::guard('api')->user();

        $user->update(['firebase_uuid' => $request->get('firebase_uuid')]);

        return response()->json(['status' => 'success']);
    }

    public function logout()
    {
        $result = Auth::guard('api')->user()->token()->revoke();

        if($result){
            $response = response()->json(['error'=>false,'message'=>'User logout successfully.','result'=>[]],200);
        }else{
            $response = response()->json(['error'=>true,'message'=>'Something is wrong.','result'=>[]],400);
        }
        return $response;
    }

    public function avatar(Request $request)
    {
        $request->validate([
            'avatar' => 'required|file',
        ]);

        $avatar_name = \Str::random().'.'.$request->file('avatar')->extension();
        $request->file('avatar')->storePubliclyAs(
            'avatars', $avatar_name
        );

        Auth::guard('api')->user()->update(['avatar_filename' => $avatar_name]);

        return response()->json(['status' => 'success']);
    }
}
