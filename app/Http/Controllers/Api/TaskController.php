<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class TaskController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::guard('api')->user();

        $tasks = Task::whereHas('notifications', function($query) use($user){
            $query->where('volunteer_id', $user->id);
        })->with('user', 'notifications')->latest()->paginate(20);

        foreach($tasks as $task)
        {
            $task->content = str_replace(array("\n","\r\n","\r"), '',  Str::limit(strip_tags( getbody($task->content) ), 180));
            $task->unread = $task->notifications->where('volunteer_id', $user->id)->first()->read_at ? false : true;
            $task->date = $task->created_at;
            $task->author = $task->user->name;
            $task->photo = $task->user->avatar ? $task->user->avatar : asset('images/person.png');
            $task->structure = $task->node ? $task->node->name : null;
        }

        $custom = collect(
            [
                'tasks_unread_count' => $user->tasks()->unread()->count(),
                'news_unread_count' => $user->news()->unread()->count(),
            ]
        );

        $data = $custom->merge($tasks);

        return response()->json($data);
    }

    public function show(Request $request, $task_id)
    {
        $user = Auth::guard('api')->user();

        $task = Task::find($task_id);

        if(! $task->notifications->where('volunteer_id', $user->id)->first()->read_at )
        {
            $task->notifications->where('volunteer_id', $user->id)->first()->update(['read_at'=> Carbon::now()]);
        }

        $task = [
            'title' => $task->title,
            'content' => getbody($task->content),
            'unread' => false,
            'date' => $task->created_at,
            'author' => $task->user->name,
            'structure' => $task->structure = $task->node ? $task->node->name : null,
            'photo' => $task->user->avatar ? $task->user->avatar : asset('images/person.png'),
            'attachments' => $task->attachments,
            'valid_from'=> $task->valid_from,
            'valid_to'=> $task->valid_to,
        ];

        return response()->json($task);
    }
}
