<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Jobs\SendUserPasswordChangedEmail;
use App\Jobs\SendUserRegisteredMail;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Http\Request;
use Validator;

class UserController extends Controller
{
    public function index()
    {
        $users = User::paginate(session()->get('pagination'));

        return view('admin.settings.users.index', compact('users'));
    }

    public function edit($user_id)
    {
        $user = User::findOrFail($user_id);
        $roles= UserRole::pluck('name', 'id');
        $roles[''] = '--- wybierz rolę ---';

        return view('admin.settings.users.edit', compact('user', 'roles'));
    }

    public function update(Request $request, $user_id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'login' => 'required|max:255|unique:users,id,'.$user_id,
        ]);

        if($validator -> fails()){
            return json_encode(['error' => 'Użytkownik o podanym loginie już istnieje.']);
        }else{
            $user = User::find($user_id);
            $user->update($request->all());
        }

        flash('Dane użytkownika '.$user->name.' zostały zaktualizowane.');
        return json_encode(['code' => 0]);
    }

    public function password($user_id)
    {
        $user = User::find($user_id);
        return view('admin.settings.users.edit-password', compact('user'));
    }

    public function updatePassword(Request $request, $user_id)
    {
        $password = Str::random();

        $user = User::find($user_id);
        $user->password = bcrypt($password);
        $user->save();

        SendUserPasswordChangedEmail::dispatch($user, $password);

        flash('Hasło użytkownika '.$user->name.' zostało zmienione i wysłane na jego adres email.');
        return json_encode(['code' => 0]);
    }

    public function delete($user_id)
    {
        $user = User::find($user_id);
        return view('admin.settings.users.delete', compact('user'));
    }

    public function remove($user_id)
    {
        $user = User::find($user_id);
        $user->delete();

        flash('Użytkownik '.$user->name.' został usunięty.');
        return json_encode(['code' => 0]);
    }

    public function create()
    {
        $roles = UserRole::pluck('name', 'id');
        $roles[''] = '--- wybierz rolę ---';
        return view('admin.settings.users.create', compact('roles'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'login' => 'required|max:255|unique:users',
            'email' => 'required',
        ]);

        if($validator -> fails()){
            return json_encode(['error' => $validator->errors()->all()]);
        }else{
            $password = Str::random();

            $request->request->set('password', bcrypt($password) );
            $user = User::create($request->all());

            SendUserRegisteredMail::dispatch($user, $password);
        }

        flash('Użytkownik został utworzony. Hasło zostało wysłany na podany adres email.');
        return json_encode(['code' => 0]);
    }

    public function searchUser(Request $request)
    {
        $q = $request->get('q');
        $users = User::where('name', 'like', $q.'%')->get();

        $response = ['users' => []];
        foreach ($users as $user)
        {
            $response['users'][] = ['id' => $user->id,  'name' => $user->name];
        }

        return response()->json($response);
    }
}
