<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\Volunteer;
use App\Models\VolunteerAttribute;
use App\Repositories\MessageRepository;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        $settings = Setting::all();

        return view('admin.settings.variables.index', compact('settings'));
    }

    public function edit($setting_id)
    {
        $setting = Setting::find($setting_id);
        $parameters = (new MessageRepository())->docParameters();

        if(! $setting->value)
        {
            $setting->update(['value' => Volunteer::initColumnFields()]);
        }

        return view('admin.settings.variables.edit', compact('setting', 'parameters'));
    }

    public function update(Request  $request, $setting_id)
    {
        $setting = Setting::find($setting_id);

        if($request->get('sortable') == 1){
            $json = [];
            foreach($request->get('attributes') as $attribute){
                if(!isset($attribute['checked'])) $attribute['checked'] = 0;
                $json[] = $attribute;
            }

            $request->request->remove('sortable');
            $request->request->set('attributes', $json);
        }

        if($request->has('content')){
            $content = $request->get("content");
            $dom = new \DOMDocument();
            $content = str_replace('o:p', 'p', $content);
            @$dom->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
            $images = $dom->getElementsByTagName('img');

            foreach ($images as $image) {
                $old_src = $image->getAttribute('src');
                if(is_absolute_path($old_src));
                {
                    $new_src = url($old_src);
                }
                $image->setAttribute('src', $new_src);
            }

            $content = $dom->saveHTML();
            $request->request->set("content", $content);
        }

        $setting->update([
           'value' => $request->except('_token')
        ]);

        return redirect()->to(url('admin/settings/variables'));
    }

    public function previewMail(Request  $request, $setting_id)
    {
        $setting = Setting::find($setting_id);
        $template = optional($setting->value)->content;

        $volunteer = Volunteer::find($request->get('volunteer_id'));
        $params = $volunteer->getDocParamValues();

        $template = str_replace(array_keys($params), $params, $template);
        return view('admin.settings.variables.mail-preview', compact('template'));
    }
}
