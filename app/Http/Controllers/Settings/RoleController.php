<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\UserRole;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function getIndex()
    {
        $roles = UserRole::with('permissions')->get();

        return view('admin.settings.roles.index', compact('roles'));
    }

    public function getEdit($role_id)
    {
        $role = UserRole::with('permissions')->find($role_id);
        $permissions = Permission::pluck('name', 'id');

        return view('admin.settings.roles.edit', compact('role', 'permissions'));
    }

    public function postUpdate(Request $request, $role_id)
    {
        $role = UserRole::find($role_id);
        $role->update($request->all());
        $role->permissions()->sync($request->get('permissions', []));

        \Flash::info('Dane zostały zaktualizowane.');
        return response()->json(['code' => 0]);
    }

    public function getDelete($role_id)
    {
        $role = UserRole::find($role_id);

        return view('admin.settings.roles.delete', compact('role'));
    }

    public function postDelete($role_id)
    {
        $role = UserRole::find($role_id);

        foreach($role->users as $user)
        {
            $user->update(['user_role_id' => null]);
        }

        $role->delete();

        \Flash::info('Rola została usunięta.');
        return response()->json(['code' => 0]);
    }

    public function getCreate()
    {
        $permissions = Permission::pluck('name', 'id');

        return view('admin.settings.roles.create', compact('permissions'));
    }

    public function postStore(Request $request)
    {
        $role = UserRole::create($request->all());
        $role->permissions()->sync($request->get('permissions', []));

        \Flash::info('Rola została utworzona.');
        return response()->json(['code' => 0]);
    }
}
