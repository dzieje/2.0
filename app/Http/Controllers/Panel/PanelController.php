<?php

namespace App\Http\Controllers\Panel;

use App\Http\Requests\VolunteerSubmit;
use App\Models\News;
use App\Models\Task;
use App\Models\Volunteer;
use App\Http\Controllers\Controller;
use App\Models\VolunteerAttribute;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PanelController extends Controller
{

    /**
     * PanelController constructor.
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $tasks_count =  auth()->user()->nodes()->withCount('tasks')->get()->sum('tasks_count') ;

            view()->composer('*', function ($view) use($tasks_count){
                $view->with('tasks_count',  $tasks_count);
            });

            return $next($request);
        });

    }

    public function news()
    {
        $news = News::whereHas('notifications', function($query){
            $query->where('volunteer_id', auth()->user()->id);
        })->with('user', 'notifications')->latest()->paginate(10);

        return view('panel.news', compact('news'));
    }

    public function tasks()
    {
        $tasks = Task::whereHas('node', function ($query){
            $query->whereHas('volunteers', function($query){
               $query->where('volunteer_id', auth()->user()->id);
            });
        })->with('node')->orderBy('valid_from')->paginate(15);

        return view('panel.tasks', compact('tasks'));
    }

    public function calendar()
    {
        return view('panel.calendar');
    }

    public function calendarFeed()
    {
        $tasks = [];
        Task::whereHas('node', function ($query){
            $query->whereHas('volunteers', function($query){
                $query->where('volunteer_id', auth()->user()->id);
            });
        })->get()->each(function ($task) use(&$tasks){
            $tasks[] = [
                'title' => $task->title,
                'start' => $task->valid_from,
                'end' => $task->valid_to,
                'allDay' => true
            ];
        });

        return response()->json($tasks);
    }

    public function editProfile()
    {
        $attributes = VolunteerAttribute::get();
        $volunteer = auth()->user();
        return view('panel.edit-profile', compact('attributes', 'volunteer'));
    }

    public function updateProfile(Request $request)
    {
        auth()->user()->update($request->all());

        if($request->has('avatar')){
            $avatar_filename = Str::random().'.'.$request->file('avatar')->extension();
            $request->file('avatar')->move('avatars', $avatar_filename);
            auth()->user()->update(['avatar_filename' => $avatar_filename]);
        }

        return redirect()->to(url('panel'));
    }

    public function projects()
    {
        return view('panel.projects');
    }
}
