<?php

namespace App\Http\Controllers\Project;

use App\Http\Controllers\ExtendedController;
use App\Models\Volunteer;
use App\Models\VolunteerAttribute;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ParticipantController extends ExtendedController
{
    public function index(Request  $request)
    {
        $query = Volunteer::project($this->project->id);

        if($request->has('name') && $request->get('name') != ''){
            $query->where('name', 'like', '%'.$request->get('name').'%');
        }
        if($request->has('surname') && $request->get('surname') != ''){
            $query->where('surname', 'like', '%'.$request->get('surname').'%');
        }
        if($request->has('gender') && $request->get('gender') != '0')
        {
            $query->where('gender', $request->get('gender'));
        }

        if($request->has('age_from') && $request->get('age_from') != ''){
            $birth = Carbon::now()->subYears($request->get('age_from'))->format('Y-m-d');

            $query->where('date_of_birth', '<=', $birth);
        }

        if($request->has('age_to') && $request->get('age_to') != ''){
            $birth = Carbon::now()->subYears($request->get('age_to'))->subYear()->addDay()->format('Y-m-d');

            $query->where('date_of_birth', '>=', $birth);
        }

        $participants = $query->orderBy('surname')->paginate(100);

        $attributes = VolunteerAttribute::whereHas('projects', function ($query){
            $query->where('project_id', $this->project->id);
        })->get();

        return view('admin.project.participants.index', compact('participants',  'attributes'));
    }

    public function export(Request $request)
    {
        $filename = 'Zestawienie aktorow';

        \Excel::create($filename, function($excel) use($request){
            $excel->sheet('zestawienie', function($sheet) use($request) {

                $sheet->appendRow(array(
                    'nazwisko i imię',
                    'wiek',
                    'telefon',
                    'email',
                    'wzrost'
                ));

                $volunteers = Volunteer::where('if_registered', 1)->whereIn('if_actor', [1,2])
                    ->where(function($query) use($request){
                        if($request->has('name') && $request->get('name') != ''){
                            $query->where('name', 'like', '%'.$request->get('name').'%');
                        }
                        if($request->has('surname') && $request->get('surname') != ''){
                            $query->where('surname', 'like', '%'.$request->get('surname').'%');
                        }

                        if($request->has('gender') && $request->get('gender') != '0')
                        {
                            $query->where('gender', $request->get('gender'));
                        }

                        if($request->has('age_from') && $request->get('age_from') != ''){
                            $birth = Carbon::now()->subYears($request->get('age_from'))->format('Y-m-d');

                            $query->where('date_of_birth', '<=', $birth);
                        }

                        if($request->has('age_to') && $request->get('age_to') != ''){
                            $birth = Carbon::now()->subYears($request->get('age_to'))->subYear()->addDay()->format('Y-m-d');

                            $query->where('date_of_birth', '>=', $birth);
                        }

                        if($request->has('group') && $request->get('group') != 0){
                            if($request->get('group') == '-1')
                            {
                                $query->whereDoesntHave('groups');
                            }else {
                                $group_id = $request->get('group');
                                $query->whereHas('groups', function ($query) use($group_id){
                                    $query->where('n_group_id', $group_id);
                                });
                            }
                        }

                        if($request->has('role') && $request->get('role') != 0){
                            $role_id = $request->get('role');
                            $query->whereHas('roles', function ($query) use($role_id){
                                $query->where('n_role_id', $role_id);
                            });
                        }

                    })
                    ->orderBy('surname')->with( 'guardian')->get();

                foreach ($volunteers as $volunteer)
                {
                    $age = $volunteer->date_of_birth->diffInYears(Carbon::now());
                    $sheet->appendRow(array(
                        $volunteer->surname.' '.$volunteer->name,
                        $age,
                        str_replace(' ', '', ltrim($volunteer->phone) ),
                        $volunteer->email,
                        $volunteer->height
                    ));
                }

            });
        })->store('xlsx', storage_path('excel'));

        Session::put('download.in.the.next.request', url('admin/volunteers/download-doc', array($filename) ));
        return response()->json([
            'code'  => 0
        ]);
    }

    public function show(Request $request)
    {
        $participant = Volunteer::find($request->route('participant_id'));

        return view('admin.project.participants.show', compact('participant'));
    }

    public function edit(Request $request)
    {
        $participant = Volunteer::find($request->route('participant_id'));

        $attributes = $this->project->attributes;

        return view('admin.project.participants.edit', compact('participant', 'attributes'));
    }

    public function update(Request $request)
    {
        $participant = Volunteer::find($request->route('participant_id'));
        $participant->update($request->all());

        return redirect()->to(url('admin/project/'.$this->project->slug.'/participants'));
    }

    public function delete(Request $request)
    {
        $participant = Volunteer::find($request->route('participant_id'));

        return view('admin.project.participants.delete', compact('participant'));
    }

    public function remove(Request $request){
        $participant = Volunteer::find($request->route('participant_id'));
        $participant->projects()->detach($this->project->id);

        flash('Uczestnik '.$participant->surname.' '.$participant->name.' został odpięty.');
        return json_encode(['code' => 0]);
    }

    public function generateContract(Request $request)
    {
        $volunteer = Volunteer::find($request->route('participant_id'));

        $setting = $this->project->settingContractTemplate();

        $template = $setting->value->contract_content[0];
        $params = $volunteer->getDocParamValues();
        $contract_number = $volunteer->projectVolunteers->where('project_id', $this->project->id)->first()->contract;
        $params['%%CONTRACT_NUMBER%%'] = $contract_number;

        $template = str_replace(array_keys($params), $params, $template);



        $pdf = \PDF::loadView('admin.templates.contract', ['template' => $template])->setPaper('a4')
            ->setOption('margin-top', '2.5cm')
            ->setOption('margin-right', '2.5cm')
            ->setOption('margin-bottom', '2.5cm')
            ->setOption('margin-left', '2.5cm');

        $filename = 'umowa_'.$volunteer->name.'_'.$volunteer->surname.'_';
        $filename = normalizeString($filename);
        return $pdf->download($filename.'.pdf');
    }
}
