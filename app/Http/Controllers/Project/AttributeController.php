<?php

namespace App\Http\Controllers\Project;

use App\Exports\ProjectParticipantAttributes;
use App\Http\Controllers\ExtendedController;
use App\Models\VolunteerAttribute;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Rinvex\Attributes\Models\Type\Varchar;

class AttributeController extends ExtendedController
{
    public function index()
    {
        $attributes = $this->project->attributes;

        return view('admin.project.attributes.index', compact('attributes'));
    }

    public function export(Request  $request)
    {
        $attribute = VolunteerAttribute::find( $request->route('attribute_id') );

        return Excel::download(new ProjectParticipantAttributes($attribute), 'zestawienie.xlsx');
    }
}
