<?php


namespace App\Http\Controllers;

use App\Models\ProjectSetting;
use App\Repositories\MessageRepository;
use Faker\Factory;
use Illuminate\Http\Request;

class SettingController extends ExtendedController
{
    public function index()
    {
        $settings = $this->project->settings;

        return view('admin.project.settings.index', compact('settings'));
    }

    public function edit(Request $request)
    {
        $setting = ProjectSetting::find($request->route('setting_id'));

        $parameters = (new MessageRepository)->docParameters();

        return view('admin.project.settings.edit', compact('setting', 'parameters'));
    }

    public function update(Request  $request)
    {
        $setting = ProjectSetting::find($request->route('setting_id'));

        if($request->has('header')){
            $content = $request->get("header");
            $dom = new \DOMDocument();
            $content = str_replace('o:p', 'p', $content);
            @$dom->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
            $images = $dom->getElementsByTagName('img');

            foreach ($images as $image) {
                $old_src = $image->getAttribute('src');
                if(is_absolute_path($old_src));
                {
                    $new_src = url($old_src);
                }
                $image->setAttribute('src', $new_src);
            }

            $content = $dom->saveHTML();
            $request->request->set("header", $content);
        }

        if($request->has('footer')){
            $content = $request->get("footer");
            $dom = new \DOMDocument();
            $content = str_replace('o:p', 'p', $content);
            @$dom->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
            $images = $dom->getElementsByTagName('img');

            foreach ($images as $image) {
                $old_src = $image->getAttribute('src');
                if(is_absolute_path($old_src));
                {
                    $new_src = url($old_src);
                }
                $image->setAttribute('src', $new_src);
            }

            $content = $dom->saveHTML();
            $request->request->set("footer", $content);
        }

        $setting->update([
            'value' => $request->except('_token')
        ]);

        return redirect()->to(url('admin/'.$this->project->type->slug.'/'.$this->project->slug.'/settings'));
    }

    public function previewMail(Request  $request)
    {
        $setting = ProjectSetting::find($request->route('setting_id'));
        $faker = Factory::create();
        $template = $faker->sentence(20);

        return view('admin.project.settings.mail-preview', compact('template', 'setting'));
    }

    public function appendTemplate()
    {
        return view('admin.project.settings.contract-template');
    }
}
