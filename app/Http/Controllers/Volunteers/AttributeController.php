<?php

namespace App\Http\Controllers\Volunteers;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\VolunteerAttribute;
use Illuminate\Http\Request;
use Rinvex\Attributes\Models\Attribute;

class AttributeController extends Controller
{
    public function index()
    {
        $attributes = VolunteerAttribute::with('projects')->get();

        return view('admin.volunteers.attributes.index', compact('attributes'));
    }

    public function edit($attribute_id)
    {
        $attribute = VolunteerAttribute::find($attribute_id);
        $projects = Project::with('type')->get();


        return view('admin.volunteers.attributes.edit', compact('attribute', 'projects'));
    }

    public function update(Request  $request, $attribute_id)
    {
        $attribute = VolunteerAttribute::find($attribute_id);
        $attribute->update(['name' => $request->get('name')]);

        $attribute->projects()->sync($request->get('projects', []));

        if($attribute->is_collection){
            $attribute->update(['description' => json_encode($request->get('values', [])), 'multiple_choice' => $request->get('multiple_choice', 0) ]);
        }

        return redirect()->to(url('admin/volunteers/attributes'));
    }

    public function create()
    {
        $types = array_combine(array_keys( VolunteerAttribute::typeMap()) , array_keys( VolunteerAttribute::typeMap()));
        $projects = Project::with('type')->get();

        $types['collection'] = 'lista';

        return view('admin.volunteers.attributes.create', compact('types', 'projects'));
    }

    public function store(Request $request)
    {
        $type = $request->get('type');
        $collection = 0;
        if($type == 'collection'){
            $type = 'tekstowy';
            $collection = 1;
        }
        $attribute = VolunteerAttribute::create([
            'type' => $type,
            'name' => $request->get('name'),
            'is_collection' => $collection,
            'entities' => ['App\Models\Volunteer'],
            'description' => ($collection ? json_encode($request->get('values', [])) : null),
            'multiple_choice' => $request->get('multiple_choice', 0)
        ]);

        foreach ($request->get('projects', []) as $project_id)
        {
            $project = Project::find($project_id);
            $project->attributes()->attach($attribute->id);
        }

        return redirect()->to(url('admin/volunteers/attributes'));
    }

    public function delete(Request $request)
    {
        $attribute = VolunteerAttribute::find($request->route('attribute_id'));

        return view('admin.volunteers.attributes.delete', compact('attribute'));
    }

    public function remove(Request $request)
    {
        $attribute = VolunteerAttribute::find($request->route('attribute_id'));
        $attribute->projects()->detach();
        $attribute->delete();

        return response()->json(['code' => 0]);
    }
}
