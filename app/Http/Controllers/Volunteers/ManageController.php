<?php


namespace App\Http\Controllers\Volunteers;


use App\Models\User;
use App\Models\Volunteer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageController extends Controller
{
    public function selectVolunteer(Request $request)
    {
        $redirect_url = $request->get('url');

        return view('admin.volunteers.select-volunteer', compact('redirect_url'));
    }

    public function searchVolunteer(Request $request)
    {
        $q = $request->get('q');
        $query = Volunteer::where(function($query) use($q){
            $query->where('name', 'like', $q.'%')->orWhere('surname', 'like', $q.'%');
        });

        $volunteers = $query->get();

        $response = ['volunteers' => []];
        foreach ($volunteers as $volunteer)
        {
            $response['volunteers'][] = ['id' => $volunteer->id,  'name' => $volunteer->name.' '.$volunteer->surname];
        }

        return response()->json($response);
    }

    public function proceedSelection(Request $request)
    {
        $volunteer_id = $request->get('volunteer_id');
        $url = $request->get('url');

        return response()->json(['code' => 4, 'url' => $url.'?volunteer_id='.$volunteer_id]);
    }



}
