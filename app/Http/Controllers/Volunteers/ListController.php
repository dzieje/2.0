<?php

namespace App\Http\Controllers\Volunteers;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\ProjectVolunteer;
use App\Models\Setting;
use App\Models\Volunteer;
use App\Models\VolunteerAttribute;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;

class ListController extends Controller
{
    public function index(Request $request)
    {
        $attributes = VolunteerAttribute::with('projects')->has('projects')->get();

        $query = Volunteer::when($request->get('name'), function($query) use($request){
            $query->where('name', 'like', $request->get('name').'%');
        })->when($request->get('surname'), function ($query) use($request){
            $query->where('surname', 'like', $request->get('surname').'%');
        })->when($request->get('gender'), function ($query) use($request){
            $query->where('gender', $request->get('gender'));
        })->when($request->get('age_from'), function ($query) use($request){
            $birth = Carbon::now()->subYears($request->get('age_from'))->format('Y-m-d');
            $query->where('date_of_birth', '<=', $birth);
        })->when($request->get('age_to'), function ($query) use($request){
            $birth = Carbon::now()->subYears($request->get('age_to'))->subYear()->addDay()->format('Y-m-d');
            $query->where('date_of_birth', '>=', $birth);
        })->when($request->get('project'), function ($query) use($request){
            $project = Cache::remember('projects.'.$request->get('project'), 3600, function () use($request){
                return Project::with('type')->where('slug', $request->get('project'))->first();
            });
            $query->whereHas('projects', function($query) use($project){
               $query->where('project_id', $project->id);
            });
        })->when($request->get('profile'), function($query) use($request){
            if($request->get('profile') == 'yes'){
                $query->whereNotNull('login');
            }else{
                $query->whereNull('login');
            }
        });

        foreach($attributes as $attribute){
            if($request->get($attribute->slug)){
                $query->hasAttribute($attribute->slug, $request->get($attribute->slug));
            }
        }

        $volunteers = $query->with('projects')->paginate( Session::get('settings.paginate', 100) );

        $projects = Project::pluck('name','slug');

        $columns = Setting::where('key', 'volunteer_attributes_list')->first()->value ? Setting::where('key', 'volunteer_attributes_list')->first()->value->attributes : [];

        return view('admin.volunteers.index', compact('volunteers', 'attributes', 'projects', 'columns'));
    }

    public function edit($volunteer_id)
    {
        $volunteer = Volunteer::find($volunteer_id);

        $attributes = VolunteerAttribute::has('projects')->get();

        return view('admin.volunteers.edit', compact('volunteer', 'attributes'));
    }

    public function update(Request $request, $volunteer_id)
    {
        $volunteer = Volunteer::find($volunteer_id);
        $volunteer->update($request->all());

        flash('Dane wolontariusza zostały zaktualizowane.');

        return redirect()->to( url('admin/volunteers/list') );
    }

    public function projects($volunteer_id)
    {
        $volunteer = Volunteer::find($volunteer_id);

        $projects = Project::with('type')->get()->groupBy('project_type_id');

        return view('admin.volunteers.projects', compact('volunteer', 'projects'));
    }

    public function saveProjects(Request  $request, $volunteer_id)
    {
        $volunteer = Volunteer::find($volunteer_id);

        $volunteer->projectVolunteers()->delete();

        foreach($request->get('projects', []) as $project_id)
        {
            $volunteerProject = $volunteer->projectVolunteers()->onlyTrashed()->where('project_id', $project_id)->first();
            if($volunteerProject){
                $volunteerProject->restore();
            }else {
                ProjectVolunteer::create([
                    'project_id' => $project_id,
                    'volunteer_id' => $volunteer->id
                ]);
            }
        }

        return redirect()->to(url('admin/volunteers/list'));
    }

    public function delete($volunteer_id)
    {
        $volunteer = Volunteer::find($volunteer_id);

        return view('admin.volunteers.delete', compact('volunteer'));
    }

    public function remove(Request $request, $volunteer_id)
    {
        $volunteer = Volunteer::find($volunteer_id);
        $volunteer->delete();

        return response()->json(['code' => 0]);
    }
}
