<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends ExtendedController
{
    public function index()
    {
        $tasks = Task::whereHas('node', function($query){
            $query->whereHas('structure', function ($query){
                $query->project($this->project->id);
            });
        })->with('node')->orderBy('id', 'desc')->paginate(100);

        return view('admin.tasks.index', compact('tasks'));
    }

    public function show(Request $request)
    {
        $task = Task::find($request->route('task_id'));

        return view('admin.tasks.show', compact('task'));
    }
}
