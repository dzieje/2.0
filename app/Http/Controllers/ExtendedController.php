<?php


namespace App\Http\Controllers;


use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ExtendedController extends Controller
{
    protected $project;

    public function __construct(Request $request)
    {
        $this->project = Cache::remember('projects.'.$request->route('project_slug'), 3600, function () use($request){
            return Project::with('type')->where('slug', $request->route('project_slug'))->first();
        });

        $this->middleware('projectable:'.$this->project->id);

        view()->composer('*', function ($view) {
            $view->with('project',  $this->project );
        });


    }
}
