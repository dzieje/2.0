<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\ProjectType;
use App\Models\User;
use App\Models\VolunteerAttribute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ProjectController extends Controller
{
    public function index()
    {
        $projects = Project::with('type', 'contributors', 'user')->orderBy('year', 'desc')->get();
        return view('admin.projects.index', compact('projects'));
    }

    public function create()
    {
        $types = ProjectType::pluck('name', 'id');
        $types[''] = '--- wybierz typ ---';

        $users = User::where('id', '!=', auth()->user()->id)->pluck('name', 'id');

        return view('admin.projects.create', compact('types', 'users'));
    }

    public function store(Request $request)
    {
        \DB::beginTransaction();
        $request->request->add(['user_id' => auth()->user()->id]);
        $project = Project::create($request->all());
        $project->contributors()->sync($request->get('users', []));

        if($project->project_type_id == 1)
        {
            $attributes = VolunteerAttribute::where('group', 'show')->get();
            foreach ($attributes as $attribute) {
                $project->attributes()->attach($attribute->id);
            }
        }
        \DB::commit();

        return json_encode(['code' => 0]);
    }

    public function edit($project_id)
    {
        $project = Project::find($project_id);

        $types = ProjectType::pluck('name', 'id');
        $types[''] = '--- wybierz typ ---';

        $users = User::where('id', '!=', $project->user_id)->pluck('name', 'id');

        return view('admin.projects.edit', compact('project', 'types', 'users'));
    }

    public function update(Request $request, $project_id)
    {
        $project = Project::find($project_id);
        $project->update($request->all());

        $project->contributors()->sync($request->get('users', []));

        Cache::forget('projects.'.$project->slug);

        return json_encode(['code'=> 0]);
    }

    public function delete($project_id)
    {
        $project = Project::find($project_id);

        return view('admin.projects.delete', compact('project'));
    }

    public function remove($project_id)
    {
        $project = Project::find($project_id);
        $project->delete();

        return response()->json(['code' => 0]);
    }
}
