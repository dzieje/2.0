<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ExtendedController;
use App\Models\Survey;
use App\Models\SurveyAgreement;
use App\Models\SurveyEntry;
use App\Models\SurveyQuestion;
use App\Models\Volunteer;
use App\Models\VolunteerAttribute;
use App\Repositories\MessageRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SurveyController extends ExtendedController
{
    /**
     * @var MessageRepository
     */
    private $message;

    public function __construct(Request $request, MessageRepository $message)
    {
        parent::__construct($request);
        $this->message = $message;
    }

    public function index()
    {
        $surveys = Survey::project($this->project->id)->orderBy('id','desc')->paginate(100);

        return view('admin.surveys.index', compact('surveys'));
    }

    public function create()
    {
        return view('admin.surveys.create');
    }

    public function store(Request $request)
    {
        $request->request->add(['project_id' => $this->project->id]);

        if($request->get('registration') == 1){
            $request->request->add(['response_mail_content' => '<p class="font-lato" style="color: #7c7e7f;font-family: Lato,Tahoma,sans-serif;font-size: 13px; line-height: 22px;Margin-top: 14px;Margin-bottom: 0;">
                    Witaj %%IMIE%% %%NAZWISKO%%,
                </p>

                <p class="font-lato" style="color: #7c7e7f;font-family: Lato,Tahoma,sans-serif;font-size: 13px; line-height: 22px;Margin-top: 14px;Margin-bottom: 0;">
                    dziękujemy z dołączenie do projektu.
                </p>

                <p class="font-lato" style="color: #7c7e7f;font-family: Lato,Tahoma,sans-serif;font-size: 13px; line-height: 22px;Margin-top: 22px;Margin-bottom: 0;">
                    Pozdrawiamy!<br/>
                    %%APP_NAME%%
                </p>']);
        }

        $survey = Survey::create($request->all());

        return response()->json(['code' =>1, 'url' => url('admin/'.$this->project->type->slug.'/'.$this->project->slug.'/surveys/manage', [$survey->id])]);
    }

    public function manage(Request $request)
    {
        $survey = Survey::find( $request->route('survey_id') );
        return view('admin.surveys.manage', compact('survey'));
    }

    public function fields()
    {
        $attributes = $this->project->attributes;

        $fields = [
            [
                'label' => 'Płeć',
                'type' => 'radio-group',
                'name' => 'gender',
                'values' => [
                    [
                        'label' => 'mężczyzna',
                        'value' => 'male'
                    ],
                    [
                        'label' => 'kobieta',
                        'value' => 'female'
                    ]
                ]
            ],
            [
                'label' => 'Imię',
                'type' => 'text',
                'name' => 'name'
            ],
            [
                'label' => 'Nazwisko',
                'type' => 'text',
                'name' => 'surname'
            ],
            [
                'label' => 'Email',
                'type' => 'text',
                'subtype' => 'email',
                'name' => 'email'
            ],
            [
                'label' => 'Telefon',
                'type' => 'text',
                'subtype' => 'number',
                'name' => 'phone'
            ],
            [
                'label' => 'Data urodzenia',
                'type' => 'date',
                'name' => 'date_of_birth'
            ]
        ];
        foreach($attributes as $attribute)
        {
            switch ($attribute->type){
                case 'tekstowy':
                    $fields[] = [
                        'label' => $attribute->name,
                        'type' => 'text',
                        'name' => $attribute->slug
                    ];
                    break;
                case 'checkbox':
                    $fields[] = [
                        'label' => $attribute->name,
                        'type' => 'checkbox-group',
                        'name' => $attribute->slug,
                        "values" => [
                            [
                                "label" => $attribute->name,
                                "value" => 1
                            ]
                        ]
                    ];
                    break;
                case 'liczbowy':
                    $fields[] = [
                        'label' => $attribute->name,
                        'type' => 'number',
                        'name' => $attribute->slug
                    ];
                    break;
                case 'data':
                    $fields[] = [
                        'label' => $attribute->name,
                        'type' => 'date',
                        'name' => $attribute->slug
                    ];
                    break;
            }
        }

        return response()->json($fields);
    }

    public function addQuestion()
    {
        return view('admin.surveys.new-question');
    }

    public function addAgreement()
    {
        return view('admin.surveys.new-agreement');
    }

    public function edit(Request  $request){
        $survey = Survey::find( $request->route('survey_id') );

        return view('admin.surveys.edit', compact('survey'));
    }

    public function update(Request $request)
    {
        $survey = Survey::find($request->route('survey_id'));

        $survey->update($request->all());

        $question_ids = $survey->questions->pluck('id');
        $existing_questions_ids = array_keys( $request->get('existing_questions', []) );
        $question_diffs = array_diff($question_ids->toArray(), $existing_questions_ids);

        if(count($question_diffs) > 0){
            SurveyQuestion::whereIn('id', $question_diffs)->delete();
        }

        foreach($request->get('questions', []) as $question)
        {
            $survey->questions()->create([
                'question' => $question
            ]);
        }

        $agreement_ids = $survey->agreements->pluck('id');
        $existing_agreements_ids = array_keys( $request->get('existing_agreements', []) );
        $agreement_diffs = array_diff($agreement_ids->toArray(), $existing_agreements_ids);

        if(count($agreement_diffs) > 0){
            SurveyAgreement::whereIn('id', $agreement_diffs)->delete();
        }

        foreach($request->get('agreements', []) as $question)
        {
            $survey->agreements()->create([
                'question' => $question
            ]);
        }

        return redirect()->to(url('admin/'.$this->project->type->slug.'/'.$this->project->slug.'/surveys/manage', [$survey->id]));
    }

    public function editBase(Request  $request){
        $survey = Survey::find( $request->route('survey_id') );

        return view('admin.surveys.edit-base', compact('survey'));
    }

    public function updateBase(Request  $request)
    {
        $survey = Survey::find( $request->route('survey_id') );
        $survey->update($request->all());

        return response()->json(['code' => 0]);
    }

    public function responseMail(Request $request){
        $survey = Survey::find( $request->route('survey_id') );
        $parameters = $this->message->docParameters();

        return view('admin.surveys.response-mail', compact('survey', 'parameters'));
    }

    public function updateResponseMail(Request $request)
    {
        if($request->has('response_mail_content')){
            $content = $request->get("response_mail_content");
            $dom = new \DOMDocument();
            $content = str_replace('o:p', 'p', $content);
            @$dom->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
            $images = $dom->getElementsByTagName('img');

            foreach ($images as $image) {
                $old_src = $image->getAttribute('src');
                if(is_absolute_path($old_src));
                {
                    $new_src = url($old_src);
                }
                $image->setAttribute('src', $new_src);
            }

            $content = $dom->saveHTML();
            $request->request->set("response_mail_content", $content);
        }

        $survey = Survey::find( $request->route('survey_id') );

        $survey->update($request->all());

        return redirect()->to(url('admin/'.$this->project->type->slug.'/'.$this->project->slug.'/surveys/manage', [$survey->id]));
    }

    public function previewResponseMail(Request $request)
    {
        $survey = Survey::find( $request->route('survey_id') );
        $template = $survey->response_mail_content;

        $volunteer = Volunteer::find($request->get('volunteer_id'));
        $params = $volunteer->getDocParamValues();

        $template = str_replace(array_keys($params), $params, $template);

        $setting = $survey->relatedProject->settingMailTemplate();
        return view('admin.surveys.response-mail-preview', compact('setting','template'));
    }

    public function submittedView(Request $request)
    {
        $survey = Survey::find( $request->route('survey_id') );
        $parameters = $this->message->docParameters();

        return view('admin.surveys.submitted-view', compact('survey', 'parameters'));
    }

    public function updateSubmittedView(Request $request)
    {
        if($request->has('submitted_view')){
            $content = $request->get("submitted_view");
            $dom = new \DOMDocument();
            $content = str_replace('o:p', 'p', $content);
            @$dom->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
            $images = $dom->getElementsByTagName('img');

            foreach ($images as $image) {
                $old_src = $image->getAttribute('src');
                if(is_absolute_path($old_src));
                {
                    $new_src = url($old_src);
                }
                $image->setAttribute('src', $new_src);
            }

            $content = $dom->saveHTML();
            $request->request->set("submitted_view", $content);
        }

        $survey = Survey::find( $request->route('survey_id') );

        $survey->update($request->all());

        return redirect()->to(url('admin/'.$this->project->type->slug.'/'.$this->project->slug.'/surveys/manage', [$survey->id]));
    }

    public function previewSubmittedView(Request $request)
    {
        $survey = Survey::find( $request->route('survey_id') );

        return view('admin.surveys.submitted-view-preview', compact('survey'));
    }

    public function deleteModal(Request  $request)
    {
        $survey = Survey::find($request->route('survey_id'));

        return view('admin.surveys.delete', compact('survey'));
    }

    public function delete(Request  $request)
    {
        $survey = Survey::find($request->route('survey_id'));
        $survey->delete();

        return response()->json(['code' => 0]);
    }

    public function duplicate(Request $request)
    {
        $survey = Survey::find($request->route('survey_id'));

        return view('admin.surveys.duplicate', compact('survey'));
    }

    public function entries(Request  $request)
    {
        $survey = Survey::with('entries.volunteer')->find($request->route('survey_id'));

        $actors = Volunteer::project($this->project->id)->whereNotIn('id', $survey->entries->pluck('volunteer_id')->toArray())->orderBy('name')->get();

        return view('admin.surveys.entries', compact('survey', 'actors'));
    }

    public function show(Request  $request)
    {
        $entry = SurveyEntry::with('answers.question')->find($request->route('survey_entry_id'));

        return view('admin.surveys.show', compact('entry'));
    }

    public function preview(Request  $request)
    {
        $survey = Survey::find($request->route('survey_id'));

        $fields = [];
        foreach(json_decode($survey->form) as $field)
        {
            if(!in_array($field->type, ['date', 'header', 'paragraph', 'radio-group'])) {
                $attribute = $survey->relatedProject->attributes()->where('slug', $field->name)->first();
                if ($attribute && $attribute->is_collection) {
                    $field->type = 'collection';
                    $field->attribute = $attribute;
                }
            }

            $fields[] = $field;
        }

        return view('surveys.preview', compact('survey', 'fields'));
    }

    public function link(Request $request)
    {
        $survey = Survey::find($request->route('survey_id'));

        return view('admin.surveys.link', compact('survey'));
    }
}
