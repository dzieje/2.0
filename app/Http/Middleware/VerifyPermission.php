<?php

namespace App\Http\Middleware;

use Closure;

class VerifyPermission
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string $ability
     * @return mixed
     */
    public function handle($request, Closure $next, $ability)
    {
        if (! $request->user()->hasAbility($ability)) {
            //flash('Nie masz uprawnień do przeglądania żądanej strony');
            return redirect()->back();
        }

        return $next($request);
    }
}
