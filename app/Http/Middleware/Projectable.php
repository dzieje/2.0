<?php

namespace App\Http\Middleware;

use App\Models\Project;
use Closure;

class Projectable
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $project_id)
    {
        if(! Project::find($project_id)){
            return abort(401);
        }
        return $next($request);
    }
}
