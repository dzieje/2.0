<?php

namespace App\Imports;

use App\Models\ProjectVolunteer;
use App\Models\Volunteer;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class VolunteersImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $volunteer = Volunteer::create([
            'gender' => $row['gender'],
            'name' => $row['name'],
            'surname' => $row['surname'],
            'phone' => $row['phone'],
            'email' => $row['email'],
            'date_of_birth' => $row['date_of_birth']
        ]);

        ProjectVolunteer::create([
            'project_id' => 2,
            'volunteer_id' => $volunteer->id
        ]);

        return $volunteer;
    }
}
