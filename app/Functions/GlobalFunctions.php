<?php

if ( ! function_exists('flash')) {

    /**
     * Arrange for a flash message.
     *
     * @param  string|null $message
     * @param  string      $level
     */
    function flash($message = null, $level = 'info')
    {
        if ( ! is_null($message)) {
            session()->flash('flash_notification.message', $message);
            session()->flash('flash_notification.level', $level);
        }

    }

}
if ( ! function_exists('normalizeString')) {
    function normalizeString($str = '')
    {
        $str = strip_tags($str);
        $str = preg_replace('/[\r\n\t ]+/', ' ', $str);
        $str = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $str);
        $str = strtolower($str);
        $str = html_entity_decode($str, ENT_QUOTES, "utf-8");
        $str = htmlentities($str, ENT_QUOTES, "utf-8");
        $str = preg_replace("/(&)([a-z])([a-z]+;)/i", '$2', $str);
        $str = str_replace(' ', '-', $str);
        $str = rawurlencode($str);
        $str = str_replace('%', '-', $str);
        return $str;
    }
}

if ( ! function_exists('is_absolute_path')) {
    function is_absolute_path($path)
    {
        if ($path === null || $path === '') throw new Exception("Empty path");
        return $path[0] === DIRECTORY_SEPARATOR || preg_match('~\A[A-Z]:(?![^/\\\\])~i', $path) > 0;
    }
}

if ( ! function_exists('getbody')) {
    function getbody($content)
    {
        $dom = new DOMDocument();
        libxml_use_internal_errors(true);
        @$dom->loadHTML($content);
        libxml_use_internal_errors(false);
        $bodies = $dom->getElementsByTagName('body');
        assert($bodies->length === 1);
        $body = $bodies->item(0);
        if(property_exists($body, 'children')) {
            for ($i = 0; $i < $body->children->length; $i++) {
                $body->remove($body->children->item($i));
            }
        }
        return $dom->saveHTML($body);
    }
}
