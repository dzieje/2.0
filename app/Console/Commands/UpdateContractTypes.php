<?php

namespace App\Console\Commands;

use App\Models\Volunteer;
use Illuminate\Console\Command;

class UpdateContractTypes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'system:update-contract-types';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update contract types base on volunteer age';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $progressBar = $this->output->createProgressBar(Volunteer::count());

        Volunteer::chunk(100, function($volunteers) use(&$progressBar){
           foreach ($volunteers as $volunteer)
           {
               $volunteer->updateContract();
               $progressBar->advance();
           }
        });

        $progressBar->finish();
        return 0;
    }
}
