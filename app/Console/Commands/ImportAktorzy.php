<?php

namespace App\Console\Commands;

use App\Imports\VolunteersImport;
use Illuminate\Console\Command;

class ImportAktorzy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:aktorzy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.php artisan make:import
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Excel::import(new VolunteersImport(), storage_path('app/imports/188.166.67.68aktorzy23.10.202021-49.csv'));

        return 0;
    }
}
