<?php

namespace App\Console\Commands;

use App\Mail\Template;
use Illuminate\Console\Command;

class SendCustomMailing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'system:send-custom-mailing {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $template = '
            <h3 class="font-lato" style="font-style: normal;font-weight: 400;font-family: lato,tahoma,sans-serif;Margin-bottom: 0;Margin-top: 0;font-size: 16px;line-height: 24px;color: #e0e0e0;"><span style="color:rgb(62, 71, 81); font-size:22px; line-height:30px">Szanowni Państwo,</span></h3>

           <p class="font-lato" style="color: #7c7e7f;font-family: Lato,Tahoma,sans-serif;font-size: 13px; line-height: 22px;Margin-top: 14px;Margin-bottom: 0;">
            Na początku chcemy się Wam przypomnieć. W lipcu 2017 r. przedstawiciele Państwa instytucji kultury wzięli udział w organizowanej przez Narodowe Centrum Kultury wizycie studyjnej w Parku Dzieje w Murowanej Goślinie koło Poznania, gdzie od wielu lat organizowane plenerowe widowiska historyczne „Orzeł i Krzyż”. Bardzo miło wspominamy Waszą wizytę.
            </p>
            <p class="font-lato" style="color: #7c7e7f;font-family: Lato,Tahoma,sans-serif;font-size: 13px; line-height: 22px;Margin-top: 14px;Margin-bottom: 0;">
            Jak Państwo może pamiętają, specyfiką organizowanych przez Stowarzyszenie Dzieje wydarzeń kulturalnych jest duża liczba uczestników (nawet 400), przy czym ogromna większość z nich to wolontariusze. Kilka lat naszych doświadczeń organizacyjnych skłoniło nas do tego by spróbować stworzyć narzędzie webowe do zarządzania wydarzeniami kulturalnymi. Okazją do przekucia zamiarów w czyn był program Kultura w sieci.
            </p>
            <p class="font-lato" style="color: #7c7e7f;font-family: Lato,Tahoma,sans-serif;font-size: 13px; line-height: 22px;Margin-top: 14px;Margin-bottom: 0;">
            W związku ze zrealizowaniem projektu chcemy zaprezentować Państwu stworzoną przez nas narzędzie - platformę Dzieje 2.0.
            </p>
            <p class="font-lato" style="color: #7c7e7f;font-family: Lato,Tahoma,sans-serif;font-size: 13px; line-height: 22px;Margin-top: 14px;Margin-bottom: 0;">
            Więcej informacji znajdziecie na stronie https://parkdzieje.pl/dzieje2/ gdzie zamieściliśmy, krótką prezentacje funkcjonalności, tutorial oraz repozytorium z plikami instalacyjnymi. Narzędzie, zgodnie z wymogami programu dotacyjnego jest darmowe i będzie serwisowane przez dwa lata.  Gorąco polecamy.
            </p>

<p class="font-lato" style="color: #7c7e7f;font-family: Lato,Tahoma,sans-serif;font-size: 13px; line-height: 22px;Margin-top: 14px;Margin-bottom: 0;">
            Z pozdrowieniami,
            </p>
            <p class="font-lato" style="color: #7c7e7f;font-family: Lato,Tahoma,sans-serif;font-size: 13px; line-height: 22px;Margin-top: 14px;Margin-bottom: 0;">
            Stefan Ogorzałek <br>
            Koordynator Wolontariuszy <br>
            Stowarzyszenie Dzieje <br>
            (i organizator wizyty Studyjnej 2017) <br>
            T: 516 937 701 <br>
            E-mail: aktorzy@parkdzieje.pl
            </p>

            <p class="font-lato" style="color: #7c7e7f;font-family: Lato,Tahoma,sans-serif;font-size: 13px; text-align: center line-height: 22px;Margin-top: 14px;Margin-bottom: 0;">
                <table style="width: 100%" cellpadding="0" cellspacing="0">
                    <tr><td style="width: 50%;"><img src="https://aktorzy.parkdzieje.pl/storage/2020-NCK_dofinans_kulturawsieci-rgb.jpg" alt="" style="max-width: 100%"></td>
                    <td style="width: 50%;"><img src="https://aktorzy.parkdzieje.pl/storage/Logo PD.png" alt="" style="max-width: 100%"></td></tr>
                </table>
            </p>
        ';
        $subject = 'Aplikacja od Stowarzyszenia Dzieje';
        $replyTo = 'aktorzy@parkdzieje.pl';

        if (($handle = fopen(storage_path('app/messages-source/').$this->argument('file'), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $mail = trim($data[0]);
                \Mail::to($mail)->send(new Template( $template , $subject, $replyTo ));
            }
            fclose($handle);
        }

        $mail = 'stefogorzalek@gmail.com';
        \Mail::to($mail)->send(new Template( $template , $subject, $replyTo ));

        return 0;
    }
}
