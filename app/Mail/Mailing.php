<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Mailing extends Mailable implements ShouldQueue
{
    use Queueable;

    public $content;
    public $setting;
    private $reply_to;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($setting, $template, $subject = null, $replyTo = null)
    {
        if(! $replyTo) $replyTo = config('mail.from.address');
        $this->reply_to = [$replyTo];
        $this->subject = $subject;
        $this->content = $template;
        $this->setting = $setting->value;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->replyTo($this->reply_to)->view('emails.template');
    }
}
