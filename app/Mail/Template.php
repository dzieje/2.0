<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Template extends Mailable
{
    use Queueable;

    public $content;
    public $setting;
    private $reply_to;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($template, $subject = null, $replyTo = null)
    {
        if(! $replyTo) $replyTo = config('mail.from.address');
        $this->reply_to = [$replyTo];
        $this->subject = $subject;
        $this->content = $template;
        $this->setting = json_decode(json_encode([
                'header' => '<table style="border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 100%;color: #7c7e7f;">
                        <tbody><tr>
                            <td style="padding: 0;vertical-align: middle;mso-line-height-rule: at-least;width: 600px;padding-top: 10px;padding-bottom: 20px;">
                                <div style="font-family: lato,tahoma,sans-serif;background-color: #FFFFFF;font-weight: 100;padding: 20px;Margin-bottom: 0;text-align: center;font-size: 26px !important;line-height: 34px !important;" align="center" >
                                    '.config('app.name').'
                                </div>
                            </td>
                        </tr>
                        </tbody></table>',
                'footer' => '<table style="border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 100%;color: #7c7e7f; background-color: #262626;">
                        <tbody><tr>
                            <td style="padding: 0;vertical-align: middle;">&nbsp;</td>
                            <td style="padding: 0;vertical-align: middle;mso-line-height-rule: at-least;width: 600px;padding-top: 10px;padding-bottom: 20px;">
                                <div style="font-family: lato,tahoma,sans-serif;color: #FFFFFF;font-weight: 100;Margin-bottom: 0;text-align: center;font-size: 26px !important;line-height: 34px !important;" align="center">
                                    &copy;  '.config('app.name').'
                                </div>
                            </td>
                            <td style="padding: 0;vertical-align: middle;">&nbsp;</td>
                        </tr>
                        </tbody></table>',
                'mail_background_color' => '#262626',
                'content_background_color' => '#ffffff',
                'content_font_color' => '#7c7e7f'
            ]));
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->replyTo($this->reply_to)->view('emails.template');
    }
}
